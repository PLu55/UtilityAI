# Developing a Modular AI System

This system is inspired by  Kevin Dill and Christopher Dragerts article "Modular AI".

## Reasoners

A Reasoner has Options, how they are executed depends on the type of the Reasoner.

### Sequential Reasoner

The Sequential Reasoner alway considers all it's Options which may or may not have Considerations. If it has considerations then every consideration has to by valid for it's actions to be executed, if there are no considerations then the actions are always executed. When all valid actions are done then the reasoner is done.

### Selection Reasoner

The Selection Reasoner select the first option that is valid and executes it's actions. When the selected option is done the reasoner is done.

### Dual Utility Reasoner

The Dual utility reasoner selects the option with the highest score.
When the selected option is done the reasoner is done.

## AIActor

Every object that are some kind of actor in the game such as a NPC, a Player are represented as an AIActor in th AI.

### Data access

AIActor has a number of blackboards. The BrainBlackboard keeps temporary data used by the AI. The ActorBlackboard has more sustain data such as status like health, stamina.

## Use cases

### Walk through AIPercepts

Walk through AIPercepts to find out if there are any threats.

An Actors Percepts are stored in the Percepts Blackboard. We like to scan this Blackboard an look at Percept data to see if the Entity is friend or foe and if it's a foe add it to the enemy blackboard.