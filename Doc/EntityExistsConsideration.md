# AIEntityExistsConsideration

```mermaid
sequenceDiagram
    AIOption1->>AIEntityExistsConsideration: Consider()
    AIEntityExistsConsideration->>AIBlackboard: Values
    AIBlackboard-->>AIEntityExistsConsideration: 
    loop foreach Entity
        create participant AIProxyOption
        AIEntityExistsConsideration->>AIProxyOption: Create(AIOption2)
        AIEntityExistsConsideration->>AIReasonerBase: AddOption(AIProxyOption)
    end
    AIProxyOption->>AIOption2: Consider
```


```mermaid
classDiagram
    class AIReasonerBase
    AIReasonerBase : -AIOption options
    AIReasonerBase : +AddOption(AIOption)
    AIReasonerBase : +Tick()
    AIReasonerBase : +Sense()
    AIReasonerBase : +Think()
    AIReasonerBase : +Act()
```

```mermaid
classDiagram
    class AIOption
    AIOption : +AIConsideration[] Considerations
    AIOption : +Consider()
    AIOption : +Update()
    AIOption : +Reset() 
```

```mermaid
sequenceDiagram
    AIReasonerBase->>AIReasonerBase: Think()
    loop foreach "option" in options 
        AIReasonerBase->>"option": Consider()
        loop foreach consideration in Considerations
            "option"->>consideration: Consider()
        end
    end
    AIReasonerBase->>AIReasonerBase: Act()
    loop foreach "option" in options
        opt isValid 
            AIReasonerBase->>"option": Update()
             loop foreach "action" in actions
                "option"->>action: Update() /* Simplified */
             end
        end
    end
```