---
header-includes:
  - \usepackage[margin=12mm]{geometry}
---

# ModularAI

## SpecificationBuilder

### Structural

```C#
Build()
```

```C#
End()
```

```C#
Partial()
```

### Considerations

```C#
Considerations(string type = "And", string addend = "",  string multiplier = "", string rank = "")
```

```C#
DistanceConsideration(string addend = "", string multiplier = "", string rank = "")
```

```C#
EntityExistsConsideration(
    string dataStore, 
    string variable, 
    string addend = "", 
    string multiplier = "", 
    string rank = "")
```

### VariableConsideration

```C#
VariableConsideration(
    string variable, 
    string variableType, 
    string dataStore = null,
    string addend = "", 
    string multiplier = "", 
    string rank = "")
```

#### Parameters

- `variable`: Name of variable.
- `variableType`: Type of variable.
- `dataStore` [optional]: Which data store the variable is stored in.
- `addend` [optional]: sets the addend.
- `multiplier` [optional]: set the multiplier.
- `rank` [optional]: set the rank.

#### Children

- `Target` [optional]
- `WeightFunction`


#### HistoryConsideration

```C#
HistoryConsideration(
    string type = "TimeSinceLastActivation", 
    string addend = "", 
    string multiplier = "", 
    string rank = "")
```


### Actions

```C#
Actions()
```

```C#
SetVariableAction(
    string variable, 
    string value, 
    string dataStore=null, 
    string variableType = null)
```

### Targets

```C#
SelfTarget()
```

```C#
PositionTarget(string position)
```

```C#
PickerEntityTarget()
```

```C#
DataStoreTarget(string dataStore, string key)
```

```C#
GlobalTarget(string dataStore = "Global", string key)
```

### Weight Functions

```C#
BoolWeightFunction()
```

```C#
StringWeightFunction()
```

```C#
IntWeightFunction(string @operator = "eq", string value = "0")
```

```C#
FloatWeightFunction(string @operator = "eq", string value = "0")
```

```C#
DoubleWeightFunction(string @operator = "eq", string value = "0")
```

```C#
ConstantWeightFunction(string addend = "", string multiplier = "", string rank = "")
```

```C#
DoubleCurveWeightFunction(
    string curveType, 
    string exponent = "1", 
    string slope = "1", 
    string xShift = "0", 
    string yShift = "0")
```

```C#
CurveWeightFunction(
    string curveType, 
    string exponent = "1", 
    string slope = "1", 
    string xShift = "0", 
    string yShift = "0")
```

### Other

```C#
Picker(string type = "Entity", string variable = "PickerEntity")
```

```C#
Entries()
```

```C#
String(string value,  string veto = "true")
```

### Options

```C#
Options()
```

Options is a set of options.

#### Parameters

It takes no parameters.

#### Children

- Option

### Option

An option belongs to a reasoner which decides how the option is used.
It scores it's considerations resulting in a weight and a rank value.
How the actions are executed depends on the outcome of the considerations
and it's the reasoner.

```C#
Option(string name)
```

#### Parameters

- `name`: The name of the option.

#### Children

- `Considerations` [optional]
- `Actions` [optional]

### Reasoner

A reasoner has a set of options

```C#
Reasoner(string type)
```

A reasoner has a set of options. It evaluate it's options and might execute the options actions if the option is valid. An option is valid if it's ConsiderationSet is valid. It depends on the type the ConsiderationSet how it's evaluated.

#### Parameters

- `type`: The type of reasoner, one of {`"Sequence"`, `"Select"`, `"DualUtility"`}

##### DualUtility

DualUtility reasoner selects an option from the highest rank with the highest weight.

##### Select

A Select Reasoner selects the first option with a weight greater than zero and execute it's actions.

##### Sequence

A Sequence Reasoner evaluates and executes all it's options that are valid and has actions.

#### Children

- `Options`
