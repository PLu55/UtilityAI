# Developing a Hybride GOAP Utility AI System

## The hybride system

GOAP takes care of the long term goal while Utility AI is a short term reactions
to the world.

## Utility AI

How can selection of alternative be implemented?

Ideas:
Actions returns a status (continue, failure, success).
Actions have a states and a state machine.
Actions returns status, score, state (exam. a selection)

UtilityAgent (UA) Decides which of it's actions are best for the situation. The UA
has a collection of actions which it iterates through and score each action, the result
is collected in a list. One of the actions with the highest score (let say the top 10)
is selected. It can be implemented as a list that only keep n actions with the highest score.

A MultiAction returns a list of results with score and state where state is a collection of blackboard keys, the values are stored in the private blackboard. The blackboard entries must be clean when the result is disposed.






Example: Select Target Action

The UtilityAgent has a proximity sensor that collects characters he is aware of 
stored in the AI context. 

The Action iterates through the collection of characters and 

PseudoCode





### Combat system using Utility AI

#### Stats

* Health

#### Abilities

* Attack, use weapons
* Defense, use shields and alike
* Use Item, throw bombs, grenades, acid, flammables ...
* Heal
* Move

####

#### Actions

Select Enemy
Select Weapon, based on DPS, distance to enemy

* MeleeAttack
  * EnemyInRange
  * CanUseMeleeAbility
  * MyHealthIsHigh  
  * EnemyHealthIsLow

* RangedAttack
  * EnemyInRange
  * CanUseRangedAbility
  * MyHealthIsHigh
  * EnemyHealthIsLow

* HealMyself
  * HasHealAbility
  * CanUseHealAbility
  * HealthIsLow

* Flee
  * MyHealthIsLow
  * EnemyHealthIsHigh

## GOOP

## Learning

Bots can learn from experience. Remember the result of attacking an enemy with
a certain attack.

## Stats system

I like to have a flexible stats system.

StatManager handles a collection of Stats.

How to handle modifiers?
