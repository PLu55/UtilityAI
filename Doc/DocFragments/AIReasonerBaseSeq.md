```mermaid
sequenceDiagram
    AIReasonerBase->>AIReasonerBase: Think()
    loop foreach "option" in options 
        AIReasonerBase->>"option": Consider()
        loop foreach consideration in Considerations
            "option"->>consideration: Consider()
        end
    end
    AIReasonerBase->>AIReasonerBase: Act()
    loop foreach "option" in options
        opt isValid 
            AIReasonerBase->>"option": Update()
             loop foreach "action" in actions
                "option"->>action: Update() /* Simplified */
             end
        end
    end
```

```mermaid
sequenceDiagram
    AISequenceReasoner->>AIReasonerBase: Think()
    loop foreach "option" in options 
        AIReasonerBase->>"option": Consider()
        loop foreach consideration in Considerations
            "option"->>consideration: Consider()
        end
        opt isValid 
            AIReasonerBase->>"option": Update()
             loop foreach "action" in actions
                "option"->>action: Update() /* Simplified */
             end
        end
    end

```