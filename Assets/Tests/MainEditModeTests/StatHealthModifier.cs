using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.Dev.AI
{
    public class StatHealthModifier : StatModifier
    {
        public string Id => _id;
        private string _id;
        private float _amount;

        public StatHealthModifier(string id, float duration, float amount) : base(duration)
        {
            _id = id;
            _amount = amount;
        }

        public override void Handle(object sender, Query query)
        {
            if (query.StatType == StatType.Health)
            {
                query.Value += _amount;
            }
        }

        public override void Dispose()
        {
            Debug.Log($"StatHealthModifier Dispose: {_id}");
            base.Dispose();
        }   

        public override string ToString()
        {
            return $"Id: {_id}, Amount: {_amount}";
        }

    }
}
