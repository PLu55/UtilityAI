using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using PLu.InventorySystem;
using PLu.Utilities;

public class InventoryTests
{   
    static string dirPath = "Assets/Tests/MainEditModeTests/";
    ItemType itemType1;
    ItemType itemType2;

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {        
        itemType1 = ScriptableObjectLoader.LoadScriptableObject<ItemType>(dirPath + "ItemType1.asset");
        Assert.IsNotNull(itemType1);
        itemType2 = ScriptableObjectLoader.LoadScriptableObject<ItemType>(dirPath + "ItemType2.asset");
        Assert.IsNotNull(itemType2);
    }

    [Test]
    public void ItemTests()
    {
        Item item1 = itemType1.Instantiate();
        Assert.IsNotNull(item1);
        Assert.AreEqual(itemType1, item1.ItemType);
        Item item2 = itemType2.Instantiate();
        Assert.IsNotNull(item2);
        Assert.AreEqual(itemType2, item2.ItemType);
        Assert.AreNotEqual(item1, item2);
        Assert.AreNotEqual(item1.ItemType, item2.ItemType);
    }

    [Test]
    public void InventoryBasicTests()
    {
        GameObject gameObject = new GameObject();
        Inventory inventory = gameObject.AddComponent<Inventory>();

        inventory.Size = 2;
        Assert.IsNotNull(inventory);

        Item item1 = itemType1.Instantiate();
        Assert.IsNotNull(item1);
        Item item2 = itemType2.Instantiate();
        Assert.IsNotNull(item2);

        Assert.IsTrue(inventory.TryAdd(item1));
        Assert.IsTrue(inventory.HasItem("ItemType1"));
        Assert.AreEqual(1, inventory.QuantityOf("ItemType1"));
        Assert.IsTrue(inventory.TryAdd(itemType1));
        Assert.AreEqual(2, inventory.QuantityOf("ItemType1"));
        Assert.IsTrue(inventory.TryAdd(new ItemContainer(itemType1, 2)));
        Assert.AreEqual(4, inventory.QuantityOf("ItemType1"));

        Assert.IsTrue(inventory.TryAdd(item2, 2));
        Assert.IsTrue(inventory.HasItem("ItemType2"));
        Assert.AreEqual(2, inventory.QuantityOf("ItemType2"));
        Assert.IsTrue(inventory.HasItem("ItemType1"));

        ItemContainer removedItem;
        removedItem = inventory.TryRemove("ItemType1", 3);
        Assert.AreEqual(3, removedItem.Quantity);
        Assert.AreEqual(1, inventory.QuantityOf("ItemType1"));
        removedItem = inventory.TryRemove("ItemType1", 1);
        Assert.AreEqual(1, removedItem.Quantity);
        Assert.AreEqual(0, inventory.QuantityOf("ItemType1"));
        Assert.IsFalse(inventory.HasItem("ItemType1"));
        Assert.AreEqual(2, inventory.QuantityOf("ItemType2"));

        Assert.IsTrue(inventory.TryAdd(itemType1, 3));
        ItemContainer[] items = inventory.RemoveAllItems();
        Assert.AreEqual(2, items.Length);
        Assert.AreEqual(itemType1, items[0].ItemType);
        Assert.AreEqual(itemType2, items[1].ItemType);
        Assert.AreEqual(3, items[0].Quantity);
        Assert.AreEqual(2, items[1].Quantity);
    }
}
