using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;

using PLu.Dev.AI;

public class StatsTest
{
    [Test]
    public void StatTest()
    {
        float value = 0f;
        float max = 0f;
        Stat stat = new Stat(StatType.Health, 100f, 100f);
        stat.OnStatChanged += (v, m) => { value = v; max = m; };
        stat.Modify(-10f);
        Assert.That(stat.Value, Is.EqualTo(90f).Within(1e-6));
        Assert.That(value, Is.EqualTo(90f).Within(1e-6));
        Assert.That(max, Is.EqualTo(100f).Within(1e-6));
        stat.SetMaxValue(50);
        Assert.That(stat.MaxValue, Is.EqualTo(50f).Within(1e-6));
        Assert.That(stat.Value, Is.EqualTo(50f).Within(1e-6));

    }

    [Test]
    public void StatContainerTest()
    {
        GameObject go = new GameObject();
        StatsManager sc = go.AddComponent<StatsManager>();
        StatDefinitions sd = ScriptableObject.CreateInstance<StatDefinitions>();
        Assert.NotNull(sd);
        sc.StatDefinitions = sd;
        sd.Definitions.Add(new BaseStat(StatType.Health, 100f ));
        sd.Definitions.Add(new BaseStat(StatType.Strength, 100f));
        StatsMediator sm = new StatsMediator();
    }

    [Test]
    public void StatMediatorTest()
    {
        GameObject go = new GameObject();
        StatsManager sm = go.AddComponent<StatsManager>();
        StatDefinitions sd = ScriptableObject.CreateInstance<StatDefinitions>();
        sm.StatDefinitions = sd;
        sd.Definitions.Add(new BaseStat(StatType.Health, 100f ));
        sd.Definitions.Add(new BaseStat(StatType.Strength, 100f));
        
        sm.StatDefinitions = sd;
        sm.Initialize();
        Assert.That(sm.GetCurrentValue(StatType.Health), Is.EqualTo(100f).Within(1e-6));
        Assert.That(sm.GetCurrentValue(StatType.Strength), Is.EqualTo(100f).Within(1e-6));
        

        sm.AddModifier(new StatHealthModifier("SM1", 3f, 10f));
        Assert.That(sm.GetCurrentValue(StatType.Health), Is.EqualTo(110f).Within(1e-6));

        for (int i = 0; i < 7; i++)
        {
            if (i == 2)
            { 
                sm.AddModifier(new StatHealthModifier("SM2", 3f, 20f)); 
            }
            sm.DoDebugUpdate(i == 0 ? 0f : 1f);
            if (i < 2)
            {
                Assert.That(sm.GetCurrentValue(StatType.Health), Is.EqualTo(110f).Within(1e-6));
            }
            else if (i < 3)
            {
                Assert.That(sm.GetCurrentValue(StatType.Health), Is.EqualTo(130f).Within(1e-6));
            }
            else if (i < 5)
            {
                Assert.That(sm.GetCurrentValue(StatType.Health), Is.EqualTo(120f).Within(1e-6));
            }
            else
            {
                Assert.That(sm.GetCurrentValue(StatType.Health), Is.EqualTo(100f).Within(1e-6));
            }
        }
    }
}
