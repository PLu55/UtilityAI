using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PLu.SimplePriorityQueue;
public class SimplePriorityQueueTest
{
    [Test]
    public void InsertAndExtractMaxTest()
    {
        SimplePriorityQueue<int> queue = new SimplePriorityQueue<int>();

        // Insert elements into the queue
        queue.Insert(5);
        queue.Insert(10);
        queue.Insert(3);
        queue.Insert(8);

        // Extract the maximum element
        int maxElement = queue.ExtractMax();

        // Assert that the maximum element is correct
        Assert.AreEqual(10, maxElement);
        // Assert that the count is updated correctly
        Assert.AreEqual(3, queue.Count);
    }

    [Test]
    public void PeekMaxTest()
    {
        SimplePriorityQueue<int> queue = new SimplePriorityQueue<int>();

        // Insert elements into the queue
        queue.Insert(5);
        queue.Insert(10);
        queue.Insert(3);
        queue.Insert(8);

        // Peek at the maximum element
        int maxElement = queue.PeekMax();

        // Assert that the maximum element is correct
        Assert.AreEqual(10, maxElement);
        // Assert that the count remains unchanged
        Assert.AreEqual(4, queue.Count);
    }

    [Test]
    public void IsEmptyTest()
    {
        SimplePriorityQueue<int> queue = new SimplePriorityQueue<int>();

        // Assert that the queue is initially empty
        Assert.IsTrue(queue.IsEmpty);

        // Insert an element into the queue
        queue.Insert(5);

        // Assert that the queue is no longer empty
        Assert.IsFalse(queue.IsEmpty);

        // Extract the element from the queue
        queue.ExtractMax();

        // Assert that the queue is empty again
        Assert.IsTrue(queue.IsEmpty);
    }


}
