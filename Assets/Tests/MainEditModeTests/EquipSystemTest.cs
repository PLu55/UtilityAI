using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PLu.Dev.AI;
using PLu.Utilities;
using PLu.InventorySystem;

[TestFixture]
public class EquipSystemTest
{
    static string testDir = "Assets/Tests/MainEditModeTests/";
    static string weaponSystemDir = "Assets/Development/WeaponSystem/";
    GameObject equipSystem;
    EquipManager equipManager;
    StatsManager statsManager;
    IEquipable knife;

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        string path = testDir + "EquipSlotDefinitions.asset";
        EquipSlotDefinitions slotDefinitions = ScriptableObjectLoader.LoadScriptableObject<EquipSlotDefinitions>(path);
        // Create a new GameObjectEquipSlotDefinitions slotDefinitions = ScriptableObjectLoader.LoadScriptableObject<EquipSlotDefinitions>(weaponSystemDir + "Weapons/" + "Knife.asset");
        path = weaponSystemDir + "Weapons/" + "Knife.asset";
        knife = ScriptableObjectLoader.LoadScriptableObject<WeaponType>(path);
        equipSystem = new GameObject();
        equipManager = equipSystem.AddComponent<EquipManager>();

        equipManager.EquipDefinition = slotDefinitions;
        equipSystem.AddComponent<Inventory>();
        statsManager = equipSystem.AddComponent<StatsManager>();
        StatDefinitions statDefinitions = ScriptableObject.CreateInstance<StatDefinitions>();
        statsManager.StatDefinitions = statDefinitions;
        statDefinitions.Definitions.Add(new BaseStat(StatType.Health, 100f ));
        statDefinitions.Definitions.Add(new BaseStat(StatType.Strength, 100f ));
        statsManager.StatDefinitions = statDefinitions;
        statsManager.Initialize();
    }

    // A Test behaves as an ordinary method
    [Test]
    public void EquipSystemTest1()
    {
        Assert.IsNotNull(equipManager);
        Assert.IsNotNull(equipManager.EquipDefinition);
        equipManager.Initialize();
        Assert.IsNotNull(equipManager.EquipSlots);
        Assert.AreEqual(2, equipManager.EquipSlots.Count);

        Assert.IsTrue(equipManager.HasSlot("LeftHand"));
        Assert.IsTrue(equipManager.HasSlot("RightHand"));
        Assert.IsNotNull(knife);
        Assert.IsTrue(equipManager.CanEquipItem("LeftHand", knife));
        equipManager.EquipItem("LeftHand", knife);
        Assert.AreEqual(knife, equipManager.EquipedItem("LeftHand"));
    }
}

