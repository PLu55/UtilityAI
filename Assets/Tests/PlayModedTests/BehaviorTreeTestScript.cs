using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PLu.BehaviourTrees;

public class BehaviourTreeTestScript
{
    BehaviourTree behaviourTree = new BehaviourTree("Test", Policies.RunUntilSuccessOrFailure);
    [OneTimeSetUp]
    public void Setup()
    {
        Debug.Log("Setup");
        Node root = new Sequence("Root");
        behaviourTree.AddChild(root);
        root.AddChild(new Leaf("Leaf 1", new ActionStrategy(() => Debug.Log("Action 1"))));
        root.AddChild(new Leaf("Leaf 2", new ActionStrategy(() => Debug.Log("Action 2"))));
        root.AddChild(new Fail());
        root.AddChild(new Leaf("Leaf 3", new ActionStrategy(() => Debug.Log("Action 3"))));
        behaviourTree.PrintTree();
    }
    [Test]
    public void NewTestScriptSimplePasses()
    {
        // Use the Assert class to test conditions
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator NewTestScriptWithEnumeratorPasses()
    {   
        int cnt = 10;
        while (cnt-- > 0)
        {
            Node.Status status = behaviourTree.Process();
            Debug.Log($"Status: {status}");
            if (status == Node.Status.Success || status == Node.Status.Failure)
            {
                yield break;
            }
        }
    }
}
