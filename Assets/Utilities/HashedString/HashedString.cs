using System;

namespace PLu.Utilities
{
    [Serializable]
    public readonly struct HashedString : IEquatable<HashedString> 
    {
        public readonly string name;
        public readonly int hash;

        public HashedString(string name) 
        {            
            if (name == null)
            {
                name = string.Empty;
            }
            this.name = name;
            hash = name.ComputeFNV1aHash();
        }
        
        public bool Equals(HashedString other) => hash == other.hash;
        
        public override bool Equals(object obj) => obj is HashedString other && Equals(other);
        public override int GetHashCode() => hash;
        public override string ToString() => "#" + name;
        
        public static bool operator ==(HashedString lhs, HashedString rhs) => lhs.hash == rhs.hash;
        public static bool operator !=(HashedString lhs, HashedString rhs) => !(lhs == rhs);

        // Implicit conversion from string to HashedString
        public static implicit operator HashedString(string value)
        {
            return new HashedString(value);
        }
    }

    public static class HashStringHelpers
    {
        public static HashedString AsHashedString(this string str) 
        {
            return HashedStringRegistry.Instance.GetOrRegister(str);
        }
    }
}