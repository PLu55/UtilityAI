using System;
using System.Collections.Generic;

namespace PLu.Utilities
{
    public class HashedStringRegistryBase<T> : ISingleton<T> where T : new()
    {
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                }
                return _instance;
            }
        }

        private static T _instance;
        private readonly Dictionary<string, HashedString> _registry = new();

        public bool IsRegistered(string key)
        {
            return _registry.ContainsKey(key);
        }
        
        public HashedString GetOrRegister(string key)
        {
            if (_registry.TryGetValue(key, out var existingKey))
            {
                if (existingKey.name != key)
                {
                    throw new HashCollisionException(key, existingKey.name);
                }
                return existingKey;
            }
            else
            {
                var hashedString = new HashedString(key);
                _registry.Add(key, hashedString);

                return hashedString;
            }
        }
    }
    public class HashedStringRegistry : HashedStringRegistryBase<HashedStringRegistry> {}

    public  class HashCollisionException : Exception
    {
        public HashCollisionException(string key, string existingKey) : base($"Hash collision, HashedString {key} has the same hash as {existingKey}")
        {
        }
    }
}