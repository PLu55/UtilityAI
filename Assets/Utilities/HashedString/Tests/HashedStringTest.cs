using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using PLu.Utilities;


namespace PLu.Utilities.Tests
{
    public class HashedStringTest
    {

        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789_";
            System.Random random = new System.Random();
            string result = new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }

        // This test can occasionally fail due to hash collisions
        [Test]
        public void TestHashedStringCollisionRate()
        {
            int keyLength = 10;
            int keyCount = 10000;
            int collisions = 0;

            HashSet<string> keys = new();

            // Generate unique keys
            for (int i = 0; i < keyCount;)
            {
                if (keys.Add(GenerateRandomString(keyLength)))
                {
                    i++;
                }
            }

            foreach (string key in keys)
            {
                try
                {
                    HashedStringRegistry.Instance.GetOrRegister(key);
                }
                catch (Exception ex)
                {
                    collisions++;
                }
            }

            Debug.Log($"Number of collisions: {collisions}/{keyCount}, {(float)collisions / keyCount}");
        }    
        
        [Test]
        public void SimpleTest()
        {
            string key = "TestString";
            HashedString hashedString = new HashedString(key);
            Assert.AreEqual("#" + key, hashedString.ToString());
            Assert.AreEqual(key.ComputeFNV1aHash(), hashedString.GetHashCode());
            Assert.AreEqual(hashedString, new HashedString(key));
            Assert.AreNotEqual(hashedString, new HashedString("test2"));
        }

        [Test]
        public void HelperTest()
        {
            string key = "TestString";
            HashedString hashedString = key.AsHashedString();
            Assert.AreEqual("#" + key, hashedString.ToString());
        }        
        
        [Test]
        public void ImplicitTest()
        {
            string key = "ImplicitTestString";
            HashedString hashedString = key;
            Assert.AreEqual("#" + key, hashedString.ToString());
        }

        [Test]
        public void NullTest()
        {
            HashedString hashedString1 = new HashedString(null);
            HashedString hashedString2 = null;
            Assert.IsTrue(hashedString1 == null);
            Assert.IsTrue(hashedString2 == null);
            Assert.AreEqual(hashedString1, hashedString2);
        }
    }
}