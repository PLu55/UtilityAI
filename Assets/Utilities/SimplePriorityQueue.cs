using System;
using System.Collections.Generic;

namespace PLu.SimplePriorityQueue
{
    /// <summary>
    /// Represents a priority queue data structure.
    /// </summary>
    /// <typeparam name="T">The type of elements in the priority queue.</typeparam>
    public class SimplePriorityQueue<T> where T : IComparable<T>
    {
        private List<T> heap = new List<T>();

        /// <summary>
        /// Gets the number of elements in the priority queue.
        /// </summary>
        public int Count => heap.Count;

        /// <summary>
        /// Gets a value indicating whether the priority queue is empty.
        /// </summary>
        public bool IsEmpty => heap.Count == 0;

        /// <summary>
        /// Inserts an item into the priority queue.
        /// </summary>
        /// <param name="item">The item to insert.</param>
        public void Insert(T item)
        {
            heap.Add(item);
            BubbleUp(heap.Count - 1);
        }

        /// <summary>
        /// Extracts and returns the maximum item from the priority queue.
        /// </summary>
        /// <returns>The maximum item in the priority queue.</returns>
        /// <exception cref="InvalidOperationException">Thrown when the priority queue is empty.</exception>
        public T ExtractMax()
        {
            if (heap.Count == 0) throw new InvalidOperationException("The priority queue is empty.");

            T maxItem = heap[0];
            heap[0] = heap[heap.Count - 1];
            heap.RemoveAt(heap.Count - 1);
            BubbleDown(0);
            return maxItem;
        }

        /// <summary>
        /// Returns the maximum item in the priority queue without removing it.
        /// </summary>
        /// <returns>The maximum item in the priority queue.</returns>
        /// <exception cref="InvalidOperationException">Thrown when the priority queue is empty.</exception>
        public T PeekMax()
        {
            if (heap.Count == 0) throw new InvalidOperationException("The priority queue is empty.");
            return heap[0];
        }
        private void BubbleUp(int childIndex)
        {
            while (childIndex > 0)
            {
                int parentIndex = (childIndex - 1) / 2;
                if (heap[childIndex].CompareTo(heap[parentIndex]) <= 0) break;

                (heap[childIndex], heap[parentIndex]) = (heap[parentIndex], heap[childIndex]);
                childIndex = parentIndex;
            }
        }

        private void BubbleDown(int parentIndex)
        {
            int lastIndex = heap.Count - 1;
            while (true)
            {
                int leftChildIndex = 2 * parentIndex + 1;
                int rightChildIndex = leftChildIndex + 1;
                int largestIndex = parentIndex;

                if (leftChildIndex <= lastIndex && heap[leftChildIndex].CompareTo(heap[largestIndex]) > 0)
                {
                    largestIndex = leftChildIndex;
                }

                if (rightChildIndex <= lastIndex && heap[rightChildIndex].CompareTo(heap[largestIndex]) > 0)
                {
                    largestIndex = rightChildIndex;
                }

                if (largestIndex == parentIndex) break;

                (heap[parentIndex], heap[largestIndex]) = (heap[largestIndex], heap[parentIndex]);
                parentIndex = largestIndex;
            }
        }
    }
}