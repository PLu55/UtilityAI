﻿using UnityEngine;

public static class Vector3Extensions 
{
    /// <summary>
    /// Sets any x y z values of a Vector3
    /// </summary>
    public static Vector3 With(this Vector3 vector, float? x = null, float? y = null, float? z = null) {
        return new Vector3(x ?? vector.x, y ?? vector.y, z ?? vector.z);
    }        
    /// <summary>
    /// Adds to any values of the Vector3
    /// </summary>
    public static Vector3 Add(this Vector3 vector, float? x = null, float? y = null, float? z = null) {
        return new Vector3(vector.x + (x ?? 0), vector.y + (y ?? 0), vector.z + (z ?? 0));
    }
    /// <summary>
    /// Calculates the squared distance between two vectors.
    /// </summary>
    /// <param name="a">The first vector.</param>
    /// <param name="b">The second vector.</param>
    /// <returns>The squared distance between the two vectors.</returns>
    public static float DistanceSqr(this Vector3 a, Vector3 b)
    {
        return (a-b).sqrMagnitude;
    }    
    /// <summary>
    /// Calculates the distance between two Vector3 points.
    /// </summary>
    /// <param name="a">The first Vector3 point.</param>
    /// <param name="b">The second Vector3 point.</param>
    /// <returns>The distance between the two Vector3 points.</returns>
    public static float Distance(this Vector3 a, Vector3 b)
    {
        return Vector3.Distance(a, b);
    }
}