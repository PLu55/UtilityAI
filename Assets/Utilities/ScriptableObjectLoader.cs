using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using UnityEditor;

namespace PLu.Utilities
{
#if UNITY_EDITOR
    public static class ScriptableObjectLoader
    {
        public static T LoadScriptableObject<T>(string assetPath) where T : ScriptableObject
        {
            // Load the asset at the specified path and cast it to type T
            T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            return asset;
        }
    }
    #endif
}
