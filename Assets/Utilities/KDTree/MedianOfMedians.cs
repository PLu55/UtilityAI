using System;
using System.Linq;
//using UnityEngine;

namespace PLu.Utilities
{
    /// <summary>
    /// Provides a method to find the median element in an array using the Median of Medians algorithm.
    /// </summary>
    public class MedianOfMedians
    {
        /// <summary>
        /// Finds the median element in the given array.
        /// </summary>
        /// <typeparam name="T">The type of elements in the array.</typeparam>
        /// <param name="array">The array to find the median in.</param>
        /// <returns>The median element in the array.</returns>
        public static T Median<T>(T[] array) where T : IComparable<T>
        {
            return Compute(array, array.Length / 2);
        }
        private static T Compute<T>(T[] arr, int k) where T : IComparable<T>
        {
            if (arr.Length == 1)
                return arr[0];

            // Divide the array into subgroups of 5
            int numGroups = (int)Math.Ceiling(arr.Length / 5.0);
            T[] medians = new T[numGroups];

            for (int i = 0; i < numGroups; i++)
            {
                // Get the group of 5 or the remaining elements if less than 5
                T[] group = arr.Skip(i * 5).Take(5).ToArray();
                Array.Sort(group);
                medians[i] = group[group.Length / 2]; // Get the median of the group
            }

            // Find the median of the medians recursively
            T medianOfMedians = Compute(medians, medians.Length / 2);

            // Partition the array around the median of medians and get the position of the pivot
            int pivotIndex = Partition(arr, medianOfMedians);

            // The pivot is in its final sorted position
            if (k == pivotIndex)
                return arr[k];
            else if (k < pivotIndex)
                return Compute(arr.Take(pivotIndex).ToArray(), k);
            else
                return Compute(arr.Skip(pivotIndex + 1).ToArray(), k - pivotIndex - 1);
        }
        private static int Partition<T>(T[] arr, T pivot) where T : IComparable<T>
        {
            int lessCount = 0, equalCount = 0, greaterCount = 0;
            T[] less = new T[arr.Length];
            T[] equal = new T[arr.Length];
            T[] greater = new T[arr.Length];

            // Partition the elements into less, equal, and greater arrays
            foreach (var item in arr)
            {
                int comparison = item.CompareTo(pivot);
                if (comparison < 0)
                    less[lessCount++] = item;
                else if (comparison == 0)
                    equal[equalCount++] = item;
                else
                    greater[greaterCount++] = item;
            }

            // Combine arrays back together
            Array.Copy(less, 0, arr, 0, lessCount);
            Array.Copy(equal, 0, arr, lessCount, equalCount);
            Array.Copy(greater, 0, arr, lessCount + equalCount, greaterCount);

            // Return the index of the first element of the equal segment
            return lessCount + (equalCount / 2);
        }
    }
}
