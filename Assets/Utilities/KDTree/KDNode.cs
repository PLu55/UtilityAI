using System;
using UnityEngine;

namespace PLu.KDTree
{
    public abstract class KDNode<T> //: IComparable<KDNode<T>> //where T : IComparable<T>
    {
        public T Value { get; set; }
        public KDNode<T> Left { get; set; }
        public KDNode<T> Right { get; set; }
        public int Axis { get; private set; }

        public abstract Vector3 Position { get; }

        public abstract float this[int axis] { get; }
        public KDNode(T value)
        {
            Value = value;
        }
        public virtual void SetNode(int axis, KDNode<T> left, KDNode<T> right)
        {
            Axis = axis;
            Left = left;
            Right = right;
        }
        public virtual float DistanceSqr(Vector3 other)
        {
            return (Position - other).sqrMagnitude;
        }
        public virtual float DistanceSqr(KDNode<T> other)
        {
            return (Position - other.Position).sqrMagnitude;
        }

    }
}
