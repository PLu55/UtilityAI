using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.KDTree
{
    public class KDNodeGameObject2DFactory : KDNodeFactory<GameObject>
    {
        public override KDNode<GameObject> CreateNode(GameObject value)
        {
            return new KDNodeGameObject2D(value);
        }
    }

    public class KDNodeGameObject2D : KDNode2D<GameObject>
    {
        public override Vector3 Position => Value.transform.position;
        public override float this[int axis] => Position[axis == 0 ? 0 : 2];
        public KDNodeGameObject2D(GameObject value) : base(value) {}
    }
    public class KDNodeGameObject3DFactory : KDNodeFactory<GameObject>
    {
        public override KDNode<GameObject> CreateNode(GameObject value)
        {
            return new KDNodeGameObject3D(value);
        }
    }

    public class KDNodeGameObject3D : KDNode<GameObject>
    {
        public override Vector3 Position => Value.transform.position;
        public override float this[int axis] => Position[axis];
        public KDNodeGameObject3D(GameObject value) : base(value) {}
    }
}
