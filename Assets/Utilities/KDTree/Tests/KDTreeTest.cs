using System;
using System.Collections.Generic;
using NUnit.Framework;
using PLu.Utilities;
using UnityEngine;
using PLu.KDTree;
using System.Security.Cryptography.X509Certificates;


namespace PLu.KDTree.Tests
{
    public class KDTreeTests
    {
        private Vector3[] _points;
        private int _n = 1000;            
        float[] _xValues;
        float[] _yValues;
        float[] _zValues;

        public float Median(float[] values)
        {
            Array.Sort(values);
            int n = values.Length;

            return values[n / 2];
        }

        public List<Vector3> FindInRange3D(Vector3[] points, Vector3 center, float radius)
        {
            float radiusSqr = radius * radius;
            List<Vector3> pointsInRange = new List<Vector3>();

            foreach (Vector3 point in points)
            {
                if (point.DistanceSqr(center) <= radiusSqr)
                {
                    pointsInRange.Add(point);
                }
            }
            return pointsInRange;
        }

        [SetUp]
        public void SetUpVector3()
        {
            _points = new Vector3[_n];
            _xValues = new float[_n];
            _yValues = new float[_n];
            _zValues = new float[_n];

            for (int i = 0; i < _n; i++)
            {
                _xValues[i] = UnityEngine.Random.Range(0f, 100f);
                _yValues[i] = UnityEngine.Random.Range(0f, 100f);
                _zValues[i] = UnityEngine.Random.Range(0f, 100f);
                _points[i] = new Vector3(_xValues[i], _yValues[i], _zValues[i]);
            }
        }        

        [Test]
        public void KDTree_BuildTree_ValidInput()
        {
            int dimensions = 2;

            KDTree<Vector3> kdTree = new KDTree<Vector3>(_points, dimensions, new KDNodeVector33DFactory());

            Assert.IsNotNull(kdTree);
            Assert.AreEqual(_n, kdTree.Count());
            Assert.AreEqual(_n, kdTree.GetNodes().Length);

            int maxImbalance;
            double averageImbalance;
            kdTree.GetBalanceFactors(out maxImbalance, out averageImbalance);
            Assert.LessOrEqual(1, maxImbalance);
        }

        [Test]
        public void KDTree_FindMedian_2D()
        {
            int dimensions = 2;
            int axis = 0;

            KDTree<Vector3> kdTree = new KDTree<Vector3>(_points, dimensions, new KDNodeVector32DFactory());

            KDNode<Vector3> medianNode = kdTree.FindMedian(kdTree.GetNodes(), axis);
            Assert.IsNotNull(medianNode);
            
            float[] values;
            if (axis == 0)
            {
                values = _xValues;
            }
            else if ((dimensions == 2 && axis == 1) || axis == 2)
            {
                values = _zValues;
            }
            else if (dimensions == 3 && axis == 1)
            {
                values = _yValues;
            }
            else
            {
                values = _zValues;
            }

            Assert.AreEqual(Median(values), medianNode[axis]);
        }        
        [Test]
        public void KDTree_RangeQuery_3D()
        {
            // Arrange
            int dimensions = 3;
  
            Vector3 center = new Vector3(50f, 50f, 50f);
            float radius = 10f;

            KDTree<Vector3> kdTree = new KDTree<Vector3>(_points, dimensions, new KDNodeVector33DFactory());

            // Act
            List<Vector3> pointsInRange = kdTree.RangeQuery(center, radius);

            foreach (var point in pointsInRange)
            {
                float distanceSqr = point.DistanceSqr(center);
                Assert.IsTrue(distanceSqr <= radius * radius);
            }

            foreach (var point in _points)
            {   
                if (!pointsInRange.Contains(point))
                {
                    float distanceSqr = point.DistanceSqr(center);
                    Assert.IsFalse(distanceSqr <= radius * radius);
                }
            }
        } 
        [Test]
        public void KDTree_RangeQuery_2D()
        {
            int dimensions = 2;
  
            Vector3 center = new Vector3(50f, 50f, 50f);
            float radius = 10f;

            KDTree<Vector3> kdTree = new KDTree<Vector3>(_points, dimensions, new KDNodeVector32DFactory());

            List<Vector3> pointsInRange = kdTree.RangeQuery(center, radius);

            foreach (var point in pointsInRange)
            {
                float distanceSqr = point.DistanceSqr2D(center);
                Assert.IsTrue(distanceSqr <= radius * radius);
            }

            foreach (var point in _points)
            {   
                if (!pointsInRange.Contains(point))
                {
                    float distanceSqr = point.DistanceSqr2D(center);
                    Assert.IsFalse(distanceSqr <= radius * radius);
                }
            }

            //Assert.AreEqual(pointsInRange2D.Count, pointsInRange.Count);
        }        
        [Test]
        public void KDTree_KDNode_Distance_2D()
        {   
            int dimensions = 2;
            KDTree<Vector3> kdTree = new KDTree<Vector3>(_points, dimensions, new KDNodeVector32DFactory());
            KDNode<Vector3>[] nodes = kdTree.GetNodes();
            
            for (int i = 0; i < nodes.Length - 1; i++)
            {
                float nodeDistanceSqr = nodes[i].DistanceSqr(nodes[i+1]);
                float distanceSqr = nodes[i].Value.DistanceSqr2D(nodes[i+1].Value);
                Assert.AreEqual(distanceSqr, nodeDistanceSqr);
            }
        }        
        [Test]
        public void KDTree_KDNode_Distance_3D()
        {   
            int dimensions = 3;
            KDTree<Vector3> kdTree = new KDTree<Vector3>(_points, dimensions, new KDNodeVector33DFactory());
            KDNode<Vector3>[] nodes = kdTree.GetNodes();
            
            for (int i = 0; i < nodes.Length - 1; i++)
            {
                float nodeDistanceSqr = nodes[i].DistanceSqr(nodes[i+1]);
                float distanceSqr = nodes[i].Value.DistanceSqr(nodes[i+1].Value);
                Assert.AreEqual(distanceSqr, nodeDistanceSqr);
            }
        }
        [Test]
        public void DistanceSqr2DTest()
        {
            Vector3 a = new Vector3(0f, 0f, 0f);
            Vector3 b = new Vector3(1f, 1f, 1f);
            float distanceSqr = a.DistanceSqr2D(b);
            Assert.AreEqual(2f, distanceSqr);            
        } 

        // Add more test cases as needed
    }    
    
    public class KDNodeVector33DFactory : KDNodeFactory<Vector3>
    {
        public override KDNode<Vector3> CreateNode(Vector3 value)
        {
            return new KDNodeVector33D(value);
        }
    }

    public class KDNodeVector33D : KDNode<Vector3>
    {
        override public Vector3 Position => Value;
        public override float this[int axis] => Position[axis];

        public KDNodeVector33D(Vector3 value) : base(value) {}
    }
    public class KDNodeVector32DFactory : KDNodeFactory<Vector3>
    {
        public override KDNode<Vector3> CreateNode(Vector3 value)
        {
            return new KDNodeVector32D(value);
        }
    }

    public class KDNodeVector32D : KDNode2D<Vector3>
    {
        override public Vector3 Position => Value;
        public override float this[int axis] => Position[axis == 0 ? 0 : 2];
        public KDNodeVector32D(Vector3 value) : base(value) {}
    }

    public static class Vector3Extensions
    {            
        public static float DistanceSqr2D(this Vector3 a, Vector3 b)
        {
            float x = a.x - b.x;
            float z = a.z - b.z;
            float distanceSqr = x * x + z * z;
            //Debug.Log($"DistanceSqr2D: {distanceSqr} {Mathf.Sqrt(distanceSqr)}");
            return distanceSqr;
        }
    }
}