namespace PLu.KDTree
{
    public abstract class KDNodeFactory<T>
    {
        public abstract KDNode<T> CreateNode(T value);

    }
}
