using System;
using UnityEngine;

namespace PLu.KDTree
{
    public abstract class KDNode2D<T> : KDNode<T>
    {
        public KDNode2D(T value) : base(value) {}
        
        public override float this[int axis] => axis == 0 ? Position.x : Position.z;

        public override float DistanceSqr(Vector3 other)
        {
            float x = Position.x - other.x;
            float z = Position.z - other.z;
            
            return x * x + z * z;
        }
        public override float DistanceSqr(KDNode<T> other)
        {
            float x = Position.x - other.Position.x;
            float z = Position.z - other.Position.z;
            
            return x * x + z * z;
        }

    }
}
