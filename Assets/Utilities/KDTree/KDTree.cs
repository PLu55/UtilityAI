using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PLu.KDTree
{
    public class KDTree<T>
    {
        private KDNode<T> _root; 
        private int _dimensions;
        KDNodeFactory<T> _nodeFactory;

        public KDTree(T[] values, int dimensions, KDNodeFactory<T> nodeFactory)
        {
            if (values == null)
                throw new ArgumentException("Points array must not be null or empty.");
            
            _dimensions = dimensions;
            _nodeFactory = nodeFactory;

            KDNode<T>[] nodes = new KDNode<T>[values.Length];
            
            for (int i = 0; i < values.Length; i++)
            {
                nodes[i] = _nodeFactory.CreateNode(values[i]);
            }

            _root = BuildTree(nodes, 0, _nodeFactory);
        }
        public int Count()
        {
            return Count(_root);
        }        
        private int Count(KDNode<T> node)
        {
            if (node == null)
            {
                return 0;
            }

            int leftHeight = Count(node.Left);
            int rightHeight = Count(node.Right);

            return leftHeight + rightHeight + 1;
        }
        public KDNode<T>[] GetNodes()
        {
            List<KDNode<T>> nodes = new List<KDNode<T>>();
            Traverse(_root, nodes);
            return nodes.ToArray();
        }
        public KDNode<T> FindMedian(KDNode<T>[] values, int axis)
        {
            return MedianOfMedians(values, values.Length / 2, axis);
        }
        public int GetHeight()
        {
            return GetHeight(_root);
        }
        public int GetHeight(KDNode<T> node)
        {
            if (node == null)
            {
                return 0;
            }

            int leftHeight = GetHeight(node.Left);
            int rightHeight = GetHeight(node.Right);

            return Math.Max(leftHeight, rightHeight) + 1;
        }
        public void GetBalanceFactors(out int maxImbalance, out double averageImbalance)
        {
            maxImbalance = 0;
            int totalImbalance = 0;
            int nodeCount = 0;

            ComputeBalanceFactor(_root, ref maxImbalance, ref totalImbalance, ref nodeCount);

            if (nodeCount > 0)
            {
                averageImbalance = (double)totalImbalance / nodeCount;
            }
            else
            {
                averageImbalance = 0;
            }
        }
        private KDNode<T> BuildTree(KDNode<T>[] nodes, int depth, KDNodeFactory<T> nodeFactory)
        {
            if (nodes.Length == 0)
                return null;
        
            int axis = depth % _dimensions;
            KDNode<T> medianNode = FindMedian(nodes, axis);

            KDNode<T>[] leftValues = nodes.Where(value => value[axis].CompareTo(medianNode[axis]) < 0).ToArray();
            KDNode<T>[] rightValues = nodes.Where(value => value[axis].CompareTo(medianNode[axis]) > 0).ToArray();

            KDNode<T> left = BuildTree(leftValues, depth + 1, nodeFactory);
            KDNode<T> right = BuildTree(rightValues, depth + 1, nodeFactory);
            medianNode.SetNode(axis, left, right);
            return medianNode;
        }

        private  KDNode<T> MedianOfMedians(KDNode<T>[] nodes, int k, int axis)
        {
            if (nodes.Length == 1)
                return nodes[0];

            int n = nodes.Length;
            int numGroups = n / 5 + (n % 5 == 0 ? 0 : 1);

            KDNode<T>[] medians = new KDNode<T>[numGroups];

            for (int i = 0; i < numGroups; i++)
            {
                KDNode<T>[] group = nodes.Skip(i * 5).Take(5).ToArray();
                Array.Sort(group, (a, b) => a[axis].CompareTo(b[axis]));
                medians[i] = group[group.Length / 2];
            }

            KDNode<T> medianOfMedians = MedianOfMedians(medians, medians.Length / 2, axis);

            float median = medianOfMedians[axis];
            KDNode<T>[] less = nodes.Where(node => node[axis].CompareTo(median) < 0).ToArray();
            KDNode<T>[] equal = nodes.Where(node => node[axis].CompareTo(median) == 0).ToArray();
            KDNode<T>[] greater = nodes.Where(node => node[axis].CompareTo(median) > 0).ToArray();

            if (k < less.Length)
                return MedianOfMedians(less, k, axis);
            else if (k < less.Length + equal.Length)
                return medianOfMedians;
            else
                return MedianOfMedians(greater, k - less.Length - equal.Length, axis);
        
        }    
        private int ComputeBalanceFactor(KDNode<T> node, ref int maxImbalance, ref int totalImbalance, ref int nodeCount)
        {
            if (node == null)
            {
                return 0;
            }

            int leftHeight = GetHeight(node.Left);
            int rightHeight = GetHeight(node.Right);
            int currentImbalance = Math.Abs(leftHeight - rightHeight);

            // Update max imbalance
            if (currentImbalance > maxImbalance)
            {
                maxImbalance = currentImbalance;
            }

            // Update total imbalance and node count for average calculation
            totalImbalance += currentImbalance;
            nodeCount++;

            // Recursively compute balance factor for left and right subtrees
            ComputeBalanceFactor(node.Left, ref maxImbalance, ref totalImbalance, ref nodeCount);
            ComputeBalanceFactor(node.Right, ref maxImbalance, ref totalImbalance, ref nodeCount);

            return currentImbalance;
        }
        
        private void Traverse(KDNode<T> node, List<KDNode<T>> nodes)
        {
            if (node == null)
                return;
        
            nodes.Add(node);
            Traverse(node.Left, nodes);
            Traverse(node.Right, nodes);
        }

        public List<T> RangeQuery(Vector3 queryPoint, float radius)
        {
            List<T> result = new List<T>();
            RangeSearch(_root, queryPoint, radius, radius * radius ,result);
            return result;
        }

        public void AddNode(T value)
        {
            KDNode<T> newNode = _nodeFactory.CreateNode(value);
            AddNode(_root, newNode, 0);
        }

        private void AddNode(KDNode<T> currentNode, KDNode<T> newNode, int depth)
        {
            if (currentNode == null)
            {
                _root = newNode;
                return;
            }

            int axis = depth % _dimensions;

            if (newNode[axis].CompareTo(currentNode[axis]) < 0)
            {
                if (currentNode.Left == null)
                {
                    currentNode.Left = newNode;
                }
                else
                {
                    AddNode(currentNode.Left, newNode, depth + 1);
                }
            }
            else
            {
                if (currentNode.Right == null)
                {
                    currentNode.Right = newNode;
                }
                else
                {
                    AddNode(currentNode.Right, newNode, depth + 1);
                }
            }
        }
        public void RemoveNode(T value)
        {
            _root = RemoveNode(_root, _nodeFactory.CreateNode(value), 0);
        }

        private KDNode<T> RemoveNode(KDNode<T> currentNode, KDNode<T> value, int depth)
        {
            if (currentNode == null)
            {
                return null;
            }

            int axis = depth % _dimensions;

            if (value.Value.Equals(currentNode.Value))
            {
                if (currentNode.Right != null)
                {
                    KDNode<T> minNode = FindMinNode(currentNode.Right, axis, depth + 1);
                    currentNode.Value = minNode.Value;
                    currentNode.Right = RemoveNode(currentNode.Right, minNode, depth + 1);
                }
                else if (currentNode.Left != null)
                {
                    KDNode<T> minNode = FindMinNode(currentNode.Left, axis, depth + 1);
                    currentNode.Value = minNode.Value;
                    currentNode.Right = RemoveNode(currentNode.Left, minNode, depth + 1);
                    currentNode.Left = null;
                }
                else
                {
                    return null;
                }
            }
            else if (value[axis].CompareTo(currentNode[axis]) < 0)
            {
                currentNode.Left = RemoveNode(currentNode.Left, value, depth + 1);
            }
            else
            {
                currentNode.Right = RemoveNode(currentNode.Right, value, depth + 1);
            }

            return currentNode;
        }

        private KDNode<T> FindMinNode(KDNode<T> node, int axis, int depth)
        {
            if (node == null)
            {
                return null;
            }

            int currentAxis = depth % _dimensions;
            if (currentAxis == axis)
            {
                if (node.Left == null)
                {
                    return node;
                }
                else
                {
                    return FindMinNode(node.Left, axis, depth + 1);
                }
            }
            else
            {
                KDNode<T> leftMin = FindMinNode(node.Left, axis, depth + 1);
                KDNode<T> rightMin = FindMinNode(node.Right, axis, depth + 1);
                KDNode<T> minNode = node;
                if (leftMin != null && leftMin[axis].CompareTo(minNode[axis]) < 0)
                {
                    minNode = leftMin;
                }
                if (rightMin != null && rightMin[axis].CompareTo(minNode[axis]) < 0)
                {
                    minNode = rightMin;
                }
                return minNode;
            }
        }
        private void RangeSearch(KDNode<T> node, Vector3 queryPoint, float radius, float radiusSqr, List<T> result)
        { 
            if (node == null) { return; }

            int axis = node.Axis;

            float distanceSqr = node.DistanceSqr(queryPoint);

            //Debug.Log($"_points node: {node.Value} dist: {Mathf.Sqrt(distanceSqr)} rad: {radius}");
            
            if (distanceSqr <= radiusSqr)
            {
                //Debug.Log($"--- add node");
                result.Add(node.Value);
            }

            float axisDistance = node[axis] - queryPoint[axis];

            if (axisDistance > -radius)
            {
                RangeSearch(node.Left, queryPoint, radius, radiusSqr, result);

                if (Math.Abs(axisDistance) <= radius)
                {
                    RangeSearch(node.Right, queryPoint, radius, radiusSqr, result);
                }
            }
            if (axisDistance < radius)
            {
                RangeSearch(node.Right, queryPoint, radius, radiusSqr, result);

                if (Math.Abs(axisDistance) <= radius)
                {
                    RangeSearch(node.Left, queryPoint, radius, radiusSqr, result);
                }
            }
        }
        // void FindPointsInRadius (KdTreeNode node, float x, float y, float radius,ref List<KdTreeNode> outpoints)
        // {
        //     if (node == null)
        //         return;

        //     float dx = x – node.x;
        //     float dy = y – node.y;

        //     float rsq = (dx * dx) + (dy * dy);
        //     if (rsq < (radius * radius))
        //         outpoints.Add(node);

        //     float axisdelta;
            
        //     if (node.SplitAxis == SplitAxis.X)
        //         axisdelta = node.x – x;
        //     else
        //         axisdelta = node.y – y;

        //     if (axisdelta > -radius) 
        //     {
        //         FindPointsInRadius(node.Left, x, y, radius, ref outpoints);
        //     }

        //     if (axisdelta < radius)
        //     {
        //         FindPointsInRadius(node.Right, x, y, radius, ref outpoints);

        //     }
        // }
    }

}
