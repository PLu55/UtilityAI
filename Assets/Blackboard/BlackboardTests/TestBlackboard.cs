using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;
using System.Security.Policy;
using PLu.BlackboardSystem;
using PLu.Utilities;
using System.Diagnostics;
using System.Runtime.InteropServices;
//using System.Numerics;

public class NewTestScript
{
    [Test]
    public void TestBlackboard()
    {
        GameObject testGameObject = new GameObject("TestGameObject");
        //testGameObject.AddComponent<BlackboardManager>();
        //Assert.IsNotNull(testGameObject.GetComponent<BlackboardManager>());
        //BlackboardManager blackboardManager = testGameObject.GetComponent<BlackboardManager>();
        BlackboardManager blackboardManager = new();
        Assert.IsNotNull(blackboardManager);
        //Assert.AreEqual(blackboardManager, BlackboardManager.Instance);

        Blackboard blackboard = blackboardManager.GetPrivateBlackboard(testGameObject);

        HashedString key = blackboardManager.GetOrRegisterKey("TestKey");
        
        blackboard.SetValue(key, 5);
        int intValue;
        blackboard.TryGetValue(key, out intValue);
        Assert.AreEqual(5, intValue);

        blackboard.SetValue(key, 5.5f);
        float floatValue;
        blackboard.TryGetValue(key, out floatValue);
        Assert.AreEqual(5.5f, floatValue);

        blackboard.SetValue(key, true);
        bool boolValue;
        blackboard.TryGetValue(key, out boolValue);
        Assert.AreEqual(true, boolValue);

        blackboard.SetValue(key, "TestString");
        string stringValue;
        blackboard.TryGetValue(key, out stringValue);
        Assert.AreEqual("TestString", stringValue);

        blackboard.SetValue(key, new Vector3(1, 2, 3));
        Vector3 vector3Value;
        blackboard.TryGetValue(key, out vector3Value);
        Assert.AreEqual(new Vector3(1, 2, 3), vector3Value);

        GameObject testGameObjectValue = new GameObject("TestGameObjectValue");
        blackboard.SetValue(key, testGameObjectValue);
        GameObject gameObjectValue;
        blackboard.TryGetValue(key, out gameObjectValue);
        Assert.AreEqual(testGameObjectValue, gameObjectValue);        
        
        GameObject[] testGameObjectArrayValue = new GameObject[3];
        testGameObjectArrayValue[0] = new GameObject("TestGameObjectArrayValue0");
        testGameObjectArrayValue[1] = new GameObject("TestGameObjectArrayValue1");
        testGameObjectArrayValue[2] = new GameObject("TestGameObjectArrayValue2");
        blackboard.SetValue(key, testGameObjectArrayValue);
        GameObject[] gameObjectArrayValue;
        blackboard.TryGetValue(key, out gameObjectArrayValue);
        Assert.AreEqual(testGameObjectArrayValue, gameObjectArrayValue);

        blackboard.SetValue<string>(key, "TestGeneric");
        string genericValue;
        blackboard.TryGetValue<string>(key, out genericValue);
        Assert.AreEqual("TestGeneric", genericValue);
        string aString = blackboard.GetValue<string>(key);
        Assert.AreEqual("TestGeneric", aString);
    }

    [Test]
    public void IntPerformanceTest()
    {        
        GameObject testGameObject = new GameObject("TestGameObject");
        BlackboardManager blackboardManager = new();
        Assert.IsNotNull(blackboardManager);
        //Assert.AreEqual(blackboardManager, BlackboardManager.Instance);

        Blackboard blackboard = blackboardManager.GetPrivateBlackboard(testGameObject);

        HashedString key = blackboardManager.GetOrRegisterKey("TestKey");
  

        HashedStringRegistry _keyRegistry = HashedStringRegistry.Instance;

        int numKeys = 1000000;

        HashedString[] keys = new HashedString[numKeys];
        for (int i = 0; i < numKeys; i++)
        {
            keys[i] = _keyRegistry.GetOrRegister("key" + i);
        }

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.SetValue(keys[j], j);
        }

        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Setting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
        
        stopWatch.Restart();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.TryGetValue(keys[j], out int value);
        }

        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Getting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
    }    
    
    [Test]
    public void Vector3PerformanceTest()
    {        
        GameObject testGameObject = new GameObject("TestGameObject");
        BlackboardManager blackboardManager = new();
        Assert.IsNotNull(blackboardManager);
        //Assert.AreEqual(blackboardManager, BlackboardManager.Instance);

        Blackboard blackboard = blackboardManager.GetPrivateBlackboard(testGameObject);

        HashedString key = blackboardManager.GetOrRegisterKey("TestKey");
  

        HashedStringRegistry _keyRegistry = HashedStringRegistry.Instance;

        int numKeys = 1000000;

        HashedString[] keys = new HashedString[numKeys];
        for (int i = 0; i < numKeys; i++)
        {
            keys[i] = _keyRegistry.GetOrRegister("key" + i);
        }

        Vector3[] vector3Array = new Vector3[numKeys];
        for (int i = 0; i < numKeys; i++)
        {
            vector3Array[i] = new Vector3(i, i, i);
        }

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.SetValue(keys[j], vector3Array[j]);
        }

        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Setting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
        
        stopWatch.Restart();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.TryGetValue(keys[j], out int value);
        }

        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Getting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
    }
}
