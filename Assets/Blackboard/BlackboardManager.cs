using System;
using System.Collections.Generic;
using UnityEngine;
using PLu.Utilities;

namespace PLu.BlackboardSystem
{
    //[DefaultExecutionOrder(-100)]
    public class BlackboardManager //: MonoBehaviour
    {
        //public static BlackboardManager Instance => _instance;

        //private static BlackboardManager _instance;
        // private Dictionary<string, HashedString> _keyRegistry;
        private HashedStringRegistry _hashedStringRegistry;
        //private HashSet<BlackboardKey> _keys;

        private Dictionary<GameObject, Blackboard> _privateBlackboards;
        private Dictionary<int, Blackboard> _sharedBlackboards;

        public BlackboardManager()
        {
            _hashedStringRegistry = HashedStringRegistry.Instance;
            //_keys = new();
            _privateBlackboards = new();
            _sharedBlackboards = new();
        }
        public HashedString GetOrRegisterKey(string key)
        {
            return _hashedStringRegistry.GetOrRegister(key);
        }
        public Blackboard GetPrivateBlackboard(GameObject gameObject)
        {
            if (_privateBlackboards.ContainsKey(gameObject))
            {
                return _privateBlackboards[gameObject];
            }
            else
            {
                var blackboard = new Blackboard();
                _privateBlackboards.Add(gameObject, blackboard);
                return blackboard;
            }
        }
        public Blackboard GetSharedBlackboard(int uniqId)
        {
            if (_sharedBlackboards.ContainsKey(uniqId))
            {
                return _sharedBlackboards[uniqId];
            }
            else
            {
                var blackboard = new Blackboard();
                _sharedBlackboards.Add(uniqId, blackboard);
                return blackboard;
            }
        }
    }
}
