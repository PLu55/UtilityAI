using System;
using System.Collections.Generic;
using UnityEngine;
using PLu.Utilities;

namespace PLu.BlackboardSystem
{
    [Serializable]
    public class Blackboard
    {
        public enum Key 
        {
            Enemy = 1,
            
         } 
        private Dictionary<HashedString, int> _intBlackboard = new();
        private Dictionary<HashedString, float> _floatBlackboard = new();
        private Dictionary<HashedString, bool> _boolBlackboard = new();
        private Dictionary<HashedString, string> _stringBlackboard = new();
        private Dictionary<HashedString, Vector3> _vector3Blackboard = new();
        private Dictionary<HashedString, GameObject> _gameObjectBlackboard = new();
        private Dictionary<HashedString, GameObject[]> _gameObjectArrayBlackboard = new();
        private Dictionary<HashedString, object> _genericBlackboard = new();

        public bool TryGetValue(HashedString key, out int value, int defaultValue = default)
        {
            if (_intBlackboard.ContainsKey(key))
            {
                value = _intBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }

        public bool TryGetValue(HashedString key, out float value, float defaultValue = default)
        {
            if (_floatBlackboard.ContainsKey(key))
            {
                value = _floatBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }
        public bool TryGetValue(HashedString key, out bool value, bool defaultValue = default)
        {
            if (_boolBlackboard.ContainsKey(key))
            {
                value = _boolBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }

        public bool TryGetValue(HashedString key, out string value, string defaultValue = default)
        {
            if (_stringBlackboard.ContainsKey(key))
            {
                value = _stringBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }

        public bool TryGetValue(HashedString key, out Vector3 value, Vector3 defaultValue = default)
        {
            if (_vector3Blackboard.ContainsKey(key))
            {
                value = _vector3Blackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }

        public bool TryGetValue(HashedString key, out GameObject value, GameObject defaultValue = default)
        {
            if (_gameObjectBlackboard.ContainsKey(key))
            {
                value = _gameObjectBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }
        public bool TryGetValue(HashedString key, out GameObject[] value, GameObject[] defaultValue = default)
        {
            if (_gameObjectArrayBlackboard.ContainsKey(key))
            {
                value = _gameObjectArrayBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }

        public bool TryGetValue<T>(HashedString key, out T value, T defaultValue = default)
        {
            if (_genericBlackboard.ContainsKey(key))
            {
                value = (T)_genericBlackboard[key];
                return true;
            }
            value = defaultValue;
            return false;
        }

        public T GetValue<T>(HashedString key)
        {
            if (_genericBlackboard.ContainsKey(key))
            {
                return (T)_genericBlackboard[key];
            }
            
            return default;
        }
        public void RemoveGenericValue(HashedString key)
        {
            _genericBlackboard.Remove(key);
        }
        
        public void RemoveValue<T>(HashedString key)
        {

            if (typeof(T) == typeof(int))
            {
                _intBlackboard.Remove(key);
            }
            else if (typeof(T) == typeof(float))
            {
                _floatBlackboard.Remove(key);
            }
            else if (typeof(T) == typeof(bool))
            {
                _boolBlackboard.Remove(key);
            }
            else if (typeof(T) == typeof(string))
            {
                _stringBlackboard.Remove(key);
            }
            else if (typeof(T) == typeof(Vector3))
            {
                _vector3Blackboard.Remove(key);
            }
            else if (typeof(T) == typeof(GameObject))
            {
                _gameObjectBlackboard.Remove(key);
            }
            else if (typeof(T) == typeof(GameObject[]))
            {
                _gameObjectBlackboard.Remove(key);
            }
            else
            {
                _genericBlackboard.Remove(key);
            }
        }

        public void RemoveValueFast(HashedString key, int value)
        {
            _intBlackboard.Remove(key);
        }

        public void RemoveValueFast(HashedString key, float value)
        {
            _floatBlackboard.Remove(key);
        }

        public void RemoveValueFast(HashedString key, bool value)
        {
            _boolBlackboard.Remove(key);
        }

        public void RemoveValueFast(HashedString key, string value)
        {
            _stringBlackboard.Remove(key);
        }

        public void RemoveValueFast(HashedString key, Vector3 value)
        {
            _vector3Blackboard.Remove(key);
        }

        public void RemoveValueFast(HashedString key, GameObject value)
        {
            _gameObjectBlackboard.Remove(key);
        }
        public void RemoveValueFast(HashedString key, GameObject[] value)
        {
            _gameObjectBlackboard.Remove(key);
        }
        public void RemoveValueFast<T>(HashedString key, T value)
        {
            _genericBlackboard.Remove(key);
        }
            
         public void SetValue(HashedString key, int value)
        {
            _intBlackboard[key] = value;
        }

        public void SetValue(HashedString key, float value)
        {
            _floatBlackboard[key] = value;
        }

        public void SetValue(HashedString key, bool value)
        {
            _boolBlackboard[key] = value;
        }

        public void SetValue(HashedString key, string value)
        {
            _stringBlackboard[key] = value;
        }

        public void SetValue(HashedString key, Vector3 value)
        {
            _vector3Blackboard[key] = value;
        }

        public void SetValue(HashedString key, GameObject value)
        {
            _gameObjectBlackboard[key] = value;
        }
        public void SetValue(HashedString key, GameObject[] value)
        {
            _gameObjectArrayBlackboard[key] = value;
        }
        public void SetValue<T>(HashedString key, T value)
        {
            _genericBlackboard[key] = value;
        }

        public string GetStatistics()
        {
            return $"Int: {_intBlackboard.Count}, Float: {_floatBlackboard.Count}, Bool: {_boolBlackboard.Count}, String: {_stringBlackboard.Count}, Vector3: {_vector3Blackboard.Count}, GameObject: {_gameObjectBlackboard.Count}, Generic: {_genericBlackboard.Count}";
        }
    }
}
