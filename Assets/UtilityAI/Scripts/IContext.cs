using UnityEditor.ShaderKeywordFilter;
using PLu.BlackboardSystem;
using PLu.Dev.AI;

namespace PLu.UtilityAI.Core
{
    public interface IContext
    {
        UtilityAgent Owner { get; }
        Blackboard PrivateBlackboard  { get; }
        public ISensor[] Sensors { get; }
    }
}
