using UnityEngine;

namespace PLu.UtilityAI.Core
{
    //[CreateAssetMenu(menuName = "UtilityAI/Consideration")]
    public abstract class BaseConsideration : ScriptableObject
    {
        [SerializeField] private bool _invert;
        [SerializeField] private int _priority;

        public string Name => name;
        public int Priority => _priority;
        public bool Invert => _invert;

        public virtual void Initialize(IContext context)
        {
        }
        public abstract float Consider(IContext context);
    }
}
