using System;
using UnityEngine;

namespace PLu.UtilityAI.Core
{
    public class Normalization : ScriptableObject
    {
        public virtual float Normalize (float score) => score;

    }    
    public class LinearNormalization : Normalization
    {
        [SerializeField] private float _scale;
        [SerializeField] private float _offset;

        public override float Normalize (float score) => _scale * score + _offset;


    }
}