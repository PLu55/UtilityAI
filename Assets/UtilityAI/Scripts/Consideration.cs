using UnityEngine;

namespace PLu.UtilityAI.Core
{
    //[CreateAssetMenu(menuName = "UtilityAI/Consideration")]
    public abstract class Consideration : BaseConsideration
    {
        [SerializeField] private AnimationCurve _responseCurve;

        public AnimationCurve ResponseCurve => _responseCurve;

        protected virtual float Evaluate(float score)
        {
            return _responseCurve.Evaluate(Invert ? 1f - Mathf.Clamp01(score) : Mathf.Clamp01(score));
        }

    }
}
