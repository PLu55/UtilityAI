using UnityEngine;

namespace PLu.UtilityAI.Core
{
    //[CreateAssetMenu(menuName = "UtilityAI/Consideration")]
    public abstract class BinaryConsideration : BaseConsideration
    {
        protected virtual float Evaluate(bool value)
        {
            return (Invert ? !value : value) ? 1f : 0f;
        }
    }
}
