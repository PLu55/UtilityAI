using System;
using System.Collections.Generic;
using PLu.BehaviourTrees;
using UnityEngine;
using PLu.Dev.AI;
using PLu.BlackboardSystem;
using PLu.Utilities;

namespace PLu.UtilityAI.Core
{
    public abstract class Action : ScriptableObject
    {
        [SerializeField] private BaseConsideration[] _considerations;
        [SerializeField] private ActionStrategy _strategy;
        [SerializeField] private float _weight = 1f;

        public string Name => name;

        public float Weight => _weight;


        public ActionContext ActionContext { get; private set; }

        public BaseConsideration[] Considerations => _considerations;

        public enum State { Success, Continue, Failed }

#if UNITY_EDITOR
        void OnValidate()
        {
            SortConsiderations();
        }
#endif
        public virtual void Initialize(IContext context)
        {
            Debug.Log($"Action: {name} initialized");
            ActionContext = new ActionContext();
            foreach (var consideration in _considerations)
            {
                consideration.Initialize(context);
            }
            SortConsiderations();
        }

        private void SortConsiderations()
        {
            Array.Sort(_considerations, (a, b) => b.Priority.CompareTo(a.Priority));
        }

        // TODO: implement bonus = weight + momentum + bonuses
        // TODO: implement min, use it to bailing out if totalScore is less than min
        public virtual bool Consider(IContext worldContext, ActionScoring actionScoring)
        {
            float totalScore = 1f; // = bonus

            UtilityAILogger.Log($"---- Consider action: {Name} >>>------------------------------>>>");

            foreach (var consideration in _considerations)
            {
                float localScore = consideration.Consider(worldContext);
                totalScore *= localScore;

                UtilityAILogger.Log($"-------- Consideration: {consideration.Name} score: {localScore} total score: {totalScore}");

                if (totalScore <= 0f)
                {
                    UtilityAILogger.Log($"---- Bail out <<<---------------------------------------------------<<<");

                    return false;
                }
            }

            float originalScore = totalScore;
            float modFactor = 1 - (1 / _considerations.Length);
            float makeupValue = (1 - originalScore) * modFactor;
            float finaleScore = (originalScore + (makeupValue * originalScore)) * _weight;

            UtilityAILogger.Log($"---- Final score: {finaleScore:F3} <<<----------------------------------------<<<");

            actionScoring.SetScore(finaleScore);
            return true;
        }
        public virtual ActionScoring[] PreConsiderations(IContext worldContext)
        {
            ActionScoring[] actionScorings = { new ActionScoring(this) };
            return actionScorings;
        }

        public virtual void Begin(IContext worldContext, ActionScoring actionScoring) {}
        public virtual void End(IContext worldContext, ActionScoring actionScoring) {}
        public abstract State Process(float deltaTime, IContext worldContext, ActionScoring actionScoring);

    }

    public class ActionScoring : IComparable<ActionScoring>
    {
        public Action Action { get; private set; }
        public float Score { get; private set; }
        private List<object> _data;

        public ActionScoring(Action action)
        {
            Action = action;
            Score = 0f;
            _data = null;
        }

        public void SetScore(float score)
        {
            Score = score;
        }
        public void AddData(object data)
        {
            if (_data == null)
            {
                _data = new List<object>();
            }
            _data.Add(data);
        }

        public int CompareTo(ActionScoring other)
        {
            return Score.CompareTo(other.Score);
        }
    }

    public class ActionEnvironment : IDisposable
    {
        public ActionContext ActionContext  { get; private set; }
        public List<HashedString> Keys { get; private set; }
        public Action Action { get; private set; }
        public float Score { get; private set; }

        public ActionEnvironment(ActionContext actionContext, Action action)
        {
            ActionContext = actionContext;
            Keys = null;
            Action = action;
            Score = 0f;
        }
        public ActionEnvironment(ActionContext actionContext, Action action, float score, List<HashedString> keys)
        {
            ActionContext = actionContext;
            Keys = keys;
            Action = action;
            Score = score;
        }

        public void AddValue<T>(HashedString key, T value)
        {
            ActionContext.SetValue<T>(key, value);
        }

        public T GetValue<T>(HashedString key)
        {
            return ActionContext.GetValue<T>(key);
        }

        public object GetObject(HashedString key)
        {
            return ActionContext.GetObject(key);
        }
        public void Remove(HashedString key)
        {
            ActionContext.Remove(key);
            Keys.Remove(key);
        }
        public void Dispose()
        {
            foreach (var key in Keys)
            {
                ActionContext.Remove(key);
            }
        }
    }

    public class ActionContext
    {
        private readonly Dictionary<HashedString, object> values = new Dictionary<HashedString, object>();

        public void SetValue<T>(HashedString key, T value)
        {
            values[key] = new TypedValue<T>(value);
        }

        public T GetValue<T>(HashedString key)
        {
            if (values.TryGetValue(key, out object value) && value is TypedValue<T> typedValue)
            {
                return typedValue.Value;
            }
            throw new InvalidOperationException($"The key '{key}' was not found in the blackboard or type mismatch.");
        }

        // New method to get a value without knowing its type
        public ITypedValue GetObject(HashedString key)
        {
            if (values.TryGetValue(key, out object value) && value is ITypedValue typedValue)
            {
                return typedValue;
            }
            throw new KeyNotFoundException($"The key '{key}' was not found in the blackboard.");
        }

        public void Remove(HashedString key)
        {
            values.Remove(key);
        }
    }

    public interface ITypedValue
    {
        object GetValue();
        Type GetValueType();
    }

    public class TypedValue<T> : ITypedValue
    {
        public T Value { get; }

        public TypedValue(T value)
        {
            Value = value;
        }
        public object GetValue() => Value;

        public Type GetValueType() => typeof(T);
    }
}