using System;
using UnityEngine;
using PLu.Utilities;
using PLu.SimplePriorityQueue;

namespace PLu.UtilityAI.Core
{
    // TODO: implement dynamic update interval
    public class UtilityAgent : MonoBehaviour
    {
        [SerializeField] private float _updateInterval = 0.5f;
        [SerializeField] private Action[] _actions;
 
        public IContext Context => _context;
        private RepeatTimer _updateTimer;
        private IContext _context;

        private ActionScoring _currentAction;

        protected void Awake()
        {
            _context = GetComponent<IContext>();
            Debug.Assert(_context != null, "No IContext component found on agent");
        }
        void Start()
        {
            //_updateTimer = new RepeatTimer(_updateInterval, true);
            //_updateTimer.OnTimerRepeat += SelectAction;
            InitializeActions();
            SortActions();
        }

        private void InitializeActions()
        {
            foreach (var action in _actions)
            {
                action.Initialize(_context);
            }
        }

        void Update()
        {
            //_updateTimer.Tick(Time.deltaTime);
            UpdateAction(Time.deltaTime);
        }
        private void UpdateAction(float deltaTime)
        {
            UtilityAILogger.Log($"UpdateAction >>>===========================================================================>>>");
            ActionScoring bestAction = DecideBestAction(_context);

            if (bestAction == null)
            {
#if UTILITY_AI_LOGGING
                if (_currentAction != null)
                {
                    UtilityAILogger.Log($"---- current action ends: {_currentAction.Action.name} <<<<");
                    UtilityAILogger.Log($"---- no new action available <<<<");
                }
#endif
 
                _currentAction?.Action.End(_context, _currentAction);
                _currentAction = null;
                return;
            }

            if (bestAction != _currentAction)
            {
#if UTILITY_AI_LOGGING
                if (_currentAction != null)
                {
                    UtilityAILogger.Log($"---- current action ends: {_currentAction.Action.name} <<<<");
                }
                else
                {
                    UtilityAILogger.Log($"---- no current action <<<<");
                }
                UtilityAILogger.Log($"---- new action begins: {bestAction.Action.Name} >>>>");
#endif
                _currentAction?.Action.End(_context, _currentAction);
                _currentAction = bestAction;
                _currentAction?.Action.Begin(_context, _currentAction);
            }

            if (_currentAction != null)
            {
                Action.State state = _currentAction.Action.Process(deltaTime, _context, _currentAction);
            
                UtilityAILogger.Log($"---- process action: {_currentAction.Action.Name} state: {state} <<<<");

                if (state != Action.State.Continue)
                {
                    _currentAction = null;
                }
            }

            UtilityAILogger.Log($"UpdateAction ends <<<=======================================================================<<<");

        }
        private void SortActions()
        {
            Array.Sort(_actions, (a, b) => b.Weight.CompareTo(a.Weight));
        }
        public ActionScoring DecideBestAction(IContext context)
        {
            if (_actions == null) { return null; }

            UtilityAILogger.Log($"Decide best action for: {gameObject.name} >>>------------------------------------------------------------->>>");

            float highestScore = 0f;
            ActionScoring bestAction = null;
            SimplePriorityQueue<ActionScoring> queue = new SimplePriorityQueue<ActionScoring>();

            foreach (Action action in _actions) 
            { 
                // Optimization: if weight is less then highest score then it's
                // no need to consider any further actions as the actions are sorted
                if (action.Weight < highestScore) { break; }

                ActionScoring[] actionScorings = action.PreConsiderations(context);

                foreach (ActionScoring actionScoring in actionScorings)
                {
                    if (action.Consider(context, actionScoring))
                    {
                        highestScore = actionScoring.Score > highestScore ? actionScoring.Score : highestScore;
                        queue.Insert(actionScoring);
                    }
                }
            }

            if (!queue.IsEmpty) 
            {
                Debug.Assert(queue.PeekMax().Score >= highestScore, "Queue is not sorted correctly");
                //Debug.Log($" Queue max score: {queue.PeekMax().Score} queue size: {queue.Count}");
                bestAction = queue.ExtractMax();
            }

#if UTILITY_AI_LOGGING
            if (bestAction == null)
            {
                UtilityAILogger.Log($"Decide best action ends, no action selected, Score: {0f:F3} <<<--------------------------------------------<<<");
            }
            else
            {
                UtilityAILogger.Log($"Decide best action ends, selected action is: {bestAction?.Action.GetType().Name} with Score: {bestAction.Score:F3} <<<----------------<<<");
            }
#endif
            return bestAction;
        }
    }
}