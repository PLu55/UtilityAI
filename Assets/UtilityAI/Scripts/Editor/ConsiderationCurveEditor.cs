using UnityEngine;
using UnityEditor;

namespace PLu.UtilityAI.Core
{
    [CustomEditor(typeof(ConsiderationCurve))]
    public class ConsiderationCurveEditor : Editor
    {
        private const int textureWidth = 512;
        private const int textureHeight = 256;

        public override void OnInspectorGUI()
        {
            ConsiderationCurve curve = (ConsiderationCurve)target;

            // Draw the default inspector
            DrawDefaultInspector();

            // Draw the curve
            Texture2D curveTexture = new Texture2D(textureWidth, textureHeight);
            DrawCurve(curve.Curve, curveTexture);

            GUILayout.Label(curveTexture);
        }

        private void DrawCurve(AnimationCurve curve, Texture2D texture)
        {
            for (int x = 0; x < textureWidth; x++)
            {
                float t = (float)x / (textureWidth - 1);
                float value = curve.Evaluate(t);

                int y = Mathf.Clamp((int)(value * (textureHeight - 1)), 0, textureHeight - 1);
                texture.SetPixel(x, y, Color.white);
            }

            texture.Apply();
        }
    }
}
