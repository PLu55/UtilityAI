using System;
using UnityEngine;

namespace PLu.UtilityAI.Core
{
    [CreateAssetMenu(menuName = "UtilityAI/ConsiderationCurve")]
    public class ConsiderationCurve : ScriptableObject
    {
        [SerializeField] private Type _type;
        [SerializeField] private float[] _parameters;
        public AnimationCurve Curve;// = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        public enum Type { Custom, Linear, Inverse, Logistic, Exponential, Polynomial}
        public float Evaluate(float t)
        {
            return Curve.Evaluate(t);
        }
        void Awake()
        {
            Debug.Log("Awake");
            if (Curve == null)
            {
                switch (_type)
                {
                    case Type.Linear:
                        Curve = AnimationCurve.Linear(_parameters[0], _parameters[1], _parameters[2], _parameters[3]);
                        break;
                    case Type.Inverse:
                        Curve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
                        break;
                    case Type.Logistic:
                        Curve = new AnimationCurve(new Keyframe(0, 0.5f, 0, 0), new Keyframe(1, 0.5f, 0, 0));
                        break;
                    case Type.Exponential:
                        Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
                        break;
                    case Type.Polynomial:
                        Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
                        Curve.preWrapMode = WrapMode.Clamp;
                        Curve.postWrapMode = WrapMode.Clamp;
                        break;
                    default:
                        Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
                        break;
                }
            }
        }
        
#if UNITY_EDITOR
        void OnValidate()
        {
            Debug.Log($"OnValidate {_type} {_parameters.Length}");
            if (true) //Curve == null)
            {
                switch (_type)
                {
                    case Type.Linear:
                        Curve = AnimationCurve.Linear(_parameters[0], _parameters[1], _parameters[2], _parameters[3]);
                        break;
                    case Type.Inverse:
                        Curve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
                        break;
                    case Type.Logistic:
                        Curve = new AnimationCurve(new Keyframe(0, 0.5f, 0, 0), new Keyframe(1, 0.5f, 0, 0));
                        break;
                    case Type.Exponential:
                        Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
                        break;
                    case Type.Polynomial:
                        Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
                        Curve.preWrapMode = WrapMode.Clamp;
                        Curve.postWrapMode = WrapMode.Clamp;
                        break;
                    default:
                        Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
                        break;
                }
                Curve = new AnimationCurve();
            }
        }    
#endif
    }
}
