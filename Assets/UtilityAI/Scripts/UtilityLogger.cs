using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 


namespace PLu.UtilityAI.Core
{

    public static class UtilityAILogger
    {
#if UTILITY_AI_LOGGING // UTILITY_AI_LOGGING is a define symbol that can be set in the project settings->player->other settings->scripting define symbols
        public static void Log(string message)
        {
            Debug.Log($"[Utility AI] {message}");
            
        }
        public static void LogWarning(string message)
        {
            Debug.LogWarning($"[Utility AI] {message}");
            
        }
        public static void LogError(string message)
        {
            Debug.LogError($"[Utility AI] {message}");
        }

#else
    public static void Log(string message) {}
    public static void LogWarning(string message) {}
    public static void LogError(string message) {}
#endif
    }
}
