using System.Collections;
using System.Collections.Generic;
using PLu.BlackboardSystem;
using PLu.Dev.AI;
using UnityEngine;

namespace PLu.UtilityAI.Core
{
    public class Context : IContext
    {
        public UtilityAgent Owner { get; protected set; }

        public Blackboard PrivateBlackboard { get; protected set; }

        public ISensor[] Sensors { get; protected set; }
    }
}
