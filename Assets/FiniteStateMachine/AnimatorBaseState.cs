using UnityEngine;

namespace PLu.StateMachine
{
    public abstract class AnimatorBaseState : IState
    {
        protected readonly PlayerController player;
        protected readonly Animator animator;

        protected static readonly int LocomotionHash = Animator.StringToHash("Locomotion");
        protected static readonly int JumpHash = Animator.StringToHash("Locomotion");

        protected const float crossFadeDuration = 0.1f;
        protected AnimatorBaseState(PlayerController player, Animator animator)
        {
            this.player = player;
            this.animator = animator;
        }
        public virtual string Name => GetType().Name;
        public virtual void FixedUpdate()
        {
            // Noop
        }

        public virtual void OnEnter()
        {
            // Noop
        }

        public virtual void OnExit()
        {
           // Noop
        }

        public virtual void Update()
        {
            // Noop
        }
    }
}
