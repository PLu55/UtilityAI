using UnityEngine;
using NUnit.Framework;
using UnityEngine.TestTools;

using PLu.StateMachine;
using System.Net;

namespace PLu.StateMachine.Tests
{
    public class Context
    {
        public int State;

        public State Enter = null;
        public State Exit = null;
        public State Update = null;
        public State FixedUpdate = null;

        public void Reset()
        {
            Enter = null;
            Exit = null;
            Update = null;
            FixedUpdate = null;
        }

        public override string ToString()
        {
            return $"Enter: {Enter?.Name}, Exit: {Exit?.Name}, Update: {Update?.Name}, FixedUpdate: {FixedUpdate?.Name}";
        }           
    }
    public class State : BaseState
    {
        public override string Name => _name;

        private string _name;
        private Context _context;

        public State(string name, Context context)
        {
            _name = name;
            _context = context;
        }
        public override string ToString()
        {
            return _name;
        }
        public override void OnEnter()
        {
            _context.Enter = this;
        }

        public override void OnExit()
        {
            _context.Exit = this;
        }

        public override void Update()
        {
            _context.Update = this;
        }

        public override void FixedUpdate()
        {
            _context.FixedUpdate = this;
        }
    }

    public class State1 : State
    {
        public State1(Context context) : base("State1", context)
        {
        }
    }
    public class State2 : State
    {
        public State2(Context context) : base("State2", context)
        {
        }
    }
    public class State3 : State
    {
        public State3(Context context) : base("State3", context)
        {
        }
    }
    public class TestFiniteStateMachine
    {
        [Test]
        public void Test1()
        {
            int _state = 0;
            IPredicate predicate = new FuncPredicate(() => _state == 0);
            Assert.IsTrue(predicate.Evaluate());
            _state = 1;
            Assert.IsFalse(predicate.Evaluate());
        }

        [Test]
        public void Test2()
        {   
            int _state = 0;
            Context context = new Context();
            IState state1 = new State1(context);
            IState state2 = new State2(context);
            IState state3 = new State3(context);

            StateMachine fsm = new StateMachine(state1);
            Assert.IsNotNull(fsm);
            Assert.AreEqual(fsm.GetCurrentState(), state1);

            fsm.AddAnyTransition(state1, new FuncPredicate(() => _state == 0));
            fsm.AddAnyTransition(state2, new FuncPredicate(() => _state == 5));
            fsm.AddTransition(state1, state2, new FuncPredicate(() => _state == 2));
            fsm.AddTransition(state2, state3, new FuncPredicate(() => _state == 3));
            fsm.AddTransition(state3, state1, new FuncPredicate(() => _state == 1));
            fsm.AddTransition(state2, state1, new FuncPredicate(() => _state == 4));
            
            _state = 2;
            fsm.Update();

            Assert.AreEqual(state2, fsm.GetCurrentState());
            Assert.AreEqual(state1, context.Exit);
            Assert.AreEqual(state2, context.Enter);
            Assert.AreEqual(state2, context.Update);

            _state = 3;
            fsm.Update();
            Assert.AreEqual(state3, fsm.GetCurrentState());
            Assert.AreEqual(state2, context.Exit);
            Assert.AreEqual(state3, context.Enter);
            Assert.AreEqual(state3, context.Update);

            _state = 0;
            fsm.Update();
            Assert.AreEqual(state1, fsm.GetCurrentState());
            Assert.AreEqual(state3, context.Exit);
            Assert.AreEqual(state1, context.Enter);
            Assert.AreEqual(state1, context.Update);

            _state = 5;
            fsm.Update();
            Assert.AreEqual(state2, fsm.GetCurrentState());
            Assert.AreEqual(state1, context.Exit);
            Assert.AreEqual(state2, context.Enter);
            Assert.AreEqual(state2, context.Update);

        }

    }
}