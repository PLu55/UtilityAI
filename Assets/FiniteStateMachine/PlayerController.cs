using System;
using UnityEditorInternal;
using UnityEngine;
using PLu.StateMachine;
using Zenject.ReflectionBaking.Mono.Cecil.Cil;

namespace PLu.StateMachine
{
    public class PlayerController : MonoBehaviour
    {
        public Animator Animator { get; }
        public bool IsGrounded { get; private set; }
        
        private StateMachine _stateMachine;
        

        static readonly int Speed = Animator.StringToHash("Speed");
        public PlayerController(Animator animator)
        {
            Animator = animator;
        }

        private void Awake()
        {
            StateMachineSetup();
        }

        private void StateMachineSetup()
        { 
            _stateMachine = new StateMachine();

            // States
            var _locomotionState = new LocomotionState(this, Animator);
            var _jumpState = new JumpState(this, Animator);

            // Transitions
            At(_locomotionState, _jumpState, () => !IsGrounded);
            At(_jumpState, _locomotionState, () => IsGrounded);

        }
        private void At(IState from, IState to, IPredicate condition) => _stateMachine.AddTransition(from, to, condition);
        private void At(IState from, IState to, Func<bool> condition) => _stateMachine.AddTransition(from, to, condition);
        private void Any(IState to, IPredicate condition) => _stateMachine.AddAnyTransition(to, condition);
        
        private void Update()
        {
            _stateMachine.Update();
        }
        private void FixedUpdate()
        {
            _stateMachine.FixedUpdate();
        }

        public void HandleMovement()
        {
            // Noop
        }

        public void HandleJump()
        {
            // Noop
        }
    }
}
