using System;

namespace PLu.StateMachine
{
    public class FuncPredicate : IPredicate
    {
        private Func<bool> _func;
        public FuncPredicate(Func<bool> func)
        {
            _func = func;
        }
        public bool Evaluate() => _func.Invoke();
    }
}
