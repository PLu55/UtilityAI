namespace PLu.StateMachine
{
    public interface ITransition
    {
        IPredicate Condition { get; }
        IState To { get; }
    }
}
