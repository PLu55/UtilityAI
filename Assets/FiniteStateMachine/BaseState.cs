namespace PLu.StateMachine
{
    public abstract class BaseState : IState
    {
        public virtual string Name => GetType().Name;
        public virtual void FixedUpdate()
        {
            // Noop
        }

        public virtual void OnEnter()
        {
            // Noop
        }

        public virtual void OnExit()
        {
           // Noop
        }

        public virtual void Update()
        {
            // Noop
        }
    }
}
