using UnityEngine;

namespace PLu.StateMachine
{
    public class JumpState : AnimatorBaseState
    {
        public JumpState(PlayerController player, Animator animator) : base(player, animator)
        {
        }

        public override void OnEnter()
        {
            animator.CrossFade(JumpHash, crossFadeDuration);
        }

        public override void FixedUpdate()
        {
            player.HandleMovement();
            player.HandleJump();
        }
    }
}
