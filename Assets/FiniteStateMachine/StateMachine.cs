using System;
using System.Collections.Generic;
using UnityEngine;

namespace PLu.StateMachine
{
    public class StateMachine
    {
        StateNode _currentState;
        Dictionary<Type, StateNode> _nodes = new();
        HashSet<ITransition> _anyTransitions = new();
        private class StateNode
        {
            public IState State { get; }
            public HashSet<Transition> Transitions { get; }

            public StateNode(IState state)
            {
                State = state;
                Transitions = new HashSet<Transition>();
            }

            public void AddTransition(IState to, IPredicate condition)
            {
                Transitions.Add(new Transition(to, condition));
            }

            public override string ToString()
            {
                return State.Name;
            }
        }
        public StateMachine()
        {}
        public StateMachine(IState initialState)
        {
            _currentState = GetOrAddNode(initialState); 
        }
        public void Update()
        {
            var transition = GetTransition();
 
             if (transition != null)
            {
                ChangeState(transition.To);
            }
            _currentState.State?.Update();
        }

        public void FixedUpdate()
        {
            _currentState.State?.FixedUpdate();
        }

        public void SetState(IState state)
        {
            _currentState.State?.OnExit();
            _currentState = _nodes[state.GetType()];
            _currentState.State?.OnEnter();
            StateNode node = new StateNode(state);
        }

       public void AddAnyTransition(IState to, IPredicate condition)
        {
            _anyTransitions.Add(new Transition(to, condition));
        }

        public void AddAnyTransition(IState to, Func<bool> condition)
        {
            _anyTransitions.Add(new Transition(to, new FuncPredicate(condition)));
        }

        public void AddTransition(IState from, IState to, IPredicate condition)
        {
            GetOrAddNode(from).AddTransition(GetOrAddNode(to).State, condition);
        }
        public void AddTransition(IState from, IState to, Func<bool> condition)
        {
            GetOrAddNode(from).AddTransition(GetOrAddNode(to).State, new FuncPredicate(condition));
        }
        private StateNode GetOrAddNode(IState state)
        {
            StateNode node = _nodes.GetValueOrDefault(state.GetType());

            if (node == null)
            {
                node = new StateNode(state);
                _nodes.Add(state.GetType(), node);
            }
            return node;
        }

        public IState GetCurrentState()
        {
            return _currentState.State;
        }   
        private void ChangeState(IState state)
        {
            if (state == _currentState.State) { return; }

            _currentState?.State.OnExit();
            _currentState = _nodes[state.GetType()];
            _currentState?.State.OnEnter(); 
        }

        private ITransition GetTransition()
        {
            foreach (var transition in _anyTransitions)
            {
                if (transition.Condition.Evaluate())
                {
                    return transition;
                }
            }

            foreach (var transition in _currentState.Transitions)
            {
                if (transition.Condition.Evaluate())
                {
                    return transition;
                }
            }
            return null;
        }

    }
}
