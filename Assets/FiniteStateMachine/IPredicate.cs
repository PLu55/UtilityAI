namespace PLu.StateMachine
{
    public interface IPredicate
    {
        bool Evaluate();
    }
}
