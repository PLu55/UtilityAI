namespace PLu.StateMachine
{
    public interface IState
    {
        string Name { get; }
        void OnEnter();
        void OnExit();
        void Update();
        void FixedUpdate();
    }
}
