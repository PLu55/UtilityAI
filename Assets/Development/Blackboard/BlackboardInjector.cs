using Zenject;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    public class BlackboardManagerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<BlackboardManager>().AsSingle().NonLazy();
        }
    }
}