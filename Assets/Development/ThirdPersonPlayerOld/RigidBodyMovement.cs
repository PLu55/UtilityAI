using UnityEngine;
// Use playerInput Component
namespace PLu
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidBodyMovement : MonoBehaviour, IMovement
    {
        private Rigidbody rb;
        [SerializeField] private float movementForce = 1f;
        [SerializeField] private float jumpForce = 5f;
        [SerializeField] private Camera playerCamera;
        [SerializeField] private float maxSpeed = 5f;
        private Vector3 forceDirection = Vector3.zero;
        

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        public void Move(Vector2 direction)
        {

            Debug.Log($"Move {direction}");
            forceDirection += direction.x * Vector3.right * movementForce;
            forceDirection += direction.y * Vector3.forward * movementForce;

            rb.AddForce(forceDirection);
            //forceDirection = Vector3.zero;

            if (rb.velocity.y < 0f)
            {
                rb.velocity += Vector3.down * Physics.gravity.y * Time.fixedDeltaTime * 0.5f;
            }

            Vector3 horizontalVelocity = rb.velocity;
            horizontalVelocity.y = 0;

            if (horizontalVelocity.sqrMagnitude > maxSpeed * maxSpeed)
            {
                rb.velocity = horizontalVelocity.normalized * maxSpeed + Vector3.up * rb.velocity.y;
            }
        }

        public void Jump()
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }
}
