using UnityEngine;
// Use playerInput Component
namespace PLu
{
    internal interface IMovement
    {
        void Move(Vector2 direction);
        void Jump();
    }
}
