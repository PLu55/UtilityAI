using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;

namespace PLu.Dev.AI
{
    public class ThirdPersonController : MonoBehaviour
    {
        private ThirdPersonActionsAsset playerActionAsset;
        private InputAction move;

        private Rigidbody rb;
        [SerializeField] private float movementForce = 1f;
        [SerializeField] private float jumpForce = 5f;
        [SerializeField] private Camera playerCamera;
        [SerializeField] private float maxSpeed = 5f;
        private Vector3 forceDirection = Vector3.zero;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            playerActionAsset = new ThirdPersonActionsAsset();
            //playerActionAsset.Enable();
            move = playerActionAsset.Player.Move;
            //move.performed += ctx => Move(ctx.ReadValue<Vector2>());
            //move.canceled += ctx => Move(Vector2.zero);
            rb = GetComponent<Rigidbody>();
        }

        private void OnEnable()
        {
            playerActionAsset.Player.Jump.started += DoJump;
            playerActionAsset.Enable();
        }

        private void OnDisable()
        {
            playerActionAsset.Player.Jump.started -= DoJump;
            playerActionAsset.Disable();
        }
        private void FixedUpdate()
        {
            //forceDirection += move.ReadValue<Vector2>().x * GetCameraRight(playerCamera) * movementForce;
            //forceDirection += move.ReadValue<Vector2>().y * GetCameraForward(playerCamera) * movementForce; 
            forceDirection += move.ReadValue<Vector2>().x * Vector3.right * movementForce;
            forceDirection += move.ReadValue<Vector2>().y * Vector3.forward *  movementForce; 
        
            rb.AddForce(forceDirection);
            forceDirection = Vector3.zero;

            if (rb.velocity.y < 0f)
            {
                rb.velocity += Vector3.down * Physics.gravity.y * Time.fixedDeltaTime * 0.5f;
            }

            Vector3 horizontalVelocity = rb.velocity;
            horizontalVelocity.y = 0;
            
            if (horizontalVelocity.sqrMagnitude > maxSpeed * maxSpeed)
            {
                //horizontalVelocity = horizontalVelocity.normalized * maxSpeed;
                //rb.velocity = new Vector3(horizontalVelocity.x, rb.velocity.y, horizontalVelocity.z);
                rb.velocity = horizontalVelocity.normalized * maxSpeed + Vector3.up * rb.velocity.y;
            }

            LookAt();
        }

        private void LookAt()
        {
            Vector3 direction = rb.velocity;
            direction.y = 0f;

            if (move.ReadValue<Vector2>().sqrMagnitude > 0.1f && direction.sqrMagnitude > 0.1f)
            {
                transform.forward = direction.normalized;
                rb.rotation = Quaternion.LookRotation(direction, Vector3.up);
            }
            else
            {
                rb.angularVelocity = Vector3.zero;
            }
        }
        private Vector3 GetCameraForward(Camera playerCamera)
        {
            Vector3 forward = playerCamera.transform.forward;
            forward.y = 0;
            return forward.normalized;
        }

        private Vector3 GetCameraRight(Camera playerCamera)
        {            
            Vector3 right = playerCamera.transform.right;
            right.y = 0;
            return right.normalized;
        }

        private void DoJump(InputAction.CallbackContext context)
        {
            if (IsGrounded())
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            }
        }

        private bool IsGrounded()
        {   
            Debug.Log("isGrounded???");
            Ray ray = new Ray(transform.position + Vector3.up * -0.9f, Vector3.down);
            if (Physics.Raycast(ray, out RaycastHit hit, 0.2f))
            {
                if (!hit.collider.isTrigger)
                {
                    Debug.Log("isGrounded");
                    return true;
                }
            }
            return false;
        }
    }
}
