using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


// Use playerInput Component, Add PlayerActions from the InputActions asset
// Generated from the Input system setup. Choose Behavior as Invoke Unity Events

namespace PLu
{
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerController : MonoBehaviour
    {
        private IMovement _movement;
        private PlayerInput _playerInput;
        private InputAction _jumpAction;
        private InputAction _moveAction;
        void Awake()
        {
            _movement = GetComponent<IMovement>();        
            _playerInput = GetComponent<PlayerInput>();
            _moveAction = _playerInput.actions["Move"];
            _jumpAction = _playerInput.actions["Jump"];
        }

        void OnEnable()
        {
            SetupInputs();
        }

        void OnDisable()
        {

            TearDownInputs();
        }

        void Start()
        {
           // IPlayerActions.Actions actions = _playerActions.Player;
        }

        public void Move(InputAction.CallbackContext context)
        {
            _movement.Move(context.ReadValue<Vector2>());
        }
        private void Jump(InputAction.CallbackContext context)
        {
            _movement.Jump();
        }

        void SetupInputs()
        {
            _moveAction.performed += Move;
            _jumpAction.performed += Jump;
        }


        void TearDownInputs()
        { 
            _moveAction.performed -= Move;
            _jumpAction.performed -= Jump;
        }
    }
}
