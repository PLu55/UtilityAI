using System;
using System.Collections.Generic;
using UnityEngine;
using PLu.Utilities;

namespace PLu.Dev.AI
{
    public class EquipManager : MonoBehaviour
    {
        [field:SerializeField] public EquipSlotDefinitions EquipDefinition { get; set; }
        //public Dictionary<string, EquipSlot> EquipSlots { get; private set; }
        [field:SerializeField] public SerializableDictionary<string, EquipSlot> EquipSlots { get; private set; }

        private StatsManager _statsManager;
        private EquipSlotDefinitions _currentEquipDefinition;
        void OnValidate()
        {
            Initialize();
        }
        void Awake()
        {
            Debug.Log($"EquipManager Awake on {gameObject.name}");
            Initialize();
        }

        public IEquipable EquipedItem(string slot)
        {
            if (EquipSlots == null || !EquipSlots.TryGetValue(slot, out var equipSlot))
            {
                Debug.LogError($"Slot {slot} not found in {gameObject.name}");
                return null;
            }
            Debug.Log($"Item {equipSlot.equippedItemType} in slot {slot}");
            return equipSlot.equippedItemType as IEquipable;
        }
        public bool HasSlot(string slot)
        {
            if (EquipSlots != null && EquipSlots.ContainsKey(slot))
            {
                return true;
            }
            return false;
        }

        public bool CanEquipItem(string slot, IEquipable item)
        {

            if (EquipSlots == null || !EquipSlots.TryGetValue(slot, out var equipSlot))
            {
                Debug.LogError($"Slot {slot} not found in {gameObject.name}");
                return false;
            }

            return equipSlot.SlotType == item.SlotType && item.EvaluateStatRequirement(_statsManager);
        }
        public void EquipItem(string slot, IEquipable item)
        {
            if (EquipSlots == null)
            {
                Debug.LogWarning("EquipSlots is null");
                return;
            }
            if (!EquipSlots.TryGetValue(slot, out var equipSlot))
            {
                Debug.LogError($"Slot {slot} not found in {gameObject.name}");
                return;
            }

            if (equipSlot.SlotType == item.SlotType && item.EvaluateStatRequirement(_statsManager))
            {
                equipSlot.EquipItem(item);
                return;
            }   
        }

        public void Initialize()
        {
            if ( _currentEquipDefinition != EquipDefinition)
            {
                Debug.Assert(EquipDefinition != null, $"EquipDefinition is null in {gameObject.name}");

                EquipSlots = new();
                foreach (var slot in EquipDefinition.Slots)
                {
                    EquipSlots.Add(slot.Name, new EquipSlot(slot));
                }
            }
            _statsManager = GetComponent<StatsManager>();
        
        }
    }

    [Serializable]
    public enum EquipSlotType
    {
        Head,
        Chest,
        Legs,
        Feet,
        Hands,
        WeaponOrTool,
        Shield
    }
}
