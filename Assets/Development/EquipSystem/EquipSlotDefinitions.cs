using System.Collections.Generic;
using UnityEngine;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "EquipSystem/EquipSlotDefinitions", fileName = "EquipSlotDefinitions")]
    public class EquipSlotDefinitions : ScriptableObject
    {
        [field:SerializeField] public List<EquipSlotDefinition> Slots { get; private set; }
    }

    [System.Serializable]
    public class EquipSlotDefinition
    {
        public EquipSlotType EquipSlotType;
        public string Name;
    }

}
