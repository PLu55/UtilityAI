namespace PLu.Dev.AI
{
    public interface IEquipable 
    {
        string Name { get; }
        EquipSlotType SlotType { get; }
        bool EvaluateStatRequirement(StatsManager statsManager);
    }
}
