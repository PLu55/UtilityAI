using System;
using UnityEngine;
using PLu.InventorySystem;

namespace PLu.Dev.AI
{
    [Serializable]
    public class EquipSlot
    {
        public string Name { get; private set; }
        [field:SerializeField] public EquipSlotType SlotType { get; private set; }

        [field:SerializeField] public ItemType equippedItemType { get; set; }

        public event System.Action<EquipSlot, IEquipable> OnEquipSlotChanged = delegate { };

        public EquipSlot(EquipSlotDefinition slotDefinition)
        {
            Name = slotDefinition.Name;
            SlotType = slotDefinition.EquipSlotType;
        }
        public EquipSlot(string name, IEquipable equipable)
        {
            Name = name;
            equippedItemType = equipable as ItemType;
        }

        public void EquipItem(IEquipable equipable)
        {
            if (equipable is ItemType == false)
            {
                Debug.LogError($"Can't coerse {equipable} to Item {Name}");
                return;
            }
            equippedItemType = equipable as ItemType;
            Debug.Log($"Equiped {equippedItemType}:{equipable} in {Name}");
            OnEquipSlotChanged.Invoke(this, equipable);
        }
    }
}
