using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PLu.FacilitySystem
{
    public interface ISensor
    {
        void Sense();
    }

    public abstract class Sensor : MonoBehaviour, ISensor
    {
        public abstract void Sense();
    }

    [RequireComponent(typeof(SphereCollider))]
    public class FacilitySensor : Sensor
    {
        [SerializeField] private string _tagToSense = "YourTag"; // The tag of the GameObjects to sense

        private List<GameObject> _sensedObjects = new List<GameObject>();

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(_tagToSense))
            {
                _sensedObjects.Add(other.gameObject);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag(_tagToSense))
            {
                _sensedObjects.Remove(other.gameObject);
            }
        }

        public List<GameObject> GetSensedObjects()
        {
            return _sensedObjects;
        }

        public override void Sense()
        {
            // You can implement additional sensing logic here
            Debug.Log("Sensing...");
        }
    }
}