using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.FacilitySystem
{
    [RequireComponent(typeof(SphereCollider))]
    public class Facility : MonoBehaviour
    {
        [SerializeField] private string _type;
        public string Type => _type;
        void Awake()
        {
            tag = "Facility";
        } 
        void Start()
        {
            
        }
        void Update()
        {
            
        }
    }
}
