using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.FacilitySystem
{
    public class FacilityManager : MonoBehaviour
    {
        private static FacilityManager _instance;
        public static FacilityManager Instance => _instance;

        [SerializeField] private List<string> ResourceTypes;
        private Dictionary<string, List<Facility>> _facilities;
        
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                // DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            _facilities= new Dictionary<string, List<Facility>>();
            FindAllFacilities();        
        }
        private void FindAllFacilities()
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("facility");
            Debug.LogFormat("ResourceManager found {0} objects with the resource tag", gameObjects.Length);
            foreach (GameObject gameObject in gameObjects)
            {
                var facility = gameObject.GetComponent<Facility>();
                Debug.LogFormat("Facility Manager found: {0} {1}", facility, facility.Type);
                AddFacility(facility);
            }
        }
        void AddFacility(Facility facility)
        {
            if (! _facilities.ContainsKey(facility.Type))
            {
                _facilities.Add(facility.Type, new List<Facility>());
            }
            _facilities[facility.Type].Add(facility); 
        }        
    }
}
