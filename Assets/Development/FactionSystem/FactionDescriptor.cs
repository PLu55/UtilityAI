using System;
using System.Collections.Generic;

namespace PLu.FactionSystem
{

    [Serializable]
    public struct Relation
    {
        public string FactionName;
        public float Value;

        public Relation(string factionName, float value)
        {
            FactionName = factionName;
            Value = value;
        }
    }

    [Serializable]
    public class FactionsData
    {
        public string Name;
        public string Description;

        public List<Relation> Relations ;
        //public SerializableDictionary<string, float> Relations;

        public FactionsData()
        {
            Relations = new ();
        }
    }

    [Serializable]
    public class FactionDescriptor
    {
        public string Name;
        public string Description;

        public List<Relation> Relations ;
        //public SerializableDictionary<string, float> Relations;

        public FactionDescriptor()
        {
            Relations = new ();
        }
    }
    [Serializable]
    public class FactionDescriptions
    {
        public List<FactionDescriptor> Factions;

        FactionDescriptions()
        {
            Factions = new List<FactionDescriptor>();
        }
    }
}

// JSON file example:

// {
//   "Factions": [
//     {
//       "Name": "ExampleFactionName",
//       "Description": "This is an example description of a faction.",
//       "Relations": [
//         {
//           "FactionName": "FactionA",
//           "Status": "Friendly",
//           "Value": 0.75
//         },
//         {
//           "FactionName": "FactionB",
//           "Status": "Hostile",
//           "Value": -0.5
//         }
//       ]
//     },
//     {
//       "Name": "AnotherFactionName",
//       "Description": "This is another example of a faction description.",
//       "Relations": [
//         {
//           "FactionName": "FactionC",
//           "Status": "Neutral",
//           "Value": 0.0
//         }
//       ]
//     }
//   ]
// }