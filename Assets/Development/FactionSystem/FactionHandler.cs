using UnityEngine;
using Zenject;
using PLu.Utilities;
using PLu.ModularAI;

namespace PLu.FactionSystem
{
    public class FactionHandler : MonoBehaviour
    {
        [field:SerializeField] private string factionName;

        [Inject]
        private FactionManager factionManager;
        private Faction _faction;
        private IAIActor _actor;


        private void Awake()
        {
            Debug.Assert(factionManager != null, "FactionManager is not injected.");

            if (!factionManager.TryGetFaction(factionName, out _faction))
            {
                Debug.LogError($"Faction {factionName} do not exists.");
            }

            _actor = gameObject.GetComponent<IAIActor>();
            Debug.Assert(_actor != null, $"Actor {gameObject.name} do not have a IAIActor component.");
        }

        private void Start()            
        {            
            Debug.Assert(_actor.ActorBlackboard != null, $"Actor {gameObject.name} actorBlackboard is null.");
            Debug.Assert(_faction != null, $"Actor {gameObject.name} faction not set.");
            Debug.Assert(_faction.Relations != null, $"Actor {gameObject.name} faction relations not set.");
            
            _actor.ActorBlackboard.SetValue("Faction", _faction);
            _actor.ActorBlackboard.SetValue("Relations", _faction.Relations);
        }

    }
}