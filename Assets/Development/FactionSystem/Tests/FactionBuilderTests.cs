using NUnit.Framework;
using PLu.Utilities;

using PLu.FactionSystem;

namespace PLu.FactionSystem.Tests
{
    [TestFixture]
    public class FactionBuilderTests
    {
        [Test]
        public void BuilderTests()
        {
            FactionManager factionManager = new();
            FactionBuilderC factionBuilder = new(factionManager);
            factionBuilder.LoadFactionDescriptions("FactionTestData");
            factionBuilder.CreateFactions();

            Assert.IsTrue(factionManager.TryGetFaction(HashedStringRegistry.Instance.GetOrRegister("Us"), out Faction factionUs));
            Assert.IsTrue(factionManager.TryGetFaction(HashedStringRegistry.Instance.GetOrRegister("Them"), out Faction factionThem));
            Assert.AreEqual(-0.9f, factionManager.GetRelation(factionUs, factionThem));
            Assert.AreEqual(-1.0f, factionManager.GetRelation(factionThem, factionUs));
        }
    }
}

