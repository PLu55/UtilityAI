
using System.Runtime.InteropServices;
using NUnit.Framework;
using PLu.FactionSystem;
using PLu.Utilities;


namespace PLu.FactionSystem.Tests
{
    [TestFixture]
    public class FactionSystemTests
    {
        [Test]
        public void FactionManagerTests()
        {
            FactionManager factionManager = new FactionManager();

            HashedString us = factionManager.CreateOrGetFaction("Us", "Us are our friends");
            HashedString them = factionManager.CreateOrGetFaction("Them", "Them are our enemies");
            HashedString us2 = factionManager.CreateOrGetFaction("Us", "Us are our friends");
            
            Assert.AreEqual(us, us2);

            Faction factionUs;
            Faction factionThem;

            Assert.IsTrue(factionManager.TryGetFaction(us, out factionUs));
            Assert.AreEqual(us, factionUs.Name);
            Assert.IsTrue(factionManager.TryGetFaction(them, out factionThem));
            Assert.AreEqual(them, factionThem.Name);

            float relation;
            Assert.IsFalse(factionManager.TryGetRelation(factionUs, factionThem, out relation));
            factionManager.SetRelation(factionUs, factionThem, 0.0f);
            Assert.IsTrue(factionManager.TryGetRelation(factionUs, factionThem, out relation));
            Assert.AreEqual(0.0f, relation);
            Assert.IsTrue(factionManager.TryGetRelation(factionThem, factionUs, out relation));
            Assert.AreEqual(0.0f, relation);

            factionManager.SetRelation(factionUs, factionThem, -1.0f, false);
            Assert.IsTrue(factionManager.TryGetRelation(factionUs, factionThem, out relation));
            Assert.AreEqual(-1.0f, relation);
            Assert.IsTrue(factionManager.TryGetRelation(factionThem, factionUs, out relation));
            Assert.AreEqual(0.0f, relation);
            
            factionManager.SetRelation(factionUs, factionThem, 0.5f, value2: -0.5f);
            Assert.IsTrue(factionManager.TryGetRelation(factionUs, factionThem, out relation));
            Assert.AreEqual(0.5f, relation);
            Assert.IsTrue(factionManager.TryGetRelation(factionThem, factionUs, out relation));
            Assert.AreEqual(-0.5f, relation);}
    }    
}

