using System.Collections;
using System.Collections.Generic;
using PLu.Utilities;
using UnityEngine;

namespace PLu.FactionSystem
{
    // FactionManager is a class that manages factions and their relations. 
    // It's supose to be a Singleton but it's not implemented as such because
    // Zenject will handle the Singleton pattern.
    public class FactionManager
    {
        private readonly Dictionary<HashedString, Faction> _factions;

        public FactionManager() : base()
        {
            _factions = new();
        }
        public HashedString CreateOrGetFaction(string name, string description = "")
        {
            HashedString hashedName = HashedStringRegistry.Instance.GetOrRegister(name);
            if (!_factions.ContainsKey(hashedName))
            {
                Faction faction = new Faction(name, description);
                _factions.Add(hashedName, faction);
            }

            return hashedName;
        }
        public bool TryGetFaction(HashedString name, out Faction faction)
        {
            return _factions.TryGetValue(name, out faction);
        }
        
        public bool TryGetRelation(Faction faction1, Faction faction2, out float relation)
        {
            return faction1.TryGetRelation(faction2, out relation);
        }
        public float GetRelation(Faction faction1, Faction faction2)
        {
            return faction1.GetRelation(faction2);
        }

        public void SetRelation(Faction faction1, Faction faction2, float value1, bool bidirectional = true, float value2 = -float.MaxValue)
        {
            if (value2 == -float.MaxValue)
            {
                value2 = value1;
            }

            value1 = Mathf.Clamp(value1, -1, 1);
            faction1.SetRelation(faction2, value1);

            if (bidirectional)
            {                      
                value2 = Mathf.Clamp(value2, -1, 1);
                faction2.SetRelation(faction1, value2);
            }
        }

    }
}