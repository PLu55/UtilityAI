using Zenject;
using PLu.FactionSystem;

namespace PLu.Dev.AI
{
    public class FactionManagerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<FactionManager>().AsSingle().NonLazy();
            Container.Bind<FactionBuilderC>().AsSingle().NonLazy();
        }
    }
}
