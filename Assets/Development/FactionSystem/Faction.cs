using System.Collections.Generic;
using PLu.ModularAI;
using PLu.Utilities;
using UnityEngine;

namespace PLu.FactionSystem
{
    public class Faction : IFaction
    {
        public HashedString Name { get; private set; }
        public string Description { get; private set; }
        public IAIBlackboard Relations { get; set; }

        public Faction(string name, string description)
        {
            Name = HashedStringRegistry.Instance.GetOrRegister(name);
            Description = description;            
            Relations = new AIBlackboard();
        }

        public void AddRelation(Faction faction, float relation = 0f)
        {
            Relations.SetValue(faction.Name, Mathf.Clamp(relation, -1f, 1f));
        }

        public void SetRelation(Faction faction, float relation)
        {
            Relations.SetValue(faction.Name, Mathf.Clamp(relation, -1f, 1f));
        }

        public void RemoveRelation(Faction faction)
        {
            Relations.RemoveValue<float>(faction.Name);
        }
        public bool TryGetRelation(Faction faction, out float relation)
        {
            return Relations.TryGetValue(faction.Name, out relation);
        }
        public float GetRelation(Faction faction)
        {
            return TryGetRelation(faction, out float relation) ? relation : -float.MaxValue;
        }

    }

    public interface IFaction
    {
        HashedString Name { get; }
        string Description { get; }
        IAIBlackboard Relations { get; }

        void AddRelation(Faction faction, float relation = 0f);
        void SetRelation(Faction faction, float relation);
        void RemoveRelation(Faction faction);
        bool TryGetRelation(Faction faction, out float relation);
        float GetRelation(Faction faction);
    }
}