using UnityEngine;
using Zenject;
using PLu.Utilities;


namespace PLu.FactionSystem
{
    public class FactionBuilder : MonoBehaviour
    {
        FactionBuilderC _factionBuilder;

        [Inject]
        private FactionManager _factionManager;

        void Awake()
        {
            _factionBuilder = new FactionBuilderC(_factionManager);

        }
    }
    public class FactionBuilderC 
    {
        FactionDescriptions factionDescriptions;
        private FactionManager _factionManager;

        public FactionBuilderC(FactionManager factionManager)
        {
            _factionManager = factionManager;            
            LoadFactionDescriptions();
            CreateFactions();
        }
        
        public void LoadFactionDescriptions(string fileName = "FactionData")
        {
            TextAsset jsonFile = Resources.Load<TextAsset>(fileName);
            if (jsonFile != null)
            {
                factionDescriptions = JsonUtility.FromJson<FactionDescriptions>(jsonFile.text);
                // Now you can use factionDescriptions as needed
                Debug.Log($"Loaded Faction Descriptions from {fileName}");
            }
            else
            {
                Debug.LogError("Failed to load JSON file.");
            }
        }

        public void CreateFactions()
        {
            foreach (FactionDescriptor factionDescription in factionDescriptions.Factions)
            {
                _factionManager.CreateOrGetFaction(factionDescription.Name, factionDescription.Description);
            }
            foreach (FactionDescriptor factionDescription in factionDescriptions.Factions)
            {
                if (!_factionManager.TryGetFaction(HashedStringRegistry.Instance.GetOrRegister(factionDescription.Name), out Faction faction1))
                {
                    continue;
                }

                foreach (Relation relation in factionDescription.Relations)
                {
                    if (_factionManager.TryGetFaction(HashedStringRegistry.Instance.GetOrRegister(relation.FactionName), out Faction faction2))
                    {
                        _factionManager.SetRelation(faction1, faction2, relation.Value, false);
                    }
                }
            }
        }
    }
}
