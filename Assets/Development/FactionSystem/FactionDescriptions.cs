using System;
using System.Collections.Generic;
using UnityEngine;
using PLu.Utilities;

namespace PLu.FactionSystem.Dev
{
    [Serializable]
    public class FactionDescriptor
    {
        public string Name;
        public string Description;
        public SerializableDictionary<string, float> Relations;

        public FactionDescriptor()
        {
            Relations = new ();
        }
    }
    [CreateAssetMenu(fileName = "Faction", menuName = "Faction System/FactionDescriptions")]
    public class FactionDescriptions : ScriptableObject
    {
        public List<FactionDescriptor> Descriptions;
    }

}