using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.KDTree;

namespace PLu
{
    public class InfluenceMap
    {
        KDTree<InfluenceMapNode> _influenceMap;

        public InfluenceMap(int dimensions)
        {
            //_influenceMap = new KDTree<InfluenceMapNode>(dimensions, new InfluenceMapNodeFactory());
        }
        public class InfluenceMapNode
        {
            public Vector3 Position { get; private set; }
            public float Value { get; private set; }
            public InfluenceMapNode(Vector3 position, float value)
            {
                Position = position;
                Value = value;
            }
        }
        public class InfluenceMapNodeFactory : KDNodeFactory<InfluenceMapNode>
        {
            public override KDNode<InfluenceMapNode> CreateNode(InfluenceMapNode value)
            {
                return new InfluenceMapKDTreeNode(value);
            }
        }
        public class InfluenceMapKDTreeNode : KDNode<InfluenceMapNode>
        {
            public override float this[int axis] => Position[axis];

            public override Vector3 Position => Value.Position;

            public InfluenceMapKDTreeNode(InfluenceMapNode value) : base(value)
            {
            }

        }
        // private KDTree.KDTree<InfluenceMapNode> _influenceMap;
        // public InfluenceMap(InfluenceMapNode[] nodes, int dimensions)
        // {
        //     _influenceMap = new KDTree.KDTree<InfluenceMapNode>(nodes, dimensions, new InfluenceMapNodeFactory());
        // }
        // public InfluenceMapNode[] GetNodes()
        // {
        //     return _influenceMap.GetNodes();
        // }
        // public InfluenceMapNode FindMedian(InfluenceMapNode[] values, int axis)
        // {
        //     return _influenceMap.FindMedian(values, axis);
        // }
        // public void GetBalanceFactors(out int maxImbalance, out double averageImbalance)
        // {
        //     _influenceMap.GetBalanceFactors(out maxImbalance, out averageImbalance);
        // }
    }
}
