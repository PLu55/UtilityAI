using System.Collections;
using UnityEngine;
using UnityEngine.UI;
 
 // https://www.youtube.com/watch?v=6U_OZkFtyxY better
 // https://www.youtube.com/watch?v=_lREXfAMUcE camera follow
 // https://www.youtube.com/watch?v=6gAmI5fOELY
namespace PLu.Dev.AI
{
    public class StatusBar : MonoBehaviour
    {
        [SerializeField] private Image _barSprite;
        [SerializeField] private float _updateSpeed = 0.2f;

        private Camera _camera;

        [Range(0, 1)]
        public float Value;
 
        void Awake()
        {
          }
        void Start()
        {
            _camera = Camera.main;
            Stat healthStat = GetComponentInParent<StatsManager>().GetStat(StatType.Health);
            healthStat.OnStatChanged += UpdateStatusBar;

        }
        void Update()
        {   
            transform.rotation = Quaternion.LookRotation(transform.position - _camera.transform.position);
        }
        public void UpdateStatusBar(float value, float maxValue)
        {
            Value = Mathf.Clamp01(value / maxValue);
            StartCoroutine(ChangeValue(Value));
        }


        private IEnumerator ChangeValue(float value)
        {
            float preChangeValue = _barSprite.fillAmount;
            float elapsed = 0f;
            while (elapsed < _updateSpeed)
            {
                elapsed += Time.deltaTime;
                _barSprite.fillAmount = Mathf.Lerp(preChangeValue, value, elapsed / _updateSpeed);
                yield return null;
            }
            _barSprite.fillAmount = value;
        }
    }


}
