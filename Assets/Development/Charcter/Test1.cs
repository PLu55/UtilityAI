using System.Collections;
using System.Collections.Generic;
using PLu.Dev.AI;
using UnityEngine;
 
namespace PLu
{
    public class Test1 : MonoBehaviour
    {
        public EquipManager EquipManager { get; private set; }

        void Awake()
        {
            EquipManager = GetComponent<EquipManager>();
            Debug.Assert(EquipManager != null, $"EquipManager is null in {gameObject.name}");
        }
        
        void Start()
        {

            
        }
    }
}
