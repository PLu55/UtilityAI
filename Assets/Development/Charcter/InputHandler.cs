using System;
using UnityEngine;
 
namespace PLu
{
    public class InputHandler : MonoBehaviour
    {
        public event Action<int> OnAbilityKeyDown = delegate { };
        private int _abilityIndex;
        void Update()
        {
            _abilityIndex = -1;
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                _abilityIndex = 0;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                _abilityIndex = 1;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                _abilityIndex = 2;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                _abilityIndex = 3;
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                _abilityIndex = 4;
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                _abilityIndex = 5;
            }
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                _abilityIndex = 6;
            }
            if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                _abilityIndex = 7;
            }
            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                _abilityIndex = 8;
            }
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                _abilityIndex = 9;
            }
            if (_abilityIndex != -1)
            {
                OnAbilityKeyDown.Invoke(_abilityIndex);
            }
        }
    }
}
