using System.Collections;
using System.Collections.Generic;
using PLu.Dev.AI;
using UnityEngine;
 
namespace PLu
{
    public class BufHealth : StatModifier
    {
        public BufHealth(float duration) : base(duration)
        {
        }

        public override void Handle(object sender, Query query)
        {
            if (query.StatType != StatType.Health) { return; }
            query.Value += 100f;
        }


    }
}
