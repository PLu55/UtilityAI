using System;
using System.Collections.Generic;
using UnityEngine;
using PLu.AbilitySystem;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;
using Zenject;
using PLu.Utilities;

namespace PLu.Dev.AI
{
    [DefaultExecutionOrder(-100)] // Before Sensors
    [RequireComponent(typeof(StatsManager))]
    public class Character : MonoBehaviour, IDamageable, IDamageDealer
    {
        [SerializeField] private Ability[] _abilities;
        [SerializeField] private float _isUnderAttackCoolDown;
        public Ability[] Abilities => _abilities;
        public StatsManager StatsManager { get; private set; }

        public event System.Action OnDeath = delegate { };

        public IContext Context => _context;

        private IContext _context;
        private Character _target;

        [Inject]
        private BlackboardManager _blackboardManager;
        private Blackboard _privateBlackboard;

        private const string _targetsInProximityString = "TargetsInProximity";
        private HashedString _targetsInProximityKey;
        private const string _damageDealersString = "DamageDealers";
        private HashedString _damageDealersKey;        
        private const string _isUnderAttackString = "IsUnderAttack";
        private HashedString _isUnderAttackKey;
        private Dictionary<GameObject, TargetData> _targets;
        private HashSet<IDamageDealer> _damageDealers;

        private CountdownTimer _isUnderAttackTimer;

        void Awake()
        {
            _context = GetComponent<IContext>();
            Debug.Assert(_context != null, $"No IContext component found on character: {gameObject.name}");
            SetupBlackboards();
            SetupTimers();
        }

        private void SetupStats()
        {
            StatsManager = GetComponent<StatsManager>();
            Debug.Assert(StatsManager != null, $"No StatsManager component found on character: {gameObject.name}");
            StatsManager.GetStat(StatType.Health).OnStatChanged += CheckForDeath;
            OnDeath += OnMyDeath;
        }

        void Start()
        {
            SetupStats();
            SetupAbilities();
        }
        private void SetupBlackboards()
        {
            _privateBlackboard = _blackboardManager.GetPrivateBlackboard(gameObject);
            Debug.Assert(_privateBlackboard != null, $"No Private Blackboard found for character: {gameObject.name}");

            _targetsInProximityKey = _blackboardManager.GetOrRegisterKey(_targetsInProximityString);
            _damageDealersKey = _blackboardManager.GetOrRegisterKey(_damageDealersString);
            _isUnderAttackKey = _blackboardManager.GetOrRegisterKey(_isUnderAttackString);
            _privateBlackboard.SetValue(_isUnderAttackKey, false);
            _targets = new();
            _damageDealers = new();
            _privateBlackboard.SetValue(_targetsInProximityKey, _targets);
            _privateBlackboard.SetValue(_damageDealersKey, _damageDealers);

        }
        private void SetupTimers()
        {
            _isUnderAttackTimer = new CountdownTimer(_isUnderAttackCoolDown);
            _isUnderAttackTimer.OnTimerStop += ClearAttackState;

        }
        private void SetupAbilities()
        {
            if (_abilities == null)
            {
                Debug.LogWarning($"No abilities found on character: {gameObject.name}");
                return;
            }
            if (Context == null)
            {
                return;
            }
            foreach (var ability in Abilities)
            {
                ability.Initialize(Context);
            }
        }

        private Character SelectTarget()
        {
            foreach (var target in _targets.Keys)
            {
                    return target.GetComponent<Character>();
            } 
            return null;
        }
   
        public void ActivateAbility(int abilityIndex)
        {
            if (abilityIndex != -1 && abilityIndex < _abilities.Length && _target != null)
            {
                Abilities[abilityIndex].Activate(this, _target);
            }
        }

        private void CheckForDeath(float value, float maxValue)
        {
            Debug.Log($"CheckForDeath Health: {value}/{maxValue}");
            if (value <= 0f)
            {
                OnDeath.Invoke();
            }
        }
        private void OnMyDeath()
        {
            Debug.Log("Character died");
            Destroy(gameObject);
        }

        // TODO: refactor this into a separate class
        public void TakeDamage(IDamageDealer damageDealer, DamageType damageType, float damage)
        {
            if (damageDealer is Character character)
            {
                if (_damageDealers.Add(damageDealer) || !_isUnderAttackTimer.IsRunning)
                {
                    _privateBlackboard.SetValue(_isUnderAttackKey, true);
                }
                _isUnderAttackTimer.Start();
            }

            StatsManager.ModifyValue(StatType.Health, -damage);
        }

        private void ClearAttackState()
        {
            _privateBlackboard.SetValue(_isUnderAttackKey, false);
            _damageDealers.Clear();

            return;
        }
    }

    public interface IDamageDealer
    {
    }
}
