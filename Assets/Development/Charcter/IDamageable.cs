using UnityEngine;

namespace PLu.Dev.AI
{
    public interface IDamageable
    {
        void TakeDamage(IDamageDealer damageDealer, DamageType damageType, float damage);
    }
}
