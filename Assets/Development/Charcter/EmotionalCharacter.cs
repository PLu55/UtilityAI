using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using PLu.EmotionSystem;
namespace PLu.CharacterSystem
{
    [CreateAssetMenu(fileName = "PersonalGoal", menuName = "Character/PersonalGoal")]
    public class Desire : ScriptableObject
    {
        [SerializeField] private float _importance; // > 0
        [SerializeField] private float _satisfaction; // -1 to 1
        [SerializeField] private float _valence; // -1 to 1
        [SerializeField] private float _arousel; // -1 to 1

        public float Importance => _importance;
        public float Satisfaction => _satisfaction;
        public float Valence => _valence;
        public float Arousel => _arousel;
        public void AdjustSatisfaction(float amount)
        {
            _satisfaction = Mathf.Clamp(_satisfaction + amount, -1f, 1f);
        }
    }
    // Activities, Unmet Needs, Stimulants, Frustrations, Threats achived goals increase arousal 

    // Positive events increase valence, negative events decrease valence
    // Social interactions increase valence, social rejection decrease valence
    // Comfortable environment increase valence, uncomfortable environment //decrease valence
    // Satisfied Needs increase valence, unsatisfied needs decrease valence
    public class EmotionalCharacter : MonoBehaviour
    {
        [SerializeField] private List<Desire> _desires;
        [SerializeField] private Emotion _emotion;
        private Dictionary<string, float> _relations;

        public List<Desire> Desires => _desires;
        void Awake()
        {
            _relations = new Dictionary<string, float>();
            _emotion = new Emotion(0f, 0f);
        }
        public void ComputeEmotionalState()
        {
            float satisfaction = GetSatisfaction();
            _emotion.AdjustEmotion(satisfaction, 0f);
        }
        public void AddDesire(Desire desire)
        {
            _desires.Add(desire);
        }

        public void RemoveDesire(Desire desire)
        {
            _desires.Remove(desire);
        }

        public void AdjustEmotion(float valenceChange, float arousalChange)
        {
            _emotion.AdjustEmotion(valenceChange, arousalChange);
        }

        public void AdjustDesireSatisfaction(Desire desire, float amount)
        {
            desire.AdjustSatisfaction(amount);
        }
        public float GetSatisfaction()
        {
            float satisfaction = 0;
            foreach (var desire in _desires)
            {
                satisfaction += desire.Satisfaction;
            }
            return satisfaction / _desires.Count;
        }
    }
}

