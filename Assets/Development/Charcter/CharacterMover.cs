using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace PLu
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class CharacterMover : MonoBehaviour
    {
        [SerializeField] private float _speed = 5.0f;
        private NavMeshAgent _navMeshAgent;
        public NavMeshAgent NavMeshAgent => _navMeshAgent;
        void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            Debug.Assert(_navMeshAgent != null, "No NavMeshAgent component found on agent");
        }

        public bool MoveTo(Vector3 destination)
        {
            bool result = _navMeshAgent.SetDestination(destination);
            _navMeshAgent.isStopped = false;
            _navMeshAgent.speed = _speed;
            return result;
        }

        public void Stop()
        {
            _navMeshAgent.isStopped = true;
        }
    }
}
