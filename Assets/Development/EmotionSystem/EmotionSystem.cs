using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace PLu.EmotionSystem
{
    public  enum EmotionType
    {
        Neutral,
        Pleased,
        Enthusiastic,
        Excited,
        Aroused,
        Frenzied,
        Upset,
        Unhappy,
        Sad,
        Tired,
        Quiet,
        Tranquil,
        Peaceful
    }
    // Pleased - Unhappy
    // Enthusiastic - Sad
    // Excited - Tired
    // Aroused - Quiet
    // Frenzied - Tranquil
    // Upset - Peaceful

    public class Emotion
    {
        public float Valence { get; set; } // Pleasant-Unpleasant
        public float Arousal { get; set; } // High-Low

        public Emotion(float valence, float arousal)
        {
            Valence = valence;
            Arousal = arousal;
        }

        public void AdjustEmotion(float valenceChange, float arousalChange)
        {
            Valence = Mathf.Clamp(Valence + valenceChange, -1f, 1f);
            Arousal = Mathf.Clamp(Arousal + arousalChange, -1f, 1f);
        }
        public float GetDistance(Emotion other)
        {
            return Mathf.Sqrt(Mathf.Pow(Valence - other.Valence, 2) + Mathf.Pow(Arousal - other.Arousal, 2));
        }
        public float GetAngle()
        {
            return Mathf.Atan2(Arousal, Valence);
        }
        public float GetStrength()
        {
            return Mathf.Sqrt(Arousal * Arousal + Valence * Valence);
        }

        public EmotionType GetEmotionType()
        {
            float strength = GetStrength();
            if (strength < 0.1f)
            {
                return EmotionType.Neutral;
            }
            
            return (EmotionType) (GetIndex() + 1);
        }
        private int GetIndex()
        {
            float angle = GetAngle() / Mathf.PI + 1f / 12f;
            angle = angle >= 0f ? angle * 6f : 12f + angle * 6f;
            return (int)angle;
        }

    }
}

