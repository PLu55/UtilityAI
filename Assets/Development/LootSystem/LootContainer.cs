using System.Collections;
using System.Collections.Generic;
using PLu.InventorySystem;
using PLu.LootSystem;
using UnityEngine;
using PLu.Utilities;
using static GameObjectExtensions;

namespace PLu.LootSystem
{
    public class LootContainer : MonoBehaviour, IVisitable
    {
        public Inventory Inventory { get; private set; }

        void Awake()
        {
            Inventory = gameObject.GetOrAdd<Inventory>();
            Debug.Assert(Inventory != null, $"Inventory is null in {gameObject.name}");
        }   

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
            Destroy(gameObject);
        }
    }
}
