
using UnityEngine;
using PLu.InventorySystem;

namespace PLu.LootSystem
{
    public class LootCollector : MonoBehaviour, IVisitor
    {
        private IInventory _inventory;
        void Awake()
        {
            _inventory = GetComponent<IInventory>();
            Debug.Assert(_inventory != null, $"Inventory is null in {gameObject.name}");
        }
        public void Visit(IVisitable visitable)
        {
            if (visitable is LootContainer lootContainer)
            {
                foreach (var itemContainer in lootContainer.Inventory.RemoveAllItems())
                {
                    if (itemContainer != null)
                    {
                        _inventory.TryAdd(itemContainer);
                        // TODO: fix this: Debug.Log($"LootCollector collected {item.Quantity} {item.Name}");
                        Debug.Log($"LootCollector collected {1} {itemContainer.Name}");
                    }
                }
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IVisitable visitable))
            {
                visitable.Accept(this);
            }
        }   
    }
}