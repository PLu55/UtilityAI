namespace PLu.LootSystem
{
    public interface IVisitable
    {
        void Accept(IVisitor visitor);
    }
}
