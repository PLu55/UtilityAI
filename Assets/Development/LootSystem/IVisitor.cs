namespace PLu.LootSystem
{

    public interface IVisitor
    {
        void Visit(IVisitable visitable);
    }
}
