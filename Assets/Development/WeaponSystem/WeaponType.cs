using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.InventorySystem;
using System;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(fileName = "New Weapon", menuName = "Resources/Weapon")]
    public class WeaponType : ItemType, IEquipable
    {
        [field:SerializeField] public StatRequirement[] StatRequirement { get; private set; } 
        [field:SerializeField] public float Damage { get; private set; }
        [field:SerializeField] public DamageType DamageType { get; private set; }
        [field:SerializeField] public float Range { get; private set; }
        [field:SerializeField] public float PreDuration { get; private set; }
        [field:SerializeField] public float PostDuration { get; private set; }
        [field:SerializeField] public float Durability { get; private set; }
        [field:SerializeField] public float MaxDurability { get; private set; }

        public EquipSlotType SlotType => EquipSlotType.WeaponOrTool;

        public void ModifyDurability(float amount)
        {
            Durability += amount;
            Durability = Mathf.Clamp(Durability, 0f, MaxDurability);
        }
        public void SetMaxDurability(float amount)
        {
            MaxDurability = amount;
            MaxDurability = Mathf.Clamp(MaxDurability, 0f, float.MaxValue);
            Durability = Mathf.Clamp(Durability, 0f, MaxDurability);
        }

        public bool EvaluateStatRequirement(StatsManager statsManager)
        {
            Debug.Log("Evaluating Stat Requirement");
            foreach (var stat in StatRequirement)
            {
                if (!statsManager.TryGetStatValue(stat.StatType, out var value))
                {
                    return false;
                }

                if (value >= stat.Value)
                {
                    return true;
                }   

                return false;
            }
            return true;
        }
    }
    [Serializable]
    public class StatRequirement
    {
        public StatType StatType;
        public float Value;
    }

    public interface IWeaponVisitor
    {
        void Visit(WeaponType weapon);
    }

    public interface IWeaponVisitable
    {
        void Accept(IWeaponVisitor visitor);
    }
}
