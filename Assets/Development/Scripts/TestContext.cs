using PLu.UtilityAI;
using PLu.BlackboardSystem;
using PLu.Dev.AI;
using PLu.UtilityAI.Core;

namespace PLu.Dev
{
    public class TestContext : IContext
    {
        public UtilityAgent Owner { get; }
        public Blackboard PrivateBlackboard { get; }

        public ISensor[] Sensors => throw new System.NotImplementedException();

        public TestContext(UtilityAgent agent)
        {
            Owner = agent;
            PrivateBlackboard = new Blackboard();
        }

    }
}