using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using PLu.BlackboardSystem;
using PLu.UtilityAI.Core;
using PLu.BehaviourTrees;
using PLu.ResourceSystem;
using PLu.InventorySystem;

namespace PLu.Dev
{
    [RequireComponent(typeof(Inventory))]
    public class TestAgent : UtilityAgent
    {
        private TestContext _context;
        private BehaviourTree _behaviourTree;
        private Blackboard _blackboard;
        private NavMeshAgent _navMeshAgent;
        //private ISensor[] _sensors;
        private Inventory _inventory;
        private bool _done = false;

        void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _context = new TestContext(this);
            _blackboard = _context.PrivateBlackboard;
            //_sensors = GetComponents<ISensor>();
            //foreach (ISensor sensor in _sensors)
            {

                //sensor.Initialize(_blackboard);
            }
        }
        void Start()
        {
            _inventory = GetComponent<Inventory>();
            BuildBehavior();
        }
        void Update()
        {
            ActionScoring _bestAction = DecideBestAction(_context);
            if (!_done)
            {
                Node.Status status = _behaviourTree.Process();

                if (status == Node.Status.Success)
                {
                    Debug.Log("Behaviour tree done");
                    _done = true;
                }
            }
        }

        void BuildBehavior()
        {
            _behaviourTree = new BehaviourTree("TestAgentBehaviourTree", Policies.RunUntilSuccess);
            GameObject go = GameObject.FindWithTag("Resource");
            Debug.Assert(go != null, "Resource not found");
            ResourceSource resourceSource = go.GetComponent<ResourceSource>();
            Debug.Assert(resourceSource != null, "ResourceSource not found");
            Transform target = go.transform;

            Node sequence = new Sequence("Root");
            _behaviourTree.AddChild(sequence);
            sequence.AddChild(new Leaf("Move to resource", new MoveToTarget(transform, _navMeshAgent, target)));
            sequence.AddChild(new Leaf("Extract resource", new ExtractResource(resourceSource, 1, 5, _inventory)));
        }

    }
}