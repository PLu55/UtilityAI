using System.Net;

namespace PLu.ModularAI
{
    public class AIWaitAction : AIActionBase
    {
        private double _duration;
        private double _timeStamp;


        public override bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != NodeName)
            {
                throw new AIUnexpectedNodeName($"AIWaitAction: NodeName expected {NodeName} but found {creationData.AISpecificationNode.Name}");
            }

            if (creationData.AISpecificationNode.TryGetAttribute("Duration", out string duration))
            {
                _duration = float.Parse(duration);
            }
            else
            {
                throw new AIMissingAttributeException($"AIWaitAction: Duration attribute expected but not found");
            }

            return true;
        }

        public override void Select()
        {
            AILogger.Log($"AIWaitAction: Select t: {AITimeManager.Instance.Time}");
            _timeStamp = AITimeManager.Instance.Time;
            IsDone = false;
        }

        public override bool Update()
        {

            if ( AITimeManager.Instance.Time - _timeStamp >= _duration)
            {
                IsDone = true;
            }
            AILogger.Log($"AIWaitAction: Update t: {AITimeManager.Instance.Time} IsDone: {IsDone}");
            return IsDone;
        }
    }
}