namespace PLu.ModularAI
{
    internal interface IAIAction : IAIAbstraction
    {
        IAIActor Actor { get; }
        bool IsDone { get; }
        bool Update();
        void Select();
        void Deselect();
    }
    public abstract class AIActionBase : IAIAction
    {
        public virtual string NodeName => "Action";
        public string NodeType { get; protected set; }
        public bool IsDone { get; protected set; } = true;
        public IAIActor Actor { get; protected set; }

        public abstract bool Initialize(AICreationData creationData);
        public virtual void Select() 
        {
            IsDone = false;
        }
        public virtual void Deselect() {}
        public abstract bool Update();
    }
}