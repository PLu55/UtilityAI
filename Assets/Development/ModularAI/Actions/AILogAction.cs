using System;

namespace PLu.ModularAI
{
    public class AILogAction : AIActionBase
    {
        public override string NodeName => "Action";
        public string DisplayName => "LogAction";

        private string _pattern;
        private Type _type;
        private AIDataPath _path;

        public override bool Initialize(AICreationData creationData)
        {
            Actor = creationData.Actor;
            AISpecificationNode node = creationData.AISpecificationNode;

            if (node.TryGetAttribute("Pattern", out _pattern))
            {
                AILogger.Log($"{DisplayName} Pattern: {_pattern}");
                _path = new(_pattern, Actor);

                if (_path == null)
                {
                    throw new AIUnexpectedDataPathException($"{DisplayName} failed to initialize path: {_pattern}");
                }
            }
            else
            {
                throw new AIMissingAttributeException($"{DisplayName} requires a Pattern attribute");
            }
            if (node.TryGetAttribute("Type", out string typeString))
            {
                AILogger.Log($"{DisplayName} Type: {typeString}");
                Type _type = Type.GetType(typeString);

                if (_type == null)
                {
                    throw new AITypeException($"{DisplayName} unknown type: {typeString}");
                }
            }
            else
            {
                throw new AIMissingAttributeException($"{DisplayName} requires a Type attribute");
            }

            return true;
        }
        public override bool Update()
        {
            object value;
            _path.TryGetValue(out value);
            if (value != null && Convert.ChangeType(value, _type) != null)
            {
                AILogger.Log($"[LogAction] {_pattern}: Value: {Convert.ChangeType(value, _type)};");

            }
            return true;
        }
    }
}