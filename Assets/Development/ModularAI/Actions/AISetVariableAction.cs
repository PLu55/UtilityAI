using System;
using PLu.Utilities;
using UnityEditor.Build;

namespace PLu.ModularAI
{
    // TODO: implement setting of variables indirectly where the value is a variable.
    //       A variable is something stored in a blackboard.
    //       In the SpecificationBuilder we need to distinguish  between a string and a dataPath value.
    //       Scenario: Walk through a list of percepts and add the hostile ones the EnemyBlackboard.
    public class AISetVariableAction : AIActionBase
    {
        private AIDataPath _dataPath;
        private object _value;
        private Type _type;
        private AITargetBase _target;

        public override bool Initialize(AICreationData creationData)
        {

            if (creationData.AISpecificationNode.Name != NodeName)
            {
                throw new AIUnexpectedNodeName($"AISetVariableAction: NodeName expected {NodeName} but found {creationData.AISpecificationNode.Name}");
            }
            
            if (creationData.AISpecificationNode.TryGetAttribute("DataPath", out string dataPath))
            { 
                _dataPath = new AIDataPath(dataPath, creationData.Actor);

                if (dataPath == null)
                {
                    throw new AIUnexpectedDataPathException($"AIVariableConsideration: Unexpected dataPath: {dataPath}");
                }
            }

             if (creationData.AISpecificationNode.TryGetChild("Target", out AISpecificationNode targetNode))
            {
                AICreationData target = new AICreationData(targetNode, creationData.Actor);
                _target = AITargetFactory.Instance.Create(target);
            }
            else if (creationData.AISpecificationNode.TryGetAttribute("Value", out string value))
            {
                if (!creationData.AISpecificationNode.TryGetAttribute("VariableType", out string variableTypeName))
                {
                    variableTypeName = "System.String";
                    //throw new AIMissingAttributeException($"AISetVariableAction: VariableType attribute expected but not found");
                }

                    
                _type = Type.GetType(variableTypeName);
                
                if (_type == null)
                {
                    throw new AITypeException ($"AISetVariableAction: Type {variableTypeName} not found, make sure the type is fully qualified");
                }
                
                switch (_type.Name)
                {
                    case "Int32":
                        _value = int.Parse(value);
                        break;
                    case "Double":
                        _value = double.Parse(value);
                        break;
                    case "Single":
                        _value = float.Parse(value);
                        break;
                    case "Boolean":
                        _value = bool.Parse(value);
                        break;
                    case "String":
                        _value = value;
                        break;
                    default:
                        AILogger.LogError($"AISetVariableAction: Type {_type.Name} not supported");
                        throw new AITypeException($"AISetVariableAction: Type {_type.Name} not supported");

                }
            }
            else
            {
                throw new AIMissingAttributeException($"AISetVariableAction: Target element expected but not found");
            }

            return true;
        }

        public override void Select()
        {
            if (_target != null && _target.HasEntity)
            {
                _value = _target.Entity;
            }

            _dataPath.SetValue(_value);
        }
        public override bool Update() { return true; }
    }
}