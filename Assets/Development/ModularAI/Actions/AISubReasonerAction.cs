namespace PLu.ModularAI
{
    public class AISubReasonerAction : AIActionBase
    {
        private AIReasonerBase _subReasoner;

        public override bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != NodeName ||
                creationData.AISpecificationNode.Children.Count == 0 ||
                creationData.AISpecificationNode.Children[0].Name != "Reasoner")
            {
                return false;
            }

            AISpecificationNode child = creationData.AISpecificationNode.Children[0];
            AICreationData data = new AICreationData(child, creationData.Actor);
            _subReasoner = AIReasonerFactory.Instance.Create(data);

            return true;
        }
        public override void Select()
        {
            base.Select();
        }

        public override void Deselect()
        {
            _subReasoner.Reset();
            base.Deselect();
        }
        // TODO: When should the Subreasoner get a tick?
        public override bool Update()
        {
            _subReasoner.Tick();
            IsDone = _subReasoner.IsDone;
            return IsDone;
        }
    }
}