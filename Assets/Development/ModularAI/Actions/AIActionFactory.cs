namespace PLu.ModularAI
{
    public class AIActionFactory : AIFactoryBase<AIActionBase, AIActionFactory>
    {
        public AIActionFactory() : base()
        {
            AddConstructor(new AIActionConstructor_Default());
        }
    }


    internal class AIActionConstructor_Default : AIConstructorBase<AIActionBase>
    {
        public override AIActionBase Create(AICreationData creationData)
        {
            AIActionBase obj;
            switch (creationData.AISpecificationNode.Type)
            {
                case "SubReasoner":
                    obj = creationData.ConstructObject<AISubReasonerAction>();
                    break;                
                case "SetVariable":
                    obj = creationData.ConstructObject<AISetVariableAction>();
                    break;                
                case "Wait":
                    obj = creationData.ConstructObject<AIWaitAction>();
                    break;
                case "Log":
                    obj = creationData.ConstructObject<AILogAction>();
                    break;
                default:
                    AILogger.LogError($"AIActionConstructor_Default: Unknown action type {creationData.AISpecificationNode.Type}");
                    return null;
            }
            return obj;
        }
    }
}