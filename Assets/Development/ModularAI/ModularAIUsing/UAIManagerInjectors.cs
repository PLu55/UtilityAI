#if UNITY
using Zenject;

namespace PLu.ModularAI
{
    public class UAIManagerInstallers : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<UAIManager>().AsSingle().NonLazy();
            Container.Bind<UAIAbstractionManager>().AsSingle().NonLazy();
            Container.Bind<UAISpecificationManager>().AsSingle().NonLazy();
            Container.Bind<UAITimeManager>().AsSingle().NonLazy();
            Container.Bind<UAILogger>().AsSingle().NonLazy();
            Container.Bind<AISpecifications>().AsSingle().NonLazy();
            Container.Bind<AIBlackboard>().AsTransient();
        }
    }
}
#endif