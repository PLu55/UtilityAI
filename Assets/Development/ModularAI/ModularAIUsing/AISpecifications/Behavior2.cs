

namespace PLu.ModularAI
{

  public class AISpecifications2
  {
    public AISpecifications2()
    {
      Create();
    }

    public void Create()
    {
      var spec = new AISpecificationBuilder("Behavior2")
        .Reasoner("RuleBased")
          .Options()
            .Option("Option1")
              .Considerations()
                .EntityExistsConsideration("Percepts", "Result")
                  .Picker()
                    .Reasoner("DualUtility")
                      .Options()
                        .Option("Option")
                          .Considerations()
                            .VariableConsideration("PickerEntity", "float")
                              .CurveWeightFunction("Linear", "0", "0.333", "1", "0", "0")
                            .End()
                          .End()
                        .End()
                      .End()
                    .End()
                  .End()
                .End()
                .BoolWeightFunction()
              .End()
            .End()
          .End()
        .End()
      .End()
      .BuildAndStore();
    }
  }
}