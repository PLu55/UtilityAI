namespace PLu.ModularAI
{

  public static class ReactOnSenses
  {
    public static void Create()
    {
      var spec = new AISpecificationBuilder("TestingUAIGoToAction")
        .Reasoner("Sequence")
          .Options()
            .Option("Option1")
              .Considerations()
              .End()
              .Actions()
                .GoToAction()
                  .PositionTarget("23, 0, 34")
                .End()
              .End()
            .End()
          .End()
        .End()
        .BuildAndStore();
    }
  }
}