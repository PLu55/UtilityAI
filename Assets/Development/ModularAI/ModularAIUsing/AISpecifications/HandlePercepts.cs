

namespace PLu.ModularAI
{

  public class HandlePercepts
  {
    public HandlePercepts()
    {
      Create();
    }

    public void Create()
    {
      var spec = new AISpecificationBuilder("HandlePercepts")
        .Reasoner("Selection")
          .Options()
            .Option("Option1")
              .Considerations()
                .EntityExistsConsideration("Percepts", "Result")
                  .Picker()
                    .Reasoner("Sequence")
                      .Options()
                        .Option("Option")
                          .Considerations()
                            .VariableConsideration("Actor:Relations.$(PickerEntity.Actor.Faction.Name)", "float")
                              .CurveWeightFunction("Linear", "0", "-1", "0.5", "0.5", "0")
                            .End()
                          .Actions()
                            .SetVariableAction("Result", "true")
                          .End()
                        .End()
                      .End()
                    .End()
                  .End()
                .End()
                .BoolWeightFunction()
              .End()
            .End()
          .End()
        .End()
      .End()
      .BuildAndStore();
    }
  }
}