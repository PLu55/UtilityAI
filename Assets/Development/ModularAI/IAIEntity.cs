//using UnityEngine;

using System;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public interface IAIEntity : IAIBlackboard
    { 
        HashedString Name { get; }
        IAIVector3 Position{ get; }
        Type EntityType { get; }
        bool HasEntity => Entity != null;
        IAIEntity Entity { get; }
    }
}
