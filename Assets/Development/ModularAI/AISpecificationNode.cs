using System.Collections.Generic;
using System.Linq;

namespace PLu.ModularAI
{
    public class AISpecificationNode
    {
        public AISpecificationNode Parent { get; private set; }
        public string Name { get; private set; }
        public string Type { get; private set; }
        public Dictionary<string, string> Attributes { get; private set; }
        public List<AISpecificationNode> Children { get; private set; }
        //public AISpecificationNode[] Children { get; private set; }

        // This is the object that is being created.
        // Warning: accessed in children nodes it is not yet initialized!
        public IAIAbstraction Abstraction { get; set; }

        public AISpecificationNode(AISpecificationNode parent, string name, string type, Dictionary<string, string> attributes, List<AISpecificationNode> children)
        {
            Parent = parent;
            Name = name;
            Type = type;
            Attributes = attributes;
            Children = children;
        }
        public AISpecificationNode(string name, string type)
        {
            Parent = null;
            Name = name;
            Type = type;
            Attributes = new();
            Children = new();
        }
        public void AddChild(AISpecificationNode child)
        {
            child.Parent = this;
            Children.Add(child);
        }
        public void AddAttribute(string key, string value)
        {
            if (value == null)
            {
                return;
            }
            Attributes[key] = value;
        }
        public string GetAttribute(string key)
        {
            return Attributes[key];
        }
        public bool HasAttribute(string key)
        {
            return Attributes.ContainsKey(key);
        }

        public bool TryGetAttribute(string key, out string value, string defaultValue = "")
        {
            if (Attributes.TryGetValue(key, out value))
            {
                return true;
            }

            value = defaultValue;
            return false;
        }
        public bool TryGetAttribute(string key, out float value, float defaultValue = 0f)
        {
            if (Attributes.TryGetValue(key, out string stringValue))
            {
                if (float.TryParse(stringValue, out value))
                {
                    return true;
                }
            }

            value = defaultValue;
            return false;
        }
        public bool TryGetAttribute(string key, out double value, double defaultValue = 0d)
        {
            if (Attributes.TryGetValue(key, out string stringValue))
            {
                if (double.TryParse(stringValue, out value))
                {
                    return true;
                }
            }

            value = defaultValue;
            return false;
        }
        public bool TryGetChild(string name, out AISpecificationNode child)
        {
            foreach (var c in Children)
            {   
                if (c.Name == name)
                {
                    child = c;
                    return true;
                }
            }
            child = null;
            return false;
        }
        public AISpecificationNode GetChild(string name)
        {
            foreach (var child in Children)
            {
                if (child.Name == name)
                {
                    return child;
                }
            }
            return null;
        }
        public bool TryGetAscendent(string name, out AISpecificationNode ascendent)
        {
            if (Parent == null)
            {
                ascendent = null;
                return false;
            }
            if (Parent.Name == name)
            {
                ascendent = Parent;
                return true;
            }
            return Parent.TryGetAscendent(name, out ascendent);
        }

        public AISpecificationNode GetAscendent(string name)
        {
            if (TryGetAscendent(name, out AISpecificationNode ascendent))
            {
                return ascendent;
            }
            else
            {
                return null;
            }
        }

        public bool TryGetDescendent(string name, out AISpecificationNode descendent)
        {
            foreach (var child in Children)
            {
                if (child.Name == name)
                {
                    descendent = child;
                    return true;
                }
                if (child.TryGetDescendent(name, out descendent))
                {
                    return true;
                }
            }
            descendent = null;
            return false;
        }
        public AISpecificationNode GetDescendent(string name)
        {
            if (TryGetDescendent(name, out AISpecificationNode descendent))
            {
                return descendent;
            }
            else
            {
                return null;
            }
        }

        public AISpecificationNode AppendChild(AISpecificationNode child)
        {
            AddChild(child);
            return this;
        }
        
        public override string ToString()
        {
            string attributes = "";
            foreach (var attribute in Attributes)
            {
                attributes += $"{attribute.Key}={attribute.Value} ";
            }
            string children = "";
            foreach (var child in Children)
            {
                children += "\n" + child.ToString();
            }
            return $"<{Name} Type={Type} {attributes}> {children}";
        }
    }
}
