using System;

#if UNITY
using UnityEngine;
#endif

namespace PLu.ModularAI
{
    public interface IAIVector3
    {
        public float X { get; }
        public float Y { get; }
        public float Z { get; }
        public float MagnitudeSqr { get; }
        public float Magnitude { get; } 
        public float DistanceSqr(IAIVector3 other);
        public float Distance(IAIVector3 other);
        public IAIVector3 Add(IAIVector3 other);
        public IAIVector3 Subtract(IAIVector3 other);
        public IAIVector3 Multiply(float scalar);
    }

    public struct AIVector3 : IAIAbstraction, IAIVector3
    {
        public string NodeName => "Vector3";

        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }

        public AIVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static AIVector3 Zero => new AIVector3(0f, 0f, 0f);
 
        public override string ToString()
        {
            return $"AIVector3({X}, {Y}, {Z})";
        }
        public static AIVector3 Parse(string vector)
        {
            string[] parts = vector.Split(',');
            if (parts.Length != 3)
            {
                throw new System.ArgumentException("Vector3 string must have 3 comma separated parts");
            }

            return new AIVector3
            {
                X = float.Parse(parts[0]),
                Y = float.Parse(parts[1]),
                Z = float.Parse(parts[2])
            };
        }

        public bool Initialize(AICreationData creationData)
        {

            throw new System.NotImplementedException();
        }
        public float MagnitudeSqr => X * X + Y * Y + Z * Z;
        public float Magnitude => System.MathF.Sqrt(MagnitudeSqr);

        public float DistanceSqr(IAIVector3 other)
        {
            return Subtract(other).MagnitudeSqr;
        }

        public float Distance(IAIVector3 other)
        {
            return System.MathF.Sqrt(DistanceSqr(other));
        }

        public IAIVector3 Add(IAIVector3 other)
        {            
            return new AIVector3
            {
                X = X + other.X,
                Y = Y + other.Y,
                Z = Z + other.Z
            };
        }

        public IAIVector3 Subtract(IAIVector3 other)
        {
            return new AIVector3
            {
                X = X - other.X,
                Y = Y - other.Y,
                Z = Z - other.Z
            };
        }

        public IAIVector3 Multiply(float scalar)
        {
            return new AIVector3
            {
                X = X * scalar,
                Y = Y * scalar,
                Z = Z * scalar
            };
        }

        public static AIVector3 operator +(AIVector3 a, AIVector3 b)
        {
            return (AIVector3)a.Add(b);
        }

        public static AIVector3 operator -(AIVector3 a, AIVector3 b)
        {
            return (AIVector3)a.Subtract(b);
        }

        public static AIVector3 operator *(AIVector3 a, float b)
        {
            return (AIVector3)a.Multiply(b);
        }

        public static AIVector3 operator /(AIVector3 a, float b)
        {
            return (AIVector3)a.Multiply(1f / b);
        }
    }

    public class AIVector3Factory : AIFactoryBase<AIVector3, AIVector3Factory>
    {
        public AIVector3Factory() : base()
        {
            AddConstructor(new AIVector3Constructor_Default());
        }
    }

    public class AIVector3Constructor_Default : AIConstructorBase<AIVector3>
    {
        public override AIVector3 Create(AICreationData creationData)
        {
            switch (creationData.AISpecificationNode.Type)
            {
                case "Vector3":
                    return creationData.ConstructObject<AIVector3>();
            }
            return new AIVector3();
        }
    }
}
