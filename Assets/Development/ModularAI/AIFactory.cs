using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

namespace PLu.ModularAI
{
    public class AIFactoryBase<T, UFactory> : Singleton<UFactory> where UFactory : AIFactoryBase<T, UFactory>, new()
    {

        private Stack<AIConstructorBase<T>> _constructors;

        public AIFactoryBase()
        {
            _constructors = new();
        }
        public void AddConstructor(AIConstructorBase<T> constructor)
        {
            _constructors.Push(constructor);
        }

        // TODO: remove this method, it just temporary for debugging
        public Stack<AIConstructorBase<T>> GetConstructors()
        {
            return _constructors;
        }
    
        public T Create(AICreationData creationData)
        {
            foreach (var constructor in _constructors)
            {
                T result = constructor.Create(creationData);
                if (result != null)
                {
                    return result;
                }
            }
            string name = creationData.AISpecificationNode.Name;
            string type = creationData.AISpecificationNode.Type;
            throw new AIInitializationFailureException($"Failed to create {name} of type: {type}, no constructor found!");
        }
    }

    public abstract class AIConstructorBase<T>
    {
        public abstract T Create(AICreationData creationData);
    }

}
