using PLu.Utilities;

namespace PLu.ModularAI
{   
    public interface IAISensor
    { 
        void Sense(IAIActor actor);
    }
    public abstract class AISensorBase : IAISensor
    {
        public abstract void Sense(IAIActor actor);
    }

    public class AIActorSensor : AISensorBase
    {
        public float Range { get; private set; }
        private HashedString _targetKey = "Target".AsHashedString();
        public override void Sense(IAIActor self)
        {
            float rangeSquared = Range * Range;

            foreach (var actor in AIManager.Instance.GetActors)
            {
                if (actor == self)
                {
                    continue;
                }
                if (actor.Position.Subtract(self.Position).MagnitudeSqr <= rangeSquared)
                {
                    self.TargetsBlackboard.SetValue<IAIActor>(_targetKey, actor);
                }
                // TODO: Implement sensing logic;
            }
        }
    }
    
}
