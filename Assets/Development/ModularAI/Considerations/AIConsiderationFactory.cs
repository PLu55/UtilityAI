//using UnityEngine;

namespace PLu.ModularAI
{
    public class AIConsiderationFactory : AIFactoryBase<AIConsiderationBase, AIConsiderationFactory>
    {

        private static AIConsiderationFactory  _instance;
        public AIConsiderationFactory() : base()
        {
            AddConstructor(new AIConsiderationConstructor_Default());
        }
    }    
    
    public class AIConsiderationConstructor_Default : AIConstructorBase<AIConsiderationBase>
    {
        public override AIConsiderationBase Create(AICreationData creationData)
        {
            switch(creationData.AISpecificationNode.Type)
            {
                case "Distance":
                    return creationData.ConstructObject<AIDistanceConsideration>();
                case "Variable":
                    return creationData.ConstructObject<AIVariableConsideration>();
                case "ExecutionHistory":
                case "History":
                    return creationData.ConstructObject<AIExecutionHistoryConsideration>();
                case "EntityExists":
                    return creationData.ConstructObject<AIEntityExistsConsideration>();
            }
            return null;
        }
    }
}
