//using UnityEngine;

using System;
using PLu.BlackboardSystem;
using PLu.Utilities;

namespace PLu.ModularAI
{
    // TODO: Decide what to do when the data path is not found!!! Currently an exception is thrown.
    public class AIVariableConsideration : AIConsiderationBase
    {
   
        private VariableType _dataType;
        private AIDataPath _dataPath;
        //private HashedString[] _variableKeys;
        //private IAIBlackboard _blackboard;     

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData) ||
                creationData.AISpecificationNode.Children.Count < 1)
            {
                throw new AIMissingChildException("AIVariableConsideration, WeightFunction is missing");
            }

            if (creationData.AISpecificationNode.TryGetAttribute("DataType", out string DataTypeName))
            {
                GetDataType(DataTypeName);
            }
            else
            { 
                throw new AIMissingAttributeException("AIVariableConsideration: Missing VariableType attribute");
            }
            
            if (creationData.AISpecificationNode.TryGetAttribute("DataPath", out string dataPath))
            { 
                _dataPath = new AIDataPath(dataPath, creationData.Actor);

                if (dataPath == null)
                {
                    throw new AIUnexpectedDataPathException($"AIVariableConsideration: Unexpected dataPath: {dataPath}");
                }
            }
            else
            {
                throw new AIMissingAttributeException("AIVariableConsideration:, missing DataPath attribute");
            }

            return true;
        }

        public override void Consider()
        {    
            AIWeightValues weightValues;

            AILogger.Log($"AIVariableConsideration: Consider {_dataPath.Name} type: {_dataType}");

            switch (_dataType)
            {
                case VariableType.Int:
                    weightValues = WeightFunction.Compute(_dataPath.GetValue<int>());
                    break;
                case VariableType.Double:
                    weightValues = WeightFunction.Compute(_dataPath.GetValue<double>());
                    break;
                case VariableType.Float:
                    weightValues = WeightFunction.Compute(_dataPath.GetValue<float>());
                    break;
                case VariableType.Bool:
                    weightValues = WeightFunction.Compute(_dataPath.GetValue<bool>());
                    break;
                case  VariableType.String:
                    weightValues = WeightFunction.Compute(_dataPath.GetValue<string>());
                    break;
                default:
                    Addend = _defaultAddend;
                    Multiplier = 0f; 
                    Rank = _defaultRank; 
                    return;
            }

            Addend = weightValues.Addend + _defaultAddend;
            Multiplier = weightValues.Multiplier * _defaultMultiplier; 
            Rank = MathF.Max(weightValues.Rank, _defaultRank);   
            AILogger.Log($" AIVariableConsideration.Consider <<< Addend: {Addend} Weight: {Multiplier}, Rank: {Rank}"); 
        }

        private enum VariableType
        {
            Int,
            Double,
            Float,
            Bool,
            String
        }

        private void GetDataType(string variableTypeName)
        {
            switch (variableTypeName)
            {
                case "int":
                case "Int32":
                    _dataType = VariableType.Int;
                    break;
                case "double":              
                case "Double":
                    _dataType = VariableType.Double;
                    break;
                case "float":
                case "Single":
                    _dataType = VariableType.Float;
                    break;
                case "bool":
                case "Boolean":
                    _dataType = VariableType.Bool;
                    break;
                case "string":
                case "String":
                    _dataType = VariableType.String;
                    break;                
                default:
                    throw new AITypeException($"Unexpected variable type {variableTypeName}");
            }
        }
    }
}

