using System;


namespace PLu.ModularAI
{
    public class AIExecutionHistoryConsideration : AIConsiderationBase
    {
        private enum Subtype
        {
            TimeSinceActivation,
            TimeSinceLastActive,
            NumberOfActivations
        }
        private AIOptionBase _option;
        private Subtype _subtype;
        private double _since;
        private double _parameter;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData) ||
                creationData.AISpecificationNode.Children.Count < 1)
            {
                AILogger.LogError("Failed to initialize AIExecutionHistoryConsideration, missing WeightFunction");
                return false;
            }

            if (creationData.AISpecificationNode.TryGetAscendent("Option" , out AISpecificationNode optionNode))
            {
                _option = optionNode.Abstraction as AIOptionBase;
            }
            else
            {
                AILogger.LogError("Failed to initialize AIExecutionHistoryConsideration, can't find Option parent");       
            }

            string subtype = creationData.AISpecificationNode.GetAttribute("Subtype");
            
            switch (subtype)
            {
                case "TimeSinceActivation":
                    _subtype = Subtype.TimeSinceActivation;
                    break;
                case "TimeSinceLastActive":
                    _subtype = Subtype.TimeSinceLastActive;
                    break;
                case "NumberOfActivations":
                    _subtype = Subtype.NumberOfActivations;
                    creationData.AISpecificationNode.TryGetAttribute("Since", out _since, double.MaxValue);
                    break;
                default:
                    throw new ArgumentException($"Failed to initialize AIExecutionHistoryConsideration, unexpected subtype attribute: {subtype}");
            }

            return true;
        }
        public override void Consider()
        {
            AIWeightValues result;
            double time;

            switch (_subtype)
            {                
                case Subtype.TimeSinceActivation:

                    time = _option.ExecutionHistory.TimeSinceActivation();

                    if (time == double.MaxValue)
                    {
                        Addend = 0f;
                        Multiplier = 0f;
                        Rank = -float.MaxValue;
                    }
                    else
                    {
                        result = WeightFunction.Compute(time);
                        Addend = result.Addend;
                        Multiplier = result.Multiplier;
                        Rank = result.Rank;
                    }
                    break;

                case Subtype.TimeSinceLastActive:
                                      
                    time = _option.ExecutionHistory.TimeSinceLastActive();

                    if (time == double.MaxValue)
                    {
                        Addend = 0f;
                        Multiplier = 0f;
                        Rank = -float.MaxValue;
                    }
                    else
                    {
                        result = WeightFunction.Compute(time);
                        Addend = result.Addend;
                        Multiplier = result.Multiplier;
                        Rank = result.Rank;
                    }
                    break;

                case Subtype.NumberOfActivations:
                    result = WeightFunction.Compute(GetNumberOfActivations(AITimeManager.Instance.Time - _since));
                    Addend = result.Addend;
                    Multiplier = result.Multiplier;
                    Rank = result.Rank;
                    break;

            }
        }

        private int GetNumberOfActivations(double since = 0d)
        {   
            int count = 0;

            foreach(var node in _option.ExecutionHistory)
            {
                if (node.Time < since)
                {
                    break;
                }
                if (node.State == AIExecutionHistoryState.Begin)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
