using System;
using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{   
    public class AIEntityExistsConsideration : AIConsiderationBase
    {
        // TODO: Optimize the code by implementing a pool of ProxyOptions
        public AIReasonerBase Reasoner { get; protected set; }
        private AIBlackboard _dataStore;
        private AIBlackboard _brain;
        private HashedString _variableKey;
        private const string _defaultPickerEntityName = "PickerEntity";
        private HashedString _pickerEntityKey;
        private IAIOption _prototypeOption;
        private List<IAIOption> _proxyOptions;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            if (!creationData.AISpecificationNode.TryGetChild("Picker", out AISpecificationNode pickerNode))
            {
                throw new AIMissingChildException("AIEntityExistsConsideration: Picker node expected but not found");
            }

            AICreationData picker = new AICreationData(pickerNode, creationData.Actor);

            if (!picker.AISpecificationNode.TryGetChild("Reasoner", out AISpecificationNode reasoner))
            {
                throw new AIMissingChildException("AIEntityExistsConsideration: Reasoner node expected to be the child of the picker node but is's not found");
            }

            if (creationData.AISpecificationNode.TryGetAttribute("DataStore", out string dataStore))
            {
                _dataStore = creationData.Actor.GetBlackboard(dataStore);

                if (_dataStore == null)
                {
                    throw new AIDataStoreNotFoundException($"AIEntityExistsConsideration: DataStore not found {dataStore}");
                }   
            }
            else
            {
                throw new AIMissingAttributeException("AIEntityExistsConsideration: No DataStore attribute found");
            }

            if (creationData.AISpecificationNode.TryGetAttribute("Variable", out string variable))
            {
                _variableKey = variable;
            }
            else
            {
                throw new AIMissingAttributeException("AIEntityExistsConsideration: No Variable attribute found");
            }

            if (picker.AISpecificationNode.TryGetAttribute("PickerVariable", out string pickerEntityName))
            {
                _pickerEntityKey = pickerEntityName;
            }
            else
            {
                _pickerEntityKey = _defaultPickerEntityName;
            }

            _brain = creationData.Actor.BrainBlackboard;

            if (_brain == null)
            {
                throw new AIDataStoreNotFoundException($"AIEntityExistsConsideration: BrainBlackboard not found of actor {creationData.Actor.Name}");
            }

            if (reasoner.Children.Count != 1)
            {
                throw new AIUnexpectedChildrenException("AIEntityExistsConsideration: in a AIEntityExistsConsideration the Reasoner in the Picker should have one and only one option");
            }

            AICreationData data = new AICreationData(reasoner, creationData.Actor);
            Reasoner = AIReasonerFactory.Instance.Create(data);

            // Save the prototype option for later use and replace the reasoner options with a list of proxies
            _prototypeOption = Reasoner.Options[0];
            _proxyOptions = new();

            Reasoner.SetOptions(_proxyOptions);

            return true;
        }
        public override void Consider()
        {
            // The given prototype option is used in each iteration. The result is stored 
            // in AIProxyOption structs. 
            //
            // Set reasoner.Options to the prototype option
            // Call reasoner.Consider for each entity
            // Replace the prototype option with the list of option proxies
            // Select the best option
            // Execute the best option which will set the result variable
            //
            // The result depend on what type of Reasoner is used.
            //
            // The SelectorReasoner will select the first valid option, run the actions
            // and set the variable to the associated Entity.
            //
            // The SequenceReasoner will consider all options
            // and run the actions for all valid option. The variable will not be set.
            // 
            // The DualUtilityReasoner will consider all options
            // will select the one with highest rank and weight and set the variable
            // to the associated Entity, see DualUtilityReasoner
            // documentation for an explanation of the selection.

            // Clear the list of proxy options and add a proxy for each entity
            // in the given data store
            _proxyOptions.Clear();

            foreach (var entity in _dataStore.Values)
            {
                //_brain.SetValue(_pickerEntityKey, entity);
                //_prototypeOption.Consider();
                //_proxyOptions.Add(new ProxyOption(_prototypeOption, entity as IAIBlackboardEntry));
                _proxyOptions.Add(new ProxyOption(_prototypeOption, entity, _brain, _pickerEntityKey));
            }

            // Try to use the Reasoner to select the best option instead of doing it here
            //Reasoner.Consider();
            //Reasoner.SelectBestOption();
            Reasoner.Think();
            Reasoner.Act();
            
            ProxyOption selectedOption = Reasoner.SelectedOption as ProxyOption;

            if (Reasoner.IsValid)
            {
                AILogger.Log($"AIEntityExistsConsideration: Selected option {selectedOption}");
                if (selectedOption != null)
                {

                    _brain.SetValue(_variableKey, selectedOption.Entity); 
                }
                
                float value = selectedOption != null ? selectedOption.Weight : 1f;
                AIWeightValues values = WeightFunction.Compute(value);
                Addend = values.Addend + _defaultAddend;
                Multiplier = values.Multiplier * _defaultMultiplier; 
                Rank = MathF.Max(values.Rank, _defaultRank);
                AILogger.Log($"AIEntityExistsConsideration: Addend {Addend}, Multiplier {Multiplier}, Rank {Rank}");
            
                _brain.RemoveValue<IAIBlackboardEntry>(_pickerEntityKey);
            }
            else
            {
                Addend = 0f;
                Multiplier = 0f; 
                Rank = -float.MaxValue;
            }  
        }

        private class ProxyOption : IAIOption
        {
            public string Name => $"ProxyOption_{_option.Name}_{Entity}";
            public float Weight { get; private set; }
            public float Rank { get; private set; }

            public bool IsDone { get; } = true;
            //public IAIEntity Entity { get; }
            //public IAIBlackboardEntry Entity { get; }
            public object Entity { get; }

            private IAIOption _option;
            private AIBlackboard _brain;
            private HashedString _pickerEntityKey;

            //public ProxyOption(IAIOption option, IAIEntity entity)
            //public ProxyOption(IAIOption option, IAIBlackboardEntry entity)
            public ProxyOption(IAIOption option, object entity, AIBlackboard brain, HashedString pickerEntityKey)
            {
                _option = option;
                Entity = entity;
                _brain = brain;
                _pickerEntityKey = pickerEntityKey;
            }

            public bool Update()
            {
                return _option.Update();
            }

            public void Reset()
            {
                _option.Reset();
            }
            public void Consider()
            {
                _brain.SetValue(_pickerEntityKey, Entity);
                _option.Consider();
                Weight = _option.Weight;
                Rank = _option.Rank;
            }

            public override string ToString()
            {
                return $"AIProxyOption: {Weight}, {Rank}, {Entity}";
            }
        }
    }
}
