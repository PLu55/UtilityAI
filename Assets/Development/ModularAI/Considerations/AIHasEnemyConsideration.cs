//using UnityEngine;

using System;

namespace PLu.ModularAI
{   
    public class AIHasEnemyConsideration : AIConsiderationBase
    {
        IAIActor _self;
        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            _self = creationData.Actor;
            return true;
        }
        public override void Consider()
        {
            // bool value = false;

            // foreach (var target in _self.TargetsBlackboard.Values)
            // {
            //     if (target.HasEntity && 
            //         target.Entity.Entity as IAIActor != null) &&
            //         (target.Entity.IsEnemy)
            //     {
            //         value = true;
            //         break;
            //     }
            // }

            // WeightValues = WeightFunction.Compute(value);
            // Addend = WeightValues.Addend + _defaultAddend;
            // Multiplier = WeightValues.Multiplier * _defaultMultiplier;
            // Rank = MathF.Max(WeightValues.Rank, _defaultRank);
        }
    }
}
