//using UnityEngine;

using PLu.BlackboardSystem;
using PLu.Utilities;
using Zenject.ReflectionBaking.Mono.Cecil;

namespace PLu.ModularAI
{
    public abstract class AIConsiderationBase : IAIAbstraction
    {
        public string NodeName => "Consideration";
        public float Addend { get; protected set; }
        public float Multiplier { get; protected set; }
        public float Rank { get; protected set; }

        protected float _defaultAddend;
        protected float _defaultMultiplier;
        protected float _defaultRank;
        public AIWeightFunctionBase WeightFunction { get; protected set; } = null;
                
        public virtual bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != NodeName)
            {
                AILogger.LogError("AIConsiderationBase Initialize failed");
                return false;
            }

            creationData.AISpecificationNode.TryGetAttribute("Addend", out _defaultAddend, 0f);
            creationData.AISpecificationNode.TryGetAttribute("Multiplier", out _defaultMultiplier, 1f);
            creationData.AISpecificationNode.TryGetAttribute("Rank", out _defaultRank, -float.MaxValue);

            if (creationData.AISpecificationNode.TryGetChild("WeightFunction", out AISpecificationNode weightFunction))
            {
                AICreationData data = new AICreationData(weightFunction, creationData.Actor);
                WeightFunction = AIWeightFunctionFactory.Instance.Create(data);
            }

            return true;
        }
        public abstract void Consider();

        public virtual void Select()
        {
            //Debug.Log("AIConsiderationBase Select");
        }

        public virtual void Deselect()
        {
            //Debug.Log("AIConsiderationBase Deselect");
        }
    } 

    public class AIExternalVariableConsideration : AIConsiderationBase
    {
        private AIBlackboard _blackboard;
        private HashedString _key;
        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }
            
            if (creationData.AISpecificationNode.TryGetAttribute("DataStore", out string dataStore))
            {
                switch(dataStore)
                {
                    case "Actor":
                        _blackboard = creationData.Actor.ActorBlackboard;
                        break;
                    case "Brain":
                        _blackboard = creationData.Actor.BrainBlackboard;
                        break;
                    case "Targets":
                        _blackboard = creationData.Actor.TargetsBlackboard;
                        break;
                    case "Contacts":
                        _blackboard = creationData.Actor.ContactsBlackboard;
                        break;
                    case "Threats":
                        _blackboard = creationData.Actor.ThreatsBlackboard;
                        break;
                    case "Global":
                        _blackboard = AIGlobalManager.Instance.GetBlackboard(AIBlackboardManager.Instance.GetOrRegister("Global"));
                        break;
                    default:
                        return false;
                }
            }

            if (!creationData.AISpecificationNode.TryGetAttribute("Key", out string key))
            {
                    return false;
            }
            _key = AIBlackboardManager.Instance.GetOrRegister(key);
            return true;
        }
        public override void Consider()
        {
        }
    }
}
