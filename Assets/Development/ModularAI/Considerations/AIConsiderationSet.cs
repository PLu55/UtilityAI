using System;
using System.Collections.Generic;
//using UnityEngine;

namespace PLu.ModularAI
{
    public abstract class AIConsiderationSetBase : IAIAbstraction
    {
        public string NodeName => "Considerations";

        public float Addend { get; protected set; }
        public float Multiplier { get; protected set; }
        public float Rank { get; protected set; }
        public float Weight { get; protected set; }

        protected float _defaultAddend;
        protected float _defaultMultiplier;
        protected float _defaultRank;
        protected AIConsiderationBase[] _considerations;

        public virtual bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != NodeName ||
                creationData.AISpecificationNode.Children == null)
            {
                return false;
            }

            creationData.AISpecificationNode.TryGetAttribute("Addend", out _defaultAddend, 1f);
            creationData.AISpecificationNode.TryGetAttribute("Multiplier", out _defaultMultiplier, 1f);
            creationData.AISpecificationNode.TryGetAttribute("Rank", out _defaultRank, -float.MaxValue);
            _considerations = new AIConsiderationBase[creationData.AISpecificationNode.Children.Count];

            for (int i = 0; i < _considerations.Length; i++)
            {
                AICreationData data = new AICreationData(creationData.AISpecificationNode.Children[i], creationData.Actor);
                _considerations[i] = AIConsiderationFactory.Instance.Create(data);
                if (_considerations[i] == null)
                {
                    _considerations = null;
                    return false;
                }
            }

            return true;
        }
        public abstract void Consider();
    }
    // TODO: Considering to merge AIAndConsiderationSet and AIOrConsiderationSet, 
    //       Subtypes can be a attribute or a parameter.                
    public class AIAndConsiderationSet : AIConsiderationSetBase
    {
        public override void Consider()
        {
            AILogger.Log($"AIAndConsiderationSet: Consider defaults: {_defaultAddend}, {_defaultMultiplier}, {_defaultRank}");

            Addend = _defaultAddend;
            Multiplier = _defaultMultiplier;
            Rank = _defaultRank;

            foreach (var consideration in _considerations)
            {                    
                consideration.Consider();

                if (consideration.Multiplier <= 0f)
                {
                    Addend = 0f;
                    Multiplier = 0f;
                    Rank = -float.MaxValue;
                    Weight = 0f;
                    return;
                }

                Addend += consideration.Addend;
                Multiplier *= consideration.Multiplier;
                Rank = MathF.Max(Rank, consideration.Rank);
            }

            Weight = Addend * Multiplier;
            AILogger.Log($"AIAndConsiderationSet: Consider Addend: {Addend}, Multiplier: {Multiplier} Weight: {Weight}, Rank: {Rank}");
        }
    }
    public class AIOrConsiderationSet : AIConsiderationSetBase
    {
        public override void Consider()
        {            
            float tmpAddend;
            float tmpMultiplier;

            foreach (var consideration in _considerations)
            {
                consideration.Consider();   
                tmpAddend = consideration.Addend + _defaultAddend;
                tmpMultiplier = consideration.Multiplier * _defaultMultiplier;                 
                Weight = tmpAddend * tmpMultiplier;

                if (Weight > 0f)
                {    
                    Addend = tmpAddend;
                    Multiplier = tmpMultiplier;
                    Rank = MathF.Max(consideration.Rank, _defaultRank);
                    return;
                }
            }
            
            Addend = 0f;
            Multiplier = 0f;
            Rank = -float.MaxValue;
            Weight = 0f;
        }
    }
    public class AIConsiderationSetFactory : AIFactoryBase<AIConsiderationSetBase, AIConsiderationSetFactory>
    {
        public AIConsiderationSetFactory() : base()
        {
            AddConstructor(new AIConsiderationSetConstructor_Default());
        }
    }

    public class AIConsiderationSetConstructor_Default : AIConstructorBase<AIConsiderationSetBase>
    {
        public override AIConsiderationSetBase Create(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != "Considerations" || 
                creationData.AISpecificationNode.Children == null)
            {
                return null;
            }

            switch (creationData.AISpecificationNode.Type)
            {
                case "Or":
                    return creationData.ConstructObject<AIOrConsiderationSet>();
                case "And":
                default:
                    return creationData.ConstructObject<AIAndConsiderationSet>();
                    
            }
        }
    }
}
