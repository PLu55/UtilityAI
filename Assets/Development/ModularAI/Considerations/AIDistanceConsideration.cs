//using UnityEngine;

using System;

namespace PLu.ModularAI
{
    public class AIDistanceConsideration : AIConsiderationBase
    {
        private AITargetBase _fromTarget;
        private AITargetBase _toTarget;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData) || creationData.AISpecificationNode.Children.Count < 3)
            {
                return false;
            }
            
            if (creationData.AISpecificationNode.Children[0].Name == "Target")
            {
                AICreationData data = new AICreationData(creationData.AISpecificationNode.Children[0], creationData.Actor); 
                _fromTarget = AITargetFactory.Instance.Create(data);
            }
            else
            {
                AILogger.LogError("Failed to initialize AIDistanceConsideration, missing Target,  expecting two targets");
                return false;
            }

            if (creationData.AISpecificationNode.Children[1].Name == "Target")
            {
                AICreationData data = new AICreationData(creationData.AISpecificationNode.Children[1], creationData.Actor); 
                _toTarget = AITargetFactory.Instance.Create(data);
            }
            else
            {
                AILogger.LogError("Failed to initialize AIDistanceConsideration, missing Target,  expecting two targets");
                return false;
            }

            return true;
        }

        public override void Consider()
        {
            float distance = _fromTarget.Position.Distance(_toTarget.Position);
            AIWeightValues values = WeightFunction.Compute(distance);
            Addend = values.Addend + _defaultAddend;
            Multiplier = values.Multiplier * _defaultMultiplier; 
            Rank = MathF.Max(values.Rank, _defaultRank); 
         }
    }
}
