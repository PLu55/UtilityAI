//using UnityEngine;

using System;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public abstract class AITargetBase : IAIAbstraction
    {
        public string NodeName => "Target";

        public abstract IAIVector3 Position { get; }

        public virtual bool HasEntity => false;
        public virtual IAIEntity Entity => null;

        public virtual bool Initialize(AICreationData creationData)
        {
            string name = creationData.AISpecificationNode.Name;

            if (name != NodeName)
            {
                return false;
            }

            return true;
        }
    }

    public class AITarget : AITargetBase
    {
        protected IAIEntity _entity;
        protected Type _entityType;
        public override bool HasEntity => _entity != null;
        public override IAIEntity Entity => _entity;

        public override IAIVector3 Position => _entity.Position;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            if (creationData.AISpecificationNode.Type == "Self")
            {
                _entity = creationData.Actor;
                _entityType = creationData.Actor.GetType();
                return true;
            }
            return false;
        }
    }
    public class AIPositionTarget : AITargetBase
    {
        private IAIVector3 _position;

        public override IAIVector3 Position => _position;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }
            if (creationData.AISpecificationNode.TryGetAttribute("Position", out string position))
            {
                _position = AIVector3.Parse(position);
                return true;
            }

            return false;
        }
    }
    public class AIDataStoreTarget : AITargetBase
    {
        private AIDataPath _dataPath;
        public override IAIVector3 Position => throw new NotImplementedException();
        public override bool HasEntity => _dataPath.TryGetValue(out IAIEntity entity);
        public override IAIEntity Entity => _dataPath.GetValue<IAIEntity>();
        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            if (creationData.AISpecificationNode.Type == "PickerEntity")
            {
                string path = "Brain:PickerEntity";

                if (creationData.AISpecificationNode.TryGetAttribute("Postfix", out string postfix))
                {
                    path +=  "." + postfix;
                }

                _dataPath = new AIDataPath(path, creationData.Actor);
                return true;
            }

            if (creationData.AISpecificationNode.TryGetAttribute("DataPath", out string dataPath))
            {
                _dataPath = new AIDataPath(dataPath, creationData.Actor);
                return true;
            }

            return false;
        }
    }
}
