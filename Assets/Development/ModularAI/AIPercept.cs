using System;
using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public enum AIPerceptType
    {
        Visual,
        Audial,
        Touch,
        Smell,
        Taste,
        Other
    }

    //            1.0 __|
    //                  |       /\ 
    //                  |      /  \ 
    //                  |     /    \
    //          Level __|    /      \_____  
    //                  |   /             \
    //            0.0 __|  /               \
    //                  |_______________________
    //                  |  |    |   |    |
    //                    
    //          Time   T0  T1   T2  T3
    //          Duration D0   D1   D2      D3

    public class AIPerceptEnvelope
    {
        public double Duration0 { get; }
        public double Duration1 { get; }
        public double Duration2 { get; }
        public double Duration3 { get; }
        public float _sustainLevel { get; }

        public AIPerceptEnvelope(double d0, double d1, double d2, double d3, float sustainLevel)
        {
            Duration0 = d0;
            Duration1 = d1;
            Duration2 = d2;
            Duration3 = d3;
            _sustainLevel = sustainLevel;
        }

        public float GetCurrentLevel(double timeSinceStart, double timeSinceRelease, float releaseFromLevel)
        {
            if (timeSinceStart < Duration0)
            {
                return 0f;
            }

            if (timeSinceRelease > 0.0)
            {
                float level = Clamp01(releaseFromLevel * (float)(1.0 - (timeSinceRelease  /  Duration3)));

                return level;
            }

            double t1 = Duration0 + Duration1;

            if (timeSinceStart < t1)
            {
                return Clamp01((float)((timeSinceStart - Duration0) / Duration1));
            }

            double t2 = t1 + Duration2;

            if (timeSinceStart < t2)
            {
                return ClampX1(1f - (1f - _sustainLevel) * (float)((timeSinceStart - t1) / Duration2), _sustainLevel);
            }            
            
            return _sustainLevel;
        }        
        
        public float Clamp01(float value)
        {
            return MathF.Max(0f, MathF.Min(1f, value));
        }        
        public float ClampX1(float value, float x)
        {
            return MathF.Max(x, MathF.Min(1f, value));
        }
    }

    public class AIPerceptManager
    {
        private static Dictionary<string, AIPerceptEnvelope> _envelopes = new();
        private AIPerceptManager()
        {
            //_envelopes = new();
        }

        public static void AddEnvelop(string name, AIPerceptEnvelope envelope)
        {
            _envelopes.Add(name, envelope);
        }

        public static AIPerceptEnvelope GetEnvelope(string name)
        {
            return _envelopes[name];
        }

    }

    public class AIPercept : IAIBlackboard, IAIEntity
    {
        
        public AIPerceptType PerceptType { get; }        
        public float BaseLevel { get;  }
        public IAIVector3 Position { get; private set; }
        public float Angle { get; }
        public float Distance { get; }
        public IAIEntity Entity { get; }
        public IAIEntity SecondaryEntity { get; }

        public string BlackboardName => "Percept";

        public ICollection<HashedString> Keys => throw new NotImplementedException();

        public ICollection<object> Values => throw new NotImplementedException();

        public int Count => throw new NotImplementedException();

        public HashedString Name => Entity.Name;

        public Type EntityType => GetType();

        private AIPerceptEnvelope _envelope;       
        private double _time0;
        private double _time1;
        private float _releaseFromLevel;

        private enum Key 
        {
            PerceptType,
            Level,
            Position,
            Angle,
            Distance,
            Entity,
            SecondaryEntity

        }
        private static Dictionary<HashedString, Key> _keys = new Dictionary<HashedString, Key>() {
            { "PerceptType".AsHashedString(), Key.PerceptType },
            { "Level".AsHashedString(), Key.Level },
            { "Position".AsHashedString(), Key.Position },
            { "Angle".AsHashedString(), Key.Angle },
            { "Distance".AsHashedString(), Key.Distance },
            { "Entity".AsHashedString(), Key.Entity },
            { "SecondaryEntity".AsHashedString(), Key.SecondaryEntity }
        };

        public AIPercept(AIPerceptType type, double time, float level, IAIVector3 position, IAIEntity entity = null, IAIEntity secondaryEntity = null, float distance = 0f, float angle = 0f, AIPerceptEnvelope envelope = default)
        {
            PerceptType = type;            
            BaseLevel = level;            
            Entity = entity;
            SecondaryEntity = secondaryEntity;
            Position = position;
            Distance = distance;
            Angle = angle;
            _envelope = envelope;
            _time0 = time;
            _time1 = 0d;
            _releaseFromLevel = 0f;
        }
        public void Release()
        {
            _time1 = AITimeManager.Instance.Time;
            _releaseFromLevel = GetEnvelopeLevel();
        }

        public void Reactivate(AIVector3 position)
        {
            _time0 = AITimeManager.Instance.Time - _envelope.Duration0 - _envelope.Duration1 - _envelope.Duration2;
            _time1 = 0d;
            Position = position;
        }

        public float CurrentLevel => GetEnvelopeLevel() * BaseLevel;
        public bool IsInReleasePhase => _time1 != 0d;

        private float GetEnvelopeLevel()
        {            
            double currentTime = AITimeManager.Instance.Time;
            double timeSinceRelease = _time1 > 0f ? currentTime - _time1 : 0f;
            float level = _envelope.GetCurrentLevel(currentTime - _time0, timeSinceRelease, _releaseFromLevel);
            return level;
        }

        public bool ContainsKey(HashedString key)
        {
            throw new NotImplementedException();
        }

        public void SetValue<T>(HashedString key, T value) 
        {
            throw new NotImplementedException();
        }

        public T GetValue<T>(HashedString key, T defaultValue = default)
        {
            if (TryGetValue<T>(key, out T value))
            {
                return value;
            }

            return defaultValue;
        }

        public bool TryGetValue<T>(HashedString key, out T value)
        {
            if (_keys.TryGetValue(key, out Key index))
            {
                switch (index)
                {
                    case Key.PerceptType:
                        value = (T)(object)PerceptType.ToString();
                        return true;
                    case Key.Level:
                        value = (T)(object)BaseLevel;
                        return true;
                    case Key.Position:
                        value = (T)(object)Position;
                        return true;
                    case Key.Angle:
                        value =  (T)(object)Angle;
                        return true;
                    case Key.Distance:
                        value = (T)(object)Distance;
                        return true;
                    case Key.Entity:
                        value = (T)(object)Entity;
                        return true;
                    case Key.SecondaryEntity:
                        value = (T)(object)SecondaryEntity;
                        return true;
                }
            }
                          
            value = default;
            return false;
        }

        public void RemoveValue<T>(HashedString key)
        {
            throw new NotImplementedException();
        }
    }
}
