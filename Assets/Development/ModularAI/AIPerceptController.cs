using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{

    // TODO: Implement handling of contacts in this class???
    public class AIPerceptController: IAIPerceptController
    {
        public IAIActor Actor { get; private set;}

        private AIBlackboard _perceptsBlackboard;
        private AIPerceptDataStore _perceptDataStore; 
        private HashSet<HashedString> _toBeRemoved;

        public AIPerceptController(IAIActor actor) 
        {
            Actor = actor;
            _perceptsBlackboard = actor.PerceptsBlackboard;
            _perceptDataStore = new();
            _toBeRemoved = new();
        }

        public void Tick() 
        {
            RemoveInvalidPercepts();
        }        
        
        public int GetPerceptCount(AIPerceptType type)
        {
            return _perceptDataStore.Count;
        }
        public bool HasPercept(IAIEntity entity, AIPerceptType type)
        {
            if (_perceptDataStore.TryGetValue(entity.Name, out var percept) &&
                percept.PerceptType == type)
            {
                return true;
            }
            return false;
        }

        public bool TryGetPercept(IAIEntity entity, AIPerceptType type, out AIPercept percept)
        {
            return _perceptDataStore.TryGetValue(entity.Name, out percept);
        }

        // TODO: remove the entity parameter and use the entity from the percept
        public void AddPercept(IAIEntity entity, AIPercept percept)
        {
            if (!_perceptDataStore.ContainsKey(entity.Name))
            {
                _perceptDataStore.Add(entity.Name, percept);
                _perceptsBlackboard.SetValue<AIPercept>(entity.Name, percept);
                CreateOrUpdateContact(entity, percept);
            }
            else
            {
                _perceptDataStore[entity.Name] = percept;
                _perceptsBlackboard.SetValue<AIPercept>(entity.Name, percept);
                CreateOrUpdateContact(entity, percept);

            }
        }

        private void CreateOrUpdateContact(IAIEntity entity, AIPercept percept)
        {                
            if (entity is IAIActor otherActor)
            {
                if (Actor.ContactsBlackboard.TryGetValue(entity.Name, out AIContact oldContact))
                {
                    oldContact?.Update(otherActor.Position, AITimeManager.Instance.Time);
                }
                else
                {
                    AIContact contact = new(otherActor, otherActor.Position, AITimeManager.Instance.Time, true);
                    Actor.ContactsBlackboard.SetValue<AIContact>(entity.Name, contact);
                }
            }
        }

        public void RemovePercept(IAIEntity entity, AIPerceptType type)
        {
            if (_perceptDataStore.ContainsKey(entity.Name))
            {
                _perceptDataStore.Remove(entity.Name);
            }
        }

        private void RemoveInvalidPercepts()
        {
            _toBeRemoved.Clear();

            foreach (var keyValuePair in _perceptDataStore)
            {
                if (keyValuePair.Value.IsInReleasePhase && keyValuePair.Value.CurrentLevel == 0f)
                {
                    _toBeRemoved.Add(keyValuePair.Key);
                }
            }

            foreach (var entity in _toBeRemoved)
            {
                _perceptDataStore.Remove(entity);
                _perceptsBlackboard.RemoveValue<AIPercept>(entity);
            }
        }
    }

}
