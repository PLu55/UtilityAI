using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public class AIBlackboardManager : HashedStringRegistryBase<AIBlackboardManager> 
    {
        public AIBlackboard GlobalBlackboard { get; private set;} 
        Dictionary<string, AIBlackboard> _blackboards;
        public AIBlackboardManager() : base()
        { 
            _blackboards = new(); 
            GlobalBlackboard = new();
            _blackboards.Add("Global", GlobalBlackboard);
        }

        public  bool TryGetBlackboard(string key, out AIBlackboard blackboard)
        {
            return _blackboards.TryGetValue(key, out blackboard);
        }
        public AIBlackboard GetBlackboard(string key)
        {
            if (_blackboards.TryGetValue(key, out AIBlackboard blackboard))
            {
                return blackboard;
            }
            return null;
        }

    }
}