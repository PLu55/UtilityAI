using UnityEditorInternal;

namespace PLu.ModularAI
{
    public abstract class AITimeManagerBase : IAITimeManager, ISingleton<IAITimeManager>
    {
        public virtual double Time { get; protected set; }
        public abstract double DeltaTime { get; }
        public static IAITimeManager Instance =>_instance;
        protected static IAITimeManager _instance;

        public virtual void UpdateTime(double deltaTime) {}

        public static void SetTimeManager(IAITimeManager timeManager)
        {
            _instance = timeManager;
        }
    }

    public class AITimeManager : AITimeManagerBase
    {
        public new static IAITimeManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AITimeManager();
                }
                return _instance;
            }
        }
        public override double DeltaTime => _deltaTime;
        private double _deltaTime = 0d;

        public override void UpdateTime(double deltaTime)
        {
            Time += deltaTime;
            _deltaTime = deltaTime;
        }
    }
    public interface IAITimeManager
    {
        double Time { get; }
        double DeltaTime { get; }
        void UpdateTime(double deltaTime);
    }
}