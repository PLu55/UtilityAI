namespace PLu.ModularAI
{
    public class AISpecification
    {
        public string Name { get; }        
        public AISpecificationNode Root { get; }
        public bool IsPartial { get; } 

        public AISpecification(string name, AISpecificationNode root, bool isPartial = false)
        {
            Name = name;
            Root = root;
            IsPartial = isPartial || Root.Name != "Reasoner";
        }
    }

    public abstract class AISpecificationLoaderBase
    {
        public bool Overwrite { get; }
        protected abstract void Load();

        public AISpecificationLoaderBase(bool overwrite = false)
        {
            Overwrite = overwrite;
            Load();
        }
    }
}
