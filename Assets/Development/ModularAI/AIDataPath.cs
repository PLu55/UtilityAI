using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using PLu.Utilities;
using UnityEngine.Assertions.Must;

namespace PLu.ModularAI
{
    // Syntax: path := [root:]subpaths
    //         root := string
    //         subpaths := subpath[.subpaths]
    //         subpath := '@(' path ')' | string
    // Example: "Contacts:Enemy.IsThreat"
    // Example: "Actor:Relations.@(PicketEntity.Name)"
    //
    // If no root is specified, the root is the actor's actor blackboard
    public class AIDataPath : IAIAbstraction
    {
        public string NodeName => "DataPath";
        public string Name { get; private set; }
        HashedString[] _keys;
        AIDataPath[] _subPaths; 
        IAIBlackboard _blackboard;

        public AIDataPath(string path, IAIActor actor)
        {
            Name = path;
            ParsePath(path, actor);
        }

        public bool Initialize(AICreationData creationData)
        {
            throw new System.NotImplementedException();
        }


        public T GetValue<T>()
        {
            if (TryGetValue<T>(out T value))
            {
                return value;
            }
            else
            {
                AILogger.LogError($"AIDataPath.GetValue: Failed to get value for path {Name} {typeof(T).Name}");
                return (T)default;
            }
        }
      
        public void SetValue<T>(T value)
        {
            IAIBlackboard blackboard = TraversePath();
            int j = _keys.Length - 1;
            HashedString key = _keys[j];

            if (_subPaths[j] != null)
            {
                if (!_subPaths[j].TryGetValue(out key))
                {
                    AILogger.LogError($"AIDataPath.GetValue: Failed to get value from subpath {_subPaths[j]}");
                }
            }
            else if (key == null)
            {
                AILogger.LogError($"AIDataPath.GetValue: No key specified for last key in path: {Name}");
            }
            
            blackboard.SetValue<T>(key, value);
        }

        public bool TryGetValue<T>(out T value)
        {
            IAIBlackboard blackboard = TraversePath();
            int j = _keys.Length - 1;
            HashedString key = _keys[j];

            // no key given, must be a subpath
            //if (key == noKey)
            if (_subPaths[j] != null)
            {
                if (!_subPaths[j].TryGetValue(out key))
                {
                    AILogger.LogError($"AIDataPath.GetValue: Failed to get key for subpath {_subPaths[j]}");
                    value = (T)default;
                    return false;
                }
            }
            else if (key == null)
            {
                AILogger.LogError($"AIDataPath.GetValue: No key specified for last key in path: {Name}");
                value = (T)default;
                return false;
            }
            
            if (!blackboard.TryGetValue(key, out value))
            {
                AILogger.LogError($"AIDataPath.GetValue: Failed to get value with key: {key} from: {(blackboard as AIBlackboard)?.BlackboardName}");
                value = (T)default;
                return false;
            }

            return true;
        }

        public override string ToString() => Name;

        private void ParsePath(string path, IAIActor actor)
        {
            // TODO: The syntax need to be improved to better catch errors. 
            //       The current implementation is very basic and will not catch all errors.
            //       This is not triggering an error: "Actor:Relations.$(PicketEntity.Name"

            string pattern = "((?<base>[^@:]+):)|(?<var>([^.]+)?(@\\([^)]+\\))|([^.:]+))";
            MatchCollection matches;
            matches = Regex.Matches(path, pattern);

            List<string> bases = new();
            List<string> vars = new();

            //AILogger.Log($"AIDataPath.ParsePath: Path: {path}");

            foreach (Match match in matches)
            {
                string s = match.Groups["base"].Value;
                if (s != "")
                {
                    bases.Add(s);
                }

                s = match.Groups["var"].Value;
                if (s != "")
                {
                    vars.Add(s);
                }
            }

            if (vars.Count == 0)
            {
                throw new AIUnexpectedValueException($"AIDataPath.ParsePath: Invalid path format, path is empty: {path}");
            }

            if (bases.Count > 1)
            {
                throw new AIUnexpectedValueException($"AIDataPath.ParsePath: Invalid path format, only one base is allowed: {path}");
            }

            if (bases.Count == 0)
            {
                //AILogger.Log($"AIDataPath.ParsePath: No base specified, using ActorBlackboard of: {actor.Name}");
                _blackboard = actor.ActorBlackboard;
            }
            else if (bases[0] == "Global")
            {
                _blackboard = AIBlackboardManager.Instance.GlobalBlackboard;
            }
            else
            {
                _blackboard = actor.GetBlackboard(bases[0]);
            }

            int n = 0;
            _keys = new HashedString[vars.Count];
            _subPaths = new AIDataPath[vars.Count];

            foreach (string var in vars)
            {
                if (var.StartsWith("@"))
                {
                    //AILogger.Log($"AIDataPath.ParsePath: Subpath: {var}");
                    _subPaths[n] = new AIDataPath(var.Substring(2, var.Length - 3), actor);
                }
                else
                {
                    //AILogger.Log($"AIDataPath.ParsePath: Key: {var}");
                    _keys[n] = var;
                }
                n++;
            }
        }

        private IAIBlackboard TraversePath()
        {
            IAIBlackboard blackboard = _blackboard;
            HashedString key = null;

            // Traverse the path up to the last key
            for (int i = 0; i < _keys.Length - 1; i++)
            {
                key = _keys[i];

                if (_subPaths[i] != null)
                {
                    if (!_subPaths[i].TryGetValue(out key))
                    {
                        AILogger.LogError($"AIDataPath.GetValue: Failed to get key for subpath {_subPaths[i]}");
                        return null;
                    }
                }

                blackboard = blackboard.GetValue<IAIBlackboard>(key);
                
                if (blackboard == null)
                {
                    AILogger.LogError($"AIDataPath.GetValue: Failed to get data store {_keys[i]} from: {(blackboard as AIBlackboard)?.BlackboardName}");
                    AILogger.LogError($"AIDataPath.GetValue: Failed to get data store {_keys[i]} from: {(blackboard as IAIActor)?.Name}");
                    return null;
                }
            }
            return blackboard;
        } 
    }
}
