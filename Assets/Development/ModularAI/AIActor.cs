using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public class AIPerceptDataStore : Dictionary<HashedString, AIPercept> {}

    public interface IAIActor : IAIEntity //, IAIBlackboard
    {
        AIReasonerBase Reasoner { get; }
        AISensorBase[] Sensors { get; }
        AIBlackboard BrainBlackboard { get; }
        AIBlackboard ActorBlackboard { get; }
        AIBlackboard TargetsBlackboard { get; }
        AIBlackboard ContactsBlackboard { get; }
        AIBlackboard ThreatsBlackboard { get; }
        AIBlackboard PerceptsBlackboard { get; }
        AIBlackboard GetBlackboard(HashedString name);

        void Tick();
    }

    public class AIActor : IAIActor
    {
        public HashedString Name { get; private set; }
        public AIReasonerBase Reasoner { get; set; }
        public AISensorBase[] Sensors { get; private set; }
        public AIBlackboard BrainBlackboard { get; private set; }
        public AIBlackboard ActorBlackboard { get; private set; }
        public AIBlackboard TargetsBlackboard { get; private set; }
        public AIBlackboard ContactsBlackboard { get; private set; }
        public AIBlackboard ThreatsBlackboard { get; private set; }
        public AIBlackboard PerceptsBlackboard { get; private set; }

        public IAIVector3 Position { get; set;}

        public Type EntityType => this.GetType();
        public IAIEntity Entity => this;
        public bool HasEntity => true;

        public ICollection<HashedString> Keys => ActorBlackboard.Keys;
        public ICollection<object> Values => ActorBlackboard.Values;
        public bool ContainsKey(HashedString key) => ActorBlackboard.ContainsKey(key);
        public void SetValue<T>(HashedString key, T value) => ActorBlackboard.SetValue<T>(key, value);
        public T GetValue<T>(HashedString key, T defaultValue = default) => ActorBlackboard.GetValue<T>(key, defaultValue);
        public bool TryGetValue<T>(HashedString key, out T value) => ActorBlackboard.TryGetValue<T>(key, out value);
        public void RemoveValue<T>(HashedString key) => ActorBlackboard.RemoveValue<T>(key);
        public int Count => ActorBlackboard.Count;

        public string BlackboardName => Name.ToString();

        public AIBlackboard GetBlackboard(HashedString name) => ActorBlackboard.GetValue<AIBlackboard>(name);

        public AIActor(string name)
        {
            Name = AIBlackboardManager.Instance.GetOrRegister(name);
            Position = new AIVector3(0f, 0f, 0f);
            SetupBlackboards();
        }

        public void Tick()
        {  
            Sense();
            Reasoner.Tick();
        }
        public void Sense()
        {
            if (Sensors == null)
            {
                return;
            }
            foreach (var sensor in Sensors)
            {
                sensor.Sense(this);
            }
        }

        public IAIEntity GetEntity()
        {
            return this;
        }

        public override string ToString()
        {
            return $"AIActor({Name.ToString()})";
        }

        private void SetupBlackboards()
        {
            BrainBlackboard = new("Brain");
            ActorBlackboard = new("Actor");
            TargetsBlackboard = new("Targets");
            ContactsBlackboard = new("Contacts");
            ThreatsBlackboard = new("Threats");
            PerceptsBlackboard = new("Percepts");

            ActorBlackboard.SetValue<IAIBlackboard>("Brain".AsHashedString(), BrainBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Actor".AsHashedString(), ActorBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Targets".AsHashedString(), TargetsBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Contacts".AsHashedString(), ContactsBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Threats".AsHashedString(), ThreatsBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Percepts".AsHashedString(), PerceptsBlackboard);

            TargetsBlackboard.SetValue<IAIActor>("Self".AsHashedString(), this);
            ActorBlackboard.SetValue("Name".AsHashedString(), Name);
        }
    }
}
