
using System.Collections.Generic;
using PLu.GenericBlackboard;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public interface IAIBlackboard : IAIBlackboardEntry
    {    
        string BlackboardName { get; }
        ICollection<HashedString> Keys { get;}
        ICollection<object> Values { get; }
        int Count { get; }
        bool ContainsKey(HashedString key);
        void SetValue<T>(HashedString key, T value);
        T GetValue<T>(HashedString key, T defaultValue = default);
        bool TryGetValue<T>(HashedString key, out T value);
        void RemoveValue<T>(HashedString key);
    }

    public class AIBlackboard : Blackboard, IAIBlackboard 
    {
        public string BlackboardName { get; private set; }
        public AIBlackboard(string name) : base() 
        {
            BlackboardName = name;
        }        
        public AIBlackboard() : base() 
        {
            BlackboardName = "anonymous";
        }
    }

    public interface IAIBlackboardEntry {}

}