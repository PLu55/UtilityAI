using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace PLu.ModularAI
{
    public class AISpecificationManager : Singleton<AISpecificationManager>
    {
        private Dictionary<string, AISpecification> _aiSpecifications;
        private string _aiSpecificationDirectoryPath;

        public AISpecificationManager()
        {
            _aiSpecifications = new ();
            _aiSpecificationDirectoryPath = "";
        }
        public void SetXMLSpecificationDirectoryPath(string path)
        {
            _aiSpecificationDirectoryPath = path;
        }
        public void LoadXMLSpecifications(string path)
        {
            _aiSpecifications = new();
            ReadXml($"{_aiSpecificationDirectoryPath}{path}");
        }
        public bool HasAISpecification(string name)
        {
            return _aiSpecifications.ContainsKey(name);
        }

        public bool TryGetAISpecification(string name, out AISpecification specification)
        {
            return _aiSpecifications.TryGetValue(name, out specification);
        }
        public AISpecification GetAISpecification(string name)
        {
            return _aiSpecifications[name];
        }

        public void AddSpecification(AISpecification specification, bool overwrite = false)
        {
            if (specification.IsPartial)
            {
                AILogger.LogError($"AISpecificationManager: Cannot add partial specification {specification.Name}");
                return;
            }

            if (_aiSpecifications.ContainsKey(specification.Name) && !overwrite)
            {
                AILogger.LogError($"AISpecificationManager: Can't overwrite existing specification {specification.Name}, set force to true to overwrite");
                return;
            }

            _aiSpecifications[specification.Name] = specification;
        }

        private void ReadXml(string path)
        {
            XDocument xdoc = XDocument.Load(path);
            XElement xelement = xdoc.Element("AISpecifications");

            IEnumerable<XElement> aiSpecifications = xelement.Elements();

            foreach (var aiSpecification in aiSpecifications)
            {
                AISpecificationNode node = Parse(aiSpecification);
                if (node == null)
                {
                    AILogger.LogError("AISpecificationManager: Error parsing AISpecification");
                    continue;
                }
                if (node.Name != "AISpecification")
                {
                    AILogger.LogError("AISpecificationManager: Expected AISpecification node");
                    continue;
                }
                string name = node.GetAttribute("Name");
                AISpecification specification = new(name, node.Children[0]);
                _aiSpecifications[name] = specification;
            }
        }
        static public AISpecificationNode Parse(XElement xelement, AISpecificationNode parent = null)
        {
            string name = xelement.Name.ToString();
            string type = "";
            Dictionary<string, string> attributes = new Dictionary<string, string>();
            
            foreach (var attribute in xelement.Attributes())
            {
                if (attribute.Name == "Type")
                {
                    type = attribute.Value;
                }
                else
                {
                    attributes.Add(attribute.Name.ToString(), attribute.Value);
                }
            }
            
            List<AISpecificationNode> children = new List<AISpecificationNode>(); 
            AISpecificationNode node = new AISpecificationNode(parent, name, type, attributes, children);          // if (xelement.HasElements)

            if (xelement.HasElements)
            {
                foreach (var child in xelement.Elements())
                {
                    children.Add(Parse(child, node));
                }

                for (int i = 0; i < xelement.Elements().Count(); i++)
                {
                    children[i] = Parse(xelement.Elements().ElementAt(i), node);
                }
            }

            // AISpecificationNode[] children = new AISpecificationNode[xelement.Elements().Count()];

            // AISpecificationNode node = new AISpecificationNode(parent, name, type, attributes, children);

            // if (xelement.HasElements)
            // {
            //     List<AISpecificationNode> childList = new List<AISpecificationNode>();

            //     for (int i = 0; i < xelement.Elements().Count(); i++)
            //     {
            //         children[i] = Parse(xelement.Elements().ElementAt(i), node);
            //     }
            // }

            return node;
        }
    }
}
