using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public interface IAIManager
    {
        void Tick();
        void RegisterActor(IAIActor actor);
        void UnregisterActor(IAIActor actor);
        bool TryGetActor(HashedString name, out IAIActor actor);
    }

    public class AIManager : Singleton<AIManager>, IAIManager
    {
        private readonly Dictionary<HashedString, IAIActor> _actors = new();
        public void Tick()
        {
            foreach (var actor in _actors.Values)
            {
                actor.Tick();
            }            
        }
            
        public void RegisterActor(IAIActor actor)
        {
            _actors[actor.Name] = actor;
        }

        public void UnregisterActor(IAIActor actor)
        {
            _actors.Remove(actor.Name);
        }

        public bool TryGetActor(HashedString name, out IAIActor actor)
        {
            return _actors.TryGetValue(name, out actor);
        }

        public ICollection<IAIActor> GetActors => _actors.Values;
    }

}
