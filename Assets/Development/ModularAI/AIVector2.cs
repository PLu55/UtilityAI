namespace PLu.ModularAI
{
    public struct AIVector2 : IAIAbstraction
    {
        public string NodeName => "Vector2";

        public float x;
        public float y;

        public override string ToString()
        {
            return $"AIVector2({x}, {y})";
        }
        public static AIVector2 Parse(string vector)
        {
            string[] parts = vector.Split(',');
            return new AIVector2
            {
                x = float.Parse(parts[0]),
                y = float.Parse(parts[1]),
            };
        }

        public bool Initialize(AICreationData creationData)
        {

            throw new System.NotImplementedException();
        }
        public float MagnitudeSqr => x * x + y * y;
        public float Magnitude => System.MathF.Sqrt(MagnitudeSqr);

        public float DistanceSqr(AIVector2 other)
        {
            return (this - other).MagnitudeSqr;
        }

        public float Distance(AIVector2 other)
        {
            return System.MathF.Sqrt(DistanceSqr(other));
        }
        
        public static AIVector2 operator +(AIVector2 a, AIVector2 b)
        {
            return new AIVector2
            {
                x = a.x + b.x,
                y = a.y + b.y
            };
        }

        public static AIVector2 operator -(AIVector2 a, AIVector2 b)
        {
            return new AIVector2
            {
                x = a.x - b.x,
                y = a.y - b.y
            };
        }

        public static AIVector2 operator *(AIVector2 a, float b)
        {
            return new AIVector2
            {
                x = a.x * b,
                y = a.y * b
            };
        }

        public static AIVector2 operator /(AIVector2 a, float b)
        {
            return new AIVector2
            {
                x = a.x / b,
                y = a.y / b
            };
        }
    }

    public class AIVector2Factory : AIFactoryBase<AIVector2, AIVector2Factory>
    {
        public AIVector2Factory() : base()
        {
            AddConstructor(new AIVector2Constructor_Default());
        }
    }

    public class AIVector2Constructor_Default : AIConstructorBase<AIVector2>
    {
        public override AIVector2 Create(AICreationData creationData)
        {
            switch (creationData.AISpecificationNode.Type)
            {
                case "Vector2":
                    return creationData.ConstructObject<AIVector2>();
            }
            return new AIVector2();
        }
    }
}
