using System;
using System.Collections.Generic;
using UnityEditor.Build;

namespace PLu.ModularAI
{
    public interface IAIOption
    {
        string Name { get; }
        float Weight { get; }
        float Rank { get; }
        bool IsDone { get; }

        void Consider();
        bool Update();
        void Reset();
    }
    public abstract class AIOptionBase : IAIAbstraction, IAIOption
    {
        public string NodeName => "Option";
        public string Name { get; private set; }
        public float Weight { get; protected set; }
        public float Rank { get; protected set; }
        public bool IsDone { get; protected set; } = true;
        public AIExecutionHistory ExecutionHistory { get; private set; }
        public bool HasConsiderations => _considerationSet != null; 
        protected AIConsiderationSetBase _considerationSet;

        protected AIActionBase[] _actions;
        protected IAIActor _actor;

        public AIOptionBase()
        {
            ExecutionHistory = new AIExecutionHistory();
        }

        public abstract bool Update();
        public abstract void Reset();

        public virtual bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != NodeName ||
                creationData.AISpecificationNode.Children.Count > 2 ||
                creationData.AISpecificationNode.Children.Count < 1)
            {
                return false;
            }

            if (creationData.AISpecificationNode.Children.Count == 1 &&
                creationData.AISpecificationNode.Children[0].Name != "Actions" &&
                creationData.AISpecificationNode.Children[0].Name != "Considerations")
            {
                AILogger.LogError("AIOption: Option must contain an Actions node and/or a Considerations node");
                return false;
            }

            if (creationData.AISpecificationNode.Children.Count == 2 &&
                (creationData.AISpecificationNode.Children[0].Name != "Considerations" ||
                 creationData.AISpecificationNode.Children[1].Name != "Actions"))
            {
                AILogger.LogError($"AIOption: Option must contain a Considerations and an Actions node in that order");
                return false;
            }

            Name = "";

            if (creationData.AISpecificationNode.TryGetAttribute("Name", out string name))
            {
                Name = name;
            }

            _actor = creationData.Actor;
            return true;
        }

        public virtual void Consider()
        {
            AILogger.Log($"AIOption: Consider {Name}");
            if (_considerationSet == null)
            {
                Weight = 1f;
                Rank = -float.MaxValue;
            }
            else
            {
                _considerationSet.Consider();
                Weight = _considerationSet.Weight;
                Rank = _considerationSet.Rank;
            }

            AILogger.Log($"AIOption: Consider {Name}  <<< Weight: {Weight}, Rank: {Rank}");
        }
    }

    public class AIOption : AIOptionBase
    {
        private IAIAction _currentAction;
        private int _actionIndex;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            if (creationData.AISpecificationNode.TryGetChild("Considerations", out AISpecificationNode considerationsNode))
            {
                AICreationData data = new AICreationData(considerationsNode, creationData.Actor);
                _considerationSet = AIConsiderationSetFactory.Instance.Create(data);
            }
            
            _actions = new AIActionBase[0];

            if (creationData.AISpecificationNode.TryGetChild("Actions", out AISpecificationNode actionsNode))
            {
                List<AIActionBase> actions = new();

                foreach (var child in actionsNode.Children)
                {
                    AICreationData actionData = new AICreationData(child, creationData.Actor);
                    AIActionBase action = AIActionFactory.Instance.Create(actionData);
                    if (action != null)
                    {
                        actions.Add(action);
                    }
                    else
                    {
                        AILogger.LogError($"AIOption: Action {child.Type} could not be created");
                    }
                }
                _actions = actions.ToArray();
            }

            return true;
        }

        public override void Reset()
        {
            if (!IsDone && _currentAction != null)
            {
                ExecutionHistory.AddEntry(AIExecutionHistoryState.End);
                _currentAction.Deselect();
            }
            IsDone = false;
            _currentAction = null;
            _actionIndex = 0;
        }

        public override bool Update()
        {
            AILogger.Log($"AIOption: Update {Name}");

            if (_currentAction == null)
            {
                AILogger.Log($"   ... Begin, no current action");
                IsDone = false;
                ExecutionHistory.AddEntry(AIExecutionHistoryState.Begin);
                _actionIndex = 0;

                if (_actions.Length == 0)
                {
                    ExecutionHistory.AddEntry(AIExecutionHistoryState.End);
                    IsDone = true;
                    return IsDone;
                }

                _currentAction = _actions[_actionIndex];
                _currentAction.Select();
            }

            AILogger.Log($"   ... has current action? {_currentAction != null} done? {_currentAction.IsDone}");
            while (_currentAction != null)
            {
                AILogger.Log($"   ... Update action");

                // Update the current action, if it is done, select the next action.
                if (_currentAction.Update())
                {
                    AILogger.Log($"   ... Done, select next action");
                    _currentAction.Deselect();
                    _actionIndex++;

                    if (_actionIndex < _actions.Length)
                    {
                        _currentAction = _actions[_actionIndex];
                        _currentAction.Select();
                    }
                    else // All actions are done
                    {
                        AILogger.Log($"   ... All actions done");
                        ExecutionHistory.AddEntry(AIExecutionHistoryState.End);
                        IsDone = true;
                        _currentAction = null;
                    }
                }
                else // All actions are not done yet
                {
                    break;
                }
            }
            AILogger.Log($"   <<< return {IsDone}");
            return IsDone;
        }
    }

    public class AIOptionFactory : AIFactoryBase<AIOptionBase, AIOptionFactory>
    {
        public AIOptionFactory() : base()
        {
            AddConstructor(new AIOptionConstructor_Default());
        }
    }

    internal class AIOptionConstructor_Default : AIConstructorBase<AIOptionBase>
    {
        public override AIOptionBase Create(AICreationData creationData)
        {
            return creationData.ConstructObject<AIOption>();
        }
    }


}