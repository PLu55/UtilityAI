using System;
using System.Collections.Generic;

namespace PLu.ModularAI
{
    /// <summary>
    /// Represents a builder class for creating an AI specification.
    /// </summary>
  
    public class AISpecificationBuilder
    {
        // Note the difference between Begin nodes and Add nodes: Begin node have children nodes while Add node does not
        // Begin nodes pushes the new node to the stack while Add nodes do not. If the stack is empty Add nodes 
        // set them selfs as root if the partial flag is set.

        private string _name;
        private AISpecificationNode _root;
        private Stack<AISpecificationNode> _stack;
        private bool _check;
        private bool _firstPartial;
        private bool _isPartial;

        public AISpecificationBuilder(string name = "anonymous")
        {
            _name = name;
            _stack = new();
            _check = true;
            _firstPartial = false;
        }

        public AISpecification BuildAndStore(bool overwrite = false)
        {
            var specification = Build();
            AISpecificationManager.Instance.AddSpecification(specification, overwrite);
            return specification;
        }
        
        public AISpecification Build()
        {
            if (_stack.Count > 0)
            {
                throw new InvalidOperationException($"Build: {_stack.Count} nodes are still open, top of stack is {_stack.Peek()}, End node is missing or too many Begin nodes");
            }

            if (_root == null)
            {
                throw new InvalidOperationException("Build: no nodes to build");
            }
            return new AISpecification(_name, _root, _isPartial);
        }

        public AISpecificationBuilder Partial()
        {
            _isPartial = true;
            _firstPartial = true;
            _check = false;
            return this;
        }

        public AISpecificationBuilder End()
        {
            if (_check && _stack.Count == 0)
            {
                throw new InvalidOperationException("End: no nodes to close");
            }

            _stack.Pop();

            return this;
        }        
        
        // TODO: Reasoner builder should maybe be split by type
        public AISpecificationBuilder Reasoner(string reasonerType, string _abortOnInvalid = null)
        {
            if (_check && _stack.Count > 0 && _stack.Peek().Name != "Picker")
            {
                throw new InvalidOperationException("BeginReasoner: must be the top node in the specification");
            }

            var node = new AISpecificationNode("Reasoner", reasonerType);
            node.AddAttribute("AbortOnInvalid", _abortOnInvalid); // only used by Sequence Reasoner

            if (_stack.Count == 0)
            {
                _root = node;
            }
            else
            {
                _stack.Peek().AddChild(node);
            }

             _stack.Push(node);

            return this;
        }

        public AISpecificationBuilder Options()
        {
            if (_check && _stack.Peek().Name != "Reasoner")
            {
                throw new InvalidOperationException("BeginOptions: must be called within a Reasoner node");
            }

            var node = new AISpecificationNode("Options", "");
            SetRootOrChild(node);
            _stack.Push(node);

            return this;
        }

        public AISpecificationBuilder Option(string optionName)
        {
            if (_check && _stack.Peek().Name != "Options")
            {
                throw new InvalidOperationException("BeginOption: must be called within a Options node");
            }

            var node = new AISpecificationNode("Option", "");
            node.AddAttribute("Name", optionName);
            SetRootOrChild(node);
            _stack.Push(node);

            return this;
        }

        public AISpecificationBuilder SelfTarget()
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("SelfTarget: must be called within a Consideration node");
            }
            
            var node = new AISpecificationNode("Target", "Self");

            if (_stack.Count > 0)
            {
                _stack.Peek().AddChild(node);
            }

            if (_firstPartial)
            {
                _root = node;
            }

            return this;
        }
        public AISpecificationBuilder PositionTarget(string position = "0 0 0")
        {
            if (_check && 
                _stack.Peek().Name != "Consideration" &&
                _stack.Peek().Name != "Action")
            {
                throw new InvalidOperationException("PositionTarget: must be called within a Consideration or Action node");
            }
            
            var node = new AISpecificationNode("Target", "Position");
            node.AddAttribute("Position", position);

            if (_stack.Count > 0)
            {
                _stack.Peek().AddChild(node);
            }

            if (_firstPartial)
            {
                _root = node;
            }

            return this;
        }
        public AISpecificationBuilder PickerEntityTarget(string postfix = null)
        {
            if (_check && !(_stack.Peek().Name == "Consideration" || _stack.Peek().Name == "Action"))
            {
                throw new InvalidOperationException("PickerEntityTarget: must be called within a Consideration node");
            }
            
            var node = new AISpecificationNode("Target", "PickerEntity");
            node.AddAttribute("Postfix", postfix);

            if (_stack.Count > 0)
            {
                _stack.Peek().AddChild(node);
            }

            if (_firstPartial)
            {
                _root = node;
            }

            return this;
        }
        public AISpecificationBuilder DataStoreTarget(string dataPath)
        {
            if (_check && !(_stack.Peek().Name == "Consideration" || _stack.Peek().Name == "Action"))
            {
                throw new InvalidOperationException("DataStoreTarget: must be called within a Consideration or Action node");
            }
            
            var node = new AISpecificationNode("Target", "DataStore");
            node.AddAttribute("DataPath", dataPath);

            if (_stack.Count > 0)
            {
                _stack.Peek().AddChild(node);
            }

            if (_firstPartial)
            {
                _root = node;
            }

            return this;
        }        
        
                // public AISpecificationBuilder DataStoreTarget(string key = "Self", string dataStore = "Brain")
        // {
        //     if (_check && (_stack.Peek().Name != "Consideration" || _stack.Peek().Name != "Action"))
        //     {
        //         throw new InvalidOperationException("DataStoreTarget: must be called within a Consideration or Action node");
        //     }
            
        //     var node = new AISpecificationNode("Target", "DataStore");
        //     node.AddAttribute("DataStore", dataStore);
        //     node.AddAttribute("Key", key);

        //     if (_stack.Count > 0)
        //     {
        //         _stack.Peek().AddChild(node);
        //     }

        //     if (_firstPartial)
        //     {
        //         _root = node;
        //     }

        //     return this;
        // } 
        public AISpecificationBuilder GlobalTarget(string dataStore = "Brain", string key = "Self")
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("GlobalTarget: must be called within a Consideration node");
            }
            
            var node = new AISpecificationNode("Target", "DataStore");
            node.AddAttribute("DataStore", dataStore);
            node.AddAttribute("Key", key);

            if (_stack.Count > 0)
            {
                _stack.Peek().AddChild(node);
            }

            if (_firstPartial)
            {
                _root = node;
            }

            return this;
        }

        public AISpecificationBuilder Considerations(string type = "And", string addend = null, string multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Option")
            {
                throw new InvalidOperationException("Considerations: must be called within a Option node");
            }

            var node = new AISpecificationNode("Considerations", type);
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);
            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder BoolConsideration(string value, string addend = null, string  multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Considerations")
            {
                throw new InvalidOperationException("BoolConsideration: must be called within a Considerations node");
            }

            var node = new AISpecificationNode("Consideration", "Bool");
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);
            
            node.AddAttribute("Value", value);

            return this;
        }

        public AISpecificationBuilder HistoryConsideration(string type = "TimeSinceLastActivation", string since = null, string addend = null, string  multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Considerations")
            {
                throw new InvalidOperationException("HistoryConsideration: must be called within a Considerations node");
            }

            var node = new AISpecificationNode("Consideration", "History");
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);

            node.AddAttribute("Subtype", type);
            node.AddAttribute("Since", type);

            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder DistanceConsideration(string addend = null, string multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Considerations")
            {
                throw new InvalidOperationException("DistanceConsideration: must be called within a Considerations node");
            }

            var node = new AISpecificationNode("Consideration", "Distance");
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);

            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder VariableConsideration(string dataPath, string dataType, string addend = null, string  multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Considerations")
            {
                throw new InvalidOperationException("VariableConsideration: must be called within a Considerations node");
            }

            var node = new AISpecificationNode("Consideration", "Variable");
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);

            node.AddAttribute("DataType", dataType);
            node.AddAttribute("DataPath", dataPath);

            _stack.Push(node);

            return this;
        }

        public AISpecificationBuilder EntityExistsConsideration(string dataStore, string variable, string addend = null, string  multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Considerations")
            {
                throw new InvalidOperationException("EntityExistsConsideration: must be called within a Considerations node");
            }

            var node = new AISpecificationNode("Consideration", "EntityExists");
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);

            node.AddAttribute("DataStore", dataStore);
            node.AddAttribute("Variable", variable);

            _stack.Push(node);

            return this;
        }

        public AISpecificationBuilder BoolWeightFunction()
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("BoolWeightFunction: must be called within a Consideration node");
            }

            var node = new AISpecificationNode("WeightFunction", "Bool");
            SetRootOrChild(node);
            
            return this;
        }

        public AISpecificationBuilder Entries()
        {
            if (_check && _stack.Peek().Name != "StringWeightFunction")
            {
                throw new InvalidOperationException("Entries: must be called within a StringWeightFunction node");
            }

            var node = new AISpecificationNode("Entries", "");
            SetRootOrChild(node);

            _stack.Push(node);

            return this;
        }

        //public AISpecificationBuilder Entry(string key, string value, string veto = "true", string valueType = "string")
        public AISpecificationBuilder String(string value = "", string veto = "true")
        {
            if (_check && _stack.Peek().Name != "Entries")
            {
                throw new InvalidOperationException("Entry: must be called within a Entries node");
            }

            // switch (valueType)
            // {
            //     case "string":
            //         break;
            //     case "float":
            //         break;
            //     case "double":
            //         break;
            //     case "int":
            //         break;
            //     case "bool":
            //         break;
            //     default:
            //         throw new InvalidOperationException("Entry: valueType must be string, float, int or bool");
            // }

            var node = new AISpecificationNode("String", "");
            SetRootOrChild(node);

            node.AddAttribute("Veto", veto);
            node.AddAttribute("Value", value);

            return this;
        }

        public AISpecificationBuilder Default(string veto = "true")
        {
            if (_check && _stack.Peek().Name != "WeightFunction")
            {
                throw new InvalidOperationException("Default: must be called within a Weight node");
            }

            var node = new AISpecificationNode("Default", "");
            SetRootOrChild(node);

            node.AddAttribute("Veto", veto);

            return this;
        }
        public AISpecificationBuilder StringWeightFunction()
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("StringWeightFunction: must be called within a Consideration node");
            }

            var node = new AISpecificationNode("WeightFunction", "String");
            SetRootOrChild(node);

            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder IntWeightFunction(string @operator = "eq", string value = "0")
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("IntWeightFunction: must be called within a Consideration node");
            }

            var node = new AISpecificationNode("WeightFunction", "Integer");
            SetRootOrChild(node);
            node.AddAttribute("Operator", @operator);
            node.AddAttribute("Value", value);

            return this;
        }
        public AISpecificationBuilder FloatWeightFunction(string @operator = "eq", string value = "0")
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("FloatWeightFunction: must be called within a Consideration node");
            }

            var node = new AISpecificationNode("WeightFunction", "Float");
            SetRootOrChild(node);
            node.AddAttribute("Operator", @operator);
            node.AddAttribute("Value", value);

            return this;
        }        
        
        public AISpecificationBuilder DoubleWeightFunction(string @operator = "eq", string value = "0")
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("DoubleWeightFunction: must be called within a Consideration node");
            }

            var node = new AISpecificationNode("WeightFunction", "Double");
            SetRootOrChild(node);
            node.AddAttribute("Operator", @operator);
            node.AddAttribute("Value", value);

            return this;
        }
        public AISpecificationBuilder ConstantWeightFunction(string addend = null, string  multiplier = null, string rank = null)
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("ConstantWeightFunction: must be called within a Consideration node");
            }

            var node = new AISpecificationNode("WeightFunction", "Constant");
            ConsiderationDefaults(node, addend, multiplier, rank);
            SetRootOrChild(node);

            return this;
        }
        
        public AISpecificationBuilder CurveWeightFunction(string curveType, string exponent = "1", string slope = "1", string xScale = null, string xShift = null, string yShift = null, string valueType = "float")
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("CurveWeightFunction: must be called within a Consideration node");
            }
            AISpecificationNode node;

            if (valueType == "float")
            {
                node = new AISpecificationNode("WeightFunction", "Curve");
            }
            else if (valueType == "double")
            {
                node = new AISpecificationNode("WeightFunction", "DoubleCurve");
            }
            else
            {
                throw new InvalidOperationException("WeightFunction: valueType must be float or double");
            }

            SetRootOrChild(node);

            node.AddAttribute("Subtype", curveType);
            node.AddAttribute("Exponent", exponent);
            node.AddAttribute("Slope", slope);
            node.AddAttribute("XScale", xScale);
            node.AddAttribute("XShift", xShift);
            node.AddAttribute("YShift", yShift);

            return this;
        }
        public AISpecificationBuilder Actions()
        {
            if (_check && _stack.Peek().Name != "Option")
            {
                throw new InvalidOperationException("Actions: must be called within a Option node");
            }

            var node = new AISpecificationNode("Actions", "");
            SetRootOrChild(node);

            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder SubReasonerAction()
        {   
            if (_check && _stack.Peek().Name != "Actions")
            {
                throw new InvalidOperationException("SubReasonerAction: must be called within a Actions node");
            }            
            
            var node = new AISpecificationNode("Action", "SubReasoner");
            SetRootOrChild(node);
            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder GoToAction()
        {   
            if (_check && _stack.Peek().Name != "Actions")
            {
                throw new InvalidOperationException("GoToAction: must be called within a Actions node");
            }            
            
            var node = new AISpecificationNode("Action", "GoTo");
            SetRootOrChild(node);
            _stack.Push(node);

            return this;
        }
        public AISpecificationBuilder LogAction(string pattern, string type)
        {   
            if (_check && _stack.Peek().Name != "Actions")
            {
                throw new InvalidOperationException("LogAction: must be called within a Actions node");
            }            
            
            var node = new AISpecificationNode("Action", "Log");
            SetRootOrChild(node);
            node.AddAttribute("Pattern", pattern);
            node.AddAttribute("Type", type);
            
            return this;
        }
        public AISpecificationBuilder WaitAction(string duration)
        {   
            if (_check && _stack.Peek().Name != "Actions")
            {
                throw new InvalidOperationException("WaitAction: must be called within a Actions node");
            }            
            
            var node = new AISpecificationNode("Action", "Wait");
            SetRootOrChild(node);
            node.AddAttribute("Duration", duration);

            return this;
        }
        public AISpecificationBuilder SetVariableAction(string dataPath, string value = null, string variableType = null)
        {
            if (_check && _stack.Peek().Name != "Actions")
            {
                throw new InvalidOperationException("SetVariableAction: must be called within a Actions node");
            }

            var node = new AISpecificationNode("Action", "SetVariable");
            SetRootOrChild(node);
            
            node.AddAttribute("Value", value);
            node.AddAttribute("VariableType", variableType);
            node.AddAttribute("DataPath", dataPath);

            if (value == null)
            {
                _stack.Push(node);
            }
            
            return this;
        }

        public AISpecificationBuilder Picker(string type = "Entity", string variable = "PickerEntity")
        {
            if (_check && _stack.Peek().Name != "Consideration")
            {
                throw new InvalidOperationException("Picker: must be the a child of a Consideration node");
            }
            
            var node = new AISpecificationNode("Picker", type);
            SetRootOrChild(node);
            
            node.AddAttribute("Variable", variable);

            _stack.Push(node);

            return this;
        }

        private void ConsiderationDefaults(AISpecificationNode node, string addend, string multiplier, string rank)
        {
                node?.AddAttribute("Addend", addend);
                node?.AddAttribute("Multiplier", multiplier);
                node?.AddAttribute("Rank", rank);
        }

        private void SetRootOrChild(AISpecificationNode node)
        {
            if (_firstPartial)
            {
                _root = node;
                _firstPartial = false;
            }
            else if (_stack.Count > 0)
            {
                _stack.Peek().AddChild(node);
            }
            else
            {
                throw new InvalidOperationException("RootOrChild: not first partial node or stack is empty");
            }
        }
    }
}