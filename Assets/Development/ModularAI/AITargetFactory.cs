namespace PLu.ModularAI
{
    public class AITargetFactory : AIFactoryBase<AITargetBase, AITargetFactory>
    {
        public AITargetFactory() : base()
        {
            AddConstructor(new AITargetConstructor_Default());
        }
    }

    public class AITargetConstructor_Default : AIConstructorBase<AITargetBase>
    {
        public override AITargetBase Create(AICreationData creationData)
        {
            switch (creationData.AISpecificationNode.Type)
            {
                case "Position":
                    return creationData.ConstructObject<AIPositionTarget>();
                case "Self":
                    return creationData.ConstructObject<AITarget>();
                case "PickerEntity":
                case "DataStore":
                    return creationData.ConstructObject<AIDataStoreTarget>();
            }
            return null;
        }
    }
}
