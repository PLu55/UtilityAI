using System;

namespace PLu.ModularAI
{        
    // TODO: Move the functionality of AIFloatSequenceWeightFunction to this class

    public class AIFloatWeightFunction : AIWeightFunctionBase
    {
        Func<float, bool> _compare;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            if (creationData.AISpecificationNode.TryGetAttribute("Operator", out string operatorStr) &&
                creationData.AISpecificationNode.TryGetAttribute("Value", out string valueStr))
            {
                float value = float.Parse(valueStr);

                _compare = CreateCompareFunction(operatorStr, value);

                if (_compare == null)
                {
                    AILogger.LogError("AIFloatWeightFunction: Invalid Operator: " + operatorStr);
                    return false;
                }
            }
            else
            {
                AILogger.LogError("AIFloatWeightFunction: Invalid Operator or Value");
                return false;
            }

            return true;
        }
        public override AIWeightValues Compute(float value)
        {
            AIWeightValues weightValues = new AIWeightValues
            {
                Addend = 0f,
                Multiplier = _compare(value) ? 1f : 0f,
                Rank = -float.MaxValue
            };

            return weightValues;
        }

        private Func<float, bool> CreateCompareFunction(string op, float value)
        {
            switch (op)
            {
                case "eq":
                    return (float val) => val == value;
                case "neq":
                    return (float val) => val != value;
                case "lt":
                    return (float val) => val < value;
                case "lte":
                    return (float val) => val <= value;
                case "gt":
                    return (float val) => val > value;
                case "gte":
                    return (float val) => val >= value;
                default:
                    AILogger.LogError("AIFloatWeightFunction: Invalid Operator: " + op);
                    return null;
            }
        }
    }  
  
}

