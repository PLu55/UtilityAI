using System.Collections.Generic;

namespace PLu.ModularAI
{
    public class AIStringWeightFunction : AIWeightFunctionBase
    {        
        private Dictionary<string, bool> _entries;
        private bool _defaultVeto;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            _entries = new();

            if (creationData.AISpecificationNode.TryGetChild("Entries", out AISpecificationNode entriesNode))
            {
                foreach (var entry in entriesNode.Children)
                {
                    if (entry.TryGetAttribute("Value", out string value) &&
                        entry.Name == "String")
                    {
                        entry.TryGetAttribute("Veto", out string vetoValue);
                        if (vetoValue == "true")
                        {
                            _entries[value] = true;
                            
                        }
                        else if (vetoValue == "false")
                        {
                            _entries[value] = false;
                        }
                        else
                        {   
                            AILogger.LogError("AIStringWeightFunction: Invalid value for Veto attribute: " + vetoValue);
                            return false;
                        }
                    }
                    else
                    {
                        AILogger.LogError($"AIStringWeightFunction: Invalid Entry {entry}");
                        return false;
                    }
                }
            }
            else
            {
                AILogger.LogError("AIStringWeightFunction: No Entries found");
                return false;
            }

            if (creationData.AISpecificationNode.TryGetChild("Default", out AISpecificationNode defaultNode))
            {
                if (defaultNode.TryGetAttribute("Veto", out string defaultValue))
                {
                    if (defaultValue == "true")
                    {
                        _defaultVeto = true;
                    }
                    else if (defaultValue == "false")
                    {
                        _defaultVeto = false;
                    }
                    else
                    {
                        AILogger.LogError("AIStringWeightFunction: Invalid value for Default attribute: " + defaultValue);
                        return false;
                    }
                }
                else
                {
                    AILogger.LogError("AIStringWeightFunction: No Default found");
                    return false;
                }
            }

            return true;
        }
        
        public override AIWeightValues Compute(string value)
        {

            bool veto = _entries.TryGetValue(value, out bool v) ? v : _defaultVeto;
            return new AIWeightValues
            {
                Addend = 0f,
                Multiplier = veto ? 0f : 1f,
                Rank = -float.MaxValue
            };
        }
    }
}

