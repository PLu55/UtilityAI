//using UnityEngine;

using System;

namespace PLu.ModularAI
{
    public enum AICurveType
    {
        Linear,
        Exponential,
        Polynomial,
        Binary,
        Logistic,
        Logit,
        Normal,
        Sine
    }

    public class AICurveWeightFunction : AIWeightFunctionBase
    {

        private float _exponent;
        private float _slope;
        private float _xShift;
        private float _xScale;
        private float _yShift;
        private AICurveType _type;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData) ||
                !creationData.AISpecificationNode.TryGetAttribute("Subtype", out string subtype))
            {
                return false;
            }

            switch (subtype)
            {
                case "Linear":
                    _type = AICurveType.Linear;
                    break;
                case "Exponential":
                    _type = AICurveType.Exponential;
                    break;
                case "Polynomial":
                    _type = AICurveType.Polynomial;
                    break;
                case "Binary":
                    _type = AICurveType.Binary;
                    break;
                case "Logistic":
                    _type = AICurveType.Logistic;
                    break;                
                case "Logit":
                    _type = AICurveType.Logit;
                    break;
                case "Normal":
                    _type = AICurveType.Normal;
                    break;
                case "Sine":
                    _type = AICurveType.Sine;
                    break;
                default:
                    return false;
            }

            creationData.AISpecificationNode.TryGetAttribute("Exponent", out _exponent, 1f);
            creationData.AISpecificationNode.TryGetAttribute("Slope", out _slope, 1f);
            creationData.AISpecificationNode.TryGetAttribute("XScale", out _xScale, 1f);
            creationData.AISpecificationNode.TryGetAttribute("XShift", out _xShift, 0f);
            creationData.AISpecificationNode.TryGetAttribute("YShift", out _yShift, 0f);

            return true;
        }

        public override AIWeightValues Compute(float x)
        {
            switch (_type)
            {
                case AICurveType.Linear:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = Clamp01(_slope * (x * _xScale - _xShift) + _yShift),
                        Rank = -float.MaxValue
                    };
                case AICurveType.Polynomial:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = Clamp01(_slope * MathF.Pow(x * _xScale - _xShift, _exponent) + _yShift),
                        Rank = -float.MaxValue
                    };
                case AICurveType.Exponential:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier= _exponent * MathF.Pow(_xShift, x * _xScale) + _slope, //????
                        Rank = -float.MaxValue
                    };
                case AICurveType.Binary:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = Clamp01(x * _xScale >= _xShift ? _slope + _yShift : _yShift),
                        Rank = -float.MaxValue
                    };
                case AICurveType.Logistic:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = Clamp01(( _slope / (1f + MathF.Exp(-10f * _exponent * (x * _xScale - 0.5f - _xShift)))) + _yShift),
                        Rank = -float.MaxValue
                    };                
                    
                case AICurveType.Logit:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = Clamp01( _slope * MathF.Log((x - _xShift) / (1f - (x * _xScale - _xShift))) / 5f + 0.5f + _yShift), 
                        Rank = -float.MaxValue
                    };                
                    
                case AICurveType.Normal:
                    return new AIWeightValues
                    {   
                        Addend = 0f,
                        Multiplier = Clamp01(_slope * MathF.Exp(-30.0f * _exponent * (x * _xScale - _xShift - 0.5f) * (x - _xShift - 0.5f)) + _yShift),
                        Rank = -float.MaxValue
                    };

                case AICurveType.Sine:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = Clamp01(0.5f * _slope * MathF.Sin(2.0f * MathF.PI * (x * _xScale - _xShift)) + 0.5f + _yShift),
                        Rank = -float.MaxValue
                    };
            
                default:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = 0f,
                        Rank = -float.MaxValue
                    };
            }
        }
        protected float Clamp01(float value)
        {
            return MathF.Max(0f, MathF.Min(1f, value));
        }
    }

}

