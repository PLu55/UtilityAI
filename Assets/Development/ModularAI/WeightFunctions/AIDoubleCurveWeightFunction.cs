//using UnityEngine;

using System;

namespace PLu.ModularAI
{
    public class AIDoubleCurveWeightFunction : AIWeightFunctionBase
    {    
        private double _exponent;
        private double _slope;
        private double _xScale;
        private double _xShift;
        private double _yShift;
        private AICurveType _type;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData) ||
                !creationData.AISpecificationNode.TryGetAttribute("Subtype", out string subtype))
            {
                return false;
            }

            switch (subtype)
            {
                case "Linear":
                    _type = AICurveType.Linear;
                    break;
                case "Exponential":
                    _type = AICurveType.Exponential;
                    break;
                case "Polynomial":
                    _type = AICurveType.Polynomial;
                    break;
                case "Binary":
                    _type = AICurveType.Binary;
                    break;
                case "Logistic":
                    _type = AICurveType.Logistic;
                    break;                
                case "Logit":
                    _type = AICurveType.Logit;
                    break;
                case "Normal":
                    _type = AICurveType.Normal;
                    break;
                case "Sine":
                    _type = AICurveType.Sine;
                    break;
                default:
                    return false;
            }

            creationData.AISpecificationNode.TryGetAttribute("Exponent", out _exponent, 1f);
            creationData.AISpecificationNode.TryGetAttribute("Slope", out _slope, 1f);
            creationData.AISpecificationNode.TryGetAttribute("XScale", out _xScale, 1f);
            creationData.AISpecificationNode.TryGetAttribute("XShift", out _xShift, 0f);
            creationData.AISpecificationNode.TryGetAttribute("YShift", out _yShift, 0f);

            return true;
        }

        public override AIWeightValues Compute(float x)
        {
            switch (_type)
            {
                case AICurveType.Linear:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)Clamp01(_slope * (x * _xScale - _xShift) + _yShift),
                        Rank = -float.MaxValue
                    };
                case AICurveType.Polynomial:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)Clamp01(_slope * Math.Pow(x * _xScale - _xShift, _exponent) + _yShift),
                        Rank = -float.MaxValue
                    };
                case AICurveType.Exponential:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)(_exponent * Math.Pow(_xShift, x * _xScale) + _slope), //????
                        Rank = -float.MaxValue
                    };
                case AICurveType.Binary:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)Clamp01(x * _xScale >= _xShift ? _slope + _yShift : _yShift),
                        Rank = -float.MaxValue
                    };
                case AICurveType.Logistic:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)Clamp01(( _slope / (1f + Math.Exp(-10 * _exponent * (x * _xScale - 0.5 - _xShift)))) + _yShift),
                        Rank = -float.MaxValue
                    };                
                    
                case AICurveType.Logit:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)Clamp01( _slope * Math.Log((x - _xShift) / (1d - (x * _xScale - _xShift))) / 5d + 0.5 + _yShift), 
                        Rank = -float.MaxValue
                    };                
                    
                case AICurveType.Normal:
                    return new AIWeightValues
                    {   
                        Addend = 0f,
                        Multiplier = (float)Clamp01(_slope * Math.Exp(-30.0 * _exponent * (x * _xScale - _xShift - 0.5) * (x - _xShift - 0.5)) + _yShift),
                        Rank = -float.MaxValue
                    };

                case AICurveType.Sine:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = (float)Clamp01(0.5f * _slope * Math.Sin(2.0f * MathF.PI * (x * _xScale - _xShift)) + 0.5 + _yShift),
                        Rank = -float.MaxValue
                    };
            
                default:
                    return new AIWeightValues
                    {
                        Addend = 0f,
                        Multiplier = 0f,
                        Rank = -float.MaxValue
                    };
            }
        }
        protected double Clamp01(double value)
        {
            return Math.Max(0d, Math.Min(1d, value));
        }
    }

}

