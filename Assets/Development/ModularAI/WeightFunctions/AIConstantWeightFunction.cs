//using UnityEngine;

namespace PLu.ModularAI
{
    public class AIConstantWeightFunction : AIWeightFunctionBase
    {
        private float _defaultAddend;
        private float _defaultMultiplier;
        private float _defaultRank;
        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            creationData.AISpecificationNode.TryGetAttribute("Addend", out _defaultAddend, 0f);
            creationData.AISpecificationNode.TryGetAttribute("Multiplier", out _defaultMultiplier, 1f);
            creationData.AISpecificationNode.TryGetAttribute("Rank", out _defaultRank, -float.MaxValue); 
            
            return true;
        }

        public override AIWeightValues Compute(string value) => Compute();
        public override AIWeightValues Compute(float value) => Compute();
        public override AIWeightValues Compute(double value) => Compute();
        public override AIWeightValues Compute(bool value) => Compute();
        public override AIWeightValues Compute(int value) => Compute();
        private AIWeightValues Compute()
        {
            return new AIWeightValues
            {
                Addend = _defaultAddend,
                Multiplier = _defaultMultiplier,
                Rank = _defaultRank
            };
        }
    }
}

