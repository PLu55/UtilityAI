namespace PLu.ModularAI
{
    public class AIDoubleSequenceWeightFunction : AIWeightFunctionBase
    {
        private bool _defaultVeto;
        private (double, bool)[] _entries;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }
                         
            if (creationData.AISpecificationNode.TryGetChild("Entries", out AISpecificationNode entriesNode))
            {
                _entries = new (double, bool)[entriesNode.Children.Count];

                for (int i = 0; i < entriesNode.Children.Count; i++)
                {
                    var entry = entriesNode.Children[i];

                    if (entry.Name == "Double" && 
                        entry.TryGetAttribute("Value", out string valueStr) &&
                        entry.TryGetAttribute("Veto", out string vetoStr))
                    {
                        double value = double.Parse(valueStr);
                        bool veto = bool.Parse(vetoStr);

                        _entries[i] = (value, veto);
                    }                
                    else
                    {
                    AILogger.LogError("AIIntSequenceWeightFunction: Invalid Entry " + entry);
                    return false;
                    }
                }
            }
            else
            {
                AILogger.LogError("AIIntWeightFunction: No Entries found");
                return false;
            }

            _defaultVeto = true;

            if (creationData.AISpecificationNode.TryGetChild("Default", out AISpecificationNode defaultNode))
            {
                if (defaultNode.TryGetAttribute("Veto", out string vetoStr))
                {
                    _defaultVeto = bool.Parse(vetoStr);
                }
                else
                {
                    AILogger.LogError("AIIntSequenceWeightFunction: Invalid Default Entry " + entriesNode);
                    return false;
                }
            }

            return true;
        }
        public override AIWeightValues Compute(double value)
        {
            // TODO: Fix this computation!
            bool veto = _defaultVeto;

            for (int i = _entries.Length - 1; i >= 0; i--)
            {
                if (value >= _entries[i].Item1)
                {
                    veto = _entries[i].Item2;
                    break;
                }
            }

            return new AIWeightValues
            {
                Addend = 0f,
                Multiplier = veto ? 0f : 1f,
                Rank = -float.MaxValue
            };
        }
    }  
}

