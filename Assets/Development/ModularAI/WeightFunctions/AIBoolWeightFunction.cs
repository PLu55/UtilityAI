//using UnityEngine;

namespace PLu.ModularAI
{
    public class AIBoolWeightFunction : AIWeightFunctionBase
    {
        public override AIWeightValues Compute(bool value)
        {
            return new AIWeightValues
            {
                Addend = 0f,
                Multiplier = value ? 1f  : 0f,
                Rank = -float.MaxValue
            };
        }
        public override AIWeightValues Compute(int value)
        {
            return Compute(value > 0);
        }        
        public override AIWeightValues Compute(float value)
        {
            return Compute(value > 0);
        }
        public override AIWeightValues Compute(double value)
        {
            return Compute(value > 0);
        }
        public override AIWeightValues Compute(string value)
        {
            return Compute(value == "true");
        }
    }
}

