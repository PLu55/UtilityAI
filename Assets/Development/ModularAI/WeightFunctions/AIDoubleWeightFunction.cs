using System;

namespace PLu.ModularAI
{
    public class AIDoubleWeightFunction : AIWeightFunctionBase
    {
        // TODO: Move the functionality of AIDoubleSequenceWeightFunction to this class
        //public override string NodeName => "DoubleWeightFunction";

        Func<double, bool> _compare;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }
            if (creationData.AISpecificationNode.TryGetAttribute("Operator", out string operatorStr) &&
                creationData.AISpecificationNode.TryGetAttribute("Value", out string valueStr))
            {
                double value = double.Parse(valueStr);

                _compare = CreateCompareFunction(operatorStr, value);

                if (_compare == null)
                {
                    throw new ArgumentException($"Invalid operator: {operatorStr} ");
                }
            }
            else
            {
                throw new ArgumentException($"Missing arguments: Operator or/and Value");
            }

            return true;
        }
        public override AIWeightValues Compute(double value)
        {
            return new AIWeightValues
            {
                Addend = 0f,
                Multiplier = _compare(value) ? 1f : 0f,
                Rank = -float.MaxValue
            };
        }
        private Func<double, bool> CreateCompareFunction(string op, double value)
        {
            switch (op)
            {
                case "eq":
                    return (double val) => val == value;
                case "neq":
                    return (double val) => val != value;
                case "lt":
                    return (double val) => val < value;
                case "lte":
                    return (double val) => val <= value;
                case "gt":
                    return (double val) => val > value;
                case "gte":
                    return (double val) => val >= value;
                default:
                    throw new ArgumentException($"Invalid Operator: {op}");
            }
        }
    }    
  
}

