using System;
using System.Collections.Generic;

namespace PLu.ModularAI
{
    public class AIIntWeightFunction : AIWeightFunctionBase
    {
        Func<int, bool> _compare;

        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }
                         
            if (creationData.AISpecificationNode.TryGetAttribute("Operator", out string operatorStr) &&
                creationData.AISpecificationNode.TryGetAttribute("Value", out string valueStr))
            {
                int value = int.Parse(valueStr);

                _compare = CreateCompareFunction(operatorStr, value);

                if (_compare == null)
                {
                    AILogger.LogError("AIDoubleWeightFunction: Invalid Operator: " + operatorStr);
                    return false;
                }
            }
            else
            {
                AILogger.LogError("AIDoubleWeightFunction: Invalid Operator or Value");
                return false;
            }

            return true;
        }

        public override AIWeightValues Compute(int value)
        {
            return new AIWeightValues
            {
                Addend = 0f,
                Multiplier = _compare(value) ? 1f : 0f,
                Rank = -float.MaxValue
            };
        }

        private Func<int, bool> CreateCompareFunction(string op, int value)
        {
            switch (op)
            {
                case "eq":
                    return (int val) => val == value;
                case "neq":
                    return (int val) => val != value;
                case "lt":
                    return (int val) => val < value;
                case "lte":
                    return (int val) => val <= value;
                case "gt":
                    return (int val) => val > value;
                case "gte":
                    return (int val) => val >= value;
                default:
                    AILogger.LogError("AIIntWeightFunction: Invalid Operator: " + op);
                    return null;
            }
        }
    }    
}

