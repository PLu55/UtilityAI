//using UnityEngine;

using Codice.CM.SEIDInfo;

namespace PLu.ModularAI
{
    public struct AIWeightValues
    {
        public float Addend;
        public float Multiplier;
        public float Rank;

        public override string ToString()
        {
            return base.ToString() + "{ Addend: " + Addend + " Multiplier: " + Multiplier + " Rank: " + Rank + "}";
        }
    }
    public abstract class 
    AIWeightFunctionBase : IAIAbstraction
    {
        public virtual string NodeName => "WeightFunction";

        public virtual bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.Name != NodeName)
            {
                return false;
            }
            return true;
        }
        public virtual AIWeightValues Compute(bool value)
        {
            throw new System.ArgumentException("Unexpected type of argument, bool ");
        }

        public virtual AIWeightValues Compute(float value)
        {
            throw new System.ArgumentException("Unexpected type of argument, float ");
        }

        public virtual AIWeightValues Compute(double value)
        {
            throw new System.ArgumentException("Unexpected type of argument, double ");
        }

        public virtual AIWeightValues Compute(int value)
        {
            throw new System.ArgumentException("Unexpected type of argument, int ");
        }

        public virtual AIWeightValues Compute(string value)
        {
            throw new System.ArgumentException("Unexpected type of argument, string ");
        }
    }

    public class AIWeightFunctionFactory : AIFactoryBase<AIWeightFunctionBase, AIWeightFunctionFactory>
    {
        public AIWeightFunctionFactory() : base()
        {
            AddConstructor(new AIWeightFunctionConstructor_Default());
        }
    }

    internal class AIWeightFunctionConstructor_Default : AIConstructorBase<AIWeightFunctionBase>
    {
        public override AIWeightFunctionBase Create(AICreationData creationData)
        {
            switch (creationData.AISpecificationNode.Type)
            {
                case "Boolean":
                case "Bool":
                    return creationData.ConstructObject<AIBoolWeightFunction>();
                case "Float":
                    return creationData.ConstructObject<AIFloatWeightFunction>();
                case "FloatSequence":
                    return creationData.ConstructObject<AIFloatSequenceWeightFunction>();
                case "Double":
                    return creationData.ConstructObject<AIDoubleWeightFunction>();
                case "DoubleSequence":
                    return creationData.ConstructObject<AIDoubleSequenceWeightFunction>();
                case "Integer":
                case "Int":
                    return creationData.ConstructObject<AIIntWeightFunction>();
                case "IntSequence":
                case "IntegerSequence":
                    return creationData.ConstructObject<AIIntSequenceWeightFunction>();
                case "String":
                    return creationData.ConstructObject<AIStringWeightFunction>();
                case "Constant":
                    return creationData.ConstructObject<AIConstantWeightFunction>();
                case "Curve":
                    return creationData.ConstructObject<AICurveWeightFunction>();
                case "DoubleCurve":
                    return creationData.ConstructObject<AIDoubleCurveWeightFunction>();
            }
            return null;
        }
    }
}

