using System;

namespace PLu.ModularAI
{
    // This reasoner will perform it's options in sequence. Each option may 
    // or may not have a considerations, if it has all the considerations must 
    // have a positive weight for the actions to be executed. Considerations
    // of an option are evaluated just before it's actions are executed.
    // The Reasoner is considered valid if at least one option is valid.
    // TODO: Implement a mode where all options are considered before any
    //       action is taken. This is the default behavior of the AIReasonerBase.
    // TODO: Implement a mode where options are executed in order until one
    //       is unsuccessful.
    // TODO: Implement a mode where options are executed in parallel.
    public class AISequenceReasoner : AIReasonerBase
    {

        public override bool IsValid => _isValid;
        private bool _isValid = false;
        private IAIOption _currentOption;
        private int _currentOptionIndex = -1;
        private bool _abortOnInvalid = false;
        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }

            if (creationData.AISpecificationNode.TryGetAttribute("AbortOnInvalid", out string abortOnInvalid))
            {
                if (abortOnInvalid == "AbortOnInvalid")
                {
                     _abortOnInvalid = true;
                }
                else
                {
                    throw new AIUnexpectedAttributeException($"AISequenceReasoner.Initialize: AbortOnInvalid attribute is invalid {abortOnInvalid}.");
                }
            }

            return true;
        }

        public override void Consider() {}
        // {
        //     SelectedOption = null;
        //     foreach (var option in _options)
        //     {
        //         option.Consider();
        //     }
        // }
  
        public override void SelectBestOption() {}

        public override void Reset() 
        {
            base.Reset();
            _currentOptionIndex = -1;
            _isValid = false;
        }
        public override void Think() {}


        // TODO: Implement the Tick method. When should the consideration be done?
        //       The options should be considered and then acted upon before moving on
        //       to the next option. This is different from the default implementation 
        //       where all options are considered before any action is taken.
        //       Maybe there should be an option to consider all options before acting?
        public override void Tick()
        {
            if (!IsEnabled)
            {
                return;
            }

            IsDone = false;
            _isValid = false;
            SelectedOption = null;
            //Sense();
            //Think();
            Act();
        } 
        public override void Act() 
        {
            // We have no options to act on.
            if (_options.Count == 0)
            {
                AILogger.LogWarning("AISequenceReasoner.Act: No options to act on.");
                IsDone = true;
                return;
            }

            // If we have no current option, find the first valid option.
            if (_currentOptionIndex == -1)
            {
                AILogger.Log("   ... no current option");
                _currentOptionIndex = 0;
            }

            // Update remaining options until there are no more valid option or
            // an option is not done, this will then be set as the current option.
            while (_currentOptionIndex < _options.Count)
            {
                AILogger.Log($"   ... index: {_currentOptionIndex}");
                _options[_currentOptionIndex].Consider();

                if (_options[_currentOptionIndex].Weight > 0)
                {
                    // This will leave the last selected option i to indicate a valid option
                    _isValid = true;

                    // Update the option, if done select the next, if not set IsDone to false.

                    if (!_options[_currentOptionIndex].Update())
                    {

                        AILogger.Log($"   ... option is not done");
                        IsDone = false;
                        return;
                    }
                }
                else if (_abortOnInvalid)
                {
                    IsDone = true;
                    _currentOptionIndex = -1;
                    AILogger.Log($"   ... option is not valid, abort flag is set, we are done");  
                    return;
                }

                _currentOptionIndex++;
            }
            
            AILogger.Log("   ... all options are done");
            IsDone = true;
            _currentOptionIndex = -1;
        }
    }
}
