using System;
using UnityEngine;

namespace PLu.ModularAI
{
    // This reasoner will walk through it's options and perform the first one that is valid, 
    // that is the first option that has a positive consideration.
    public class AISelectionReasoner : AIReasonerBase
    {
        public override bool Initialize(AICreationData creationData)
        {
            return base.Initialize(creationData);
        }
        public override void Consider() 
        {
            SelectedOption = null;
            foreach (var option in _options)
            {
                option.Consider();
                if (option.Weight > 0)
                {
                    SelectedOption = option;
                    break;
                }
            }
        }
        
        public override void SelectBestOption() {}

    }
}
