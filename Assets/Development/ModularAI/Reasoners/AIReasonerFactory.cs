namespace PLu.ModularAI
{
    public class AIReasonerFactory : AIFactoryBase<AIReasonerBase, AIReasonerFactory>
    {
        public AIReasonerFactory()
        {
            AddConstructor(new AIReasonerConstructor_Default());
        }

    }

    public class AIReasonerConstructor_Default : AIConstructorBase<AIReasonerBase>
    {
        public override AIReasonerBase Create(AICreationData creationData)
        { 
            AIReasonerBase reasoner = null;
            
            if (creationData.AISpecificationNode.Name != "Reasoner")
            {
                return null;
            }
            
            if (creationData.AISpecificationNode.Type == "Selection")
            {
                reasoner = creationData.ConstructObject<AISelectionReasoner>();
            }
            else if (creationData.AISpecificationNode.Type == "Sequence")
            {
                reasoner = creationData.ConstructObject<AISequenceReasoner>();
            }
            else if (creationData.AISpecificationNode.Type == "DualUtility")
            {
                reasoner = creationData.ConstructObject<AIDualUtilityReasoner>();
            }

            return reasoner;
        }
    }
}
