using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PLu.ModularAI
{
    public class AIDualUtilityReasoner : AIReasonerBase
    {
        private float _rankSlotSize;


        public override bool Initialize(AICreationData creationData)
        {
            if (!base.Initialize(creationData))
            {
                return false;
            }
            
            _rankSlotSize = 0f;

            if (creationData.AISpecificationNode.TryGetAttribute("RankSlotSize", out string rankFactorStr))
            {
                _rankSlotSize = float.Parse(rankFactorStr);
            }
            return true;
        }

        public override void Consider()
        {
            AILogger.Log("AIDualUtilityReasoner: Consider");
            foreach (IAIOption option in _options)
            {
                option.Consider();
            }
        }
        public override void SelectBestOption()
        {
            foreach (IAIOption option in _options)
            {
                AILogger.Log($"AIDualUtilityReasoner.SelectBestOption: {option.Weight} {option.Rank} {option.Name}");
            }

            List<IAIOption> remainingOptions = _options.Where(option => option.Weight > 0f).ToList();
            float maxRank = -float.MaxValue;
            AILogger.Log($"AIDualUtilityReasoner: SelectBestOption, remainingOptions: {remainingOptions.Count}");

            foreach (IAIOption option in remainingOptions)
            {
                if (option.Rank > maxRank)
                {
                    maxRank = option.Rank;
                }
            }
            
            IAIOption highestOption = null;

            float maxWeight = -float.MaxValue;

            foreach (IAIOption option in remainingOptions)
            {
                if (option.Rank >= maxRank - _rankSlotSize)
                {
                    if (option.Weight > maxWeight)
                    {
                        maxWeight = option.Weight;
                        highestOption = option;
                    }
                }
            }

            SelectedOption = highestOption;

            AILogger.Log($"AIDualUtilityReasoner: SelectBestOption, SelectedOption: {SelectedOption?.Name}");
            return;  
        }
    }
}
