using System.Collections.Generic;

namespace PLu.ModularAI
{
    public abstract class AIReasonerBase : IAIAbstraction
    {
        public string NodeName => "Reasoner"; 
        public bool IsEnabled { get; protected set; }
        public bool IsDone { get; protected set; }
        public virtual bool IsValid => SelectedOption != null;

        public List<IAIOption> Options => _options;
        public void SetOptions(List<IAIOption> options) => _options = options;
        public void Enable() => IsEnabled = true;
        public void Disable() => IsEnabled = false;
        public bool HasSelectedOption => SelectedOption != null;
        public IAIOption SelectedOption { get; protected set; }
        //public AIOptionBase Option => SelectedOptions[0];
        //public virtual bool IsDone()
        //{
        //    return _isDone;
        //}
        private IAIActor _actor;
        //private bool _isDone;

        protected List<IAIOption> _options;

        protected IAIOption _lastSelectedOption; 

        public AIReasonerBase()
        {
        }

        public virtual bool Initialize(AICreationData creationData)
        {
            AISpecificationNode options;
            _options = new List<IAIOption>();
            
            if (creationData.AISpecificationNode.Name != NodeName)
            {
                return false;
            }

            _actor = creationData.Actor;
            
            // Special case with one single option
            if (creationData.AISpecificationNode.TryGetChild("Considerations", out AISpecificationNode considerations))
            {
                AISpecificationNode optionSpec = new( null, "Option", "", new() , new List<AISpecificationNode> {considerations} );
                AICreationData data = new(optionSpec, creationData.Actor);
                AIOptionBase option = AIOptionFactory.Instance.Create(data);
                _options.Add(option);

                return true;
            }

            // Normal case with multiple options
            if (!creationData.AISpecificationNode.TryGetChild("Options", out options))
            {
                return false;
            }

            foreach (var child in options.Children)
            {
                if (child.Name != "Option")
                {
                    return false;
                }

                AICreationData data = new AICreationData(child, creationData.Actor);
                AIOptionBase option = AIOptionFactory.Instance.Create(data);
                _options.Add(option);
            }
            
            Enable();
            return true;
        }

        public void AddOption(AIOptionBase option)
        {
            _options.Add(option);
        }

        public virtual void Reset()
        {
            _lastSelectedOption?.Reset();
            _lastSelectedOption = null;
        }
        public virtual void Tick()
        {
            if (!IsEnabled)
            {
                return;
            }

            IsDone = false;
            SelectedOption = null;
            Sense();
            Think();
            Act();
        }        
        public abstract void Consider();
        public abstract void SelectBestOption();
        protected virtual void Sense() {}
        public virtual void Think()
        {
            Consider();
            SelectBestOption();
        }
        public virtual void Act() 
        {
            if (SelectedOption != _lastSelectedOption)
            {
                _lastSelectedOption?.Reset();
                _lastSelectedOption = SelectedOption;
            }

            if (SelectedOption == null)
            {
                IsDone = true;
                return;
            }
            
            IsDone = SelectedOption.Update();
        }
    }
}
