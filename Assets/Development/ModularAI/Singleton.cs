namespace PLu.ModularAI
{
    public interface ISingleton<T>
    {
        static T Instance { get; }
    }

    public class Singleton<T> : ISingleton<T> where T : new()
    {
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                }
                return _instance;
            }
        }

        private static T _instance;
    }
}
