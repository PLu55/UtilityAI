using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public class AIContact : AIBlackboard
    {
        // public IAIActor Actor { get; }
        // public IAIVector3 LastPosition { get; private set; }
        // public double Time { get; private set; }
        // public bool IsThreat { get; set; }

        private static HashedString ActorKey = "Actor";
        private static HashedString LastPositionKey = "LastPosition";
        private static HashedString TimeKey = "Time";
        private static HashedString IsThreatKey = "IsThreat";
        private static HashedString IsEnemyKey = "Enemy";

        public AIContact(IAIActor actor, IAIVector3 position, double time, bool isThreat = false) : base("Contact")
        {
            _storage.Add(ActorKey, actor);
            _storage.Add(LastPositionKey, position);
            _storage.Add(TimeKey, time);
            _storage.Add(IsThreatKey, isThreat);
        }


        public void Update(IAIVector3 position)
        {
            _storage.Add(LastPositionKey, position);
        }

        public void Update(double time)
        {
            _storage.Add(TimeKey, time);
        }

        public void Update(IAIVector3 position, double time)
        {

            _storage[LastPositionKey] = position;
            _storage[TimeKey] = time;

        }
    }
}
