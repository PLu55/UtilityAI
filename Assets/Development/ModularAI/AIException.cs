namespace PLu.ModularAI
{
    public class AIException : System.Exception
    {
        public AIException(string message) : base(message) { }
    }

    public class AIUnexpectedChildrenException : AIException
    {
        public AIUnexpectedChildrenException(string message) : base(message) { }
    }

    public class AIMissingChildException : AIException
    {
        public AIMissingChildException(string message) : base(message) { }
    }

    public class AIMissingAttributeException : AIException
    {
        public AIMissingAttributeException(string message) : base(message) { }
    }
    public class AIUnexpectedAttributeException : AIException
    {
        public AIUnexpectedAttributeException(string message) : base(message) { }
    }
    public class AIUnexpectedNodeName : AIException
    {
        public AIUnexpectedNodeName(string message) : base(message) { }
    }

    public class AITypeException : AIException
    {
        public AITypeException(string message) : base(message) { }
    }
    public class AIDataStoreNotFoundException : AIException
    {
        public AIDataStoreNotFoundException(string message) : base(message) { }
    }
    public class AIDataStoreKeyNotFoundException : AIException
    {
        public AIDataStoreKeyNotFoundException(string message) : base(message) { }
    }
    public class AIUnexpectedOperatorException : AIException
    {
        public AIUnexpectedOperatorException(string message) : base(message) { }
    }
    public class AIUnexpectedValueException : AIException
    {
        public AIUnexpectedValueException(string message) : base(message) { }
    }
    public class AIUnexpectedDataPathException : AIException
    {
        public AIUnexpectedDataPathException(string message) : base(message) { }
    }
    public class AIMissingComponentException : AIException
    {
        public AIMissingComponentException(string message) : base(message) { }
    }    
}
