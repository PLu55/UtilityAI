using System;
using System.Collections;
using System.Collections.Generic;

namespace PLu.ModularAI
{
    public class AIExecutionHistory: IEnumerable<AIExecutionHistoryNode>
    {
        private int _maxHistoryLength;
        private LinkedList<AIExecutionHistoryNode> _history;

        public AIExecutionHistory(int maxHistoryLength = 100)
        {
            _history = new LinkedList<AIExecutionHistoryNode>();
            _maxHistoryLength = maxHistoryLength;
        }

        public int Count => _history.Count;
        public AIExecutionHistoryNode First => _history.First?.Value;
        public void AddEntry(AIExecutionHistoryState state)
        {
            AddNode(new AIExecutionHistoryNode(AITimeManager.Instance.Time, state));
        }

        public void AddNode(AIExecutionHistoryNode node)
        {
            _history.AddFirst(node);
            if (_history.Count > _maxHistoryLength)
            {
                _history.RemoveLast();
            }
        }
        public double TimeSinceActivation()
        {
            if (_history.Count == 0 || _history.First.Value.State != AIExecutionHistoryState.Begin)
            {
                return double.MaxValue;
            }

            return AITimeManager.Instance.Time - _history.First.Value.Time;
        }
        public double TimeSinceLastActive()
        {
            if (_history.Count == 0)
            {
                return double.MaxValue;
            }
            else if (_history.First.Value.State == AIExecutionHistoryState.Begin)
            {
                return 0.0;
            }

            return AITimeManager.Instance.Time - _history.First.Value.Time;
        }

        public int NumberOfActivationsSince(double since)
        {
            LinkedListNode<AIExecutionHistoryNode> node = _history.First;
            int count = 0;
            
            double timeLimit = AITimeManager.Instance.Time - since;

            while (node != null && node.Value.Time > timeLimit)
            {
                if (node.Value.State == AIExecutionHistoryState.Begin)
                count++;
                node = node.Next;
            }
            return count;
        }
        public void Clear()
        {
            _history.Clear();
        }
        public void Prune(int maxHistoryLength)
        {
            while (_history.Count > maxHistoryLength)
            {
                _history.RemoveLast();
            }
        }
        public void Prune(double timeStamp)
        {
            while (_history.Last.Value.Time < timeStamp)
            {
                _history.RemoveLast();
            }
        }

        public IEnumerator<AIExecutionHistoryNode> GetEnumerator()
        {
            return _history.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _history.GetEnumerator();
        }
    }

    public enum AIExecutionHistoryState
    {
        Begin,
        End,
        Running
    }
    
    public class AIExecutionHistoryNode : IComparable<AIExecutionHistoryNode>
    {
        public readonly double Time;
        public readonly AIExecutionHistoryState State;

        public AIExecutionHistoryNode(double timeStamp, AIExecutionHistoryState state)
        {
            Time = timeStamp;
            State = state;
        }

        public AIExecutionHistoryNode(AIExecutionHistoryState state)
        {

            Time = AITimeManager.Instance.Time;
            State = state;
        }

        public int CompareTo(AIExecutionHistoryNode other)
        {
            return Time.CompareTo(other.Time);
        }

        public override string ToString()
        {
            return $"AIExecutionHistoryNode({Time}, {State})";
        }
    }
}