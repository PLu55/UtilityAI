using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public class AIGlobalManager : Singleton<AIGlobalManager>
    {
        private AIBlackboardManager _blackboardManager;
        private Dictionary<HashedString, AIBlackboard> _blackboards;

        public AIGlobalManager()
        {
            Initialize();
        }

        private void Initialize()
        {
            _blackboardManager = AIBlackboardManager.Instance;
            _blackboards = new();
            RegisterBlackboard(_blackboardManager.GetOrRegister("Global"), new AIBlackboard());
        }

        public AIBlackboard GetBlackboard(HashedString name)
        {
            return _blackboards[name];
        }

        public void RegisterBlackboard(HashedString name, AIBlackboard blackboard)
        {
            _blackboards.Add(name, blackboard);
        }

        public void UnregisterBlackboard(HashedString name)
        {
            _blackboards.Remove(name);
        }
    }



}

