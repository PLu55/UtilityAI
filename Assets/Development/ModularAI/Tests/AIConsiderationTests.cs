using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public partial class AIConsiderationTests
    {      
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void AIConsiderationDefaultValuesTests()
        {      
            var specification = new AISpecificationBuilder().Partial()
            .DistanceConsideration()
                .SelfTarget()
                .SelfTarget()
                .ConstantWeightFunction()
            .End()
            .Build();

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIConsiderationBase consideration = AIConsiderationFactory.Instance.Create(creationData);
            Assert.IsNotNull(consideration);
            Assert.AreEqual(typeof(AIDistanceConsideration), consideration.GetType());
            Assert.AreEqual(typeof(AIConstantWeightFunction), consideration.WeightFunction.GetType());            
            consideration.Consider();
            Assert.AreEqual(0f, consideration.Addend);
            Assert.AreEqual(1f, consideration.Multiplier, 1e-6f);
            Assert.AreEqual(-float.MaxValue, consideration.Rank);
        }
        [Test]
        public void AIConsiderationGivenValuesTests()
        {   
            var specification = new AISpecificationBuilder().Partial()
                .DistanceConsideration(addend: "1", multiplier: "2", rank: "10")
                    .SelfTarget()
                    .SelfTarget()
                    .ConstantWeightFunction()
                .End()
                .Build();

            var creationData = AITestUtilities.CreateCreationData(specification.Root);
            var consideration = AIConsiderationFactory.Instance.Create(creationData);
            Assert.IsNotNull(consideration);
            Assert.AreEqual(typeof(AIDistanceConsideration), consideration.GetType());

            consideration.Consider();
            Assert.AreEqual(1f, consideration.Addend);
            Assert.AreEqual(2f, consideration.Multiplier, 1e-6f);
            Assert.AreEqual(10f, consideration.Rank);
        }
    }
}