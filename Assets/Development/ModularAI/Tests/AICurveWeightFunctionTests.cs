using System.Xml.Linq;
using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AICurveWeightFunctionTests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void BinaryWeightFunctionTest()
        {   
            var specification = new AISpecificationBuilder().Partial()
                .CurveWeightFunction("Binary", slope: "0.6", xShift: "0.6", yShift: "0.3")
                .Build();     

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            var weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);

            Assert.IsNotNull(creationData);     
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AICurveWeightFunction), weightFunction.GetType());
            
            AIWeightValues values = weightFunction.Compute(0.5f);
            Assert.AreEqual(0.3f, values.Multiplier);
            values = weightFunction.Compute(0.6f);
            Assert.AreEqual(0.9f, values.Multiplier, 1e-6f);
        }

        [Test]
        public void LinearWeightFunctionTest()
        {         
            var specification = new AISpecificationBuilder().Partial()
                .CurveWeightFunction("Linear", slope: "2.0")
                .Build(); 

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            var weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AICurveWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(0f);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(0.5f);
            Assert.AreEqual(1f, values.Multiplier);
        }

        [Test]
        public void PolynomialCurveWeightFunctionTest()
        {            
           var specification = new AISpecificationBuilder().Partial()
                .CurveWeightFunction("Polynomial", exponent: "2", slope: "0.7", yShift: "0.3")
                .Build(); 

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            var weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AICurveWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(0f);
            Assert.AreEqual(0.3f, values.Multiplier);
            values = weightFunction.Compute(0.5f);
            Assert.AreEqual(0.475f, values.Multiplier, 1e-6f);
            values = weightFunction.Compute(1f);
            Assert.AreEqual(1f, values.Multiplier);
        }

        [Test]       
        public void LogisticCurveWeightFunctionTest()
        {            
           var specification = new AISpecificationBuilder().Partial()
                .CurveWeightFunction("Logistic", exponent: "1.5", slope: "0.91", xShift: "0.2", yShift: "0.1")
                .Build(); 

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            var weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AICurveWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(0f);
            Assert.AreEqual(0.100025057f, values.Multiplier);
            values = weightFunction.Compute(0.7f);
            Assert.AreEqual(0.555f, values.Multiplier, 1e-6f);
            values = weightFunction.Compute(1f);
            Assert.AreEqual(1f, values.Multiplier);
        }
    }
}
