using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AILogActionTests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void SimpleTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .LogAction("TestVariable", "System.Boolean")
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIActionBase logAction = AIActionFactory.Instance.Create(creationData);
            Assert.IsNotNull(logAction);
            Assert.AreEqual(logAction.GetType(), typeof(AILogAction));

            actor.SetValue("TestVariable", true);
            logAction.Update();
        }        
        
        [Test]
        public void Test()
        {
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Actions()
                        .LogAction("TestVariable", "System.Boolean")
                    .End()
                .End()
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIOptionBase option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);
            Assert.AreEqual(option.GetType(), typeof(AIOption));
            actor.SetValue("TestVariable", true);
            option.Update();
        }
    }
}