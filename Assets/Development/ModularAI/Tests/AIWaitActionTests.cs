using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIWaitActionTests
    {                     
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif

            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void Test()
        {
            var specification = new AISpecificationBuilder().Partial()
                .WaitAction("5")
                .Build();

            AIActor actor = new AIActor("Actor");
            var creationData = new AICreationData(specification, actor);

            var action = AIActionFactory.Instance.Create(creationData);
            Assert.IsNotNull(action);
            Assert.AreEqual(typeof(AIWaitAction), action.GetType());

            AITimeManager.Instance.UpdateTime(7d);
            action.Select();
            Assert.IsFalse(action.IsDone);
            action.Update();
            Assert.IsFalse(action.IsDone);
            AITimeManager.Instance.UpdateTime(3d);
            action.Update();
            Assert.IsFalse(action.IsDone);            
            AITimeManager.Instance.UpdateTime(3d);
            action.Update();
            Assert.IsTrue(action.IsDone);
            action.Select();            
            Assert.IsFalse(action.IsDone);
            AITimeManager.Instance.UpdateTime(3d);
            action.Update();
            Assert.IsFalse(action.IsDone);            
            AITimeManager.Instance.UpdateTime(3d);
            action.Update();
            Assert.IsTrue(action.IsDone);
        }
    }
}