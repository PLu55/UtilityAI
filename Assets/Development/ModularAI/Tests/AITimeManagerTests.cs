using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AITimeManagerTests
    {
        [OneTimeSetUp]
        public void AITimeManagerTestsSetup()
        {
            AITimeManagerBase.SetTimeManager(new AITimeManager());

#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void AISimpleTimeManagerTests()
        {
            Assert.IsTrue(AITimeManagerBase.Instance is IAITimeManager);

            Assert.AreEqual(AITimeManager.Instance.Time, 0d);
            Assert.AreEqual(AITimeManager.Instance.DeltaTime, 0d);
            AITimeManager.Instance.UpdateTime(1f);
            Assert.AreEqual(AITimeManager.Instance.Time, 1d);
            Assert.AreEqual(AITimeManager.Instance.DeltaTime, 1d);
            AITimeManager.Instance.UpdateTime(0.5f);
            Assert.AreEqual(AITimeManager.Instance.Time, 1.5);
            Assert.AreEqual(AITimeManager.Instance.DeltaTime, 0.5f);
        }
    }
}