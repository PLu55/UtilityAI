using System;
using NUnit.Framework;
using PLu.Utilities;
using UnityEngine.UIElements;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AITargetTests
    {

        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void SelfTargetTest()
        {
            AISpecification specification = new AISpecificationBuilder().Partial()
                .SelfTarget()
                .Build();
            
            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AITargetBase target = AITargetFactory.Instance.Create(creationData);
            Assert.IsNotNull(target);
            Assert.IsTrue(target.HasEntity);
            Assert.AreEqual(actor, target.Entity);
            Assert.AreEqual(actor.GetType(), target.Entity.EntityType);
            Assert.AreEqual(actor.Position, target.Position);
        } 
        
        [Test]
        public void PositionTargetTest()
        {
            AISpecification specification = new AISpecificationBuilder().Partial()
                .PositionTarget("1, 2.0, 3.0e1")
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AITargetBase target = AITargetFactory.Instance.Create(creationData);
            Assert.IsNotNull(target);
            Assert.IsFalse(target.HasEntity);
            Assert.AreEqual(new AIVector3(1f, 2f, 30f), target.Position);
        }

        [Test]
        public void PickerEntityTargetTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .PickerEntityTarget("Entity")
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);

            actor.BrainBlackboard.SetValue("PickerEntity", new AIBlackboard());
            AIDataPath dataPath = new AIDataPath("Brain:PickerEntity.Entity", actor);
            dataPath.SetValue(actor);

            AITargetBase target = AITargetFactory.Instance.Create(creationData);
            Assert.IsNotNull(target);
            Assert.AreEqual(actor, target.Entity);
            Assert.IsTrue(target.HasEntity);
        }

        [Test]
        public void DataStoreTargetTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .DataStoreTarget("Targets:TestData")
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIBlackboard blackboard = actor.GetBlackboard("Targets");
            HashedString key = AIBlackboardManager.Instance.GetOrRegister("TestData");
            blackboard.SetValue(key, actor);

            AITargetBase target = AITargetFactory.Instance.Create(creationData);
            Assert.IsNotNull(target);
            Assert.AreEqual(actor, target.Entity);
            Assert.IsTrue(target.HasEntity);
        }

        [Test]
        public void GlobalDataStoreTargetTest()
        { 
            var specification = new AISpecificationBuilder().Partial()
                .DataStoreTarget("Global:TestData")
                .Build();
            
            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIBlackboard globalBlackboard = AIBlackboardManager.Instance.GlobalBlackboard;
            globalBlackboard.SetValue("TestData", actor);

            AITargetBase target = AITargetFactory.Instance.Create(creationData);
            Assert.IsNotNull(target);
            Assert.AreEqual(actor, target.Entity);
            Assert.IsTrue(target.HasEntity);
        }
    }
}
