using NUnit.Framework;
using PLu.GenericBlackboard;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public partial class AIActorTests
    {      
        [OneTimeSetUp]
        public void ActorTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void ActorTests()
        { 
            IAIActor actor = new AIActor("TestActor");
            Assert.NotNull(actor);
            Assert.AreEqual("TestActor".AsHashedString(), actor.Name);

            Assert.AreEqual(actor.Name, actor.GetValue<HashedString>("Name".AsHashedString()));
            Assert.AreEqual(actor.BrainBlackboard, actor.GetValue<Blackboard>("Brain".AsHashedString()));
        }
    }
}