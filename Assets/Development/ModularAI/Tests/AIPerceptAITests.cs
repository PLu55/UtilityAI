using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    public class AIPerceptAITests
    {
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void HandlePerceptsTest()
        {
            var specification = new AISpecificationBuilder("HandlePercepts")
                .Reasoner("Selection")
                    .Options()
                        .Option("Option1")
                            .Considerations()
                                .EntityExistsConsideration("Percepts", "Result")
                                    .Picker()
                                        .Reasoner("DualUtility")
                                            .Options()
                                                .Option("EntityExistsOption")
                                                    .Considerations()
                                                        .VariableConsideration("Relations.@(Brain:PickerEntity.Entity.Faction)", "float")
                                                            .CurveWeightFunction("Linear", "0", "-1", "0.5", "0.5", "0")
                                                        .End()
                                                    .End()
                                                    .Actions() // TODO: Actions are not called. Consider how EntityExistsConsideration should work!
                                                        //.LogAction("Brain:PickerEntity.Entity", "PLu.ModularAI.IAIEntity")
                                                        .SetVariableAction("HasResult", "True", variableType: "System.Boolean")
                                                    .End()
                                                .End()
                                            .End()
                                        .End()
                                    .End()
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                //.LogAction("Brain:Result.Actor.Name", "System.String")
                                .SetVariableAction("HasResult", "True", variableType: "System.Boolean")
                            .End()
                        .End()
                    .End()
                .End()
                .BuildAndStore(true);

            AIDataPath dataPath;
            Assert.NotNull(specification);

            AITimeManager.Instance.UpdateTime(100d);
            AIPerceptEnvelope envelope = new(0.1, 0.2, 0.3, 0.6, 0.75f);

            //Setup Me Actor
            AIActor me = new AIActor("Me");
            AIPerceptController perceptController = new(me);
            AICreationData creationData = new AICreationData(specification, me);
            me.Reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(me.Reasoner);

            // Setup Enemy Actor
            AIActor enemy = new AIActor("Enemy");
            enemy.SetValue<HashedString>("Faction", "HostileFaction");            
            Assert.AreEqual("HostileFaction".AsHashedString(), enemy.GetValue<HashedString>("Faction"));

            // Set my relations to the HostileFaction

            me.SetValue("Relations", new AIBlackboard("Relations"));
            dataPath = new("Relations.HostileFaction", me);
            dataPath.SetValue(-0.5f);
            Assert.AreEqual(-0.5f, dataPath.GetValue<float>());

            // Generate Percept, I see the enemy
            // Add percept to the percept controller 
            AIPercept percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 2.0f, AIVector3.Zero, envelope: envelope, entity: enemy);
            perceptController.AddPercept(enemy, percept);
            AITimeManager.Instance.UpdateTime(0.3);
            // Check if the percept was received by me
            Assert.IsTrue(me.PerceptsBlackboard.TryGetValue("Enemy", out AIPercept receivedPercept));
            Assert.AreEqual(percept, receivedPercept);
            Assert.AreEqual(2.0f, receivedPercept.CurrentLevel);

            me.Reasoner.Tick();
            dataPath = new("Brain:Result", me);
            Assert.IsTrue(me.GetValue<bool>("HasResult"));
            Assert.AreEqual(percept, dataPath.GetValue<AIPercept>());
        }        
        
        [Test]
        public void LoopThroughTest()
        {
            var specification = new AISpecificationBuilder("LoopThrough")
                .Reasoner("Selection")
                    .Options()
                        .Option("Option1")
                            .Considerations()
                                .EntityExistsConsideration("Percepts", "Result")
                                    .Picker()
                                        .Reasoner("Sequence")
                                            .Options()
                                                .Option("EntityExistsOption")
                                                    .Considerations()
                                                        .VariableConsideration("Relations.@(Brain:PickerEntity.Entity.Faction)", "float")
                                                            .CurveWeightFunction("Linear", "0", "-1", "1", "0", "0")
                                                        .End()
                                                    .End()
                                                    .Actions()
                                                        .SetVariableAction("Threats.@(Brain:PickerEntity.Entity.Name)")
                                                            //.DataStoreTarget("Brain:PickerEntity.Entity")
                                                            .PickerEntityTarget("Entity")
                                                        .End()
                                                    .End()
                                                .End()
                                            .End()
                                        .End()
                                    .End()
                                    .BoolWeightFunction()
                                .End()
                            .End()
                        .End()
                    .End()
                .End()
                .BuildAndStore(true);

            AIDataPath dataPath;
            Assert.NotNull(specification);

            AITimeManager.Instance.UpdateTime(100d);
            AIPerceptEnvelope envelope = new(0.1, 0.2, 0.3, 0.6, 0.75f);

            //Setup Me Actor
            AIActor me = new AIActor("Me");
            AIPerceptController perceptController = new(me);
            AICreationData creationData = new AICreationData(specification, me);
            me.Reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(me.Reasoner);

            // Set my relations to Factions
            me.SetValue("Relations", new AIBlackboard("Relations"));
            dataPath = new("Relations.HostileFaction", me);
            dataPath.SetValue(-0.5f);
            dataPath = new("Relations.FriendlyFaction", me);
            dataPath.SetValue(0.5f);

            // Setup Other Actors
            AIActor enemy1 = new AIActor("Enemy1");
            enemy1.SetValue<HashedString>("Faction", "HostileFaction");            
            AIActor enemy2 = new AIActor("Enemy2");
            enemy2.SetValue<HashedString>("Faction", "HostileFaction");  
            AIActor friend = new AIActor("Friend");
            friend.SetValue<HashedString>("Faction", "FriendlyFaction"); 


            // Generate Percept, I see the enemy
            // Add percept to the percept controller 
            AIPercept percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 2.0f, AIVector3.Zero, envelope: envelope, entity: enemy1);
            perceptController.AddPercept(enemy1, percept);            
            percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 2.0f, AIVector3.Zero, envelope: envelope, entity: enemy2);
            perceptController.AddPercept(enemy2, percept);
            percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 2.0f, AIVector3.Zero, envelope: envelope, entity: friend);
            perceptController.AddPercept(friend, percept);
            AITimeManager.Instance.UpdateTime(0.3);
            // Check if the percept was received by me
            // Assert.IsTrue(me.PerceptsBlackboard.TryGetValue("Enemy1", out AIPercept receivedPercept));
            // Assert.AreEqual(percept, receivedPercept);
            // Assert.AreEqual(2.0f, receivedPercept.CurrentLevel);

            me.Reasoner.Tick();
            Assert.AreEqual(2, me.ThreatsBlackboard.Count);

            // dataPath = new("Brain:Result", me);
            // Assert.IsTrue(me.GetValue<bool>("HasResult"));
            // Assert.AreEqual(percept, dataPath.GetValue<AIPercept>());
        }
    }
}