using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    public class AIVector3Tests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        [Test]
        public void AIVector3BasicTests()
        {
            AIVector3 vector1 = new AIVector3(1f, 2f, 3f);
            Assert.AreEqual(1f, vector1.X);
            Assert.AreEqual(2f, vector1.Y);
            Assert.AreEqual(3f, vector1.Z);

            Assert.AreEqual("AIVector3(1, 2, 3)", vector1.ToString());
            Assert.AreEqual(new AIVector3(1f, 2f, 3f), AIVector3.Parse("1, 2, 3"));
            Assert.AreEqual(14f, vector1.MagnitudeSqr);
            Assert.AreEqual(3.7416574f, vector1.Magnitude);
            Assert.AreEqual(new AIVector3(2f, 4f, 6f), vector1 + vector1);
            Assert.AreEqual(new AIVector3(3f, 6f, 9f), vector1 * 3f);
            Assert.AreEqual(new AIVector3(0.5f, 1f, 1.5f), vector1 / 2f);
            Assert.AreEqual(33f, vector1.DistanceSqr(new AIVector3(3f, 7f, 5f)));
            Assert.AreEqual(5.7445627f, vector1.Distance(new AIVector3(3f, 7f, 5f)));
        }
    }
}
