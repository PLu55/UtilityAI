using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIOptionTests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void ConstructionTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Considerations()
                        .DistanceConsideration()
                            .SelfTarget()
                            .PositionTarget("1, 2.0, 3.0e1")
                            .CurveWeightFunction("Polynomial", "2", "0.7", "0.3")
                        .End()
                    .End()
                    .Actions()
                        .SetVariableAction("TestVariable", "TestValue")
                    .End()
                .End()
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIOptionBase option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);
            Assert.AreEqual(option.GetType(), typeof(AIOption));
        }
        
        [Test]
        public void ActivationTest()
        {   
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Considerations()
                        .VariableConsideration("TestVariable", "bool")
                            .BoolWeightFunction()
                        .End()
                    .End()
                    .Actions()                        
                        .WaitAction("10")
                        .SetVariableAction("ResultVariable", "Set1")
                        .WaitAction("10")
                        .SetVariableAction("ResultVariable", "Set2")
                    .End()
                .End()
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIOptionBase option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);

            actor.SetValue("TestVariable", true);
            option.Consider();
            Assert.AreEqual(1f, option.Weight);
            Assert.IsFalse(option.Update());

            Assert.AreNotEqual("Set1", actor.GetValue<string>("ResultVariable"));
            Assert.AreEqual(1, option.ExecutionHistory.Count);

            AITimeManager.Instance.UpdateTime(10.0f);
            Assert.IsFalse(option.Update());

            Assert.AreEqual("Set1", actor.GetValue<string>("ResultVariable"));
            Assert.AreEqual(1, option.ExecutionHistory.Count);
            
            AITimeManager.Instance.UpdateTime(10.0f);
            Assert.IsTrue(option.Update());
            Assert.AreEqual("Set2", actor.GetValue<string>("ResultVariable"));
            Assert.AreEqual(2, option.ExecutionHistory.Count);

            actor.SetValue("TestVariable", false);
            option.Consider();
            Assert.AreEqual(0f, option.Weight);
        }        
        
        [Test]
        public void ResetTest()
        {   
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Considerations()
                        .VariableConsideration("TestVariable", "bool")
                            .BoolWeightFunction()
                        .End()
                    .End()
                    .Actions()                        
                        .WaitAction("10")
                        .SetVariableAction("ResultVariable", "Set1")
                        .WaitAction("10")
                        .SetVariableAction("ResultVariable", "Set2")
                    .End()
                .End()
                .Build();

            AIActor actor = new AIActor("Actor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIOptionBase option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);

            actor.SetValue("TestVariable", true);
            option.Consider();
            Assert.AreEqual(1f, option.Weight);
            Assert.IsFalse(option.Update());

            Assert.AreNotEqual("Set1", actor.GetValue<string>("ResultVariable"));
            Assert.AreEqual(1, option.ExecutionHistory.Count);

            option.Reset();
            Assert.AreEqual(2, option.ExecutionHistory.Count);
         }
    }
}