using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using NUnit.Framework;
using UnityEditorInternal;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIWeightFunctionTests
    {            
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void BoolWeightFunctionTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .BoolWeightFunction()
                .Build();

            //const string specification = "<WeightFunction Type=\"Bool\"/>";            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIBoolWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(false);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(true);
            Assert.AreEqual(1f, values.Multiplier);
        }

        [Test]
        public void ConstantWeightFunctionTest()
        {
            //const string specification = "<WeightFunction Type=\"Constant\" Addend=\"1\" Multiplier=\"2\" Rank=\"10\"/>";
            var specification = new AISpecificationBuilder().Partial()
                .ConstantWeightFunction(addend: "1", multiplier: "2", rank: "10")
                .Build();
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIConstantWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(false);
            Assert.AreEqual(1f, values.Addend);
            Assert.AreEqual(2f, values.Multiplier);
            Assert.AreEqual(10f, values.Rank);
        }               
        
        [Test]
        public void StringWeightFunctionTest1()
        {   
            var specification = new AISpecificationBuilder().Partial()
                .StringWeightFunction()
                    .Entries()
                        .String(value: "A", veto: "true")
                        .String(value: "B", veto: "false")
                    .End()
                    .Default(veto: "true")
                .End()
                .Build();
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);

            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIStringWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute("A");
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute("B");
            Assert.AreEqual(1f, values.Multiplier);        
            values = weightFunction.Compute("C");
            Assert.AreEqual(0f, values.Multiplier);
        }         
                [Test]
        public void StringWeightFunctionTest2()
        {   
            var specification = new AISpecificationBuilder().Partial()
                .StringWeightFunction()
                    .Entries()
                        .String(value: "A", veto: "true")
                        .String(value: "B", veto: "false")
                    .End()
                    .Default(veto: "false")
                .End()
                .Build();
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);

            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIStringWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute("A");
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute("B");
            Assert.AreEqual(1f, values.Multiplier);        
            values = weightFunction.Compute("C");
            Assert.AreEqual(1f, values.Multiplier);
        } 
        [Test]
        public void NumericWeightFunctionTest()
        {
            string[] types = new string[] {"Float", "Double", "Int"};

            foreach (string type in types)
            {
                //AILogger.Log("NumericWeightFunctionTest: testing: " + type);
                AllOperatorsTests(type);
            }
    
        }
        private AIWeightFunctionBase CreateNumericWeightFunction(string @operator, string value, string type)
        {
            Type expectedType;

            AISpecificationBuilder builder = new AISpecificationBuilder().Partial();

            AISpecification specification;

            switch (type)
            {
                case "Int":
                    builder = builder.IntWeightFunction(@operator, "10");
                    expectedType = typeof(AIIntWeightFunction);
                    break;
                case "Float":
                    builder = builder.FloatWeightFunction(@operator, "10");
                    expectedType = typeof(AIFloatWeightFunction);
                    break;
                case "Double":
                    builder = builder.DoubleWeightFunction(@operator, "10");
                    expectedType = typeof(AIDoubleWeightFunction);
                    break;
                default:
                    return null;
            }
            specification = builder.Build();

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(expectedType, weightFunction.GetType());

            return weightFunction;
        }

        private void AllOperatorsTests(string type)
        {
            List<string> operators = new List<string> { "eq", "neq", "lt", "lte", "gt", "gte" };
    

            foreach (string op in operators)
            {
                float[] results = new float[3]{0f, 0f, 0f};

                //AILogger.Log("AllOperatorsTests: operator: " + op);

                switch (op)
                {
                    case "eq":
                        results = new float[3]{0f, 1f, 0f};
                        break;
                    case "neq":
                        results = new float[3]{1f, 0f, 1f};
                        break;
                    case "lt":
                        results = new float[3]{1f, 0f, 0f};
                        break;
                    case "lte":
                        results = new float[3]{1f, 1f, 0f};
                        break;
                    case "gt":
                        results = new float[3]{0f, 0f, 1f};
                        break;
                    case "gte":
                        results = new float[3]{0f, 1f, 1f};
                        break;
                }

                AIWeightFunctionBase weightFunction = CreateNumericWeightFunction(op, "10", type);
                AIWeightValues values;

                switch (type)
                {
                    case "Int":
                        values = weightFunction.Compute(9);
                        Assert.AreEqual(results[0], values.Multiplier);            
                        values = weightFunction.Compute(10);
                        Assert.AreEqual(results[1], values.Multiplier);
                        values = weightFunction.Compute(11);
                        Assert.AreEqual(results[2], values.Multiplier);
                        break;
                    case "Float":
                        values = weightFunction.Compute(9f);
                        Assert.AreEqual(results[0], values.Multiplier);            
                        values = weightFunction.Compute(10f);
                        Assert.AreEqual(results[1], values.Multiplier);
                        values = weightFunction.Compute(11f);
                        Assert.AreEqual(results[2], values.Multiplier);
                        break;
                    case "Double":
                        values = weightFunction.Compute(9d);
                        Assert.AreEqual(results[0], values.Multiplier);            
                        values = weightFunction.Compute(10d);
                        Assert.AreEqual(results[1], values.Multiplier);
                        values = weightFunction.Compute(11d);
                        Assert.AreEqual(results[2], values.Multiplier);
                        break;
                }
           }
        }
    }
}
