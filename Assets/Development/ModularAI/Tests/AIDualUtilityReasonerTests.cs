using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIDualUtilityReasonerTests
    {                     
        [OneTimeSetUp]
        public void AIReasonerTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif

            AITimeManager.SetTimeManager(new AITimeManager());
        }
        [Test]
        public void DualUtilityReasonerTest()
        {
            var specification = new AISpecificationBuilder()
                .Reasoner("DualUtility")
                    .Options()
                        .Option("Option1")
                            .Considerations()
                                .VariableConsideration("TestVariable1", "float")
                                    .CurveWeightFunction("Linear", "0", "0.1")
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option1")
                            .End()
                        .End()
                        .Option("Option2")
                            .Considerations()
                                .VariableConsideration("TestVariable2", "float")
                                    .CurveWeightFunction("Linear", "0", "0.1")
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option2")
                            .End()
                        .End()                        
                        .Option("Option3")
                            .Considerations()
                                .VariableConsideration("TestVariable3", "float")
                                    .CurveWeightFunction("Linear", "0", "0.1")
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option3")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            var reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            IAIActor actor = creationData.Actor;
            actor.SetValue("TestVariable1".AsHashedString(), 0f);
            actor.SetValue("TestVariable2".AsHashedString(), 5f);
            actor.SetValue("TestVariable3".AsHashedString(), 10f);
            reasoner.Tick();
            Assert.AreEqual("Option3", actor.GetValue<string>("ResultVariable".AsHashedString()));
        }
    }
}