using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AISpecificationLoaderTests
    {                     
        [OneTimeSetUp]
        public void AISpecificationTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
            AITimeManager.SetTimeManager(new AITimeManager());
        }

        public class SpecificationLoader : AISpecificationLoaderBase 
        {
            public SpecificationLoader(bool overwrite = false) : base(overwrite) { }
            protected override void Load()
            {
                var specification = new AISpecificationBuilder("Behaviour1")
                    .Reasoner("RuleBased")
                        .Options()
                            .Option("Option1")
                                .Considerations()
                                    .VariableConsideration("TestVariable1", "bool")
                                        .BoolWeightFunction()
                                    .End()
                                .End()
                                .Actions()
                                    .SetVariableAction("ResultVariable", "Option1")
                                .End()
                            .End()
                        .End()
                    .End()
                    .Build();

                AISpecificationManager.Instance.AddSpecification(specification, Overwrite);
            }

        }

        [Test]
        public void BuilderTests_1()
        {
            new SpecificationLoader(overwrite: true);
            var spec = AISpecificationManager.Instance.GetAISpecification("Behaviour1");
            Assert.IsNotNull(spec);
        }
    }
}