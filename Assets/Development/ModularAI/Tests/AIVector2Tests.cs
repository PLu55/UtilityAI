using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    public class AIVector2Tests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void AIVector2BasicTests()
        {
            AIVector2 vector1 = new AIVector2 { x = 1f, y = 2f };
            Assert.AreEqual(1, vector1.x);
            Assert.AreEqual(2, vector1.y);
            Assert.AreEqual("AIVector2(1, 2)", vector1.ToString());
            Assert.AreEqual(new AIVector2 { x = 1f, y = 2f }, AIVector2.Parse("1, 2"));
            Assert.AreEqual(5, vector1.MagnitudeSqr);
            Assert.AreEqual(2.23606798f, vector1.Magnitude);
            Assert.AreEqual(new AIVector2 { x = 2f, y = 4f }, vector1 + vector1);
            Assert.AreEqual(new AIVector2 { x = 0f, y = 0f }, vector1 - vector1);
            Assert.AreEqual(new AIVector2 { x = 3f, y = 6f }, vector1 * 3f);
            Assert.AreEqual(new AIVector2 { x = 0.5f, y = 1f }, vector1 / 2f);
            Assert.AreEqual(29f, vector1.DistanceSqr(new AIVector2 { x = 3f, y = 7f }));
            Assert.AreEqual(5.38516481f, vector1.Distance(new AIVector2 { x = 3f, y = 7f }));
        }
    }
}