using NUnit.Framework;
using System.Linq;

namespace PLu.ModularAI.Tests
{
    public class AISpecificationManagerTests
    {
        const string ModularAIDirectory = "Assets/Development/ModularAI/";

        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void ReadAndParseXml()
        {
            AISpecificationManager specificationManager = AISpecificationManager.Instance;
            specificationManager.LoadXMLSpecifications(ModularAIDirectory + "Tests/TestData1.xml");
            AISpecification spec = specificationManager.GetAISpecification("Spec1");
            Assert.IsNotNull(spec);
            Assert.AreEqual("Spec1", spec.Name);
            AISpecificationNode root = spec.Root;
            Assert.IsNotNull(root);

            Assert.AreEqual("Reasoner", root.Name);
            Assert.AreEqual("Options", root.Children[0].Name);
            Assert.AreEqual(2, root.Children[0].Children.Count());
            AISpecificationNode option0 = root.Children[0].Children[0];
            Assert.AreEqual("Option", option0.Name);
            AISpecificationNode option1 = root.Children[0].Children[1];
            Assert.AreEqual("Option", option1.Name);
            Assert.AreEqual(2, option0.Children[0].Children.Count());
            AISpecificationNode consideration0 = option0.Children[0].Children[0];
            Assert.AreEqual("Consideration", consideration0.Name);
            AISpecificationNode consideration1 = option0.Children[0].Children[1];
            Assert.AreEqual("Consideration", consideration1.Name);
            Assert.AreEqual("Distance", consideration1.Type);
            Assert.AreEqual(3, consideration1.Children.Count());
            Assert.AreEqual(consideration1, consideration1.Children[0].Parent);


            IAIActor actor = new AIActor("TestActor");
            AICreationData data = new AICreationData(consideration1, actor);
            // Assert.IsNotNull(data);
            


            // Debug.Log($"Factory class: {AIConsiderationFactory.Instance.GetType().Name}");
            // Debug.Log($"Constructors: {AIConsiderationFactory.Instance.GetConstructors().Count()}");

            AIConsiderationBase cons1 = AIConsiderationFactory.Instance.Create(data);
            // Assert.IsNotNull(cons1);
            // Assert.AreEqual("AIConsiderationBase", cons1.GetType().Name);
            
            // AILogger.SetLogger(new UnityAILogger());
            // AILogger.Log("Test log message");
        }

    }
}
