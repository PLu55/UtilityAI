using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public partial class AIPerceptControllerTests
    {      
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
            AITimeManager.SetTimeManager(new AITimeManager());
        }
        
        [Test]
        public void PerceptTest()
        {
            AIActor self = new AIActor("Self");
            AIPerceptController perceptController = new(self);

            AIActor actor = new AIActor("TestActor");

            Assert.IsNotNull(perceptController);
            Assert.AreEqual(self, perceptController.Actor);
            
            AIPercept percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 1.0f, actor.Position, actor, envelope: new(0d, 0.1, 0.1,  0.3, 0.75f));
            perceptController.AddPercept(actor, percept);

            Assert.IsTrue(perceptController.HasPercept(actor, AIPerceptType.Visual));
            Assert.IsTrue(perceptController.TryGetPercept(actor, AIPerceptType.Visual, out AIPercept percept2));
            Assert.AreEqual(percept, percept2);
            Assert.AreEqual(1, perceptController.GetPerceptCount(AIPerceptType.Visual));

            AITimeManager.Instance.UpdateTime(1.0);
            perceptController.Tick();
            Assert.IsTrue(perceptController.HasPercept(actor, AIPerceptType.Visual));
            percept.Release();
            AITimeManager.Instance.UpdateTime(0.15);
            perceptController.Tick();
            Assert.IsTrue(perceptController.HasPercept(actor, AIPerceptType.Visual));
            AITimeManager.Instance.UpdateTime(0.25);
            perceptController.Tick();
            Assert.IsFalse(perceptController.HasPercept(actor, AIPerceptType.Visual));
        }        
        
        [Test]
        public void ContactTest()
        {
            AIActor self = new AIActor("Self");
            AIPerceptController perceptController = new(self);

            AIPerceptEnvelope envelope = new(0d, 0.1, 0.1,  0.3, 0.75f);

            AIActor actor = new AIActor("TestActor");
            actor.Position = new AIVector3(7.0f, 0.0f, 13.0f);
            Assert.IsNotNull(perceptController);
            Assert.AreEqual(self, perceptController.Actor);
            AIPercept percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 1.0f, actor.Position, actor, envelope: envelope);
            perceptController.AddPercept(actor, percept);

            Assert.IsTrue(perceptController.HasPercept(actor, AIPerceptType.Visual));
            Assert.IsTrue(self.ContactsBlackboard.TryGetValue(actor.Name, out IAIBlackboard contact));
            Assert.IsNotNull(contact);
            Assert.IsTrue(contact.TryGetValue("Actor".AsHashedString(), out AIActor theActor));
            Assert.AreEqual(actor, theActor);
            Assert.IsTrue(contact.TryGetValue("Time".AsHashedString(), out double time));
            Assert.AreEqual(0.0, time);
            AITimeManager.Instance.UpdateTime(1.0);            
            Assert.IsTrue(contact.TryGetValue("LastPosition".AsHashedString(), out IAIVector3 position));
            Assert.AreEqual(new AIVector3(7f, 0f, 13f), position);

            //AITimeManager.Instance.UpdateTime(1.0);

            actor.Position = new AIVector3(1.0f, 0.0f, 3.0f);
            percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 1.0f, actor.Position, actor, envelope: envelope);
            perceptController.AddPercept(actor, percept);

            Assert.IsTrue(self.ContactsBlackboard.TryGetValue(actor.Name, out IAIBlackboard contact2));
            Assert.IsNotNull(contact2);
            Assert.AreEqual(contact, contact2);

            Assert.IsTrue(contact2.TryGetValue("Actor".AsHashedString(), out theActor));
            Assert.AreEqual(actor, theActor);
            Assert.IsTrue(contact2.TryGetValue("Time".AsHashedString(), out time));
            Assert.AreEqual(1.0, time);            
            Assert.IsTrue(contact2.TryGetValue("LastPosition".AsHashedString(), out position));
            Assert.AreEqual(new AIVector3(1f, 0f, 3f), position);
        }
    }
 }