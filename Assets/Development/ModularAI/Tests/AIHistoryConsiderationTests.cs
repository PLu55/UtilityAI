using NUnit.Framework;

namespace PLu.ModularAI.Tests
{

    [TestFixture]
    public class AIHistoryConsiderationTests
    {      
        [SetUp]
        public void HistoryConsiderationTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
            AITimeManager.SetTimeManager(new AITimeManager());
        }
        
        [Test]
        public void TimeSinceLastActiveTests()
        {
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Considerations()
                        .HistoryConsideration("TimeSinceLastActive")
                            .DoubleWeightFunction("lt", "3.0")
                        .End()
                    .End()
                .End()
                .Build();
            
            //AITimeManager.Instance.Reset();
            var creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIOptionBase option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);

            // No history yet
            option.Consider();
            Assert.AreEqual(0f, option.Weight);

            // History added
            option.Update();
            AITimeManager.Instance.UpdateTime(2f);
            //option.Deselect();
            Assert.AreEqual(2, option.ExecutionHistory.Count);
            Assert.AreEqual(option.ExecutionHistory.First.State,AIExecutionHistoryState.End);
            option.Consider();
            Assert.AreEqual(1f, option.Weight);
            Assert.AreEqual(-float.MaxValue, option.Rank);

            AITimeManager.Instance.UpdateTime(4f);
            option.Consider();
            Assert.AreEqual(0f, option.Weight);
        }        
        
        // TODO: This can never work as the history is not updated when the option is selected
        // it will never be selected because the weight is always 0
        // [Test]
        // public void TimeSinceActivationTests()
        // {
        //     var specification = new AISpecificationBuilder().Partial()
        //         .Option("TestOption1")
        //             .Considerations()
        //                 .HistoryConsideration("TimeSinceActivation")
        //                     .DoubleWeightFunction("lt", "3.0")
        //                 .End()
        //             .End()
        //         .End()
        //         .Build();

        //     //AITimeManager.Instance.Reset();
        //     var creationData = AITestUtilities.CreateCreationData(specification.Root);
        //     var option = AIOptionFactory.Instance.Create(creationData);
        //     Assert.IsNotNull(option);

        //     // No history yet
        //     option.Consider();
        //     Assert.AreEqual(0f, option.Weight);

        //     // History added
        //     option.Act();
        //     option.Consider();
        //     Assert.AreEqual(1f, option.Weight);
        //     Assert.AreEqual(-float.MaxValue, option.Rank);

        //     option.Reset();            
        //     option.Consider();
        //     Assert.AreEqual(0f, option.Weight);
            
        //     AITimeManager.Instance.UpdateTime(4f);
        //     option.Consider();
        //     Assert.AreEqual(0f, option.Weight);
        // }        
        [Test]
        public void NumberOfActivationTests()
        {
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Considerations()
                        .HistoryConsideration("NumberOfActivations")
                            .IntWeightFunction("lte", "2")
                        .End()
                    .End()
                .End()
                .Build();

            //AITimeManager.Instance.Reset();
            var creationData = AITestUtilities.CreateCreationData(specification.Root);
            var option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);

            // No history yet
            option.Consider();
            Assert.AreEqual(1f, option.Weight);

            // History added
            option.Update();
            option.Consider();
            Assert.AreEqual(1f, option.Weight);
            Assert.AreEqual(-float.MaxValue, option.Rank);
  
            option.Update();        
            option.Consider();
            Assert.AreEqual(1f, option.Weight);

            option.Update();        
            option.Consider();
            Assert.AreEqual(0f, option.Weight);
        }
    }
    
 }