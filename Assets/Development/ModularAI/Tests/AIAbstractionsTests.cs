using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIAbstractionsTests
    {
        const string ModularAIDirectory = "Assets/Development/ModularAI/";
        AISpecification specification;
        AISpecificationNode root;

        [OneTimeSetUp]
        public void AIAbstractionTestsSetup()
        {
            AISpecificationManager specificationManager = AISpecificationManager.Instance;
            specificationManager.LoadXMLSpecifications(ModularAIDirectory + "Tests/TestData1.xml");
            specification = specificationManager.GetAISpecification("Spec1");
            Assert.IsNotNull(specification);
            AISpecificationNode root = specification.Root;
            Assert.IsNotNull(root);
        }
        
        //[Test]
        public void AIOptionTests()
        {
            AISpecificationNode optionSpec = root.GetDescendent("Option");
            Assert.IsNotNull(optionSpec);

            IAIActor actor = new AIActor("TestActor");
            AICreationData data = new AICreationData(optionSpec, actor);

            AIOptionFactory factory = AIOptionFactory.Instance;
            AIOptionBase option = factory.Create(data);
            Assert.IsNotNull(option);
        }

    }    
}
