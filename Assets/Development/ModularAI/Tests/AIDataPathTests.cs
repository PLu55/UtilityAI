using NUnit.Framework;
using PLu.Utilities;

using System.Text.RegularExpressions;
using UnityEngine;

namespace PLu.ModularAI.Tests
{
    public class AIDataPathTests
    {
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void SimpleDataPathTests()
        {
            IAIActor actor = new AIActor("TestActor");
            AIDataPath dataPath1 = new AIDataPath("Brain:V1", actor);
            Assert.NotNull(dataPath1);
            Assert.AreEqual("Brain:V1", dataPath1.Name);

            AIDataPath dataPath2 = new AIDataPath("V2", actor);
            Assert.NotNull(dataPath2);
            Assert.AreEqual("V2", dataPath2.Name);

            actor.BrainBlackboard.SetValue("V1", 5);
            actor.SetValue("V2", "513");
            actor.SetValue("V3", actor);

            Assert.AreEqual(5, dataPath1.GetValue<int>());
            Assert.AreEqual("513", dataPath2.GetValue<string>());
            Assert.AreEqual(actor, new AIDataPath("V3", actor).GetValue<IAIActor>());

            dataPath1.SetValue(13);
            Assert.AreEqual(13, dataPath1.GetValue<int>());
            dataPath2.SetValue("1026");
            Assert.AreEqual("1026", dataPath2.GetValue<string>());
        }

        [Test]
        public void HierarchicTest()
        {
            IAIActor me = new AIActor("Me");            
            IAIActor enemy = new AIActor("Enemy");
            
            IAIBlackboard contacts = me.GetBlackboard("Contacts");
            Assert.NotNull(contacts);
            IAIBlackboard contact = new AIContact(enemy, AIVector3.Zero, 0.0, true);
            contacts.SetValue("Enemy", contact);
            Assert.AreEqual(contact, contacts.GetValue<AIContact>("Enemy"));

            AIDataPath dataPath1 = new("Contacts:Enemy.IsThreat", me);
            Assert.NotNull(dataPath1);
            Assert.AreEqual("Contacts:Enemy.IsThreat", dataPath1.Name);
            Assert.IsTrue(dataPath1.GetValue<bool>());

            AIDataPath dataPath2 = new("Contacts:Enemy.Actor.Name", me);
            Assert.AreEqual(enemy.Name, dataPath2.GetValue<HashedString>());
        }        
        
        [Test]
        public void IndirectTest()
        {
            IAIActor me = new AIActor("Me");            
            IAIActor enemy = new AIActor("Enemy");
            AIContact contact = new AIContact(enemy, new AIVector3(1f ,2f ,3f), 0.0, true);

            me.ContactsBlackboard.SetValue("Enemy", contact);
            me.SetValue<HashedString>("Key", "Enemy");

            AIDataPath dataPath = new("Contacts:@(Key).Actor", me);
            Assert.AreEqual(enemy, dataPath.GetValue<IAIActor>());
            dataPath = new("Contacts:@(Key).LastPosition", me);
            Assert.AreEqual(new AIVector3(1f, 2f, 3f), dataPath.GetValue<AIVector3>());

            AIDataPath dataPath2 = new("Contacts:@(Key)", me);
            dataPath2.SetValue(new AIContact(enemy, new AIVector3(4f ,5f ,6f), 0.0, true));
            Assert.AreEqual(new AIVector3(4f, 5f, 6f), dataPath.GetValue<AIVector3>());
        }        
        
        [Test]
        public void ComplexDataPathTest()
        {
            IAIActor me = new AIActor("Me");            
            IAIActor enemy = new AIActor("Enemy");
            enemy.SetValue<HashedString>("Faction", "HostileFaction");
            AILogger.Log($"Enemy faction: {enemy.GetValue<HashedString>("Faction")}");
            Assert.AreEqual("HostileFaction".AsHashedString(), enemy.GetValue<HashedString>("Faction"));
            Assert.AreEqual(typeof(HashedString), enemy.GetValue<HashedString>("Faction").GetType());
            me.ContactsBlackboard.SetValue("Enemy", new AIContact(enemy, AIVector3.Zero, 0.0, true));
            me.BrainBlackboard.SetValue("PickerEntity", enemy);
            Assert.AreEqual("HostileFaction".AsHashedString(), me.BrainBlackboard.GetValue<IAIBlackboard>("PickerEntity").GetValue<HashedString>("Faction"));
            Assert.AreEqual("HostileFaction".AsHashedString(), new AIDataPath("Brain:PickerEntity.Faction", me).GetValue<HashedString>());

            AIBlackboard relations = new AIBlackboard("Relations");
            me.SetValue("Relations", relations);
            relations.SetValue("HostileFaction", -0.5f);

            AILogger.Log($"Enemy faction: {enemy.GetValue<HashedString>("Faction")}");
            
            AIDataPath dataPathx = new("Actor:Relations.HostileFaction", me);
            AILogger.Log($"DataPath: {dataPathx}");

            Assert.AreEqual(-0.5f, dataPathx.GetValue<float>());

            AIDataPath dataPath = new("Actor:Relations.@(Brain:PickerEntity.Faction)", me);
            AILogger.Log($"DataPath: {dataPath}");

            Assert.AreEqual(-0.5f, dataPath.GetValue<float>());
        }

        [Test]
        public void MyTest()
        {
            IAIActor me = new AIActor("Me");
            AIBlackboard relations = new AIBlackboard("Relations");
            me.SetValue("Relations", relations);
            relations.SetValue("HostileFaction", -0.5f);

            IAIActor enemy = new AIActor("Enemy");
            enemy.SetValue<HashedString>("Faction", "HostileFaction");
            AIContact contact = new AIContact(enemy, new AIVector3(1f, 2f, 3f), 0.0, true);
            me.GetValue<IAIBlackboard>("Contacts").SetValue("Enemy", contact);
            me.ContactsBlackboard.SetValue("Enemy", contact);
            AIDataPath dataPath;
            
            dataPath = new("Contacts:Enemy", me);
            Assert.AreEqual(contact, dataPath.GetValue<AIContact>());
            dataPath = new("Contacts:Enemy.LastPosition", me);
            Assert.AreEqual(new AIVector3(1f, 2f, 3f), dataPath.GetValue<AIVector3>());


            //me.SetValue("Brain:PickerEntity", enemy);
            me.BrainBlackboard.SetValue("PickerEntity", enemy);

            Assert.AreEqual("HostileFaction".AsHashedString(), me.BrainBlackboard.GetValue<IAIEntity>("PickerEntity").GetValue<HashedString>("Faction"));
            dataPath = new("Relations.@(Brain:PickerEntity.Faction)", me);
            Assert.AreEqual(-0.5f, dataPath.GetValue<float>());

            dataPath = new("Relations.@(Brain:PickerEntity.Actor.Faction)", me);
            Assert.AreEqual(-0.5f, dataPath.GetValue<float>());
        }       
        
        [Test]
        public void MySetTest()
        {            
            IAIActor me = new AIActor("Me");
            AIBlackboard relations = new AIBlackboard("Relations");
            me.SetValue("Relations", relations);
            relations.SetValue("HostileFaction", -0.5f);

            IAIActor enemy = new AIActor("Enemy");
            enemy.SetValue<HashedString>("Faction", "HostileFaction");
            AIContact contact = new AIContact(enemy, new AIVector3(1f, 2f, 3f), 0.0, true);
            me.GetValue<IAIBlackboard>("Contacts").SetValue("Enemy", contact);
            me.ContactsBlackboard.SetValue("Enemy", contact);
            AIDataPath dataPath;

            me.BrainBlackboard.SetValue("PickerEntity", enemy);
            IAIEntity entity = me.BrainBlackboard.GetValue<IAIEntity>("PickerEntity");
            Assert.AreEqual("HostileFaction".AsHashedString(), entity.GetValue<HashedString>("Faction"));

            dataPath = new("Relations.@(Brain:PickerEntity.Faction)", me);
            Assert.AreEqual(-0.5f, dataPath.GetValue<float>());
            
        }
    }
}