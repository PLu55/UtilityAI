using NUnit.Framework;
using System.Linq;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIXMLSpecificationTests
    {
        const string ModularAIDirectory = "Assets/Development/ModularAI/";

        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void ReadAndParseXml()
        {
            const string specFile = "TestData2.xml";
            AISpecificationManager specificationManager = AISpecificationManager.Instance;
            specificationManager.LoadXMLSpecifications(ModularAIDirectory + "Tests/" + specFile);
            AISpecification spec = specificationManager.GetAISpecification("Spec2");
            Assert.IsNotNull(spec);
            Assert.AreEqual("Spec2", spec.Name);
            AISpecificationNode root = spec.Root;
            Assert.IsNotNull(root);
            Assert.AreEqual("Reasoner", root.Name);

            Assert.AreEqual("Selection", root.Type);
            Assert.AreEqual("Options", root.Children[0].Name);
            Assert.AreEqual(1, root.Children[0].Children.Count());
            AISpecificationNode option0 = root.Children[0].Children[0];
            Assert.AreEqual("Option", option0.Name);

            AISpecificationNode consideration0 = option0.Children[0].Children[0];
            Assert.AreEqual("Consideration", consideration0.Name);
            AISpecificationNode consideration1 = option0.Children[0].Children[1];
            Assert.AreEqual("Consideration", consideration1.Name);
            Assert.AreEqual("Distance", consideration1.Type);
            Assert.AreEqual(3, consideration1.Children.Count());
            Assert.AreEqual(consideration1, consideration1.Children[0].Parent);

            IAIActor actor = new AIActor("TestActor");
            AICreationData data = new AICreationData(root, actor);
            Assert.IsNotNull(data);

            var factory = AIReasonerFactory.Instance;
            var reasoner = factory.Create(data);
            Assert.IsNotNull(reasoner);
        }

    }
}
