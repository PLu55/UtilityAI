using NUnit.Framework;

namespace PLu.ModularAI.Tests
{

    [TestFixture]
    public class AISequenceReasonerTests
    {                     
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif

            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void SingleValidOptionTest()
        {
            var specification = new AISpecificationBuilder()
                .Reasoner("Sequence")
                    .Options()
                        .Option("Option1")                            
                            .Considerations()
                                .VariableConsideration("TestVariable1", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable1", "Option1")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new(specification, actor);
            var reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            Assert.AreEqual(typeof(AISequenceReasoner), reasoner.GetType());

            actor.SetValue("TestVariable1", true);

            reasoner.Tick();
            Assert.IsTrue(reasoner.IsDone);
            Assert.AreEqual("Option1", actor.GetValue<string>("ResultVariable1"));

        }

        [Test]
        public void SingleInvalidOptionTest()
        {
            var specification = new AISpecificationBuilder()
                .Reasoner("Sequence")
                    .Options()
                        .Option("Option1")                            
                            .Considerations()
                                .VariableConsideration("TestVariable1", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable1", "Option1")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new(specification, actor);
            var reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            Assert.AreEqual(typeof(AISequenceReasoner), reasoner.GetType());

            actor.SetValue("TestVariable1", false);

            reasoner.Tick();
            Assert.IsTrue(reasoner.IsDone);
            Assert.AreNotEqual("Option1", actor.GetValue<string>("ResultVariable1"));
            
        }

        [Test]
        public void MultipleOptionTest()
        {
            var specification = new AISpecificationBuilder()
                .Reasoner("Sequence")
                    .Options()
                        .Option("Option1")
                            .Actions()
                                .SetVariableAction("ResultVariable1", "Option1")
                            .End()
                        .End()
                        .Option("Option2")                            
                            .Considerations()
                                .VariableConsideration("TestVariable2", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable2", "Option2")
                            .End()
                        .End()
                        .Option("Option3")
                            .Considerations()
                                .VariableConsideration("TestVariable3", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable3", "Option3")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new(specification, actor);
            var reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            Assert.AreEqual(typeof(AISequenceReasoner), reasoner.GetType());

            actor.SetValue("TestVariable2", false);
            actor.SetValue("TestVariable3", true);

            reasoner.Tick();

            Assert.AreEqual("Option1", actor.GetValue<string>("ResultVariable1"));
            Assert.AreNotEqual("Option2", actor.GetValue<string>("ResultVariable2"));
            Assert.AreEqual("Option3", actor.GetValue<string>("ResultVariable3"));
        }        
        
        [Test]
        public void MultipleOptionWithWaitTest()
        {
            var specification = new AISpecificationBuilder()
                .Reasoner("Sequence")
                    .Options()
                        .Option("Option1")
                            .Actions()
                                .SetVariableAction("ResultVariable1", "Option1")
                            .End()
                        .End()                        .
                        Option("Option2")
                            .Actions()
                                .WaitAction("5")
                                .SetVariableAction("ResultVariable2", "Option2")
                            .End()
                        .End()
                        .Option("Option3")
                            .Considerations()
                                .VariableConsideration("TestVariable3", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable3", "Option3")
                            .End()
                        .End()                        
                        .Option("Option4")
                            .Actions()
                                .SetVariableAction("ResultVariable4", "Option4")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new AICreationData(specification, actor);
            var reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            Assert.AreEqual(typeof(AISequenceReasoner), reasoner.GetType());

            actor.SetValue("TestVariable3", false);

            reasoner.Tick();
            Assert.AreEqual("Option1", actor.GetValue<string>("ResultVariable1"));
            Assert.AreNotEqual("Option2", actor.GetValue<string>("ResultVariable2"));
            
            AITimeManager.Instance.UpdateTime(10f);
            reasoner.Tick();
            Assert.AreEqual("Option2", actor.GetValue<string>("ResultVariable2"));
            Assert.AreNotEqual("Option3", actor.GetValue<string>("ResultVariable3"));
            Assert.AreEqual("Option4", actor.GetValue<string>("ResultVariable4"));
        }

        [Test]
        public void MultipleOptionWithAbortTest()
        {
            var specification = new AISpecificationBuilder()
                .Reasoner("Sequence", "AbortOnInvalid")
                    .Options()
                        .Option("Option1")
                            .Actions()
                                .SetVariableAction("ResultVariable1", "Option1")
                            .End()
                        .End()
                        .Option("Option2")                            
                            .Considerations()
                                .VariableConsideration("TestVariable2", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable2", "Option2")
                            .End()
                        .End()
                        .Option("Option3")
                            .Considerations()
                                .VariableConsideration("TestVariable3", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable3", "Option3")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new(specification, actor);
            var reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            Assert.AreEqual(typeof(AISequenceReasoner), reasoner.GetType());

            actor.SetValue("TestVariable2", false);
            actor.SetValue("TestVariable3", true);

            reasoner.Tick();

            Assert.AreEqual("Option1", actor.GetValue<string>("ResultVariable1"));
            Assert.AreNotEqual("Option2", actor.GetValue<string>("ResultVariable2"));
            Assert.AreNotEqual("Option3", actor.GetValue<string>("ResultVariable3"));
        } 
    }
}