using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public partial class AIBlackboardTests
    {       
        HashedString key1 = AIBlackboardManager.Instance.GetOrRegister("key1");
        HashedString key2 = AIBlackboardManager.Instance.GetOrRegister("key2");
        HashedString key3 = AIBlackboardManager.Instance.GetOrRegister("key3");

        [OneTimeSetUp]
        public void PerceptTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void BlackboardKeysTest()
        {
            HashedString key1 = AIBlackboardManager.Instance.GetOrRegister("key1");
            Assert.AreEqual("key1".AsHashedString(), key1);
        }

        [Test]
        public void BlackboardTest()
        {
            AIBlackboard blackboard = new();

            Assert.IsNotNull(blackboard);
            
            blackboard.SetValue(key1, 1.0f);
            blackboard.SetValue(key2, "2.0");
            blackboard.SetValue(key3, 3.0);

            Assert.AreEqual(1.0f, blackboard.GetValue<float>(key1));
            Assert.AreEqual("2.0", blackboard.GetValue<string>(key2));
            Assert.AreEqual(3.0, blackboard.GetValue<double>(key3));
            Assert.IsTrue(blackboard.TryGetValue(key1, out float floatValue));
            Assert.AreEqual(1.0f, floatValue);
            Assert.IsTrue(blackboard.TryGetValue(key2, out string stringValue));
            Assert.AreEqual("2.0", stringValue);
            Assert.IsTrue(blackboard.TryGetValue(key3, out double doubleValue));
            Assert.AreEqual(3.0, doubleValue);
            Assert.AreEqual(3, blackboard.Count);

            blackboard.RemoveValue<string>(key2);

            Assert.AreEqual(2, blackboard.Count);

            Assert.AreEqual(1.0f, blackboard.GetValue<float>(key1));
            Assert.AreEqual(3.0f, blackboard.GetValue<double>(key3));
        }        
        
        [Test]
        public void BlackboardIterationTest()
        {
            AIBlackboard blackboard = new();

            Assert.IsNotNull(blackboard);
            
            blackboard.SetValue(key1, 1.0f);
            blackboard.SetValue(key2, "2.0");
            blackboard.SetValue(key3, 3.0);

            int n = 0;
            foreach (var key in blackboard.Keys)
            {
                n++;
                Assert.IsTrue(blackboard.ContainsKey(key));

            }

            Assert.AreEqual(3, n);
            n = 0;

            foreach (var value in blackboard.Values)
            {
                Assert.IsNotNull(value);
            }

        }
    }
 }