using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIEntityExistsConsiderationTests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void SimpleTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .EntityExistsConsideration("Threats", "Result")
                    .Picker()
                        .Reasoner("DualUtility")
                            .Options()
                                .Option("Option")
                                    .Considerations()
                                        .VariableConsideration("Brain:PickerEntity", "float")
                                            .CurveWeightFunction("Linear", "0", "0.333")
                                        .End()
                                    .End()
                                .End()
                            .End()
                        .End()
                    .End()
                    .BoolWeightFunction()
                .End()
                .Build();
            
            var actor = new AIActor("Me");
            var creationData = new AICreationData(specification, actor);
            var consideration = AIConsiderationFactory.Instance.Create(creationData);
            Assert.IsNotNull(consideration);

            actor.ThreatsBlackboard.SetValue("Value1", 1f);
            actor.ThreatsBlackboard.SetValue("Value2", 2f);
            actor.ThreatsBlackboard.SetValue("Value3", 3f);
            consideration.Consider();
            Assert.AreEqual(0f, consideration.Addend);
            Assert.AreEqual(1f, consideration.Multiplier);
            Assert.AreEqual(-float.MaxValue, consideration.Rank);
            Assert.AreEqual(1, actor.BrainBlackboard.Count);
            Assert.IsTrue(actor.BrainBlackboard.ContainsKey("Result"));
            Assert.AreEqual(3f, actor.BrainBlackboard.GetValue<float>("Result"));
        }        
        
        [Test]
        public void ItemKeyTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .EntityExistsConsideration("Percepts", "Result")
                    .Picker()
                        .Reasoner("DualUtility")
                            .Options()
                                .Option("Option")
                                    .Considerations()
                                        .VariableConsideration("Brain:PickerEntity.PerceptType", "string")
                                            .StringWeightFunction()
                                                .Entries()
                                                    .String(value: "Visual", veto: "false")
                                                .End()
                                                .Default(veto: "true")
                                            .End()
                                        .End()
                                    .End()
                                .End()
                            .End()
                        .End()
                    .End()
                    .BoolWeightFunction()
                .End()
                .Build();
            
            var actor = new AIActor("Me");
            var creationData = new AICreationData(specification.Root, actor);
            var consideration = AIConsiderationFactory.Instance.Create(creationData);
            Assert.IsNotNull(consideration);
            var pbb1 = actor.PerceptsBlackboard;

            var entity = new AIActor("Actor1");
            var percept = new AIPercept(AIPerceptType.Visual, 0.0, 1.0f, AIVector3.Zero, entity);
            actor.PerceptsBlackboard.SetValue("Percept1", percept);

            var percept2 = actor.PerceptsBlackboard.GetValue<AIPercept>("Percept1");
            Assert.AreEqual(percept, percept2);

            consideration.Consider();

            Assert.AreEqual(0f, consideration.Addend);
            Assert.AreEqual(1f, consideration.Multiplier);
            Assert.AreEqual(-float.MaxValue, consideration.Rank);
            Assert.AreEqual(1, actor.BrainBlackboard.Count);
            Assert.IsTrue(actor.BrainBlackboard.ContainsKey("Result"));
            Assert.IsTrue(actor.BrainBlackboard.TryGetValue("Result", out AIPercept result));
            Assert.NotNull(result);
        }

        [Test]
        public void HandlePerceptsTest()
        {
            var specification = new AISpecificationBuilder("HandlePercepts")
                .Reasoner("Selection")
                    .Options()
                        .Option("Option1")
                            .Considerations()
                                .EntityExistsConsideration("Percepts", "Result")
                                    .Picker()
                                        .Reasoner("DualUtility")
                                            .Options()
                                                .Option("EntityExistsOption")
                                                    .Considerations()
                                                        .VariableConsideration("Relations.@(Brain:PickerEntity.Entity.Faction)", "float")
                                                            .CurveWeightFunction("Linear", "0", "-1", "0.5", "0.5", "0")
                                                        .End()
                                                    .End()
                                                    .Actions() // TODO: Actions are not called. Consider how EntityExistsConsideration should work!
                                                        //.LogAction("Brain:PickerEntity.Entity", "PLu.ModularAI.IAIEntity")
                                                        .SetVariableAction("HasResult", "True", variableType: "System.Boolean")
                                                    .End()
                                                .End()
                                            .End()
                                        .End()
                                    .End()
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                //.LogAction("Brain:Result.Actor.Name", "System.String")
                                .SetVariableAction("HasResult", "True", variableType: "System.Boolean")
                            .End()
                        .End()
                    .End()
                .End()
                .BuildAndStore(true);

            AIDataPath dataPath;
            Assert.NotNull(specification);

            AITimeManager.Instance.UpdateTime(100d);
            AIPerceptEnvelope envelope = new(0.1, 0.2, 0.3, 0.6, 0.75f);

            //Setup Me Actor
            AIActor me = new AIActor("Me");
            AIPerceptController perceptController = new(me);
            AICreationData creationData = new AICreationData(specification, me);
            me.Reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(me.Reasoner);

            // Setup Enemy Actor
            AIActor enemy = new AIActor("Enemy");
            enemy.SetValue<HashedString>("Faction", "HostileFaction");            
            Assert.AreEqual("HostileFaction".AsHashedString(), enemy.GetValue<HashedString>("Faction"));


            // Set my relations to the HostileFaction

            me.SetValue("Relations", new AIBlackboard("Relations"));
            dataPath = new("Relations.HostileFaction", me);
            dataPath.SetValue(-0.5f);
            Assert.AreEqual(-0.5f, dataPath.GetValue<float>());

            // Generate Percept, I see the enemy
            // Add percept to the percept controller 
            AIPercept percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 2.0f, AIVector3.Zero, envelope: envelope, entity: enemy);
            perceptController.AddPercept(enemy, percept);
            AITimeManager.Instance.UpdateTime(0.3);
            // Check if the percept was received by me
            Assert.IsTrue(me.PerceptsBlackboard.TryGetValue("Enemy", out AIPercept receivedPercept));
            Assert.AreEqual(percept, receivedPercept);
            Assert.AreEqual(2.0f, receivedPercept.CurrentLevel);

            me.Reasoner.Tick();
            dataPath = new("Brain:Result", me);
            Assert.IsTrue(me.GetValue<bool>("HasResult"));
            Assert.AreEqual(percept, dataPath.GetValue<AIPercept>());
        }
    }
}