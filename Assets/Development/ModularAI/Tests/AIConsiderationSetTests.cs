using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIConsiderationSetTests
    {   
        private string ConsiderationSpecification(float Addend, float Multiplier, float Rank)
        {
            return
                "<Consideration Type=\"Distance\" Addend=\"" + Addend + "\" Multiplier=\"" + Multiplier + "\" Rank=\"" + Rank + "\">" +
                    "<Target Type=\"Self\"/>" +
                    "<Target Type=\"Self\"/>" +
                    "<WeightFunction Type=\"Constant\"/>" +
                "</Consideration>";
        }
        // private AISpecificationNode ConsiderationSpecification(float Addend, float Multiplier, float Rank)
        // {
        //     AISpecificationNode root = new AISpecificationBuilder().Partial()
        //         Be
        //     .Build();
        //     return root;
        // }

        [OneTimeSetUp]
        public void AIConsiderationSetTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void AIDefaultConsiderationSetTests()
        {       
            // string specification =                         
            //     "<Considerations Addend=\"1\" Multiplier=\"2\" Rank=\"10\">" +
            //         ConsiderationSpecification( 0,  1,  -float.MaxValue) +
            //     "</Considerations>";

            var specification = new AISpecificationBuilder().Partial()
                .Considerations(addend: "1", multiplier: "2", rank: "10")
                    .DistanceConsideration(addend: "0", multiplier: "1")
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction()
                    .End()
                .End()
                .Build();

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            var considerationSet = AIConsiderationSetFactory.Instance.Create(creationData);
            Assert.IsNotNull(considerationSet);

            Assert.AreEqual(typeof(AIAndConsiderationSet), considerationSet.GetType());
            considerationSet.Consider();
            Assert.AreEqual(1f, considerationSet.Addend);
            Assert.AreEqual(2f, considerationSet.Multiplier, 1e-6f);
            Assert.AreEqual(10f, considerationSet.Rank);

        }        
        
        [Test]
        public void AIAndConsiderationSetTests()
        {            
            // string specification =                         
            //     "<Considerations Type=\"And\">" +
            //         ConsiderationSpecification( 1,  1,  -float.MaxValue) +
            //         ConsiderationSpecification( 0,  2,  0) +
            //         ConsiderationSpecification( 2,  3,  10) +
            //     "</Considerations>";

            var specification = new AISpecificationBuilder().Partial()
                .Considerations("And")
                    .DistanceConsideration(addend: "0", multiplier: "1")
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction()
                    .End()                    
                    .DistanceConsideration(addend: "0", multiplier: "2", rank: "0")
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction()
                    .End()
                    .DistanceConsideration(addend: "2", multiplier: "3", rank: "10")
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction()
                    .End()
                .End()
                .Build();
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIConsiderationSetBase considerationSet = AIConsiderationSetFactory.Instance.Create(creationData);
            Assert.IsNotNull(considerationSet);
            Assert.AreEqual(typeof(AIAndConsiderationSet), considerationSet.GetType());

            considerationSet.Consider();
            Assert.AreEqual(3f, considerationSet.Addend);
            Assert.AreEqual(6f, considerationSet.Multiplier, 1e-6f);
            Assert.AreEqual(10f, considerationSet.Rank);
            Assert.AreEqual(18f, considerationSet.Weight, 1e-6f);
        }        
        
        [Test]
        public void AIOrConsiderationSetTests()
        {                
        //     string specification = 
        //         "<Considerations Type=\"Or\">" +
        //             ConsiderationSpecification( 0,  1,  -float.MaxValue) +
        //             ConsiderationSpecification( 1,  0,  -float.MaxValue) +
        //             ConsiderationSpecification( 2,  3,  0) +
        //             ConsiderationSpecification( 1,  2,  10) +
        //         "</Considerations>";            
                
            var specification = new AISpecificationBuilder().Partial()
                .Considerations("Or",addend: "0")
                    .DistanceConsideration()
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction(addend: "0", multiplier: "1")
                    .End()                    
                    .DistanceConsideration()
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction(addend: "1", multiplier: "0")
                    .End()
                    .DistanceConsideration()
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction(addend: "2", multiplier: "3", rank: "0")
                    .End()                    
                    .DistanceConsideration()
                        .SelfTarget()
                        .SelfTarget()
                        .ConstantWeightFunction(addend: "3", multiplier: "2", rank: "10")
                    .End()
                .End()
                .Build();

            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIConsiderationSetBase considerationSet = AIConsiderationSetFactory.Instance.Create(creationData);
            Assert.IsNotNull(considerationSet);
            Assert.AreEqual(typeof(AIOrConsiderationSet), considerationSet.GetType());

            considerationSet.Consider();
            Assert.AreEqual(typeof(AIOrConsiderationSet), considerationSet.GetType());            
            Assert.AreEqual(2f, considerationSet.Addend);
            Assert.AreEqual(3f, considerationSet.Multiplier, 1e-6f);
            Assert.AreEqual(0f, considerationSet.Rank);
            Assert.AreEqual(6f, considerationSet.Weight, 1e-6f);
        }
    }
}