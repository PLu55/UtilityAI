using System.Collections;
using System.Diagnostics;
using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AISelectionReasonerTests
    {                     
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif

            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void Test()
        {     
            var specification = new AISpecificationBuilder()
                .Reasoner("Selection")
                    .Options()
                        .Option("Option1")
                            .Considerations()
                                .VariableConsideration("TestVariable1", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option1")
                            .End()
                        .End()
                        .Option("Option2")
                            .Considerations()
                                .VariableConsideration("TestVariable2", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()                              
                                .WaitAction("1")
                                .SetVariableAction("ResultVariable", "Option2")
                            .End()
                        .End()                        
                        .Option("Option3")
                            .Considerations()
                                .VariableConsideration("TestVariable3", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option3")
                            .End()
                        .End()
                    .End()
                .End()   
                .Build();
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification.Root);
            IAIActor actor = creationData.Actor;
            AIReasonerBase reasoner = AIReasonerFactory.Instance.Create(creationData);
            Assert.IsNotNull(reasoner);
            Assert.AreEqual(typeof(AISelectionReasoner), reasoner.GetType());

            actor.SetValue("TestVariable1", false);
            actor.SetValue("TestVariable2", false);
            actor.SetValue("TestVariable3", false);

            Assert.IsFalse(reasoner.IsDone);
            Assert.DoesNotThrow(reasoner.Tick);
            Assert.IsTrue(reasoner.IsDone);
            Assert.AreNotEqual("Option3", actor.GetValue<string>("ResultVariable"));

            actor.SetValue("TestVariable3", true);
            reasoner.Tick();
            Assert.IsTrue(reasoner.IsDone);
            Assert.AreEqual("Option3", actor.GetValue<string>("ResultVariable"));
            
            actor.SetValue("TestVariable2", true);
            reasoner.Tick();
            Assert.IsFalse(reasoner.IsDone);     
            Assert.AreNotEqual("Option2", actor.GetValue<string>("ResultVariable"));
    
            AITimeManager.Instance.UpdateTime(1d);
            reasoner.Tick();
            Assert.IsTrue(reasoner.IsDone);
            Assert.AreEqual("Option2", actor.GetValue<string>("ResultVariable"));
        }
    }    
}