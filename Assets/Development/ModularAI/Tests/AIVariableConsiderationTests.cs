using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIVariableConsiderationTests
    {
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void SimpleTest()
        {              
            // const string specification =

            //     "<Consideration Type=\"Variable\" DataPath=\"TestVariable\" DataType=\"bool\">" +
            //         "<WeightFunction Type=\"Bool\"/>" +
            //     "</Consideration>";

            var specification = new AISpecificationBuilder().Partial()
                .VariableConsideration(dataPath: "TestVariable", dataType: "bool")
                    .BoolWeightFunction()
                .End()
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIBlackboard blackboard = actor.ActorBlackboard;
            AIConsiderationBase consideration = AIConsiderationFactory.Instance.Create(creationData);
            Assert.IsNotNull(consideration);
            Assert.AreEqual(consideration.GetType(), typeof(AIVariableConsideration));

            blackboard.SetValue("TestVariable", true);
            consideration.Consider();
            Assert.AreEqual(1f, consideration.Multiplier);
            blackboard.SetValue("TestVariable", false);
            consideration.Consider();
            Assert.AreEqual(0f, consideration.Multiplier);
        }        
        
        [Test]
        public void HierarchicDataStoreTest()
        {
            var specification = new AISpecificationBuilder().Partial()
                .VariableConsideration(dataPath: "Brain:bb1.bb2.TestValue", dataType: "int")
                    .IntWeightFunction("eq", "13")
                .End()
                .Build();

            AIActor actor = new AIActor("TestActor");

            IAIBlackboard blackboard1 = new AIBlackboard();
            IAIBlackboard blackboard2 = new AIBlackboard();
            actor.BrainBlackboard.SetValue("bb1".AsHashedString(), blackboard1);
            blackboard1.SetValue("bb2".AsHashedString(), blackboard2);
            blackboard2.SetValue("TestValue".AsHashedString(), 13);

            AICreationData creationData = new AICreationData(specification, actor); 
            AIConsiderationBase consideration = AIConsiderationFactory.Instance.Create(creationData);
            Assert.IsNotNull(consideration);

            consideration.Consider();
            Assert.AreEqual(1f, consideration.Multiplier);
        }
    }
}