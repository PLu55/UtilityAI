using System.Collections;
using System.Diagnostics;
using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AISubReasonerActionTests
    {                     
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif

            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void SubReasonerTest()
        {     
            var specification = new AISpecificationBuilder().Partial()
                .SubReasonerAction()
                    .Reasoner("Sequence")
                        .Options()
                            .Option("Option1")
                                .Considerations()
                                    .VariableConsideration("TestVariable1", "bool")
                                        .BoolWeightFunction()
                                    .End()
                                .End()
                                .Actions()
                                    .SetVariableAction("ResultVariable", "Option1")
                                .End()
                            .End()
                            .Option("Option2")
                                .Considerations()
                                    .VariableConsideration("TestVariable2", "bool")
                                        .BoolWeightFunction()
                                    .End()
                                .End()
                                .Actions()
                                    .SetVariableAction("ResultVariable", "Option2")
                                .End()
                            .End()                        
                            .Option("Option3")
                                .Considerations()
                                    .VariableConsideration("TestVariable3", "bool")
                                        .BoolWeightFunction()
                                    .End()
                                .End()
                                .Actions()
                                    .SetVariableAction("ResultVariable", "Option3")
                                .End()
                            .End()
                        .End()
                    .End()
                .End() 
                .Build();

            AIActor actor = new AIActor("TestActor");
            AICreationData creationData = new AICreationData(specification, actor);
            AIActionBase action = AIActionFactory.Instance.Create(creationData);
            Assert.IsNotNull(action);
            Assert.AreEqual(typeof(AISubReasonerAction), action.GetType());

            actor.SetValue("TestVariable1", false);
            actor.SetValue("TestVariable2", false);
            actor.SetValue("TestVariable3", false);
            actor.SetValue("ResultVariable", "None");

            action.Select();
            Assert.IsFalse(action.IsDone);
            action.Update();
            Assert.IsTrue(action.IsDone);
            Assert.AreEqual("None", actor.GetValue<string>("ResultVariable"));
            
            actor.SetValue("TestVariable3", true);
            action.Update();
            Assert.AreEqual("Option3", actor.GetValue<string>("ResultVariable"));
            Assert.IsTrue(action.IsDone);

            Assert.AreEqual("Option3", actor.GetValue<string>("ResultVariable"));
            
            actor.SetValue("TestVariable3", false);
            actor.SetValue("TestVariable2", true);
            action.Update();
            Assert.AreEqual("Option2", actor.GetValue<string>("ResultVariable"));
        }

    }
}