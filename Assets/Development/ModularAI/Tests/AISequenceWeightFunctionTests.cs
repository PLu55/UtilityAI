using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AISequenceWeightFunctionTests
    {            
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void IntSequenceWeightFunctionTest()
        {
            const string specification = 
                "<WeightFunction Type=\"IntSequence\">" +
                    "<Entries>" +
                        "<Integer Value=\"10\" Veto=\"false\"/>" + // if value > = 10, then multiplier is 1
                        "<Integer Value=\"20\" Veto=\"true\"/>" +  // if value > = 20, then multiplier is 0
                    "</Entries>" +
                "</WeightFunction>";
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIIntSequenceWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(5);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(19);
            Assert.AreEqual(1f, values.Multiplier);
            values = weightFunction.Compute(20);
            Assert.AreEqual(0f, values.Multiplier);       
        }         
        [Test]
        public void IntSequenceWeightFunctionTest2()
        {
            const string specification = 
                "<WeightFunction Type=\"IntSequence\">" +
                    "<Entries>" +
                        "<Integer Value=\"10\" Veto=\"true\"/>" + // if value > = 10, then multiplier is 0
                        "<Integer Value=\"20\" Veto=\"false\"/>" +  // if value > = 20, then multiplier is 1
                    "</Entries>" +
                    "<Default Veto=\"false\"/>" +
                "</WeightFunction>";
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIIntSequenceWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(5);
            Assert.AreEqual(1f, values.Multiplier);
            values = weightFunction.Compute(19);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(20);
            Assert.AreEqual(1f, values.Multiplier);       
        }       
        [Test]
        public void FloatSequenceWeightFunctionTest()
        {
            const string specification = 
                "<WeightFunction Type=\"FloatSequence\">" +
                    "<Entries>" +
                        "<Float Value=\"10\" Veto=\"false\"/>" + // if value > = 10, then multiplier is 1
                        "<Float Value=\"20\" Veto=\"true\"/>" +  // if value > = 20, then multiplier is 0
                    "</Entries>" +
                "</WeightFunction>";
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIFloatSequenceWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(5f);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(19f);
            Assert.AreEqual(1f, values.Multiplier);
            values = weightFunction.Compute(20f);
            Assert.AreEqual(0f, values.Multiplier);       
        }
        
        [Test]
        public void FloatSequenceWeightFunctionTest2()
        {
            const string specification = 
                "<WeightFunction Type=\"FloatSequence\">" +
                    "<Entries>" +
                        "<Float Value=\"10\" Veto=\"true\"/>" + // if value > = 10, then multiplier is 0
                        "<Float Value=\"20\" Veto=\"false\"/>" +  // if value > = 20, then multiplier is 1
                    "</Entries>" +
                    "<Default Veto=\"false\"/>" +
                "</WeightFunction>";
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIFloatSequenceWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(5f);
            Assert.AreEqual(1f, values.Multiplier);
            values = weightFunction.Compute(19f);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(20f);
            Assert.AreEqual(1f, values.Multiplier);       
        }
        [Test]
        public void DoubleSequenceWeightFunctionTest()
        {
            const string specification = 
                "<WeightFunction Type=\"DoubleSequence\">" +
                    "<Entries>" +
                        "<Double Value=\"10\" Veto=\"false\"/>" + // if value > = 10, then multiplier is 1
                        "<Double Value=\"20\" Veto=\"true\"/>" +  // if value > = 20, then multiplier is 0
                    "</Entries>" +
                "</WeightFunction>";
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIDoubleSequenceWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(5d);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(19d);
            Assert.AreEqual(1f, values.Multiplier);
            values = weightFunction.Compute(20d);
            Assert.AreEqual(0f, values.Multiplier);       
        }        
        
        [Test]
        public void DoubleSequenceWeightFunctionTest2()
        {
            const string specification = 
                "<WeightFunction Type=\"DoubleSequence\">" +
                    "<Entries>" +
                        "<Double Value=\"10\" Veto=\"true\"/>" + // if value > = 10, then multiplier is 0
                        "<Double Value=\"20\" Veto=\"false\"/>" +  // if value > = 20, then multiplier is 1
                    "</Entries>" +
                    "<Default Veto=\"false\"/>" +
                "</WeightFunction>";
            
            AICreationData creationData = AITestUtilities.CreateCreationData(specification);
            AIWeightFunctionBase weightFunction = AIWeightFunctionFactory.Instance.Create(creationData);
            Assert.IsNotNull(weightFunction);
            Assert.AreEqual(typeof(AIDoubleSequenceWeightFunction), weightFunction.GetType());

            AIWeightValues values = weightFunction.Compute(5d);
            Assert.AreEqual(1f, values.Multiplier);
            values = weightFunction.Compute(19d);
            Assert.AreEqual(0f, values.Multiplier);
            values = weightFunction.Compute(20d);
            Assert.AreEqual(1f, values.Multiplier);       
        }
    }
}
