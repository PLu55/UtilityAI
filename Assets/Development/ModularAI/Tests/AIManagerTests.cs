using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIManagerTests
    {
        const string ModularAIDirectory = "Assets/Development/ModularAI/";
        AISpecification specification;

        [OneTimeSetUp]
        public void AIAbstractionTestsSetup()
        {
            AISpecificationManager specificationManager = AISpecificationManager.Instance;
            specificationManager.LoadXMLSpecifications(ModularAIDirectory + "Tests/TestData1.xml");
            specification = specificationManager.GetAISpecification("Spec1");
            Assert.IsNotNull(specification);

#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        //[Test]
        public void AIOptionTests()
        {
            AISpecificationNode optionSpec = specification.Root.GetDescendent("Option");
            Assert.IsNotNull(optionSpec);

            IAIActor actor = new AIActor("TestActor");
            AICreationData data = new AICreationData(optionSpec, actor);

            AIOptionFactory factory = AIOptionFactory.Instance;
            AIOptionBase option = factory.Create(data);
            Assert.IsNotNull(option);
        }

    } 
}
