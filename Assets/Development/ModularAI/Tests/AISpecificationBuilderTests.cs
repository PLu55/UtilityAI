using System.Runtime.InteropServices;
using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    public class AISpecificationBuilderTests
    {                     
        [OneTimeSetUp]
        public void AISpecificationTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void BuilderTests_1()
        {
            AISpecification spec = new AISpecificationBuilder()
                .Reasoner("Selection")
                    .Options()
                        .Option("Option1")
                            .Considerations()
                                .VariableConsideration("TestVariable1", "bool")
                                    .BoolWeightFunction()
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option1")
                            .End()
                        .End()
                        .Option("Option2")
                            .Considerations()
                                .VariableConsideration("TestVariable2", "float")
                                    .CurveWeightFunction("Linear", "0", "1", "0", "1")
                                .End()
                            .End()
                            .Actions()
                                .SetVariableAction("ResultVariable", "Option2")
                            .End()
                        .End()                        
                    .End()
                .End()   
                .Build(); 
                
            AICreationData creationData = AITestUtilities.CreateCreationData(spec.Root);
            Assert.IsNotNull(creationData);                         
            
            var option = AIReasonerFactory.Instance.Create(creationData);
            Assert.AreEqual(typeof(AISelectionReasoner), option.GetType());
        }

        [Test]
        public void PartialBuildTests_1()
        {
            AISpecification spec = new AISpecificationBuilder().Partial()
                .VariableConsideration("TestVariable2", "float")
                    .CurveWeightFunction("Linear", "0", "1", "1", "0", "1")
                .End()                
                .Build();            
            
            AICreationData creationData = AITestUtilities.CreateCreationData(spec.Root);
            Assert.IsNotNull(creationData);              
            
            var option = AIConsiderationFactory.Instance.Create(creationData);
            Assert.AreEqual(typeof(AIVariableConsideration), option.GetType());
        }        
        
        [Test]
        public void PartialBuildTests_2()
        {
            AISpecification spec = new AISpecificationBuilder().Partial()           
                .Option("Option2")
                    .Considerations()
                        .VariableConsideration("TestVariable2", "float")
                            .CurveWeightFunction("Linear", "0", "1", "1", "0", "1")
                        .End()
                    .End()
                    .Actions()
                        .SetVariableAction("ResultVariable", "Option2")
                    .End()
                .End() 
                .Build(); 

            AICreationData creationData = AITestUtilities.CreateCreationData(spec.Root);
            Assert.IsNotNull(creationData);
            
            var option = AIOptionFactory.Instance.Create(creationData);
            Assert.AreEqual(typeof(AIOption), option.GetType());
        }
    }                
}