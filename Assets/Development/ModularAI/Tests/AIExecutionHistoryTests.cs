using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AIExecutionHistoryTests
    {
        [SetUp]
        public void AIExecutionHistoryTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif

            AITimeManager.SetTimeManager(new AITimeManager());
        }

        [Test]
        public void ExecutionHistoryTests()
        {
            AIExecutionHistory history = new AIExecutionHistory();
            Assert.NotNull(history);

            AIExecutionHistoryNode node = new AIExecutionHistoryNode(13.0, AIExecutionHistoryState.Begin);
            Assert.AreEqual(13.0, node.Time);
            Assert.AreEqual(AIExecutionHistoryState.Begin, node.State);
            history.AddNode(node);

            Assert.AreEqual(1, history.Count);

            AITimeManager.Instance.UpdateTime(13.0f);

            Assert.AreEqual(0.0, history.TimeSinceActivation());
            Assert.AreEqual(0.0, history.TimeSinceLastActive());

            AITimeManager.Instance.UpdateTime(4.0f);

            Assert.AreEqual(4.0, history.TimeSinceActivation());
            node = new AIExecutionHistoryNode(AIExecutionHistoryState.End);
            Assert.AreEqual(17.0, node.Time);
            history.AddNode(node);

            AITimeManager.Instance.UpdateTime(3.0f);

            node = new AIExecutionHistoryNode(AIExecutionHistoryState.Begin);
            history.AddNode(node);

            AITimeManager.Instance.UpdateTime(2.0f);

            Assert.AreEqual(2.0, history.TimeSinceActivation());
            Assert.AreEqual(3, history.Count);
            Assert.AreEqual(0.0, history.TimeSinceLastActive());
            Assert.AreEqual(2, history.NumberOfActivationsSince(double.MaxValue));
            Assert.AreEqual(1, history.NumberOfActivationsSince(4.0));
            Assert.AreEqual(0, history.NumberOfActivationsSince(2.0));

            int count = 0;
            foreach (AIExecutionHistoryNode historyNode in history)
            {
                count++;
            }
            Assert.AreEqual(3, count);
            Assert.AreEqual(history.Count, count);
            history.Clear();
            Assert.AreEqual(0, history.Count);
        }

        [Test]
        public void OptionsExecutionHistoryTests()
        {
            var specification = new AISpecificationBuilder().Partial()
                .Option("TestOption1")
                    .Considerations()
                        .HistoryConsideration("TimeSinceLastActive")
                            .DoubleWeightFunction("lt", "3.0")
                        .End()
                    .End()
                    .Actions()
                        .WaitAction("2")
                        .SetVariableAction("ResultVariable", "Set1")
                        .WaitAction("2")
                        .SetVariableAction("ResultVariable", "Set2")
                    .End()
                .End()
                .Build();
            
            var creationData = AITestUtilities.CreateCreationData(specification.Root);
            AIOptionBase option = AIOptionFactory.Instance.Create(creationData);
            Assert.IsNotNull(option);

            Assert.AreEqual(0, option.ExecutionHistory.Count);

            AITimeManager.Instance.UpdateTime(2.0);
            Assert.IsFalse(option.Update());
            Assert.AreEqual(1, option.ExecutionHistory.Count);
            Assert.AreEqual(AIExecutionHistoryState.Begin, option.ExecutionHistory.First.State);
            AITimeManager.Instance.UpdateTime(2.0);
            Assert.IsFalse(option.Update());

            option.Reset();
            Assert.AreEqual(2, option.ExecutionHistory.Count);
            Assert.AreEqual(AIExecutionHistoryState.End, option.ExecutionHistory.First.State);
            Assert.AreEqual(4.0, option.ExecutionHistory.First.Time);


        } 
    }    
}