using System;
using NUnit.Framework;
using PLu.Utilities;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class AISetVariableActionTests
    { 
        [OneTimeSetUp]
        public void AIOptionTestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }
        
        [Test]
        public void FactoryTest()
        {            
            var specification = new AISpecificationBuilder("SetVariable").Partial()
                .SetVariableAction("TestVariable", "TestValue")
                .Build();

            IAIActor me = new AIActor("Me");
            AICreationData creationData = new (specification, me);
            AIActionBase action = AIActionFactory.Instance.Create(creationData);
            Assert.IsNotNull(action);
            Assert.AreEqual(action.GetType(), typeof(AISetVariableAction));
        }

        [Test]
        public void GivenStringValueTest()
        {            
            var specification = new AISpecificationBuilder("SetVariable").Partial()
                .SetVariableAction("TestVariable", "TestValue")
                .Build();

            IAIActor me = new AIActor("Me");
            AICreationData creationData = new (specification, me);
            AIActionBase action = AIActionFactory.Instance.Create(creationData);
            action.Select();
            Assert.AreEqual("TestValue", me.GetValue<string>("TestVariable"));
        }
        
        [Test]
        public void GivenBoolValueTest()
        {            
            var specification = new AISpecificationBuilder("SetVariable").Partial()
                .SetVariableAction("TestVariable", "True", variableType: "System.Boolean")
                .Build();

            IAIActor me = new AIActor("Me");
            AICreationData creationData = new (specification, me);
            AIActionBase action = AIActionFactory.Instance.Create(creationData);
            action.Select();
            Assert.IsTrue(me.GetValue<bool>("TestVariable"));
        }
        [Test]
        public void GivenFloatValueTest()
        {            
            var specification = new AISpecificationBuilder("SetVariable").Partial()
                .SetVariableAction("TestVariable", "13", variableType: "System.Single")
                .Build();

            IAIActor me = new AIActor("Me");
            AICreationData creationData = new (specification, me);

            AIActionBase action = AIActionFactory.Instance.Create(creationData);
            action.Select();
            Assert.AreEqual(13f, me.GetValue<float>("TestVariable"));
        }
        [Test]
        public void TargetTest()
        {            
            var specification = new AISpecificationBuilder("SetVariable").Partial()
                .SetVariableAction("TestVariable")
                    .DataStoreTarget("TestValue")
                .End()            
                .Build();

            IAIActor me = new AIActor("Me");
            AICreationData creationData = new (specification, me);
            AIActionBase action = AIActionFactory.Instance.Create(creationData);
            me.SetValue("TestValue", me);
            action.Select();
            Assert.AreEqual(me, me.GetValue<IAIActor>("TestValue"));
        }
    }
}