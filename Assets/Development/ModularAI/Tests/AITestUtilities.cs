using System.Xml.Linq;

namespace PLu.ModularAI.Tests
{
    public class AITestUtilities
    {
        // public static TAbstraction CreateAbstraction<TAbstraction, UFactory>(string XMLSpec, UFactory factory)
        // {
        //     var xml = XDocument.Parse(XMLSpec);
        //     AISpecificationNode node = AISpecificationManager.Parse(xml.Root);
        //     IAIActor actor = new AIActor("TestActor");
        //     AICreationData data = new AICreationData(node, actor);
        //     return factory.Create(data);
        // }        
        
        public static AICreationData CreateCreationData(string specification)
        {
            var xml = XDocument.Parse(specification);
            AISpecificationNode node = AISpecificationManager.Parse(xml.Root);
            IAIActor actor = new AIActor("TestActor");
            AICreationData data = new AICreationData(node, actor);
            return data;
        }
        
        public static AICreationData CreateCreationData(AISpecificationNode root)
        {
            IAIActor actor = new AIActor("TestActor");
            AICreationData data = new AICreationData(root, actor);
            return data;
        }
    }
}
