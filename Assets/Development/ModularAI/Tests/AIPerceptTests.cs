using NUnit.Framework;

namespace PLu.ModularAI.Tests
{
    public class AIPerceptTests
    {
        [OneTimeSetUp]
        public void TestsSetup()
        {
#if UNITY
            AILogger.SetLogger(new UAILogger());
#else
            AILogger.SetLogger(new ConsoleAILogger());
#endif
        }

        [Test]
        public void EnvelopTests()
        {
            AIPerceptEnvelope envelop = new(0.1f, 0.2f, 0.3f, 0.6f, 0.75f);
            Assert.AreEqual(0f, envelop.GetCurrentLevel(0.0, 0.0, 0f));
            Assert.AreEqual(0.5f, envelop.GetCurrentLevel(0.2, 0.0, 0f));
            Assert.AreEqual(1f, envelop.GetCurrentLevel(0.3, 0.0, 0f));
            Assert.AreEqual(0.875f, envelop.GetCurrentLevel(0.45, 0.0, 0f));
            Assert.AreEqual(0.75f, envelop.GetCurrentLevel(0.6, 0.0, 0f));
            Assert.AreEqual(0.75f, envelop.GetCurrentLevel(2.0, 0.0, 0f));
            Assert.AreEqual(0.375f, envelop.GetCurrentLevel(0.6, 0.3, 0.75f), 1e-6);
            Assert.AreEqual(0.0f, envelop.GetCurrentLevel(0.6, 0.6, 0.75f), 1e-6);
        }

        [Test]
        public void PerceptTests()
        {
            AITimeManager.Instance.UpdateTime(100d);
            AIPerceptEnvelope envelope = new(0.1, 0.2, 0.3, 0.6, 0.75f);
            AIPercept percept = new(AIPerceptType.Visual, AITimeManager.Instance.Time, 2.0f, AIVector3.Zero, envelope: envelope);
            Assert.NotNull(percept);
            Assert.AreEqual(0f, percept.CurrentLevel);
            AITimeManager.Instance.UpdateTime(0.2);
            Assert.AreEqual(1.0f, percept.CurrentLevel);
            AITimeManager.Instance.UpdateTime(0.4);
            Assert.AreEqual(1.5f, percept.CurrentLevel);
            AITimeManager.Instance.UpdateTime(0.2);
            Assert.IsFalse(percept.IsInReleasePhase);
            percept.Release();
            Assert.IsTrue(percept.IsInReleasePhase);
            AITimeManager.Instance.UpdateTime(0.3);
            Assert.AreEqual(0.75f, percept.CurrentLevel);
        }

    }
}