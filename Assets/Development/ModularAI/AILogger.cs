using System;

namespace PLu.ModularAI
{
    public static class AILogger
    {
        private static AILoggerBase _logger;
        public static void SetLogger(AILoggerBase logger)
        {
            _logger = logger;
        }
        public static AILoggerBase GetLogger()
        {
            return _logger;
        }

#if MODULAR_AI_LOGGING // MODULAR_:AI_LOGGING is a define symbol that can be set in the project settings->player->other settings->scripting define symbols
        
        public static void Log(string message)
        {
            _logger?.Log($"[ModularAI] {message}");
        }
        public static void LogWarning(string message)
        {
            _logger?.LogWarning($"[ModularAI] {message}");
        }
        public static void LogError(string message)
        {
            _logger?.LogError($"[ModularAI] {message}");
        }
        public static void Assert(bool condition, string message)
        {
            if (!condition)
            {
                _logger?.LogError($"[ModularAI] {message}");
            }
        }

#else
        public static void Log(string message) {}
        public static void LogWarning(string message) {}
        public static void LogError(string message) {}
        public static void Assert(bool condition, string message) {}
#endif
    }

    public class AILoggerBase
    {
        public virtual void Log(string message) {}
        public virtual void LogWarning(string message) {}
        public virtual void LogError(string message) {}
        public virtual void Assert(bool condition, string message) 
        {
            if (!condition)
            {
                LogError(message);
            }
        }
    }

    public class AIConsoleLogger : AILoggerBase
    {
        public override void Log(string message)
        {
            Console.WriteLine($" Info: {message}");
        }
        public override void LogWarning(string message)
        {
            Console.WriteLine($" Warning: {message}");
        }
        public override void LogError(string message)
        {
            Console.WriteLine($" Error: {message}");
        }
        public override void Assert(bool condition, string message) 
        {
            if (!condition)
            {
                LogError(message);
            }
        }
    }
}