using System;

namespace PLu.ModularAI
{
    // TODO: Consider making this to a struct
    public class AICreationData
    {
        public AISpecificationNode AISpecificationNode { get; private set; }
        public IAIActor Actor { get; private set; }

        private Func<IAIAbstraction, IAIAbstraction> _preLoadCallback;
        public AICreationData(AISpecification aiSpecification, IAIActor aiActor)
        {
            AISpecificationNode = aiSpecification.Root;
            Actor = aiActor;
        }
        
        public AICreationData(AISpecificationNode aiSpecificationNode, IAIActor aiActor)
        {
            AISpecificationNode = aiSpecificationNode;
            Actor = aiActor;
        }

        public void SetPreLoadCallback(Func<IAIAbstraction, IAIAbstraction> callback)
        {
            _preLoadCallback = callback;
        }

        public T ConstructObject<T>() where T : IAIAbstraction, new()   
        {
            T obj = new();
            AISpecificationNode.Abstraction = obj;

            if (_preLoadCallback != null)
            {
                obj = (T)_preLoadCallback(obj);
            }

            if (!obj.Initialize(this))
            {
                // TODO: Log error and better error handling
                throw new AIInitializationFailureException($"Failed to initialize {obj.GetType().Name}");
            }

            return obj;
        }
    }

    public class AIInitializationFailureException : Exception
    {
        public AIInitializationFailureException(string message) : base(message)
        {
        }
    }
}
