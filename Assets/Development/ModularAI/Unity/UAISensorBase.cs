#if UNITY

using UnityEngine;

namespace PLu.ModularAI
{
    public abstract class UAISensorBase : MonoBehaviour
    {
        public virtual void Initialize() { }
        public virtual void UpdateSensor() { }
        public virtual void OnDrawGizmos() { }
        public virtual void OnDrawGizmosSelected() {}
    }

    public class ContactData
    {
        public readonly UAIActorBase Actor;

        public ContactData(UAIActorBase actor)
        {
            Actor = actor;
        }

        public float Distance;
        public Vector3 Direction;
    }
}
#endif