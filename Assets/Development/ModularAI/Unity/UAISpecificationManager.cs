#if UNITY
using UnityEngine;

namespace PLu.ModularAI
{
    // TODO: Replace with a proper implementation using something like Resources.Load<TextAsset>
    // and not System.IO as used in the AISpecificationManager
    public class UAISpecificationManager : AISpecificationManager
    {
        private string _directory = "Assets/Resources/AI/";
        private const string _extension = ".xml";
        private const string _defaultSpecification = "DefaultSpecifications";

        public UAISpecificationManager()
        {
            SetXMLSpecificationDirectoryPath(_directory);
            LoadXMLSpecifications(_defaultSpecification + _extension);
        }
    }
}
#endif