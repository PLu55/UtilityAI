#if UNITY

using UnityEngine;
using PLu.Utilities;
using System.Collections.Generic;

namespace PLu.ModularAI
{
    // TODO: use Layers instead of tags, create a Layer with detectable objects
    // TODO: handle object destruction!
    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(UAIPerceptController))]

    public class UAIVisualSensor : UAISensorBase
    {
        [Header("Sensor Settings")]
        [Tooltip("The interval in seconds between sensor updates")]
        [SerializeField] private float _updateInterval = 1.0f;
        [Tooltip("The radius of the sphere collider used for detection")]
        [SerializeField] private float detectionRadius = 10f;
        [Tooltip("The field of vision in degrees")]
        [SerializeField] private float _fieldOfVision = 170f;

        [Header("Gizmos")]
        [SerializeField] private bool _drawGizmos = false;
        [SerializeField] private Color _gizmoNoTargetColor = Color.green;
        [SerializeField] private Color _gizmoHasTargetColor = Color.red;

        private UAIActorBase _actor;
        private AIBlackboard _actorBrain;
        private Transform _head;
        private UAIPerceptController _perceptController;
        private AIPerceptEnvelope _visualPerceptEnvelope;
        private SphereCollider _collider;
        private RepeatTimer _timer;

        private float _oldUpdateInterval;
        private float _oldDetectionRadius;

        private void Awake()
        {
            Initialize();
        }
#if UNITY_EDITOR
        private void OnValidate()
        {

           if (_timer != null && _updateInterval != _oldUpdateInterval)
           {
                _timer.SetDuration(_updateInterval);
                _oldUpdateInterval = _updateInterval;
           }
           if (_collider != null && detectionRadius != _oldDetectionRadius)
           {    
               _collider.radius = detectionRadius;
               _oldDetectionRadius = detectionRadius;
           }
        }
#endif
        private void Start()
        {
            _timer = new RepeatTimer(_updateInterval, true);
            _timer.Start();
            _timer.OnTimerRepeat += (float deltaTime) => UpdateSensor();
            _perceptController = GetComponent<UAIPerceptController>();
            Debug.Assert(_perceptController != null, $"No PerceptController found on actor {gameObject.name}");
            
            _visualPerceptEnvelope = new(0d, 0.1,0.1, 0.4, 0.75f);
        }

        private void Update()
        {
            _timer.Tick(Time.deltaTime);
        }

        public override void Initialize()
        {
            base.Initialize();        
            _oldUpdateInterval = _updateInterval;
            _oldDetectionRadius = detectionRadius;
            _actor = GetComponent<UAIActorBase>();
            _head = transform.Find("Head");
            Debug.Assert(_head != null, $"No Head found on actor {gameObject.name}");
            _actorBrain = _actor.BrainBlackboard;
            Debug.Assert(_actorBrain != null, $"No Brain Blackboard found on actor {gameObject.name}");
            _collider = GetComponent<SphereCollider>();
            Debug.Assert(_collider != null, $"No SphereCollider component found on GameObject: {gameObject.name}");
            _collider.isTrigger = true;
            _collider.radius = detectionRadius;
        }
        private bool IsVisible(GameObject other)
        {
            RaycastHit hit;
            Vector3 direction = _head.TransformDirection(other.transform.position - _head.position);
            //direction = _head.TransformDirection(_head.position - gameObject.transform.position);

            direction.Normalize();
            float angle = Vector3.Angle(direction, _head.forward);

            if (angle <= _fieldOfVision * 0.5f && Physics.Raycast(_head.position, direction, out hit, detectionRadius))
            {
                if (hit.collider.gameObject == other)
                { 
                    Debug.DrawRay(_head.position, direction * hit.distance, Color.red);
                    return true;
                }
            }
            Debug.DrawRay(_head.position, direction * detectionRadius, Color.white);
            return false;
        }

        private void OnTriggerEnter(Collider other) 
        {   
            IAIEntity entity = other.gameObject.GetComponent<IAIEntity>();

            if (entity == null)
            {
                return;
            }

            if (!_perceptController.HasPercept(entity, AIPerceptType.Visual) &&  IsVisible(other.gameObject))
            { 
                AILogger.Log("Entity is in range and is visible, add new percept");

                AIVector3 position = other.transform.position.ToAIVector3();
                AIPercept percept = new AIPercept(AIPerceptType.Visual, AITimeManager.Instance.Time, 1f, position, entity, envelope: _visualPerceptEnvelope);
                _perceptController.AddPercept(entity, percept);
            }
        }

        private void OnTriggerStay(Collider other)
        {
            IAIEntity entity = other.gameObject.GetComponent<IAIEntity>();

            if (entity == null)
            {
                return;
            }

            if (IsVisible(other.gameObject))
            {
                if (_perceptController.TryGetPercept(entity, AIPerceptType.Visual, out AIPercept percept))
                {
                    if (percept.IsInReleasePhase)
                    {
                        AILogger.Log("Entity has become visible, reactivate percept");
                        AIVector3 position = other.transform.position.ToAIVector3();
                        percept.Reactivate(position);
                    }
                }
                else
                {
                    AILogger.Log("Entity has become visible, add percept");
                    AIVector3 position = other.transform.position.ToAIVector3();
                    percept = new AIPercept(AIPerceptType.Visual, AITimeManager.Instance.Time, 1f, position, entity, envelope: _visualPerceptEnvelope);
                    _perceptController.AddPercept(entity, percept);
                }
            }
            else
            {
                if (_perceptController.TryGetPercept(entity, AIPerceptType.Visual, out AIPercept percept) &&
                    !percept.IsInReleasePhase)
                {
                    AILogger.Log($"Entity has become invisible, release percept {percept.IsInReleasePhase}");
                    percept.Release();
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {            
            IAIEntity entity = other.gameObject.GetComponent<IAIEntity>();

            if (entity == null)
            {
                return;
            }

            if (_perceptController.TryGetPercept(entity, AIPerceptType.Visual, out AIPercept percept)
                && !percept.IsInReleasePhase)
            {
                AILogger.Log($"Entity is out of range, release percept");
                percept.Release();
            }
        }
    

        // private void OnActorDeath(UAIActorBase agent)
        // {
        //     _detectedActors.Remove(agent);
        // }   
        // private void UpdateObject(GameObject obj, bool entering)
        // {
        //     Debug.Log($"UpdateActor {obj.name} {entering}");
        //     if (obj == null) return;

        //     UAIActorBase actor = obj.GetComponent<UAIActorBase>();

        //     if (entering && !_detectedObjects.Contains(actor))
        //     {
        //         _detectedObjects.Add(actor);
        //         actor.OnDeath += OnActorDeath;
        //     }
        //     else if (!entering && _detectedObjects.Contains(actor))
        //     {
        //         _detectedObjects.Remove(actor);
        //         actor.OnDeath -= OnActorDeath;
        //     }
        // }

        public override void UpdateSensor()
        { 
        }

        public override void OnDrawGizmos() 
        {
            if (!_drawGizmos) return;
            if (_perceptController == null) return;
            Gizmos.color = _perceptController.GetPerceptCount(AIPerceptType.Visual) > 0 ? _gizmoHasTargetColor : _gizmoNoTargetColor;
            Gizmos.DrawWireSphere(transform.position, detectionRadius);
        } 
    }
}
#endif