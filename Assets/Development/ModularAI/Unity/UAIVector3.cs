#if UNITY

using System;
using UnityEngine;

namespace PLu.ModularAI
{
    public static class Vector3Extension
    {
        public static AIVector3 ToAIVector3(this Vector3 vector3)
        {
            return new AIVector3(vector3.x, vector3.y, vector3.z);
        }

        public static Vector3 ToVector3(this AIVector3 vector3)
        {
            return new Vector3(vector3.X, vector3.Y, vector3.Z);
        }
    }

    public struct WrappedUnityVector3 : IAIVector3
    {
        public Vector3 Vector3 { get; private set; }

        public float MagnitudeSqr => X * X + Y * Y + Z * Z;

        public float Magnitude => MathF.Sqrt(MagnitudeSqr);

        public float X => Vector3.x;

        public float Y => Vector3.y;

        public float Z => Vector3.z;

        public WrappedUnityVector3(Vector3 vector3)
        {
            Vector3 = vector3;
        }

        public WrappedUnityVector3( float x, float y, float z)
        {
            Vector3 = new Vector3(x, y, z);
        }

        public static implicit operator Vector3(WrappedUnityVector3 wrapped)
        {
            return wrapped.Vector3;
        }

        public float DistanceSqr(IAIVector3 other)
        {
            return Subtract(other).MagnitudeSqr;
        }

        public float Distance(IAIVector3 other)
        {
            return Subtract(other).Magnitude;
        }

        public IAIVector3 Add(IAIVector3 other)
        {
            return new WrappedUnityVector3(new Vector3(Vector3.x + other.X, Vector3.y + other.Y, Vector3.z + other.Z)); 
        }

        public IAIVector3 Subtract(IAIVector3 other)
        {
            return new WrappedUnityVector3(new Vector3(Vector3.x - other.X, Vector3.y - other.Y, Vector3.z - other.Z)); 
        }

        public IAIVector3 Multiply(float scalar)
        {
            return new WrappedUnityVector3(Vector3 * scalar);
        }        
        
        public static WrappedUnityVector3 operator +(WrappedUnityVector3 a, WrappedUnityVector3 b)
        {
            return (WrappedUnityVector3)a.Add(b);
        }

        public static WrappedUnityVector3 operator -(WrappedUnityVector3 a, WrappedUnityVector3 b)
        {
            return (WrappedUnityVector3)a.Subtract(b);
        }

        public static WrappedUnityVector3 operator *(WrappedUnityVector3 a, float b)
        {
            return (WrappedUnityVector3)a.Multiply(b);
        }

        public static WrappedUnityVector3 operator /(WrappedUnityVector3 a, float b)
        {
            return (WrappedUnityVector3)a.Multiply(1f / b);
        }
    }
}

#endif