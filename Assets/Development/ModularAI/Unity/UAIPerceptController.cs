using UnityEngine;
using PLu.Utilities;
using System.Collections.Generic;

namespace PLu.ModularAI
{

    // TODO: extend to include other types of percepts
    public class UAIPerceptController : MonoBehaviour, IAIPerceptController
    {        
        [Tooltip("The interval in seconds between percepts updates")]
        [SerializeField] private float _updateInterval = 1.0f;
        
        public  IAIActor Actor { get; private set; }
        private AIPerceptController _perceptController;
        private RepeatTimer _timer;
        
        private void Awake()
        {

           Actor = GetComponent<IAIActor>();
           Debug.Assert(Actor != null, "UAIPerceptController requires an IAIActor component but it's not found");
           _perceptController = new(Actor);
        }
        private void Start()
        {
            _timer = new RepeatTimer(_updateInterval, true);
            _timer.Start();
            _timer.OnTimerRepeat += (float deltaTime) => Tick();
        }

        private void Update()
        {
            _timer?.Tick(Time.deltaTime);
        }

        public void Tick() => _perceptController?.Tick();
        public int GetPerceptCount(AIPerceptType type) => _perceptController.GetPerceptCount(type);
        public bool HasPercept(IAIEntity entity, AIPerceptType type) => _perceptController.HasPercept(entity, type);
        public bool TryGetPercept(IAIEntity entity, AIPerceptType type, out AIPercept percept) => _perceptController.TryGetPercept(entity, type, out percept);
        public void AddPercept(IAIEntity entity, AIPercept percept) => _perceptController.AddPercept(entity, percept);
        public void RemovePercept(IAIEntity entity, AIPerceptType type) => _perceptController.RemovePercept(entity, type);
    }
}
