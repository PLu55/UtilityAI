#if UNITY

namespace PLu.ModularAI
{
    public class UAILogger : AILoggerBase
    {
        public UAILogger()
        {
            AILogger.SetLogger(this);
        }

#if MODULAR_AI_LOGGING // MODULAR_:AI_LOGGING is a define symbol that can be set in the project settings->player->other settings->scripting define symbols

        public override void Log(string message)
        {
            UnityEngine.Debug.Log(message);
        }
        public override void LogWarning(string message)
        {
            UnityEngine.Debug.LogWarning(message);
        }
        public override void LogError(string message)
        {
            UnityEngine.Debug.LogError(message);
        }
        public override void Assert(bool condition, string message)
        {
            if (!condition)
            {
                LogError(message);
            }
        }
#else
        public override void Log(string message) {}
        public override void LogWarning(string message) {}
        public override void LogError(string message) {}
        public override void Assert(bool condition, string message) {}
#endif
    }
}
#endif