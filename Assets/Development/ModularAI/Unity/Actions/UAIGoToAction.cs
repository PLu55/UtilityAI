#if UNITY

using UnityEngine;
using UnityEngine.AI;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public class UAIGoToAction : UAIActionBase
    {
        private UAIActorBase _uActor;
        private UAIMoverComponent _mover;
        private AITargetBase _target;

        public override bool Initialize(AICreationData creationData)
        {
            if (creationData.Actor is UAIActorBase uActor)
            {
                _uActor = uActor;
            }
            else
            {
                throw new AITypeException("UAIGoToAction requires a UAIActorBase actor");
            }
            
            if (creationData.AISpecificationNode.TryGetChild("Target", out var targetNode))
            {
                AICreationData data = new AICreationData(targetNode, creationData.Actor); 
                _target = AITargetFactory.Instance.Create(data);
            }
            else
            {
                throw new AIUnexpectedChildrenException("UAIGoToAction requires one child, the target");
            }

            if (!_uActor.TryGetValue("UAIMoverComponent", out _mover))
            {
                throw new AIMissingComponentException("UAIGoToAction requires the actor to have a UAIMoverComponent");
            }

            return true;
        }

        public override void Select() 
        {
            base.Select();
            _mover.SetDestination(_target.Position);  
        }   

        public override void Deselect() 
        {
            base.Deselect();
            _mover.Stop();
        }            

        public override bool Update()
        {   
            if (IsDone || _mover.HasReachedDestination)
            {
                IsDone = true;
            }
            else
            {
                _mover.SetDestination(_target.Position);
            }
            return IsDone;
       }
    }
}

#endif