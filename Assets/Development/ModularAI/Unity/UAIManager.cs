#if UNITY
using JetBrains.Annotations;
using PLu.Utilities;
using UnityEngine;
using Zenject;

namespace PLu.ModularAI
{
    //[DefaultExecutionOrder(100)]
    //public class UAIManager : MonoBehaviour, IAIManager
    //public class UAIManager : ITickable, IAIManager
    public class UAIManager : IAIManager
    {
        //public static UAIManager Instance => _instance;
        //[SerializeField] private float _updateInterval = 0.2f;

        //private static UAIManager _instance;
        private IAIManager _aiManager;
        private RepeatTimer _updateTimer;

        // private void Awake()
        // {
            // if (_instance == null)
            // {
            //     _instance = this;
            //     DontDestroyOnLoad(gameObject);
            // }
            // else
            // {
            //     Destroy(gameObject);
            // }
        public UAIManager()
        // private void Awake()
        {
            _aiManager = AIManager.Instance;
            // _updateTimer = new RepeatTimer(_updateInterval);
            // _updateTimer.OnTimerRepeat += Tick;
            // _updateTimer.Start();
        }        
        
        // void Update()
        // {
        //     _updateTimer.Tick(Time.deltaTime);
        // }
        // public void Tick()
        // {
        //     Debug.Log("UAIManager Tick");
        //     _updateTimer.Tick(Time.deltaTime);
        // }
        // public void Tick(float deltaTime)
        // {
        //     Debug.Log("UAIManager Tick");
        //     _aiManager.Tick(deltaTime);
        // }
        public void RegisterActor(IAIActor actor)
        {
            _aiManager.RegisterActor(actor);
        }

        public void Tick() {}

        public bool TryGetActor(HashedString name, out IAIActor actor)
        {
            return _aiManager.TryGetActor(name, out actor);
        }

        public void UnregisterActor(IAIActor actor)
        {
            _aiManager.UnregisterActor(actor);            
        }
    }
}
#endif