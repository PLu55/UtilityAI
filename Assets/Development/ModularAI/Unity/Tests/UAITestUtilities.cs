using System.Xml.Linq;
using PLu.ModularAI;
using UnityEngine;

namespace PLu.ModularAI.Tests
{
    public class UAITestUtilities
    {
        // public static TAbstraction CreateAbstraction<TAbstraction, UFactory>(string XMLSpec, UFactory factory)
        // {
        //     var xml = XDocument.Parse(XMLSpec);
        //     AISpecificationNode node = AISpecificationManager.Parse(xml.Root);
        //     IAIActor actor = new AIActor("TestActor");
        //     AICreationData data = new AICreationData(node, actor);
        //     return factory.Create(data);
        // }        
        
        public static AICreationData CreateCreationData(string specification)
        {
            var xml = XDocument.Parse(specification);
            AISpecificationNode node = AISpecificationManager.Parse(xml.Root);
            GameObject  gameObject = new GameObject();
            IAIActor actor = gameObject.AddComponent<UAIActorBase>();
            AICreationData data = new AICreationData(node, actor);
            return data;
        }
    }
}
