using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PLu.ModularAI;
using System.Diagnostics;

namespace PLu.ModularAI.Tests
{
    [TestFixture]
    public class WrappedUnityVector3Tests
    {
        [Test]
        public void WrappedUnityVector3BasicTests()
        {
            WrappedUnityVector3 vector1 = new WrappedUnityVector3(1f, 2f, 3f);
            Assert.AreEqual(1f, vector1.X);
            Assert.AreEqual(2f, vector1.Y);
            Assert.AreEqual(3f, vector1.Z);

            Assert.AreEqual(14f, vector1.MagnitudeSqr);
            Assert.AreEqual(3.7416574f, vector1.Magnitude);
            Assert.AreEqual(new WrappedUnityVector3(new Vector3(2f, 4f, 6f)), vector1 + vector1);
            Assert.AreEqual(new WrappedUnityVector3(new Vector3(3f, 6f, 9f)), vector1 * 3f);
            Assert.AreEqual(new WrappedUnityVector3(new Vector3(0.5f, 1f, 1.5f)), vector1 / 2f);
            Assert.AreEqual(33f, vector1.DistanceSqr(new WrappedUnityVector3(3f, 7f, 5f)));
            Assert.AreEqual(5.7445627f, vector1.Distance(new WrappedUnityVector3(3f, 7f, 5f)));
        }
        int iterations = 1000000;

        [Test]
        public void TimingExperimentWrapped()
        {            
            Stopwatch stopwatch = new Stopwatch();
            WrappedUnityVector3 vector1 = new WrappedUnityVector3(1f, 2f, 3f);
            WrappedUnityVector3 vector2 = new WrappedUnityVector3(3f, 7f, 5f);
            stopwatch.Start();
            float distance;

            for (int i = 0; i < iterations; i++)
            {
                distance = vector1.Distance(vector2);
            }
            stopwatch.Stop();
            UnityEngine.Debug.Log($"WrappedUnityVector3 Distance: {stopwatch.ElapsedMilliseconds}ms");
        }        
        
        [Test]
        public void TimingExperimentUnwrapped()
        {            
            Stopwatch stopwatch = new Stopwatch();
            Vector3 vector1 = new Vector3(1f, 2f, 3f);
            Vector3 vector2 = new Vector3(3f, 7f, 5f);
            stopwatch.Start();
            float distance;

            for (int i = 0; i < iterations; i++)
            {
                distance = vector1.Distance(vector2);
            }
            stopwatch.Stop();
            UnityEngine.Debug.Log($"Vector3 Distance: {stopwatch.ElapsedMilliseconds}ms");
        }
    }
}
