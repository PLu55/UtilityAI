#if UNITY
using UnityEngine;

namespace PLu.ModularAI
{
    public class UAIAbstractionManager
    {
        public UAIAbstractionManager()
        {
            LoadUAIAbstraction();
        }
        private void LoadUAIAbstraction()
        {
            Debug.Log("Loading UAI Abstraction Constructors");
            AIConsiderationFactory.Instance.AddConstructor(new UAIConsiderationConstructor());
            AIActionFactory.Instance.AddConstructor(new UAIActionConstructor());
        }
    }
}
#endif