#if UNITY

using PLu.Utilities;

namespace PLu.ModularAI
{
    public interface IUAIComponent 
    {
        HashedString Name { get; }
        IAIEntity Entity { get; }
    }
}
#endif