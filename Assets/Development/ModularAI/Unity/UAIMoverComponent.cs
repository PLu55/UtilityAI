#if UNITY

using UnityEngine;
using UnityEngine.AI;
using PLu.Utilities;

namespace PLu.ModularAI
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class UAIMoverComponent: MonoBehaviour, IUAIComponent
    {
        [field:SerializeField] public float _stoppingDistance = 1f;
        [field:SerializeField] private float _speed = 5.0f;
        public HashedString Name { get; } = "UAIMoverComponent";
        public IAIEntity Entity { get; private set; }
        private NavMeshAgent _navMeshAgent;

        private void Awake()
        {
            Debug.Log("UAIMoverComponent Awake");
            _navMeshAgent = GetComponent<NavMeshAgent>();
            Debug.Assert(_navMeshAgent != null, "NavMeshAgent is not found");
        }

        public bool HasReachedDestination => !_navMeshAgent.pathPending && _navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance;
        public void SetDestination(Vector3 destination)
        {
            // TODO: maybe check if destination has changed
            _navMeshAgent.isStopped = false;
            _navMeshAgent.stoppingDistance = _stoppingDistance;
            _navMeshAgent.speed = _speed;
            _navMeshAgent.SetDestination(destination);
        }
        public void SetDestination(IAIVector3 destination)
        {
            if (destination is AIVector3 position)
            {
                SetDestination(position.ToVector3());
            }
            else
            {
                throw new AITypeException("UAIGoToAction can't handle position expecting a (AIVector3)");
            }
        }

        public void Stop()
        {
            _navMeshAgent.isStopped = true;
        }

        public void Resume()
        {
            _navMeshAgent.isStopped = false;
        }          
    }

}
#endif