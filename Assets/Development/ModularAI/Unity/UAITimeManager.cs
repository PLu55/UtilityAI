#if UNITY
using UnityEngine;

namespace PLu.ModularAI
{
    // This is intended to use Zenject instantiation.
    public class UAITimeManager : AITimeManagerBase
    {
        public override double Time => UnityEngine.Time.timeAsDouble;
        public override double DeltaTime => UnityEngine.Time.deltaTime;

        public new static IAITimeManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UAITimeManager();
                }
                return _instance;
            }
        }
        public UAITimeManager()
        {
            _instance = this;
        }
        public override void UpdateTime(double deltaTime) {}
    }
}
#endif