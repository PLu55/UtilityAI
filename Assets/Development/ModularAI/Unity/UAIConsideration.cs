#if UNITY
namespace PLu.ModularAI
{
    public abstract class UAIConsiderationBase : AIConsiderationBase
    {
        public override void Consider()
        {
            throw new System.NotImplementedException();
        }
    }

    public class UAIConsiderationConstructor : AIConstructorBase<AIConsiderationBase>
    {
        public override AIConsiderationBase Create(AICreationData creationData)
        {
            switch (creationData.AISpecificationNode.Type)
            {
                case "IsEnemy":
                    return creationData.ConstructObject<UAIIsEnemyConsideration>();
                default:
                    return null;
            }
        }

    }
}
#endif