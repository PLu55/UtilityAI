#if UNITY

using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using PLu.Utilities;

namespace PLu.ModularAI
{
    public class UAIActorBase : MonoBehaviour, IAIActor
    {
        [SerializeField] private string _aiDefinition;
        public HashedString Name { get; private set; }
        [Inject]
        public AIBlackboard BrainBlackboard { get; private set; }
        [Inject]
        public AIBlackboard ActorBlackboard { get; private set; }
        [Inject]
        public AIBlackboard TargetsBlackboard { get; private set; }
        [Inject]
        public AIBlackboard ContactsBlackboard { get; private set; }
        [Inject]
        public AIBlackboard ThreatsBlackboard { get; private set; }   
        [Inject]     
        public AIBlackboard PerceptsBlackboard { get; private set; }
        public AIReasonerBase Reasoner { get; set; }
        [Inject]
        private UAIManager _uAIManager;

        public AISensorBase[] Sensors => throw new System.NotImplementedException();
        public IAIEntity Entity => this;
        public AIVector3 Position => transform.position.ToAIVector3();
        public ICollection<HashedString> Keys => ActorBlackboard.Keys;
        public ICollection<object> Values => ActorBlackboard.Values;
        public bool ContainsKey(HashedString key) => ActorBlackboard.ContainsKey(key);
        public void SetValue<T>(HashedString key, T value) => ActorBlackboard.SetValue<T>(key, value);
        public T GetValue<T>(HashedString key, T defaultValue = default) => ActorBlackboard.GetValue<T>(key, defaultValue);
        public bool TryGetValue<T>(HashedString key, out T value) => ActorBlackboard.TryGetValue<T>(key, out value);
        public void RemoveValue<T>(HashedString key) => ActorBlackboard.RemoveValue<T>(key);
        public int Count => ActorBlackboard.Count;
        public AIBlackboard GetBlackboard(string name) => ActorBlackboard.GetValue<AIBlackboard>(name);
        public AIBlackboard GetBlackboard(HashedString name) => ActorBlackboard.GetValue<AIBlackboard>(name);
        IAIVector3 IAIEntity.Position => new WrappedUnityVector3(transform.position);

        public Type EntityType => GetType();

        public string BlackboardName => Name.ToString();
        public event System.Action<UAIActorBase> OnDeath = delegate { };
  
        void Awake()
        {
            Debug.Log("UAIActorBase Awake");
            Debug.Assert(!AIBlackboardManager.Instance.IsRegistered(name), $"Actor with name {name} already exists");
            Name = AIBlackboardManager.Instance.GetOrRegister(name);
            Debug.Assert(_uAIManager != null, "UAIManager is not injected");
            _uAIManager.RegisterActor(this);
            SetupBlackboards();
            GetIUAIComponents();
        }

        private void Start()
        {            
            LoadAISpecification();
        }        
        
        private void Update()
        {
            Reasoner?.Tick();
        }
        public void Tick() {}

        public IAIEntity GetEntity()
        {
            return this;
        }

        private void LoadAISpecification(string aiDefinition = null)
        {
            if (!string.IsNullOrEmpty(aiDefinition))
            {
                _aiDefinition = aiDefinition;
            }
            
            if (string.IsNullOrEmpty(_aiDefinition))
            {
                AILogger.LogWarning($"UAIActorBase: AI definition is not set for {name} and no definition is passed as argument");
                return;
            }

            if (AISpecificationManager.Instance.TryGetAISpecification(_aiDefinition, out AISpecification specification))
            {
                Debug.Log($"UAIActorBase: Building reasoner for AI definition {_aiDefinition} for Actor {name}");
                var creationData = new AICreationData(specification.Root, this);
                Reasoner = AIReasonerFactory.Instance.Create(creationData);

                if (Reasoner == null)
                {
                    AILogger.LogError($"UAIActorBase: Could not build reasoner created AI definition {_aiDefinition} for Actor {name}");
                }
            }
            else
            {
                AILogger.LogError($"UAIActorBase: Specification not found {_aiDefinition}");
            }
        }
        
        private void GetIUAIComponents()
        {
            Debug.Log("GetIUAIComponents, scanning for components");
            var components = GetComponents<IUAIComponent>();

            foreach (var component in components)
            {
                Debug.Log($"Found UAIcomponent: {component.Name}");
                ActorBlackboard.SetValue(component.Name, component);
            }
        }
        private void SetupBlackboards()
        {
            ActorBlackboard = new();
            ActorBlackboard.SetValue<IAIBlackboard>("Brain", BrainBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Actor", ActorBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Targets", TargetsBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Contacts", ContactsBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Threats", ThreatsBlackboard);
            ActorBlackboard.SetValue<IAIBlackboard>("Percepts", PerceptsBlackboard);

            TargetsBlackboard.SetValue<IAIActor>("Self", this);
            ActorBlackboard.SetValue("Name", Name);
        }
    }
}
#endif