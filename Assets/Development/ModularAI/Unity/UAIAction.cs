#if UNITY

using UnityEngine;

namespace PLu.ModularAI
{
    public abstract class UAIActionBase : AIActionBase
    {
        public UAIActionBase UActor { get; protected set; }
    }

    public class UAIActionConstructor : AIConstructorBase<AIActionBase>
    {
        public override AIActionBase Create(AICreationData creationData)
        {
            Debug.Log("UAIActionConstructor: " + creationData.AISpecificationNode.Type);
            switch (creationData.AISpecificationNode.Type)
            {
                case "GoTo":
                    return creationData.ConstructObject<UAIGoToAction>();
                default:
                    return null;
            }
        }
    }
}



#endif