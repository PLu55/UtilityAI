namespace PLu.ModularAI
{
    public interface IAIAbstraction
    {
        string NodeName { get; }
        bool Initialize(AICreationData creationData);

    }
}
