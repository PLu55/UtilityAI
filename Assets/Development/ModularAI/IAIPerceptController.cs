namespace PLu.ModularAI
{
    public interface IAIPerceptController
    {
        IAIActor Actor { get; }
        void Tick() {}
        int GetPerceptCount(AIPerceptType type);
        bool HasPercept(IAIEntity entity, AIPerceptType type);
        bool TryGetPercept(IAIEntity entity, AIPerceptType type, out AIPercept percept);
        void AddPercept(IAIEntity entity, AIPercept percept);
        void RemovePercept(IAIEntity entity, AIPerceptType type);
    }

}
