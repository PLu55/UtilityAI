//using UnityEngine;

namespace PLu.ModularAI
{


    public abstract class AIRegionBase : IAIAbstraction
    {
        public string NodeName => "Region";

        public abstract bool Initialize(AICreationData creationData);

        public abstract bool IsInRegion(AIVector3 position);
        public abstract AIVector3 GetRandomPosition();
    }

    public class AIRegionFactory : AIFactoryBase<AIRegionBase, AIRegionFactory>
    {
        public AIRegionFactory() : base()
        {
            AddConstructor(new AIRegionConstructor_Default());
        }
    }

    public class AIRegionConstructor_Default : AIConstructorBase<AIRegionBase>
    {
        public override AIRegionBase Create(AICreationData creationData)
        {
            switch (creationData.AISpecificationNode.Type)
            {
                case "Position":
                    return creationData.ConstructObject<AIRegionPosition>();
            }
            return null;
        }
    }

    public class AIRegionPosition : AIRegionBase
    {
        public AIVector3 Position { get; private set; }
        public float Tolerance = 0.1f;

        public override AIVector3 GetRandomPosition()
        {
            throw new System.NotImplementedException();
        }

        public override bool Initialize(AICreationData creationData)
        {
            if (creationData.AISpecificationNode.TryGetAttribute("Position", out string position))
            {
                creationData.AISpecificationNode.TryGetAttribute("Tolerance", out string tolerance);
                Tolerance = float.Parse(tolerance);

                Position = AIVector3.Parse(position);
                return true;
            }
            return false;
        }

        public override bool IsInRegion(AIVector3 position)
        {
            return (Position - position).MagnitudeSqr < Tolerance * Tolerance;
        }
    }
}
