using System;
using UnityEngine;

namespace PLu.InventorySystem
{
    [Serializable]
    public class ItemContainer //: Item
    {
        [field:SerializeField] public ItemType ItemType { get; private set; }
        [field:SerializeField] public int Quantity { get; private set; }
        public bool IsEmpty => ItemType == null || Quantity == 0;
        public string Name => ItemType != null ? ItemType.Name : "";

        public ItemContainer()
        {
            ItemType = null;
            Quantity = 0;
        }        
        public ItemContainer(ItemContainer self, int quantity)
        {
            ItemType = self.ItemType;
            Quantity = quantity;
        }

        public ItemContainer(ItemType itemType, int quantity = 1)
        {
            ItemType = itemType;
            Quantity = quantity;
        }

        public void Clear()
        {
            ItemType = null;
            Quantity = 0;
        }

        public void Set(ItemType itemType, int quantity)
        {
            ItemType = itemType;
            Quantity = quantity;
        }
        public void AddQuantity(int quantity)
        {
            Quantity += quantity;
        }        
        public ItemContainer RemoveQuantity(int quantity, out bool isExhausted)
        {
            isExhausted = false;

            if (quantity >= Quantity)
            {
                quantity = Quantity;
                Quantity = 0;
                isExhausted = true;
                return quantity == 0 ? null : new ItemContainer(this, quantity);
            }

            Quantity -= quantity;
            return new ItemContainer(this, quantity);
        }
    }
}
