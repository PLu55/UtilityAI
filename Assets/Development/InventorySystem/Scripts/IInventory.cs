using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.InventorySystem
{
    public interface IInventory
    {
	    delegate void OnItemChanged();
        ItemContainer[] Items { get; }
        bool TryAdd(ItemContainer itemContainer);
        bool TryAdd(Item item, float quantity = 1f);
        bool TryRemove(string name, out Item item, float quantity = -1f);
        bool HasItem(string name);
        float QuantityOf(string name);       
    }
}
