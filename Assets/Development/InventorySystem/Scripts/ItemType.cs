using Codice.Client.Common;
using Codice.CM.Interfaces;
using UnityEngine;

namespace PLu.InventorySystem
{
    [CreateAssetMenu(fileName = "New ItemType", menuName = "Resources/ItemType")]
    public class ItemType : ScriptableObject
    {
        public string Name  { get { return name; }}      
        [field:TextArea(3, 5)]
        [field:SerializeField] public string Description { get; private set; }
        [field:SerializeField] public float Weight { get; private set; }
        [field:SerializeField] public Sprite Icon  { get; private set; }
        [field:SerializeField] public Item Prototype { get; protected set; }
        public int Hash { get; private set; }

#if UNITY_EDITOR
        private void OnValidate()
        {
            Hash = Name.ComputeFNV1aHash();            
        }
#endif
        void OnEnable()
        {
            Hash = Name.ComputeFNV1aHash();
        }

        public override string ToString()
        {
            return $"ItemType(Name: {Name}, Description: {Description}, Weight: {Weight})";
        }

        public virtual Item Instantiate()
        {
            Debug.Assert(Prototype != null, $"Trying to instantiate and Item but prototype is null in {Name}");
            GameObject go = Instantiate(Prototype.gameObject);
            if (go.TryGetComponent(out Item item))
            {
                return item;
            }
            return null;
        } 
    }
}
