namespace PLu.InventorySystem
{
    public interface IItem
    {
        ItemType ItemType { get; }
        string Name { get; }
        int Hash { get; }
        //int Quantity { get; }
        //void AddQuantity(int quantity);
        //int RemoveQuantity(int quantity);
    }    
}
