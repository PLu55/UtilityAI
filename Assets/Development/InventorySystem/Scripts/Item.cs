using System;
using UnityEngine;

namespace PLu.InventorySystem
{
    [Serializable]
    public class Item : MonoBehaviour, IItem
    {
        [field:SerializeField] public ItemType ItemType { get; protected set; }
        //[field:SerializeField] public int Quantity { get; protected set; }
        public string Name => ItemType != null ? ItemType.Name : "";
        public int Hash => ItemType != null ? ItemType.Hash : 0;
        
    }
}
