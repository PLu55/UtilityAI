using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using PLu.Utilities;

namespace PLu.InventorySystem
{
    public class Inventory : MonoBehaviour
    {
        [field:SerializeField] public ItemContainer[] ItemSlots { get; private set; }
        [field:SerializeField] public int Size { get; set; } = 1;

        private Dictionary<int, int> _itemTypes = new(); 

        public event System.Action OnInventoryChanged = delegate { };

        private bool _initialized = false;

        public void Awake()
        {
            Initialize();
        }
        public bool TryAdd(ItemContainer itemContainer)
        {
            return TryAdd(itemContainer.ItemType, itemContainer.Quantity);
        }
        public bool TryAdd(Item item, int quantity = 1)
        {
            return TryAdd(item.ItemType, quantity);
        }
        public bool TryAdd(ItemType itemType, int quantity = 1)
        {
            if (!_initialized)
            {
                Initialize();
            }
            
            if(_itemTypes.TryGetValue(itemType.Hash, out int index))
            {
                ItemSlots[index].AddQuantity(quantity);
                OnInventoryChanged.Invoke();
                return true;
            }

            for (int i = 0; i < ItemSlots.Length; i++)
            {   
                if (ItemSlots[i].IsEmpty)
                {
                    ItemSlots[i].Set(itemType, quantity);
                    _itemTypes.Add(itemType.Hash, i);
                    OnInventoryChanged.Invoke();
                    return true;
                }
            }
            return false;
        }
        public ItemContainer TryRemove(string name, int quantity = 1)
        {
            ItemContainer itemContainer = null;

            if (ItemSlots == null)
            {

                return null;
            }
            for (int i = 0; i < ItemSlots.Length; i++)
            {
                ItemContainer slot = ItemSlots[i];

                if (slot != null && !slot.IsEmpty && slot.ItemType.Name == name)
                {
                    itemContainer = ItemSlots[i].RemoveQuantity(quantity , out bool isExhausted);

                    if (isExhausted)
                    {
                        _itemTypes.Remove(slot.ItemType.Hash);
                    }

                    OnInventoryChanged.Invoke(); 
                    return itemContainer; 
                }
            }

            return null;
        }
        public ItemContainer[] RemoveAllItems()
        {
            List<ItemContainer> items = new();
            for (int i = 0; i < ItemSlots.Length; i++)
            {
                ItemContainer slot = ItemSlots[i];
                if (slot != null && !slot.IsEmpty)
                {
                    items.Add(new(slot.ItemType, slot.Quantity));
                    slot.Clear();
                    _itemTypes.Clear();
                }
            }
            return items.ToArray();
        }
        public bool HasItem(int hash)
        {
            return _itemTypes.ContainsKey(hash) && ItemSlots[_itemTypes[hash]].Quantity > 0;
        }
        public bool HasItem(string name)
        {     
            return HasItem(name.ComputeFNV1aHash());       
        }
        public int QuantityOf(int hash)
        {
            return _itemTypes.ContainsKey(hash) ? ItemSlots[_itemTypes[hash]].Quantity : 0;
        }
        public int QuantityOf(string name)
        { 
            return QuantityOf(name.ComputeFNV1aHash());
        }
        private void Initialize()
        {
            if (!_initialized)
            {
                _itemTypes = new(); 
                ItemSlots = new ItemContainer[Size];
                for (int i = 0; i < ItemSlots.Length; i++)
                {
                    ItemSlots[i] = new();
                }   

                _initialized = true;
            }
        }
        
    }
}
