using UnityEngine;
using PLu.UtilityAI.Core;
using System.Collections.Generic;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Actions/Flee", fileName = "Flee")]
    public class Flee : Action
    {
        [SerializeField] private float _safeDistance = 10.0f;
        [SerializeField] private float _fleeDistance = 3.0f;
        private HashSet<IDamageDealer> _damageDealers;
        private Character _enemy;
        private Vector3 _fleeDirection;

        public override void Initialize(IContext context)
        {
            base.Initialize(context);
            if (context is UtilityAIContext cx)
            {
                _damageDealers = cx.PrivateBlackboard.GetValue<HashSet<IDamageDealer>>(cx.DamageDealersKey);
            }
        }
        public override void Begin(IContext worldContext, ActionScoring actionScoring)
        {
            base.Begin(worldContext, actionScoring);
            
            if (worldContext is UtilityAIContext cx)
            { 
                _enemy = SelectEnemy(cx);
                _fleeDirection = _enemy.transform.position - cx.Owner.transform.position;
            }
        }
        public override Action.State Process(float deltaTime, IContext worldContext, ActionScoring actionScoring)
        {
            if (worldContext is UtilityAIContext cx)
            { 
                float distanceSqr = cx.Owner.transform.position.DistanceSqr(_enemy.transform.position);
                if (distanceSqr > _fleeDistance * _fleeDistance)
                {
                    return Action.State.Success;
                }
                _fleeDirection = _enemy.transform.position - cx.Owner.transform.position;
                _fleeDirection.Normalize();
                _fleeDirection *= _safeDistance;
                _fleeDirection = _fleeDirection.With(y: 0f);
                cx.CharacterMover.MoveTo(cx.Owner.transform.position - _fleeDirection );
                return Action.State.Continue;
            }
            
            return Action.State.Failed;
        }
        private Character SelectEnemy(UtilityAIContext cx)
        {
            // Select the enemy to flee from, this selects the closest enemy

            float minDistanceSqr = float.MaxValue;
            Character enemy = null;

            foreach (var damageDealer in _damageDealers)
            {
                if (damageDealer is Character character)
                {
                    float distanceSqr = cx.Owner.transform.position.DistanceSqr(character.transform.position);
                    if (distanceSqr < minDistanceSqr)
                    {
                        minDistanceSqr = distanceSqr;
                        enemy = character;
                    }
                }
            }
 
            return enemy;
        }
    }
}
