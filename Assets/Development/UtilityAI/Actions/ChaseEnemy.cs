using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.BlackboardSystem;
using PLu.UtilityAI.Core;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Actions/ChaseEnemy", fileName = "ChaseEnemyAction")]

    public class ChaseEnemyAction : Action
    {
        const float _minDistance = 1.0f;
        private GameObject _enemy;

        public override void Begin(IContext context, ActionScoring actionScoring)
        {
            if (context is UtilityAIContext cx)
            { 
                if (!cx.PrivateBlackboard.TryGetValue(cx.EnemyKey, out _enemy))
                {
                    Debug.LogWarning("ChaseEnemyAction: No enemy in private blackboard");
                    return;
                }
            }
        }
        public override void End(IContext context, ActionScoring actionScoring)
        {
            if (context is UtilityAIContext cx)
            {
                cx.CharacterMover.Stop();
            }
        }

        public override Action.State Process(float deltaTime, IContext context, ActionScoring actionScoring)
        {
            if (context is UtilityAIContext cx && _enemy != null)
            {
                float distanceSqr = cx.Owner.transform.position.DistanceSqr(_enemy.transform.position);
                if (distanceSqr <= _minDistance * _minDistance)
                {
                    return Action.State.Success;
                }
                cx.CharacterMover.MoveTo(_enemy.transform.position);
                return Action.State.Continue;
            }

            return Action.State.Failed;
        }
    }
}
