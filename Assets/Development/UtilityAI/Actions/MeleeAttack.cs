using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;
using PLu.AbilitySystem;
using TMPro;
namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Actions/MeleeAttack", fileName = "MeleeAttack")]

    public class MeleeAttack : Action
    {
        private Ability[] _abilities;
        private Character _character;

        public override void Initialize(IContext context)
        {
            base.Initialize(context);
            _character = context.Owner.GetComponent<Character>();
            _abilities = _character.Abilities;
        }

        public override Action.State Process(float deltaTime, IContext context, ActionScoring actionScoring)
        {
            if (context is UtilityAIContext cx)
            {
                GameObject enemy;

                if (cx.PrivateBlackboard.TryGetValue(cx.EnemyKey, out enemy))
                {
                    Character enemyCharacter = enemy.GetComponent<Character>();

                    foreach (var ability in _abilities)
                    {
                        if (ability.TypeOfAbility == AbilityType.Melee && ability.CanActivate(_character, enemyCharacter))
                        {
                            ability.Activate(_character, enemyCharacter);
                            return Action.State.Success;
                        }
                    }
                }
            }
    
            return Action.State.Failed;
        }
    }
}
