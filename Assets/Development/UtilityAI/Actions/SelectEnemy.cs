using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Actions/SelectEnemy", fileName = "SelectEnemy")]

    public class SelectEnemy : Action
    {
        public override Action.State Process(float deltaTime, IContext context, ActionScoring actionScoring)
        {
            if (context is UtilityAIContext cx)
            {

                if (!context.PrivateBlackboard.TryGetValue(cx.EnemyKey, out GameObject enemy))
                {
                    //Debug.Log($"SelectEnemy found enemy in private blackboard: {enemy.name}");
                    if (enemy == null)
                    {
                        var enemies = context.PrivateBlackboard.GetValue<Dictionary<GameObject, TargetData>>(cx.TargetsInProximityKey);
                        UtilityAILogger.Log($"-------- SelectEnemy targets in proximity: {enemies.Count}");
                        if (enemies.Count > 0)
                        {
                            var enemyData = new List<TargetData>(enemies.Values);
                            enemy = enemyData[Random.Range(0, enemies.Count)].Target;
                            context.PrivateBlackboard.SetValue(cx.EnemyKey, enemy);
                            UtilityAILogger.Log($"-------- SelectEnemy selected enemy: {enemy.name}");
                            enemy.GetComponent<Character>().OnDeath += () => context.PrivateBlackboard.RemoveValue<GameObject>(cx.EnemyKey);
                            return Action.State.Success;
                        }
                    }
                }
            }
            
            return Action.State.Failed;
        }

        public void DrawGizmoLine(GameObject owner, GameObject enemy)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(owner.transform.position, enemy.transform.position);
        }

    }
}
