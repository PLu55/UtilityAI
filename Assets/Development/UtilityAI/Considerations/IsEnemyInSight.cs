using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(fileName = "IsEnemyInSight", menuName = "UtilityAI/Consideration/IsEnemyInSight")]
    public class IsEnemyInSight : BinaryConsideration
    {
        private LayerMask _characterLayerMask;
        public override void Initialize(IContext context)
        {
            _characterLayerMask = LayerMask.GetMask("Character");
        }
        public override float Consider(IContext context)
        {
            if (context is UtilityAIContext cx)
            {
                context.PrivateBlackboard.TryGetValue(cx.EnemyKey, out GameObject target);
                UtilityAILogger.Log($"-------- IsEnemyInSight: Target: {target.name}");
                if (target != null)
                {
                    Vector3 myPosition = context.Owner.transform.position;
                    Vector3 targetPosition = target.transform.position;
                    Vector3 direction = Vector3.Normalize(targetPosition - myPosition);

                    if (Physics.Raycast(myPosition, direction.normalized, out RaycastHit hit, Mathf.Infinity)) //, _characterLayerMask))
                    {
                        // Check if the hit object is the target
                        if (hit.collider.gameObject == target.gameObject)
                        {
                            return Evaluate(true);
                        }
                    }
                }
            }

            return Evaluate(false);
        }
    }
}
