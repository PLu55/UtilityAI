using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/AttackerHealth", fileName = "AttackerHealth")]
    public class AttackerHealth : Consideration
    {
        public override float Consider(IContext context)
        {
            if (context is UtilityAIContext cx)
            {
                if (cx.PrivateBlackboard.TryGetValue(cx.EnemyKey, out GameObject target))
                {
                    StatsManager targetStats = target.GetComponent<Character>().StatsManager;
                    float healthRatio = targetStats.GetCurrentRatio(StatType.Health);
                    return Evaluate(healthRatio);
                }
            }
        
            return Evaluate(0f);
        }
    }
}
