using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/HasEnemy")]
    public class HasEnemy : BinaryConsideration
    {
        public override float Consider(IContext context)
        {
            if (context is UtilityAIContext cx)
            {
                GameObject enemy;
                cx.PrivateBlackboard.TryGetValue(cx.EnemyKey, out enemy);
                

                return Evaluate(enemy != null);
            }
            return Evaluate(false);
        }
    }
}
