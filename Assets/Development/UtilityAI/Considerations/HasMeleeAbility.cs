using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.AbilitySystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/HasMeleeAbility", fileName = "HasMeleeAbility")]
    public class HasMeleeAbility : BinaryConsideration
    {
        private Ability[] _abilities;
   
        public override void Initialize(IContext context)
        {
            base.Initialize(context);
            UtilityAILogger.Log($"HasMeleeAbility initialized: owner: {context.Owner.name}");
            _abilities = context.Owner.GetComponent<Character>().Abilities;
        }
        public override float Consider(IContext context)
        {
            UtilityAILogger.Log($"HasMeleeAbility initialized: owner: {context.Owner.name} abilities: {_abilities.Length} ");
            foreach (var ability in _abilities)
            {
                if (ability.TypeOfAbility == AbilityType.Melee)
                {
                    return Evaluate(true);
                }
            }

            return Evaluate(false);
        }
    }
}
