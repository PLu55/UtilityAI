using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.AbilitySystem;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/TargetInRange", fileName = "TargetInRange")]
    public class TargetInRange : BinaryConsideration
    {
        [field:SerializeField] public AbilityType TypeOfAbility { get; protected set; }

        private Ability[] _abilities;
        private GameObject _target;

        public override void Initialize(IContext context)
        {
            base.Initialize(context);
            if (context is UtilityAIContext cx)
            {
                _abilities = cx.Owner.GetComponent<Character>().Abilities;
            }
        }
   
        public override float Consider(IContext context)
        {
            if (_abilities == null)
            {
                Debug.LogWarning("TargetInRange: Abilities are null");
                return 0f;
            }
            if (context is UtilityAIContext cx)
            {
                cx.PrivateBlackboard.TryGetValue(cx.EnemyKey, out _target);
                float distance = cx.Owner.transform.position.Distance(_target.transform.position);
            
                if (_target == null)
                {
                    return 0f;
                }

                foreach (var ability in _abilities)
                {
                    if (ability.TypeOfAbility == TypeOfAbility)
                    {
                        if (ability.Range >= distance)
                        {
                            return Evaluate(true);
                        }
                    }
                }
            }
            return Evaluate(false);
        }
    }
}
