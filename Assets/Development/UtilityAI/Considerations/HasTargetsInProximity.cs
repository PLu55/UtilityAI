using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/HasTargetsInProximity", fileName = "HasTargetsInProximity")]
    public class HasTargetsInProximity : BinaryConsideration
    {
        private Dictionary<GameObject, TargetData> _targets;

        public override void Initialize(IContext context)
        {
            if (context is UtilityAIContext cx)
            {

                if (!context.PrivateBlackboard.TryGetValue(cx.TargetsInProximityKey, out _targets))
                {
                    Debug.LogWarning("HasTargetsInProximity: No targets collection in private blackboard");
                }
            }
        }
        public override float Consider(IContext context)
        {

            if (context is UtilityAIContext cx)
            {
                foreach (var data in _targets.Values)
                {
                    if (data.InRange)
                    {
                        return Evaluate(true);
                    }
                }
            }
            return Evaluate(false);
        }
    }
}
