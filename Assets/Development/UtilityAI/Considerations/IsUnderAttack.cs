using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.BlackboardSystem;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/IsUnderAttack")]
    public class IsUnderAttack : BinaryConsideration
    {
        public override float Consider(IContext context)
        {
            if (context is UtilityAIContext cx)
            {
                bool isUnderAttack;
                cx.PrivateBlackboard.TryGetValue(cx.IsUnderAttackKey, out isUnderAttack);

                return Evaluate(isUnderAttack);
            }
            
            return Evaluate(false);
        }
    }
}
