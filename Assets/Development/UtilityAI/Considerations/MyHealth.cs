using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;

namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "UtilityAI/Consideration/MyHealth", fileName = "MyHealth")]
    public class MyHealth : Consideration
    {
        public override float Consider(IContext context)
        {
            if (context is UtilityAIContext cx)
            {
                var health = cx.StatsManager.GetStat(StatType.Health);
                return Evaluate(health.Value/health.MaxValue);
            }
            return 0f;
        }
    }
}
