using System;
using System.Collections.Generic;
using UnityEngine;
using PLu.BlackboardSystem;
using PLu.UtilityAI.Core;
using PLu.Utilities;
using Zenject;

namespace PLu.Dev.AI
{
    // TODO: Implement multi target detection
    [RequireComponent(typeof(SphereCollider))]
    public class ProximitySensor : Sensor
    {
        [SerializeField] private float _updateInterval = 1.0f;
        [SerializeField] private float detectionRadius = 5f;

        // TODO: change tag to Character
        //[SerializeField] private string _targetTag = "Character";
        [SerializeField] private string _targetLayer = "Agent";

        [Header("Gizmos")]
        [SerializeField] private bool _drawGizmos = false;
        [SerializeField] private Color _gizmoNoTargetColor = Color.green;
        [SerializeField] private Color _gizmoHasTargetColor = Color.red;
    
        public override event Action<Sensor, TargetData> OnTargetChanged = delegate { };
        public bool IsTargetInRange => TargetPosition != Vector3.zero;

        private SphereCollider _collider;
        private Dictionary<GameObject, TargetData> _targets;
        //private UtilityAIContext _context;

        private RepeatTimer _timer;
        [Inject]
        private BlackboardManager _blackboardManager;
        private Blackboard _blackboard;
        private int _targetLayerIndex;

        private const string _targetsInProximityString = "TargetsInProximity";
        private HashedString _targetsInProximityKey;

        void Initialize(BlackboardManager blackboardManager)
        {
            Debug.Log($"ProximitySensor Initialize {blackboardManager != null}");
            _blackboardManager = blackboardManager;
        }
        void Awake() 
        {
            _collider = GetComponent<SphereCollider>();
            Debug.Assert(_collider != null, $"No SphereCollider component found on character: {gameObject.name}");
            _collider.isTrigger = true;
            _collider.radius = detectionRadius;
            _targetLayerIndex = LayerMask.NameToLayer(_targetLayer);
            //_context = GetComponentInParent<UtilityAIContext>();
            SetupBlackboards();
         }

        void SetupBlackboards()
        {
            GameObject root = gameObject.transform.root.gameObject;
            Debug.Log($"ProximitySensor: Root object: {root.name}");
            _targets = new();
            //Debug.Assert(BlackboardManager.Instance, $"No BlackboardManager.Instance object found for character: {gameObject.name}");
            _blackboard = _blackboardManager.GetPrivateBlackboard(root);
            Debug.Assert(_blackboard != null, $"Private Blackboard couldn't found or created on character: {root}");
            _targetsInProximityKey = _blackboardManager.GetOrRegisterKey(_targetsInProximityString);
            _blackboard.SetValue<Dictionary<GameObject, TargetData>>(_targetsInProximityKey, _targets);
        }
        void Start() 
        {
            _timer = new RepeatTimer(_updateInterval, true);
            _timer.Start();
            _timer.OnTimerRepeat += (float deltaTime) => UpdateTarget(_target.OrNull(), false);
        }
        void Update() 
        {
            _timer.Tick(Time.deltaTime);
        }
        public bool Sense(IContext context) 
        {
             return IsTargetInRange;
        }

        private void UpdateTarget(GameObject target, bool inRange) 
        {
            if (target == null) return;
            TargetData data;

            if (!_targets.TryGetValue(target, out data))
            {
                data = new TargetData
                {   Target = target, 
                    InRange = inRange, 
                    TimeStamp = Time.time, 
                    TargetPosition = target.transform.position
                };
                _targets.Add(target, data);
                target.GetComponent<Character>().OnDeath += () => OnTargetDeath(target);

                OnTargetChanged.Invoke(this, data);
            }
            else
            {
                data.TimeStamp = Time.time;
                if (data.InRange != inRange || data.TargetPosition != target.transform.position)
                {
                    data.TargetPosition = target.transform.position;
                    data.InRange = inRange;
                    OnTargetChanged.Invoke(this, data);
                }
            }
        }
        private void OnTargetDeath(GameObject target) 
        {
            _targets.Remove(target);
        }
        private void OnTriggerEnter(Collider other) 
        {   
            if (other.gameObject.layer != _targetLayerIndex) { return; }
            //if (!other.CompareTag(_targetTag)) return;
            UpdateTarget(other.gameObject, true);
        }   
    
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer != _targetLayerIndex) { return; }
            //if (!other.CompareTag(_targetTag)) return;
            UpdateTarget(other.gameObject, false);
        }
        private void OnDrawGizmos() 
        {
            if (!_drawGizmos) return;
            Gizmos.color = IsTargetInRange ? _gizmoHasTargetColor : _gizmoNoTargetColor;
            Gizmos.DrawWireSphere(transform.position, detectionRadius);
        }
    }

    public class TargetData
    {
        public GameObject Target;
        public bool InRange;
        public float TimeStamp;
        public Vector3 TargetPosition;
    }
}
