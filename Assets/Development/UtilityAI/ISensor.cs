using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.UtilityAI.Core;
using System;

namespace PLu.Dev.AI
{
    public interface ISensor
    {
        string Name { get; }
        Func<Vector3> ObservedLocation { get;}
        public event Action<Sensor, TargetData> OnTargetChanged;
        bool Sense();
    }
}
