using System.Collections;
using System.Collections.Generic;
using PLu.UtilityAI;
using PLu.BlackboardSystem;
using UnityEngine;
using Zenject;
using PLu.UtilityAI.Core;
using System;
using PLu.Utilities;


namespace PLu.Dev.AI
{
    public class UtilityAIContext : MonoBehaviour, IContext
    {
        [field:SerializeField] public  float TargetRemoveDelay { get; protected set; }= 10.0f;
        public UtilityAgent Owner { get; protected set; }
        [Inject]
        public BlackboardManager BlackboardManager { get; protected set; }
        public Blackboard PrivateBlackboard { get; protected set; }
        public ISensor[] Sensors { get; protected set; }
        public CharacterMover CharacterMover { get; protected set; }
        public Dictionary<GameObject, TargetData> Targets { get; protected set; }

        public StatsManager StatsManager { get; private set; }

        private const string _enemyKey = "Enemy";
        private const string _targetsInProximityKey = "TargetsInProximity";
        private const string _isUnderAttackKey = "IsUnderAttack";
        private const string _damageDealersKey = "DamageDealers";
        public HashedString EnemyKey { get; protected set; }
        public HashedString IsUnderAttackKey { get; protected set; }
        public HashedString TargetsInProximityKey { get; protected set; }
        public HashedString DamageDealersKey { get; protected set; }

        void Awake()
        {
            Owner = GetComponent<UtilityAgent>();
            Debug.Assert(Owner != null, $"No UtilityAgent component found on {gameObject.name}");
            CharacterMover = GetComponent<CharacterMover>();
            Debug.Assert(CharacterMover != null, $"No CharacterMover component found on {gameObject.name}");
            Debug.Assert(BlackboardManager != null, $"No BlackboardManager injected on {gameObject.name}");
            PrivateBlackboard = BlackboardManager.GetPrivateBlackboard(gameObject);
            Sensors = GetComponentsInChildren<ISensor>();
            //ComputeSensorIndex();
            Debug.Log($"Context Awake on {gameObject.name}");
            Targets= new Dictionary<GameObject, TargetData>();
            StatsManager= GetComponent<StatsManager>();
            CreateBlackboardKeys();
        }

        private void CreateBlackboardKeys()
        {
            EnemyKey = BlackboardManager.GetOrRegisterKey(_enemyKey);
            TargetsInProximityKey = BlackboardManager.GetOrRegisterKey(_targetsInProximityKey);
            IsUnderAttackKey = BlackboardManager.GetOrRegisterKey(_isUnderAttackKey);
            DamageDealersKey = BlackboardManager.GetOrRegisterKey(_damageDealersKey);
        }

        void Start()
        {
            foreach (var sensor in Sensors)
            {
                Debug.Log($"UtilityAIContext Connect Sensor: {sensor.Name}");
                //sensor.OnTargetChanged += OnTargetChanged;
            }
        }

        // void OnTargetChanged(Sensor sensor, TargetData targetData)
        // {
        //     if (targetData.InRange)
        //     {
        //         Debug.Log($"UtilityAIContext: Target in range: {targetData.Target.name}");
        //         Targets[targetData.Target] = targetData;
        //         if (_target == null)
        //         {
        //             _target = targetData.Target;
        //             _target.GetComponent<Stats>().OnDeath += () => RemoveTarget(_target);
        //         }
        //     }
        //     else
        //     {
        //         Debug.Log($"UtilityAIContext Target out of range: {targetData.Target.name}");
        //         _targets[targetData.Target] = targetData;
        //         StartCoroutine(DelayedTargetRemoval(TargetRemoveDelay, targetData.Target));
        //     }
        // }

        // void RemoveTarget(GameObject target)
        // {
        //     if (_targets.ContainsKey(target))
        //     {
        //         _targets.Remove(target);
        //         if (_target == target)
        //         {
        //             _target = null;
        //         }
        //     }
        // }

        // IEnumerator DelayedTargetRemoval(float delay, GameObject target)
        // {
        //     yield return new WaitForSeconds(delay);
            
        //     if (!_targets[target].InRange)
        //     {
        //         Debug.Log($"Remove Target: {target.name}");
        //         _targets.Remove(target);
        //         if (_target == target)
        //         {
        //             _target = null;
        //         }   
        //     }
        // }
        // public ISensor GetSensor(int index)
        // {
        //     return Sensors[index];
        // }
        // public int GetSensorIndex(string name)
        // {
        //     if( _sensorIndex.TryGetValue(name, out int index))
        //     {
        //         return index;
        //     }
        //     return -1;
        // }

        // private void ComputeSensorIndex()
        // {
        //     _sensorIndex = new Dictionary<string, int>();

        //     for (int i = 0; i < Sensors.Length; i++)
        //     {
        //         Debug.Log($"Sensor: {Sensors[i].Name}");
        //         _sensorIndex[Sensors[i].Name] = i;
        //     }
        //     Debug.Log($"Sensor Index: {_sensorIndex.Count}");
        // } 
        // void Update()
        // {
        //     if (_targets.Count > 0)
        //     {
        //         foreach (var target in _targets)
        //         {
        //             //Debug.Log($"Target: {target.Key.name} {target.Value.InRange} ");
        //         }

        //     }
        // }  
    }
}
