using System;
using UnityEngine;
using PLu.UtilityAI.Core;
using PLu.Utilities;



namespace PLu.Dev.AI
{
    public class Sensor : MonoBehaviour, ISensor
    {
        public string Name => name;
        protected GameObject _target;
        Func<Vector3> ISensor.ObservedLocation => throw new NotImplementedException();
        public Vector3 TargetPosition => _target ? _target.transform.position : Vector3.zero;
        public float DistanceToTarget => _target ? Vector3.Distance(transform.position, _target.transform.position) : float.MaxValue;
        public virtual event Action<Sensor, TargetData> OnTargetChanged = delegate { };
        public virtual bool Sense()
        {
            return false;
        }
    }
}
