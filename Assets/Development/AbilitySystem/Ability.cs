using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.Dev.AI;
using Unity.VisualScripting;
using System.Runtime.InteropServices;
using PLu.UtilityAI.Core;

namespace PLu.AbilitySystem
{
    // Use the strategy pattern to define abilities
    public enum AbilityType 
    {
        Melee,
        Ranged,
        Magic
    }
    public abstract class Ability : ScriptableObject
    {
        [field:SerializeField] public AbilityType TypeOfAbility { get; protected set; }
        [field:SerializeField] public float Damage { get; protected set; }
        [field:SerializeField] public float Range { get; protected set; }
        [field:SerializeField] public float CoolDownTime { get; protected set; }

        public float CooldownEndTime { get; protected set; }

        public bool IsCoolingDown => Time.time < CooldownEndTime;
        public void BeginCoolDown() => CooldownEndTime = Time.time + CoolDownTime;
        public virtual void Initialize(IContext context) {}
        public abstract void Activate(Character user, Character target);
        public abstract bool CanActivate(Character user, Character target);

        void OnEnable()
        {
            CooldownEndTime = 0f;
        }
    }    
}