using UnityEngine;
using PLu.Dev.AI;
using PLu.UtilityAI.Core;

namespace PLu.AbilitySystem
{
    [CreateAssetMenu(fileName = "AttackAbility", menuName = "Ability System/AttackAbility")]
    public class AttackAbility : Ability
    {
        public override bool CanActivate(Character user, Character target)
        {
            Debug.Log($"[Ability] AttackAbility.CanActivate {this.GetType().Name} for {user.gameObject.name} Attacking: {target.gameObject.name}");
            if (target == null || IsCoolingDown)
            {
                Debug.Log($"[Ability] --- cooling down or enemy is dead <<<");
                return false;
            }
            float distanceSqr = user.gameObject.transform.position.DistanceSqr(target.gameObject.transform.position);
            Debug.Log($"[Ability] ---- Distance: {distanceSqr}");
            if (distanceSqr <= Range * Range)
            {
                Debug.Log($"[Ability] true <<<");
                return true;
            }
            Debug.Log($"[Ability] false <<<");
            return false;
        }
        public override void Activate(Character user, Character target) 
        {   
            Debug.Log($"[Ability] AttackAbility.Activate  {user.gameObject.name} is attacking: {target.gameObject.name} >>>");
            if (!CanActivate(user, target)) { return; }

            BeginCoolDown();
            target.TakeDamage(user, DamageType.Physical, Damage);
            //target.StatsManager.ModifyValue(StatType.Health, -Damage);
            Debug.Log("[Ability] AttackAbility Activated <<<");
        }
    }
}