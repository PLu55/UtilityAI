using System.Collections;
using System.Collections.Generic;
using Codice.CM.Common;
using UnityEngine;

namespace PLu.Dev
{
    [RequireComponent(typeof(SphereCollider))]
    public abstract class Interactable : MonoBehaviour, IInteractable
    {
        [field:SerializeField] public float Radius { get; private set; } = 1.0f;

        private SphereCollider _collider;

        public virtual void Awake()
        {
            _collider = GetComponent<SphereCollider>();
            // TODO: Maybe: InteractableManager.Instance.RegisterInteractable(this);
        }
        void Start()
        {
            _collider.radius = Radius;
        }
        public virtual void OnTriggerEnter(Collider other)
        {
            IInteractor interactor = other.GetComponent<IInteractor>();
            if (interactor != null)
            {
                interactor.AcceptInteractable(this);
            }
        }
        public virtual void Interact() 
        {
            // Noop
        }

        public abstract void Visit(IInteractor interactor);

        public virtual void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, Radius);
        }
    }
}
