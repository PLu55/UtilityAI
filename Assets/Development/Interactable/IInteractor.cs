using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.Dev
{
    public interface IInteractor
    {
        virtual void AcceptInteractable(IInteractable interactable)
        {
            interactable.Visit(this);
        }
    }
}
