using UnityEngine;

namespace PLu.Dev.AI
{
    public class Bomb : Interactable
    {
        public override void Interact()
        {
            Debug.Log("Interacting with " + name);
        }

        public override void Visit(IInteractor interactor)
        {
            if (interactor is Player player)
            {
                Debug.Log($"Bang!!! You are dead!");
                Destroy(player.gameObject);
                Destroy(gameObject);
                return;
            }
        }
    }
}
