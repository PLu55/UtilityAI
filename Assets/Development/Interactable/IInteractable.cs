
namespace PLu.Dev
{
    public interface IInteractable
    {
        void Visit(IInteractor interactor);
        void Interact();
    }
}
