using UnityEngine;

namespace PLu.Dev.AI
{
    public class TestInteractable : Interactable
    {
        public override void Interact()
        {
            Debug.Log("Interacting with " + name);
        }

        public override void Visit(IInteractor interactor)
        {
            if (interactor is Player player)
            {
                Debug.Log($"{player} is visiting {name}");
                return;
            }
        }
    }
}
