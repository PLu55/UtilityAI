using System;
using System.Collections.Generic;
using UnityEngine;

namespace PLu.Dev.AI
{
    public class StatsMediator
    {
        readonly LinkedList<StatModifier> _statModifiers = new LinkedList<StatModifier>();

        public event EventHandler<Query> Queries;
        public void PerformQuery(object sender, Query query) //=> 
        { 
            Queries?.Invoke(sender, query); 
        }
        public void AddModifier(StatModifier modifier)
        {
            _statModifiers.AddLast(modifier);

            Action<object, Query> handler = modifier.Handle;

            Queries += modifier.Handle;

            modifier.OnDispose += _=>
            {
                _statModifiers.Remove(modifier);
                Queries -= modifier.Handle;
                
            };
        }

        public void Update(float deltaTime)
        {
            var node = _statModifiers.First;
            // while (node != null)
            // {
            //     var modifier = node.Value;
            //     var next = node.Next;
            //     modifier.Update(deltaTime);

            //     if (modifier.MarkedForRemoval)
            //     {
            //         Debug.Log($"--- Dispose: {modifier.ToString()}");
            //         //modifier.Dispose();
            //         _statModifiers.Remove(modifier);

            //     }
            //     node = next;
            // }
            
            // node = _statModifiers.First;
            while (node != null)
            {
                var modifier = node.Value;
                modifier.Update(deltaTime);
                node = node.Next;
            }
        }
    }

    public class Query
    {   
        public readonly StatType StatType;
        public float Value;

        public Query(StatType statType, float value)
        {
            StatType = statType;
            Value = value;
        }
    }
}
