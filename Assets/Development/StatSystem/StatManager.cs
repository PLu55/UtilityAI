using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace PLu.Dev.AI
{
    public class StatsManager : MonoBehaviour
    {
        public StatDefinitions StatDefinitions;

        public Dictionary<StatType, Stat> StatDictionary;

        public StatsMediator Mediator => _mediator;
        private StatsMediator _mediator;

        void Awake()
        { 
            Initialize();
        }

        void Start()
        {
            foreach (var stat in StatDictionary)
            {
                stat.Value.Modify(0f);
            }
        }

        void Update()
        {
            _mediator.Update(Time.deltaTime);
        }
        public void DoDebugUpdate(float deltaTime)
        {
            _mediator.Update(deltaTime);
        }
        public void Initialize()
        {            
            _mediator = new StatsMediator();
            StatDictionary = new Dictionary<StatType, Stat>();
            foreach (var stat in StatDefinitions.Definitions)
            {
                StatDictionary.Add(stat.StatType, new Stat(stat.StatType, stat.Value, stat.Value));
                Debug.Log($"Stat Type: {stat.StatType}, Value: {stat.Value}");
            }
        }
        public void AddModifier(StatModifier modifier)
        {
            _mediator.AddModifier(modifier);
        }

        public bool TryGetStatValue(StatType statType, out float value)
        {
            if (StatDictionary.TryGetValue(statType, out var stat))
            {
                value = stat.Value;
                return true;
            }
            value = -1f;
            return false;
        }
        public float GetBaseValue(StatType statType)
        {
            return StatDictionary[statType].Value;
        }        
        public Stat GetStat(StatType statType)
        {
            return StatDictionary[statType];
        }
        public float GetCurrentValue(StatType statType)
        {   
            Stat stat = StatDictionary[statType];
            var q = new Query(statType, StatDictionary[statType].Value);
            _mediator.PerformQuery(this, q);

            return q.Value;
        }
        public float GetCurrentRatio(StatType statType)
        { 
            Stat stat = StatDictionary[statType];
            var q = new Query(statType, stat.Value);
            _mediator.PerformQuery(this, q);

            return q.Value / stat.MaxValue;
        }

        public void SetMaxValue(StatType statType, float amount)
        {
            StatDictionary[statType].SetMaxValue(amount);
        }

        public void ModifyValue(StatType statType, float amount)
        {
            StatDictionary[statType].Modify(amount);
        }
    }
}
