using System;
using PLu.Utilities;

namespace PLu.Dev.AI
{
    public abstract class StatModifier : IDisposable
    {
        public bool MarkedForRemoval { get; private set; }
        public event Action<StatModifier> OnDispose = delegate { };
        
        public float RemainingTime => _timer.Time;
        readonly CountdownTimer _timer;

        private bool _isDisposed = false;

        protected StatModifier(float duration)
        {
            if (duration <= 0f) { return;}

            _timer = new CountdownTimer(duration);
            _timer.OnTimerStop += Dispose;
            _timer.Start();
        } 

        public void Update(float deltaTime) => _timer?.Tick(deltaTime);

        public abstract void Handle(object sender, Query query);
        public virtual void Dispose()
        {
            if (_isDisposed) { return; }
            _isDisposed = true;
            MarkedForRemoval = true;
            OnDispose.Invoke(this);
        }
    }
}
