using System;
using System.Collections.Generic;
using UnityEngine; 
 
namespace PLu.Dev.AI
{
    public enum StatType
    {
        Health,
        Strength
    }

    [Serializable]
    public class Stat
    {
        [field:SerializeField] public StatType StatType { get; private set; }
        [field:SerializeField] public float Value { get; private set; }
        [field:SerializeField] public float MaxValue { get; private set; }

        public event Action<float, float> OnStatChanged = delegate { };

        public Stat(StatType type, float value, float maxValue)
        {
            StatType = type;
            Value = value;
            MaxValue = maxValue;
        }

        public void Modify(float amount)
        {
            Value += amount;
            Value = Mathf.Clamp(Value, 0f, MaxValue);
            OnStatChanged.Invoke(Value, MaxValue);
        }        
        
        public void SetValue(float amount)
        {
            Value = amount;
            Value = Mathf.Clamp(Value, 0f, MaxValue);
            OnStatChanged.Invoke(Value, MaxValue);
        }        
        public void ModifyMaxValue(float amount)
        {
            MaxValue += amount;
            MaxValue = Mathf.Clamp(MaxValue, 0f, float.MaxValue);
            Value = Mathf.Clamp(Value, 0f, MaxValue);
            OnStatChanged.Invoke(Value, MaxValue);
        }
        public void SetMaxValue(float amount)
        {
            MaxValue = amount;
            MaxValue = Mathf.Clamp(MaxValue, 0f, float.MaxValue);
            Value = Mathf.Clamp(Value, 0f, MaxValue);
            OnStatChanged.Invoke(Value, MaxValue);
        }
    }
}
