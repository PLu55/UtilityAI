using System;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.Dev.AI
{
    [CreateAssetMenu(menuName = "Character/StatDefinitions", fileName = "StatDefinitions")]
    public class StatDefinitions : ScriptableObject
    {
        [field:SerializeField] public List<BaseStat> Definitions { get; set; } = new List<BaseStat>();

    }

    [Serializable]
    public class BaseStat
    {
        public StatType StatType;
        public float Value;

        public BaseStat(StatType statType, float value)
        {
            StatType = statType;
            Value = value;
        }

    }
}
