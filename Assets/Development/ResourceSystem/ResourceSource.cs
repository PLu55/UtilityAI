using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.InventorySystem;

namespace PLu.ResourceSystem
{
    public class ResourceSource : MonoBehaviour
    {
        [SerializeField] private ItemType _itemType;
        [SerializeField] private int _initialQuantity;
        private ItemContainer _resource;

        public string Type => _itemType.Name;
        public int Quantity =>  _resource.Quantity;

        public delegate void ResourceSourceExhaustedEventHandler(ResourceSource resourceSource);
        public event ResourceSourceExhaustedEventHandler ResourceSourceExhausted;

        void Start()
        {
            gameObject.tag = "Resource";
            _resource = new(_itemType, _initialQuantity);
        }
        private void OnResourceSourceExhausted()
        {
            ResourceSourceExhausted?.Invoke(this);
        }
        public ItemContainer RemoveQuantity(int quantity)
        {
            bool isExhausted;
            ItemContainer itemContainer = _resource.RemoveQuantity(quantity, out isExhausted);
            if (isExhausted)
            {
                OnResourceSourceExhausted();
                Destroy(gameObject);
            }

            return itemContainer;
        }
    }
}
