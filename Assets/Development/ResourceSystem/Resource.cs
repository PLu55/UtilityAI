using UnityEngine;

public class Resource
{
    [SerializeField] private string _type;
    [SerializeField] private float _quantity;

    public string Type => _type;
    public float Quantity => _quantity;

    public Resource(string type, float quantity)
    {
        _type = type;
        _quantity = quantity;
    }
    public void AddQuantity(float quantity)
    {
        _quantity += quantity;
    }
    public bool RemoveQuantity(float quantity, out float actualQuantity)
    {
        bool isExhausted = false;
        _quantity -= quantity;
        
        if (_quantity <= 0)
        {
            quantity += _quantity;
            _quantity = 0f;
            isExhausted = true; 
        }
        actualQuantity = quantity;
        return isExhausted;

    }
}
