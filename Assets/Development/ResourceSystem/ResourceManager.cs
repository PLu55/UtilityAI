using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

namespace PLu.ResourceSystem
{
    public class ResourceManager : MonoBehaviour
    {
        private static ResourceManager _instance;
        public static ResourceManager Instance => _instance;

        [SerializeField] private List<string> ResourceTypes;
        private Dictionary<string, List<ResourceSource>> _resourceSources;
        
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                // DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            _resourceSources = new Dictionary<string, List<ResourceSource>>();
            FindAllResources();        
        }
        private void FindAllResources()
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("resource");
            Debug.LogFormat("ResourceManager found {0} objects with the resource tag", gameObjects.Length);
            foreach (GameObject gameObject in gameObjects)
            {
                var resourceSource = gameObject.GetComponent<ResourceSource>();
                Debug.LogFormat("ResourceManager found: {0} {1}", resourceSource, resourceSource.Type);
                AddResource(resourceSource);
                resourceSource.ResourceSourceExhausted += OnResourceExhausted;
            }
        }

        public void AddResource(ResourceSource resourceSource)
        {
            if (! _resourceSources.ContainsKey(resourceSource.Type))
            {
                _resourceSources.Add(resourceSource.Type, new List<ResourceSource>());
            }
            _resourceSources[resourceSource.Type].Add(resourceSource); 
        }
        public List<ResourceSource> GetResources(string type)
        {
            Debug.Assert(_resourceSources != null);
            if (_resourceSources.ContainsKey(type))
            {
                return _resourceSources[type];
            }
            Debug.LogFormat("No: {0} found!", type);
            return null;
        }
        public ResourceSource GetClosestResourceSource(string type, Vector3 position)
        {
            List<ResourceSource> resourceSources = GetResources("IronOre");
            if (resourceSources == null || resourceSources.Count == 0)
            {
                return null;
            }

            ResourceSource closest = null;
            float minDistance = float.MaxValue;

            foreach (var resourceSource in resourceSources)
            {
                Vector3 resourceSourcePosition = resourceSource.GetComponent<Transform>().position;
                float sqrDistance = (position - resourceSourcePosition).sqrMagnitude;
                if (sqrDistance < minDistance)
                {
                    closest = resourceSource;
                    minDistance = sqrDistance;
                }
            }
            return closest;
        }
        private void OnResourceExhausted(ResourceSource resourceSource)
        {

            List<ResourceSource> resourceSources;
            if (_resourceSources.TryGetValue(resourceSource.Type, out resourceSources))
            {
                Debug.LogFormat("Removing {0} from ResourceManager", resourceSource);
                resourceSources.Remove(resourceSource);
            }
        }


    }
}
