using UnityEngine;
using UnityEngine.AI;
using PLu.ResourceSystem;
using PLu.InventorySystem;
namespace PLu.BehaviourTrees
{
    public class ExtractResource : IStrategy {
        readonly ResourceSource resourceSource;
        readonly int quantity;
        readonly float duration;
        readonly Inventory sink;
        private float startTime = 0f;

        public ExtractResource(ResourceSource resourceSource, int quantity, float duration, Inventory sink)
        {   
            Debug.Assert(resourceSource != null, "ResourceSource is null");
            this.resourceSource = resourceSource;
            this.quantity = quantity;
            this.duration = duration;
            this.sink = sink;
        }

        public Node.Status Process() 
        {
            if (startTime == 0) 
            {
                startTime = Time.time;
            }

            if (Time.time - startTime >= duration) 
            {
                startTime = 0;
                ItemContainer itemContainer = resourceSource.RemoveQuantity(quantity);
                if (itemContainer == null) 
                {
                    return Node.Status.Failure;
                }
                sink.TryAdd(itemContainer);
                return Node.Status.Success;
            }
            return Node.Status.Running;
        }

        public void Reset() => startTime = 0;
    }    
}