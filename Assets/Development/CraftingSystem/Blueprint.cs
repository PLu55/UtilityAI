using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.InventorySystem;
 
namespace PLu.CraftingSystem
{
    [CreateAssetMenu(fileName = "New Blueprint", menuName = "Crafting/Blueprint")]
    public class Blueprint : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField] private Sprite _icon;
        [SerializeField] private Item _result;
        [SerializeField] private Item[] _ingredients;
        [SerializeField] private float _craftingTime;
        [SerializeField] private float _experience;
    }
}
