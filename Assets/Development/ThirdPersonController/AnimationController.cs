using UnityEngine;

#if ENABLE_INPUT_SYSTEM
#endif

/* Note: animations are called via the controller for both the character and capsule using animator null checks
 */

namespace PLu.CharacterSystem
{
    [RequireComponent(typeof(Animator))]
    public class AnimationController : MonoBehaviour
    {
        private Animator _animator;
        private ThirdPersonController _thirdPersonController;
        private int _animIDSpeed;
        private int _animIDGrounded;
        private int _animIDJump;
        private int _animIDFreeFall;
        private int _animIDMotionSpeed;

        private void Awake()
        {
            _thirdPersonController = GetComponent<ThirdPersonController>();
            _animator = GetComponent<Animator>();
            AssignAnimationIDs();
        }

        private void AssignAnimationIDs()
        {
            _animIDSpeed = Animator.StringToHash("Speed");
            _animIDGrounded = Animator.StringToHash("Grounded");
            _animIDJump = Animator.StringToHash("Jump");
            _animIDFreeFall = Animator.StringToHash("FreeFall");
            _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        }

        private void Update()
        {
            _animator.SetBool(_animIDGrounded, _thirdPersonController.IsGrounded);
            _animator.SetBool(_animIDJump, _thirdPersonController.IsJumping);
            _animator.SetBool(_animIDFreeFall, _thirdPersonController.IsFreeFalling);
            _animator.SetFloat(_animIDSpeed, _thirdPersonController.CurrentSpeed);
            _animator.SetFloat(_animIDMotionSpeed, _thirdPersonController.MovementMagnitude);
        }
    }
}