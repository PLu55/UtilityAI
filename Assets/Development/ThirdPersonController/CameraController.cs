using UnityEngine;
using UnityEngine.InputSystem;

namespace PLu.CharacterSystem
{
    [RequireComponent(typeof(PlayerInput))]
    public class CameraController : MonoBehaviour
    {         
        [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow.")]
        [field:SerializeField] public GameObject CinemachineCameraTarget { get; private set; }

        [Tooltip("How far in degrees can you move the camera up.")]
        [field:SerializeField] public float TopClamp { get; private set; } = 70.0f;

        [Tooltip("How far in degrees can you move the camera down")]
        [field:SerializeField] public float BottomClamp { get; private set; } = -30.0f;

        [Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked.")]
        [field:SerializeField] public float CameraAngleOverride { get; private set; } = 0.0f;

        [Tooltip("For locking the camera position on all axis.")]
        [field:SerializeField] public bool LockCameraPosition { get; private set; } = false;

        private float _cinemachineTargetYaw;
        private float _cinemachineTargetPitch;

#if ENABLE_INPUT_SYSTEM 
        private PlayerInput _playerInput;
#endif
        private BasicPlayerInputs _input;
        private const float _threshold = 0.01f;
        private bool IsCurrentDeviceMouse => _playerInput.currentControlScheme == "KeyboardMouse";

        private void Awake()
        {
            _playerInput = GetComponent<PlayerInput>();
            _input = GetComponent<BasicPlayerInputs>();
        }

        private void Start()
        {
            _cinemachineTargetYaw = CinemachineCameraTarget.transform.rotation.eulerAngles.y;

        }
        private void LateUpdate()
        {
            CameraRotation();
        }
        private void CameraRotation()
        {
            // if there is an input and camera position is not fixed
            if (_input.Look.sqrMagnitude >= _threshold && !LockCameraPosition)
            {
                //Don't multiply mouse input by Time.deltaTime;
                float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;

                _cinemachineTargetYaw += _input.Look.x * deltaTimeMultiplier;
                _cinemachineTargetPitch += _input.Look.y * deltaTimeMultiplier;
            }

            // clamp our rotations so our values are limited 360 degrees
            _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

            // Cinemachine will follow this target
            CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
                _cinemachineTargetYaw, 0.0f);
        }
        private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }
    }
}
