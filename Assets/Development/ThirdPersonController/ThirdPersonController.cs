﻿using UnityEngine;
using Codice.CM.Common.Merge;


#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif


/* Note: animations are called via the controller for both the character and capsule using animator null checks
 */

namespace PLu.CharacterSystem
{
    [RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM 
    [RequireComponent(typeof(PlayerInput))]
#endif
    [DefaultExecutionOrder(-100)]
    public class ThirdPersonController : MonoBehaviour
    {
        [field:Header("Player")]
        [Tooltip("Move speed of the character in m/s")]
        [field:SerializeField] public float MoveSpeed { get; private set; } = 2.0f;

        [Tooltip("Sprint speed of the character in m/s")]
        [field:SerializeField] public float SprintSpeed { get; private set; } = 5.335f;

        [Tooltip("How fast the character turns to face movement direction")]
        [field:Range(0.0f, 0.3f)]
        [field:SerializeField] public float RotationSmoothTime { get; private set; } = 0.12f;

        [Tooltip("Acceleration and deceleration")]
        [field:SerializeField] public float SpeedChangeRate { get; private set; } = 10.0f;

        [field:Space(10)]
        [Tooltip("The height the player can jump")]
        [field:SerializeField] public float JumpHeight { get; private set; } = 1.2f;

        [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
        [field:SerializeField] public float Gravity { get; private set; } = -15.0f;

        [field:Space(10)]
        [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
        [field:SerializeField] public float JumpTimeout { get; private set; } = 0.50f;

        [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
        [field:SerializeField] public float FallTimeout { get; private set; } = 0.15f;

        [field:Header("Player Grounded")]
        [Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
        [field:SerializeField] public bool IsGrounded { get; private set; } = true;

        [Tooltip("Useful for rough ground")]
        [field:SerializeField] public float GroundedOffset { get; private set; } = -0.14f;

        [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
        [field:SerializeField] public float GroundedRadius { get; private set; } = 0.28f;

        [Tooltip("What layers the character uses as ground")]
        [field:SerializeField] public LayerMask GroundLayers { get; private set; }

        public float MovementMagnitude { get; private set; }
        public float CurrentSpeed { get; private set; }
        public bool IsFreeFalling => _fallTimeoutDelta < 0f;
        public bool IsJumping { get; private set; }

        // player
        private float _speed;
        private float _targetRotation = 0.0f;
        private float _rotationVelocity;
        private float _verticalVelocity;
        private float _terminalVelocity = 53.0f;

        // timeout deltatime
        private float _jumpTimeoutDelta;
        private float _fallTimeoutDelta;

#if ENABLE_INPUT_SYSTEM 
        private PlayerInput _playerInput;
#endif
        private Animator _animator;
        private CharacterController _controller;
        private IPlayerInputs _input;
        private Transform _mainCamera;


        private const float _threshold = 0.01f;
        readonly Color _transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
        readonly Color _transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

        private bool _hasAnimator;

        private void Awake()
        {
            _mainCamera = Camera.main.transform;
            _hasAnimator = TryGetComponent(out _animator);
            _controller = GetComponent<CharacterController>();
            _playerInput = GetComponent<PlayerInput>();
            _input = GetComponent<BasicPlayerInputs>();
            Debug.Assert(_input != null, "No IPlayerInputs found on ThirdPersonController");
        }

        private void Start()
        {
            // reset our timeouts on start
            _jumpTimeoutDelta = JumpTimeout;
            _fallTimeoutDelta = FallTimeout;
            IsJumping = false;
        }

        private void Update()
        {
            GroundedCheck();
            JumpAndGravity();
            Move();
        }

        private void GroundedCheck()
        {
            // set sphere position, with offset
            Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
                transform.position.z);
            IsGrounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers,
                QueryTriggerInteraction.Ignore);
        }

        private void Move()
        {
            // set target speed based on move speed, sprint speed and if sprint is pressed
            float targetSpeed = _input.IsSprinting ? SprintSpeed : MoveSpeed;

            // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

            // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is no input, set the target speed to 0
            if (_input.Move == Vector2.zero) targetSpeed = 0.0f;

            // a reference to the players current horizontal velocity
            float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

            float speedOffset = 0.1f;
            MovementMagnitude = _input.AnalogMovement ? _input.Move.magnitude : 1f;

            // accelerate or decelerate to target speed
            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                // creates curved result rather than a linear one giving a more organic speed change
                // note T in Lerp is clamped, so we don't need to clamp our speed
                _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * MovementMagnitude,
                    Time.deltaTime * SpeedChangeRate);

                // round speed to 3 decimal places
                _speed = Mathf.Round(_speed * 1000f) / 1000f;
            }
            else
            {
                _speed = targetSpeed;
            }

            CurrentSpeed = Mathf.Lerp(CurrentSpeed, targetSpeed, Time.deltaTime * SpeedChangeRate);
            if (CurrentSpeed < 0.01f) CurrentSpeed = 0f;

            // normalise input direction
            Vector3 inputDirection = new Vector3(_input.Move.x, 0.0f, _input.Move.y).normalized;

            // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is a move input rotate player when the player is moving
            if (_input.Move != Vector2.zero)
            {
                _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                                  _mainCamera.eulerAngles.y;
                float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                    RotationSmoothTime);

                // rotate to face input direction relative to camera position
                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }


            Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

            // move the player
            _controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) +
                             new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

        }

        private void JumpAndGravity()
        {
            if (IsGrounded)
            {
                // reset the fall timeout timer
                _fallTimeoutDelta = FallTimeout;

                IsJumping = false;

                // stop our velocity dropping infinitely when grounded
                if (_verticalVelocity < 0.0f)
                {
                    _verticalVelocity = -2f;
                }

                // Jump
                if (_input.IsJumping && _jumpTimeoutDelta <= 0.0f)
                {
                    // the square root of H * -2 * G = how much velocity needed to reach desired height
                    _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);
                    IsJumping = true;
                }

                // jump timeout
                if (_jumpTimeoutDelta >= 0.0f)
                {
                    _jumpTimeoutDelta -= Time.deltaTime;
                }
            }
            else
            {
                // reset the jump timeout timer
                _jumpTimeoutDelta = JumpTimeout;

                // fall timeout
                if (_fallTimeoutDelta >= 0.0f)
                {
                    _fallTimeoutDelta -= Time.deltaTime;
                }

            }

            // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
            if (_verticalVelocity < _terminalVelocity)
            {
                _verticalVelocity += Gravity * Time.deltaTime;
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (IsGrounded) Gizmos.color = _transparentGreen;
            else Gizmos.color = _transparentRed;

            // when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
            Gizmos.DrawSphere(
                new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z),
                GroundedRadius);
        }

    }
}