using System;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

namespace PLu.CharacterSystem
{	[DefaultExecutionOrder(-200)]
    public class BasicPlayerInputs : MonoBehaviour, IPlayerInputs
	{
		[field:Header("Character Input Values")]
		[field:SerializeField] public Vector2 Move { get; private set; }
		[field:SerializeField] public Vector2 Look { get; private set; }
		[field:SerializeField] public bool IsJumping { get; private set; }
		[field:SerializeField] public bool IsSprinting { get; private set; }

		[field:Header("Movement Settings")]
		[field:SerializeField] public bool AnalogMovement { get; private set; }

		[field:Header("Mouse Cursor Settings")]
		[field:SerializeField] public bool IsCursorLocked { get; private set; } = true;
		//[field:SerializeField] public bool cursorInputForLook { get; private set; } = true;

		private PlayerInput _playerInput;

		private void Awake()
		{
			_playerInput = GetComponent<PlayerInput>();

		}

#if ENABLE_INPUT_SYSTEM
        public void OnMove(InputAction.CallbackContext context)
		{
			MoveInput(context.ReadValue<Vector2>());
	
		}

		public void OnLook(InputAction.CallbackContext context)
		{
			LookInput(context.ReadValue<Vector2>());
		}

		public void OnJump(InputAction.CallbackContext context)
		{
			JumpInput(context.action.triggered);
		}
		public void OnSprint(InputAction.CallbackContext context)
		{
			SprintInput(context.ReadValue<float>() > 0);
		}

#endif
		public void MoveInput(Vector2 newMoveDirection)
		{
			Move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			Look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			IsJumping = newJumpState;
		}

		public void SprintInput(bool newSprintState)
		{
			IsSprinting = newSprintState;
		}

		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(IsCursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}
	}
	
}