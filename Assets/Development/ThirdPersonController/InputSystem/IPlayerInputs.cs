using UnityEngine;

namespace PLu.CharacterSystem
{
    public interface IPlayerInputs
	{
		Vector2 Move { get; }
		Vector2 Look { get; }
		bool IsJumping { get; }
		bool IsSprinting { get; }
		bool AnalogMovement { get; }
		bool IsCursorLocked { get; }
	}
	
}