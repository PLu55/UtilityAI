using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PLu.CharacterSystem
{
    public class AudioController : MonoBehaviour
    {        
        [field:SerializeField] public AudioClip LandingAudioClip{  get; private set; }
        [field:SerializeField] public AudioClip[] FootstepAudioClips { get; private set; }
        [field:Range(0, 1)] [field:SerializeField] public float FootstepAudioVolume { get; private set; } = 0.5f;

        private CharacterController _controller;

        void Awake()
        {
            _controller = GetComponent<CharacterController>();
        }
        private void OnFootstep(AnimationEvent animationEvent)
        {
            if (FootstepAudioClips == null) { return;}

            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                if (FootstepAudioClips.Length > 0)
                {
                    var index = Random.Range(0, FootstepAudioClips.Length);
                    AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(_controller.center), FootstepAudioVolume);
                }
            }
        }

        private void OnLand(AnimationEvent animationEvent)
        {
            if (LandingAudioClip == null) { return;}

            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                AudioSource.PlayClipAtPoint(LandingAudioClip, transform.TransformPoint(_controller.center), FootstepAudioVolume);
            }
        }
    }
}
