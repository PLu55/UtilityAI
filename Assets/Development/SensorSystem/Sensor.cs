using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.SensorSystem
{
    public class SensorManager : MonoBehaviour
    {

        private List<TargetData> _targets;

        void Start()
        {
            
        }

        void Update()
        {
            
        }
    }
    public abstract class Sensor : MonoBehaviour
    {
        private SensorManager _sensorManager;
        void Start()
        {
            _sensorManager = GetComponent<SensorManager>();
        }
        void Update()
        {
            
        }
    }

    public enum SensorType
    {
        Proximity,
        Sight,
        Hearing,

    }

    public enum StimulusSource
    {
        Explosion,
        WeaponFire,
        Footsteps
    }

    public class TargetData
    {
        public float TimeStamp { get; private set; }
        public SensorType SensorType { get; private set; }
        public StimulusSource StimulusSource { get; private set; }
        public GameObject Target { get; private set; }
        public float Radius { get; private set; }

        public TargetData(float timeStamp, SensorType sensorType, StimulusSource stimulusSource, GameObject target, float radius)
        {
            TimeStamp = timeStamp;
            SensorType = sensorType;
            StimulusSource = stimulusSource;
            Target = target;
            Radius = radius;
        }
    }


}
