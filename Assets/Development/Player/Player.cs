using System.Collections;
using System.Collections.Generic;
using PLu.AbilitySystem;
using UnityEngine;
 
namespace PLu.Dev.AI
{
    public class Player : MonoBehaviour, IInteractor
    {
        private InputHandler _inputHandler;
        private Character _character;
        private Ability[] _abilities;

        private void Awake()
        {
            _character = GetComponent<Character>();
            Debug.Assert(_character != null, $"No Character component found on character: {gameObject.name}");
            _abilities = _character.Abilities;
            _inputHandler = GetComponent<InputHandler>();
            Debug.Assert(_inputHandler != null, $"No InputHandler component found on character: {gameObject.name}");
            if (_inputHandler != null)
            {
                _inputHandler.OnAbilityKeyDown += OnAbilityKeyDown;
            }
        }
        public void AcceptInteractable(IInteractable interactable)
        {
            if (interactable is Bomb bomb)
            {
                Debug.Log("Disarming the bomb!");
                Destroy(bomb.gameObject);
                return;
            }
            interactable.Visit(this);
        }

        private void OnAbilityKeyDown(int abilityIndex)
        {
            Debug.Log($"KeyDown ability: {abilityIndex}");
            _character?.ActivateAbility(abilityIndex);
        }
        private void OnControllerColliderHitX(ControllerColliderHit hit)
        {

            Debug.Log("OnControllerColliderHit");
            IInteractable interactable = hit.gameObject.GetComponent<IInteractable>();
            if (interactable != null)
            {
                interactable.Visit(this);
            }
        }
    }
}
