namespace PLu.Patterns.Visitor
{
    public interface IVisitor
    {
        void Visit(IVisitable visitable);
    }
}