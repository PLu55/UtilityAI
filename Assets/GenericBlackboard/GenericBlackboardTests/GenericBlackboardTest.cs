using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PLu.GenericBlackboard;
using PLu.Utilities;
using System.Diagnostics;

public class GenericBlackboardTest
{
    [Test]
    public void TestGenericBlackboard()
    {
        var blackboard = new PLu.GenericBlackboard.Blackboard();
        HashedStringRegistry _keyRegistry = HashedStringRegistry.Instance;
        HashedString key1 = _keyRegistry.GetOrRegister("key1");
        HashedString key2 = _keyRegistry.GetOrRegister("key2");

        blackboard.SetValue(key1, 42);
        Assert.IsTrue(blackboard.TryGetValue(key1, out int value));
        Assert.AreEqual(42, value);
        Assert.AreEqual(42, blackboard.GetValue<int>(key1));
        blackboard.SetValue(key1, 43);
        Assert.AreEqual(43, blackboard.GetValue<int>(key1));
        blackboard.RemoveValue<int>(key1);
        Assert.IsFalse(blackboard.TryGetValue(key1, out int value2));

        blackboard.SetValue(key2, "Hello");
        Assert.IsTrue(blackboard.TryGetValue(key2, out string value3));
        Assert.AreEqual("Hello", value3);
    }

    [Test]
    public void PerformanceTest()
    {
        var blackboard = new PLu.GenericBlackboard.Blackboard();
        HashedStringRegistry _keyRegistry = HashedStringRegistry.Instance;

        int numKeys = 1000000;
        
        HashedString[] keys = new HashedString[numKeys];
        for (int i = 0; i < numKeys; i++)
        {
            keys[i] = _keyRegistry.GetOrRegister("key" + i);
        }

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.SetValue(keys[j], j);
        }

        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Setting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
        
        stopWatch.Restart();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.TryGetValue(keys[j], out int value);
        }

        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Getting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");

    }

    [Test]
    public void Vector3PerformanceTest()
    {       
        var blackboard = new PLu.GenericBlackboard.Blackboard();
        HashedStringRegistry _keyRegistry = HashedStringRegistry.Instance; 
        //GameObject testGameObject = new GameObject("TestGameObject");

        int numKeys = 1000000;

        HashedString[] keys = new HashedString[numKeys];
        for (int i = 0; i < numKeys; i++)
        {
            keys[i] = _keyRegistry.GetOrRegister("key" + i);
        }

        Vector3[] vector3Array = new Vector3[numKeys];
        for (int i = 0; i < numKeys; i++)
        {
            vector3Array[i] = new Vector3(i, i, i);
        }

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.SetValue(keys[j], vector3Array[j]);
        }

        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Setting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
        
        stopWatch.Restart();

        for (int j = 0; j < numKeys; j++)
        {
            blackboard.TryGetValue(keys[j], out int value);
        }

        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log($"Getting {numKeys} keys took {(float)ts.TotalMilliseconds/numKeys * 1e-3f} s per key");
    }
}
