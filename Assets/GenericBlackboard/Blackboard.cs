using System;
using System.Collections;
using System.Collections.Generic;
using PLu.Utilities;

namespace PLu.GenericBlackboard
{
    public class Blackboard : IEnumerable
    {
        public int Count => _storage.Count;    
        
        protected Dictionary<HashedString, object> _storage = new Dictionary<HashedString, object>();

        public ICollection<HashedString> Keys => _storage.Keys;
        public ICollection<object> Values => _storage.Values;
        // Add or update a value
        public void SetValue<T>(HashedString key, T value)
        {
            _storage[key] = value;
        }

        // Try to get a value. Returns true if the key exists and the value can be cast to T, false otherwise.
        public bool TryGetValue<T>(HashedString key, out T value)
        {
            if (_storage.TryGetValue(key, out object objValue) && objValue is T typedValue)
            {
                value = typedValue;
                return true;
            }

            value = default;
            return false;
        }

        public bool ContainsKey(HashedString key) => _storage.ContainsKey(key);

        // Get a value with a default if the key doesn't exist or the value cannot be cast to T.
        public T GetValue<T>(HashedString key, T defaultValue = default)
        {
            if (TryGetValue(key, out T value))
            {
                return value;
            }

            return defaultValue;
        }

        public object GetRawValue(HashedString key)
        {
            return _storage[key];
        }
        public bool TryGetRawValue(HashedString key, out object value)
        {
            if (_storage.TryGetValue(key, out value))
            {
                return true;
            }

            value = default;
            return false;
        }

        // Remove a value
        public void RemoveValue<T>(HashedString key)
        {
            _storage.Remove(key);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _storage.GetEnumerator();
        }


    }
}
