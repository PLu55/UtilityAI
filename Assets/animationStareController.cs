using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationStareController : MonoBehaviour
{
    private Animator _animator;
    private float _velocity = 0.0f;
    private float _maxVelocity = 1.0f;
    private float _acceleration = 0.1f;
    private float _deceleration = 0.1f;
    private int _velocityHash = Animator.StringToHash("Velocity");

    // Start is called before the first frame update
    void Awake()
    {
        _animator = GetComponent<Animator>();
        _velocityHash = Animator.StringToHash("Velocity");
    }


    // Update is called once per frame
    void Update()
    {
        bool forwardPressed = Input.GetKey("w");
        bool runPressed = Input.GetKey("left shift");

        if (forwardPressed)
        {
            _velocity += Time.deltaTime * _acceleration;
            _velocity = Mathf.Clamp(_velocity, 0.0f, _maxVelocity);
            _animator.SetFloat(_velocityHash, _velocity);
        }
        if (!forwardPressed)
        {
            _velocity -= Time.deltaTime * _deceleration;
            _velocity = Mathf.Clamp(_velocity, 0.0f, _maxVelocity);
            _animator.SetFloat(_velocityHash, _velocity);
        }
    }
}
